CREATE VIEW dbo.PlaylistVideos
AS
SELECT     dbo.Playlist_Content.ContentID AS VideoID, CAST(dbo.Playlist_Content.[Order] AS int) AS SortOrder, ISNULL(dbo.PlaylistVideosLEGACY.Sharing, 0) AS Sharing, 
                      dbo.Playlist_Content.PlaylistID, dbo.PlaylistVideosLEGACY.ContentItemID
FROM         dbo.Playlist_Content INNER JOIN
                      dbo.PlaylistVideosLEGACY ON dbo.Playlist_Content.PlaylistID = dbo.PlaylistVideosLEGACY.PlaylistID AND 
                      dbo.Playlist_Content.ContentID = dbo.PlaylistVideosLEGACY.ContentID INNER JOIN
                      dbo.[Content] ON dbo.Playlist_Content.ContentID = dbo.[Content].ContentID
WHERE     (dbo.[Content].Active = 1)
