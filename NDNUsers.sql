CREATE VIEW dbo.NDNUsers
AS
SELECT     dbo.[User].UserID, dbo.NDNUsersLEGACY.UserTypeID, dbo.[User].Login AS UserName, dbo.NDNUsersLEGACY.EmailID, dbo.[User].PasswordFormat, 
                      CAST(dbo.[User].PasswordSalt AS varbinary(128)) AS PasswordSalt, CAST(dbo.[User].Password AS varbinary(128)) AS Password, 
                      ISNULL(dbo.NDNUsersLEGACY.ReceiveDailyIQEmail, 0) AS ReceiveDailyIQEmail, dbo.[User].FirstName, dbo.NDNUsersLEGACY.MI, dbo.[User].LastName, 
                      dbo.NDNUsersLEGACY.Gender, dbo.NDNUsersLEGACY.DOB, dbo.NDNUsersLEGACY.AddressID, ISNULL(dbo.[User].PartnerID, 0) AS OrganizationID, 
                      ISNULL(dbo.NDNUsersLEGACY.ContentItemID, 0) AS ContentItemID, dbo.NDNUsersLEGACY.PartnerId, dbo.NDNUsersLEGACY.FreeWheelType, dbo.[User].Active, 
                      ISNULL(dbo.NDNUsersLEGACY.IsFtpEnabled, 0) AS IsFtpEnabled, dbo.NDNUsersLEGACY.XSLT, ISNULL(dbo.NDNUsersLEGACY.AccountStatus, 1) AS AccountStatus, 
                      ISNULL(dbo.[User].UpdatedDate, dbo.[User].CreatedDate) AS LastModified, ISNULL(dbo.NDNUsersLEGACY.IsAdmin, 0) AS IsAdmin, dbo.[User].PasswordToken, 
                      dbo.NDNUsersLEGACY.AllowVideoUpload, dbo.NDNUsersLEGACY.ProposedOrganization, ISNULL(dbo.NDNUsersLEGACY.EnableRssFeed, 0) AS EnableRssFeed, 
                      dbo.NDNUsersLEGACY.LandingURL, dbo.NDNUsersLEGACY.CustomRssFeedXSLT
FROM         dbo.[User] INNER JOIN
                      dbo.NDNUsersLEGACY ON dbo.[User].UserID = dbo.NDNUsersLEGACY.UserID
