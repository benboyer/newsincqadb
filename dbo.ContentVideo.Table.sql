USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContentVideo](
	[ContentID] [int] NOT NULL,
	[GUID] [nvarchar](250) NOT NULL,
	[ContentVideoImportStatusID] [smallint] NOT NULL,
	[FileName] [nvarchar](500) NULL,
	[ImportFilePath] [nvarchar](1000) NOT NULL,
	[FileSizeBytes] [bigint] NULL,
	[Height] [decimal](10, 2) NULL,
	[Width] [decimal](10, 2) NULL,
	[Duration] [decimal](10, 2) NULL,
	[ThumbnailImportFilePath] [nvarchar](1000) NULL,
	[ThumbnailFileName] [nvarchar](500) NULL,
	[StillframeImportFilePath] [nvarchar](1000) NULL,
	[StillframeFileName] [nvarchar](500) NULL,
	[CopyFileMessage] [varchar](500) NULL,
 CONSTRAINT [PK_ContentVideo] PRIMARY KEY CLUSTERED 
(
	[ContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_ContentVideo_ImpStatusID] ON [dbo].[ContentVideo] 
(
	[ContentVideoImportStatusID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_CVID_GUID] ON [dbo].[ContentVideo] 
(
	[GUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_CV_FN] ON [dbo].[ContentVideo] 
(
	[FileName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContentVideo]  WITH CHECK ADD  CONSTRAINT [FK_ContentVideo_Content] FOREIGN KEY([ContentID])
REFERENCES [dbo].[Content] ([ContentID])
GO
ALTER TABLE [dbo].[ContentVideo] CHECK CONSTRAINT [FK_ContentVideo_Content]
GO
ALTER TABLE [dbo].[ContentVideo]  WITH CHECK ADD  CONSTRAINT [FK_ContentVideo_ContentVideoImportStatus] FOREIGN KEY([ContentVideoImportStatusID])
REFERENCES [dbo].[ContentVideoImportStatus] ([ContentVideoImportStatusID])
GO
ALTER TABLE [dbo].[ContentVideo] CHECK CONSTRAINT [FK_ContentVideo_ContentVideoImportStatus]
GO
