USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[PartnerEmbedDefaults_get]
	@PartnerID int
as
BEGIN
	set fmtonly off
	SET NOCOUNT ON
	exec SprocTrackingUpdate 'PartnerEmbedDefaults_get'

	select	isnull(DefaultEmbedWidth, 0) DefaultEmbedWidth, isnull(DefaultEmbedHeight, 0) DefaultEmbedHeight, isnull(DefaultEmbedRatio, 0) DefaultEmbedRatio
	from	partner
	where	PartnerID = @PartnerID

	set nocount off
END
GO
