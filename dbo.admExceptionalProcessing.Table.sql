USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[admExceptionalProcessing](
	[admExceptionalProcessingID] [int] IDENTITY(1,1) NOT NULL,
	[PartnerID] [int] NULL,
	[ProcedureName] [varchar](200) NULL,
	[LastContentID] [bigint] NULL,
	[LastRunDTM] [datetime] NULL,
	[CreatedDTM] [datetime] NULL,
	[UpdatedDTM] [datetime] NULL,
	[UpdatedUser] [varchar](200) NULL,
	[RunIntervalMinutes] [int] NULL,
	[Active] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[admExceptionalProcessingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[admExceptionalProcessing] ADD  CONSTRAINT [DF_admExceptionalProcessing_Active]  DEFAULT ((1)) FOR [Active]
GO
