USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adsrv_GetNewUpdatedContent]
	-- declare
    @datefrom datetime = null,
    @dateto datetime = null,
    @numbertoreturn int = 500,	-- cap the superset size by adding this to a Top statement
    @numberinpage int = 100,	-- the number to return for a page
	@page int = 0,				-- page number
    @Updates bit = 0			-- 1=only consider updateddate, 0=consider createddate and updateddate
AS
	--select @datefrom = '2013-05-01 00:00:00', @dateto = '2013-05-10 12:00:00'
BEGIN
	SET FMTONLY OFF
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'adsrv_GetNewUpdatedContent'
	if @numbertoreturn > 500 set @numbertoreturn = 500
	declare @@SortOrder varchar(10) = 'DESC'

-- if @page > 0 set @page = @page -1
		--	select @sortorder
if @datefrom is null or @dateto is null
begin
	if @datefrom is null and @dateto is null
	begin
		set @dateto = GETDATE()
		set @datefrom = DATEADD(hh, -1, @dateto)
	end
	else
	begin
		if @dateto is null and @datefrom is not null
		begin
			set @dateto = dateadd(hh, 1, @datefrom)
		end
		else
		begin
			if @dateto is not null and @datefrom is null
			set @datefrom = DATEADD(hh, -1, @dateto)
		end
	end
end

-- declare @updates bit = 0, @numbertoreturn int = 400, @numberinpage int = 100, @datefrom datetime = '2013-05-01 00:11:22', @dateto datetime = '2013-06-01 00:11:22', @page int = 1-- , @curURL varchar(200), @nextURL varchar(200)
declare @offset int = @page, @resultset int = @numbertoreturn, @sendset int = @numberinpage, @TotalSet int, @curURL varchar(200), @nextURL varchar(200)
declare	@serviceURL varchar(100) = (select Value from vw_DefaultSettings where Entity = 'AdServer' and Setting = 'serviceURL' )
--declare	@rel_me varchar(20) = '''self'''-- (select value from vw_sysDefaultSettings where Entity = 'AdServer' and Setting = 'atom:Link_currentpage')
--declare	@rel_next varchar(20) = '''next'''-- (select value from vw_sysDefaultSettings where Entity = 'AdServer' and Setting = 'atom:Link_nextpage')
-- select @serviceURL
-- select * from templates where Name = 'DFPatom'
--set @curURL = @rel_me + convert(varchar(20), @dateto, 22) -- (select  REPLACE(replace(urlline, '{feedurl}', replace(@serviceurl, '{datetime}', convert(varchar(20), @dateto, 20))), '{rel}', @rel_me)  from templates where Name = 'DFPatom')
--set @nextURL = (select  REPLACE(replace(urlline, '{feedurl}', replace(@serviceurl, '{datetime}', convert(varchar(20), @dateto, 20))), '{rel}', @rel_next)  from templates where Name = 'DFPatom')

--select @dateto, @offset, @page, @rel_me, @rel_next
--select @curURL, @nextURL

create table #MyConRecord(id int not null identity(1,1) primary key, contentid bigint)
insert into #MyConRecord(contentid)
select ContentID
	FROM Content
	where	ContentImportStatus = 5
	and		((@updates = 0 and (CreatedDate between @datefrom and @dateto or UpdatedDate  between @datefrom and @dateto))
			or
			(@Updates = 1 and UpdatedDate  between @datefrom and @dateto))
	order by ContentID asc

	set @TotalSet = (select COUNT(*) from #MyConRecord (nolock))

	-- declare @sendset int = 12, @offset int = 0
    SELECT TOP(@sendset) 
--			replace(@curURL, '{pageno}', CONVERT(varchar(10), @page)) link, 
			convert(varchar(20),@datefrom,20) + '/' +  convert(varchar(10),@page) as link,
			case when @TotalSet - (@page * @sendset) < @sendset then null
				else convert(varchar(20),@datefrom,20) + '/' + CONVERT(varchar(10), @page+1) 
				end linkNext, 
			@sendset itemsPerPage,
			(@page * @sendset) + 1 startIndex, @TotalSet totalResults, 
			dbo.GetPathToStreamAsset(cav.assetid) content, dbo.GetMimeTypeToProgressiveAsset(cav.AssetID) mediatype, 'video' medium, avid.duration, c.Name title, c.description, 
			nc.NDNCategory category, c.Keyword keywords, /*c.effectivedate pubdate,*/
			case c.Active when 1 then 'active' else 'inactive' end status, dbo.GetPathToServeAsset(ca.AssetID) thumbnail, 
			a.Height ThumbnailHeight, a.Width ThumbnailWidth,
			c.contentID, c.PartnerID owner, --, c.EffectiveDate, c.ExpirationDate, c.CreatedDate, 
			-- case when noads.contentid is null then 0 else 1 end as 
			convert(int, 1) as 
			monetizeable
			,c.effectivedate pubdate
	-- declare @sendset int = 12, @offset int = 0 select *
	from	#MyConRecord (nolock) ref
	join	Content (nolock) c
	on		ref.contentid = c.ContentID
	join	Content_Asset (nolock) ca
	on		c.ContentID	= ca.ContentID
	and		3 = ca.AssetTypeID
	join	Asset a
	on		ca.assetid = a.AssetID
	join	Content_Asset (nolock) cav
	on		c.ContentID = cav.ContentID
	and		1 = cav.AssetTypeID
	join	Asset avid
	on		cav.AssetID = avid.AssetID
	left	join (select * from Playlist_Content_PartnerMap (nolock) where NDNCategoryID is not null) pcpm
	on		c.PartnerID = pcpm.PartnerID
	left join NDNCategory (nolock) nc
	on		pcpm.NDNCategoryID = nc.NDNCategoryID
	--left join archives.dbo.VideoIDsToFilterAds noads
	--on		ref.contentid = noads.contentid
	WHERE	ref.id > (@sendset * @offset)
	and		c.PartnerID not in (select partnerid from vw_partnerDefaultSettings where Entity = 'BlockContentFromAdServer' and Value = 'true')
    ORDER BY 
    c.ContentID

	drop table #MyConRecord

END
GO
