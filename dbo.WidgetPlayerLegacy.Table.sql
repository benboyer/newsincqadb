USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WidgetPlayerLegacy](
	[LauncherID] [int] NOT NULL,
	[DisableSocialNetworking] [bit] NULL,
	[DisableAds] [bit] NULL,
	[DisableVideoPlayOnLoad] [bit] NULL,
	[DisableSingleEmbed] [bit] NULL,
	[PartnerLogo] [varbinary](max) NULL,
	[StyleSheet] [varbinary](max) NULL,
	[DisableKeywords] [bit] NULL,
	[ContinuousPlay] [int] NULL,
 CONSTRAINT [PK_WidgetPlayerLegacy] PRIMARY KEY CLUSTERED 
(
	[LauncherID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
