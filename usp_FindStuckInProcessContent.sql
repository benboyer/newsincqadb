CREATE procedure [dbo].[usp_FindStuckInProcessContent]
as
BEGIN
	select	distinct c.PartnerID, ca.ContentID, c.createddate, c.UpdatedDate, c.Name, ca.TargetPlatformID, ca.AssetTypeID, ca.AssetID, cis.Name ContentStatus, cais.Name ContentAssetStatus, ais.Name AssetStatus, cvis.Name ContentVideoStatus, c.Active	Cactive, ca.Active CAactive, a.Active AActive
		, a.FilePath + '/' + a.Filename as AssetURL, case when ca.AssetTypeID = 1 then cv.FileName when ca.AssetTypeID = 2 then cv.StillframeFileName when ca.AssetTypeID = 3 then cv.ThumbnailFileName else null end as LegacyFileURL, a.EncodingID 
	-- update ca set ca.importstatusid = 5, ca.active = 1
	from	Content c join ImportStatus cis on c.ContentImportStatus = cis.ImportStatusID
	left join	ContentVideo cv on c.ContentID = cv.ContentID left join ContentVideoImportStatus cvis on cv.ContentVideoImportStatusID = cis.ImportStatusID
	join	Content_Asset ca 
	on		c.ContentID = ca.ContentID join ImportStatus cais on ca.ImportStatusID = cais.ImportStatusID
	join	Asset a 
	on		ca.AssetID = a.AssetID join ImportStatus ais on a.ImportStatusID = ais.ImportStatusID
	where	c.Active = 1 and c.ContentImportStatus = 5
	and		ca.Active = 0 and ca.ImportStatusID = 7
	and		a.Active = 1 and a.ImportStatusID = 5
	and		cv.ContentVideoImportStatusID = 4
	and		isnull(c.EffectiveDate, c.CreatedDate) <= DATEADD(N, -30, GETDATE())
	--and		not exists 
	--		(select 1
	--		from	Asset aa
	--		where	aa.AssetID = ca.AssetID
	--		and		(aa.Active = 0 or aa.ImportStatusID <> 5))
	order by ca.ContentID, ca.AssetID
END
