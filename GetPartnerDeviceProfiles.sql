CREATE procedure dbo.GetPartnerDeviceProfiles 
	@partnerid int
as
BEGIN
	select pd.Name Profile, tp.Name Device, tp.Format, ptpp.isImportEnabled, ptpp.isDistributionEnabled
	from Partner_TargetPlatformProfile ptpp
	join ProfileDescription pd
	on		ptpp.ProfileDescriptionID = pd.ProfileDescriptionID
	join	TargetPlatform tp
	on		ptpp.TargetPlatformID = tp.TargetPlatformID
	where PartnerID = @partnerid
END
