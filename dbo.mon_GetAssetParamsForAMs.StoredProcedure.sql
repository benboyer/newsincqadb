USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[mon_GetAssetParamsForAMs]
	@contentid bigint
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'mon_GetAssetParamsForAMs'
	-- declare @contentid bigint = 23899763
		select c.ContentID, tp.Name Platform, at.Name Asset, /*a.AssetID, isnull(at.MinWidth, 0) MinWidth, */isnull(fca.Width, 0) SourceWidth, isnull(fca.Height, 0) SourceHeight, /*isnull(a.Width, 0) FinalAssetWidth, isnull(at.MaxWidth, 0) MaxWidth, isnull(at.MinBitrate, 0) MinBitrate, */isnull(fca.Bitrate, 0) SourceBitrate, a.Duration, /*isnull(a.Bitrate, 0) FinalAssetBitrate, isnull(at.MaxBitrate, 0) MaxBitrate, isnull(at.Transcode, 1) CanTranscode, */ case when a.EncodingID is null then 'No' else 'Yes' end as Transcoded, fca.MimeType SourceMimeType, mt.FileExtension FinalMedia
		, fca.FilePath SourceFile, a.FilePath + '/' + a.Filename FinalFile
		from Content c
		join Content_Asset ca
		on c.ContentID = ca.ContentID
		join TargetPlatform_AssetType tpat
		on	ca.TargetPlatformID = tpat.TargetPlatformID
		and	ca.AssetTypeID = tpat.AssetTypeID
		join TargetPlatform tp
		on	tpat.TargetPlatformID = tp.TargetPlatformID
		join AssetType at
		on		ca.AssetTypeID = at.AssetTypeID
		and		tpat.AssetTypeID = at.AssetTypeID
		join MimeType mt
		on	at.MimeTypeID = mt.MimeTypeID
		join Asset a
		on	ca.AssetID = a.AssetID
		join FeedContent fc
		on	c.contentid = fc.contentid
		join FeedContentAsset fca
		on	fc.FeedContentID = fca.FeedContentID
		and	a.FeedContentAssetID = fca.FeedContentAssetID
		where c.ContentID = @contentid
		order by tp.TargetPlatformID, at.AssetTypeID
		set nocount on
END
GO
