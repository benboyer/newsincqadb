CREATE PROCEDURE [dbo].[cGetVideoGroupsByVideoID_sp]

	@VideoID			int

AS
	exec SprocTrackingUpdate 'cGetVideoGroupsByVideoID_sp'

	/*

	exec cGetVideoGroupsByVideoID_sp 70257
	SELECT * FROM VideoGroups VG
		INNER JOIN Playlists P ON P.VideoGroupId = VG.VideoGroupId
		LEFT OUTER JOIN PlaylistVideos PLV ON PLV.PlaylistID = P.PlaylistID
	WHERE
		PLV.VideoID IS NOT NULL
	ORDER BY
		PLV.VideoID

	SELECT * FROM VideoGroups order by VideoGroupID


	SELECT * FROM PlaylistVideos WHERE
	PlaylistId in
	(select PlaylistId from Playlists WHERE Title LIKE '%_CAT')


	SELECT P.* FROM Playlists P INNER JOIN VideoGroups VG ON P.VideoGroupID = VG.VideoGRoupID WHERE
	P.PlaylistId in
	(select PlaylistId from Playlists WHERE Title LIKE '%_CAT')

	*/
BEGIN

	-- ##BEGIN STORED PROCEDURE UNIT TESTS
	-- EXEC cGetVideoGroupsByVideoID_sp 59021  test case for no video groups
	-- EXEC cGetVideoGroupsByVideoID_sp 70382  test case for two video groups
	-- EXEC cGetVideoGroupsByVideoID_sp 71743  test case for one video group
	-- ##END STORED PROCEDURE UNIT TESTS

	SELECT DISTINCT VG.VideoGroupId, VG.VideoGroupName FROM VideoGroups VG
		INNER JOIN Playlists P ON P.VideoGroupId = VG.VideoGroupId
		LEFT OUTER JOIN PlaylistVideos PLV ON PLV.PlaylistID = P.PlaylistID
	WHERE
		PLV.VideoID = @VideoID
	ORDER BY
		VG.VideoGroupName

	RETURN(0)

END
