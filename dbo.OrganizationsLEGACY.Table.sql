USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrganizationsLEGACY](
	[PartnerID] [int] NOT NULL,
	[ContentItemId] [int] NULL,
	[Phone] [nchar](10) NULL,
	[OrganizationType] [int] NOT NULL,
	[Acronym] [nvarchar](50) NULL,
	[CandidateOfficePlaceholder] [nvarchar](100) NULL,
	[AllowUploadingVideo] [bit] NOT NULL,
	[LogoUrl] [nvarchar](1000) NULL,
	[IsMediaSource] [bit] NULL,
	[IsContentPrivate] [bit] NOT NULL,
 CONSTRAINT [PK_OrganizationsLEGACY] PRIMARY KEY CLUSTERED 
(
	[PartnerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
