TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	sysdiagrams	name	-9	sysname	128	256	None	None	0	None	None	-9	None	256	1	NO	39
NewsincQA	dbo	sysdiagrams	principal_id	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	sysdiagrams	diagram_id	4	int identity	10	4	0	10	0	None	None	4	None	None	3	NO	56
NewsincQA	dbo	sysdiagrams	version	4	int	10	4	0	10	1	None	None	4	None	None	4	YES	38
NewsincQA	dbo	sysdiagrams	definition	-4	image	2147483647	2147483647	None	None	1	None	None	-4	None	2147483647	5	YES	37

index_name	index_description	index_keys
PK__sysdiagr__C2B05B6146D27B73	clustered, unique, primary key located on PRIMARY	diagram_id
UK_principal_name	nonclustered, unique, unique key located on PRIMARY	principal_id, name
