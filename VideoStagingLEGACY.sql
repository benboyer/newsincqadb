TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	VideoStagingLEGACY	ContentID	4	int	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	VideoStagingLEGACY	Duration	6	float	15	8	None	10	1	None	None	6	None	None	2	YES	109
NewsincQA	dbo	VideoStagingLEGACY	ReadyForEncoding	-7	bit	1	1	None	None	1	None	None	-7	None	None	3	YES	50
NewsincQA	dbo	VideoStagingLEGACY	EncodingPercentComplete	4	int	10	4	0	10	1	None	None	4	None	None	4	YES	38
NewsincQA	dbo	VideoStagingLEGACY	EncodingStarted	11	datetime	23	16	3	None	1	None	None	9	3	None	5	YES	111
NewsincQA	dbo	VideoStagingLEGACY	EncodingCompleted	-9	date	10	20	None	None	1	None	None	-9	None	None	6	YES	0
NewsincQA	dbo	VideoStagingLEGACY	Status	4	int	10	4	0	10	1	None	None	4	None	None	7	YES	38
NewsincQA	dbo	VideoStagingLEGACY	FileSize	4	int	10	4	0	10	1	None	None	4	None	None	8	YES	38
NewsincQA	dbo	VideoStagingLEGACY	AudioTrackURL	-9	nvarchar	1000	2000	None	None	1	None	None	-9	None	2000	9	YES	39
NewsincQA	dbo	VideoStagingLEGACY	EncodedURL	-9	nvarchar	1000	2000	None	None	1	None	None	-9	None	2000	10	YES	39
NewsincQA	dbo	VideoStagingLEGACY	NexidiaStatus	4	int	10	4	0	10	0	None	None	4	None	None	11	NO	56
NewsincQA	dbo	VideoStagingLEGACY	Mp3Started	11	datetime	23	16	3	None	1	None	None	9	3	None	12	YES	111
NewsincQA	dbo	VideoStagingLEGACY	Mp3Completed	11	datetime	23	16	3	None	1	None	None	9	3	None	13	YES	111
NewsincQA	dbo	VideoStagingLEGACY	NiaStatus	4	int	10	4	0	10	0	None	None	4	None	None	14	NO	56
NewsincQA	dbo	VideoStagingLEGACY	CodecUsed	-8	nchar	100	200	None	None	1	None	None	-8	None	200	15	YES	39
NewsincQA	dbo	VideoStagingLEGACY	FileExtension	-9	nvarchar	5	10	None	None	1	None	None	-9	None	10	16	YES	39
NewsincQA	dbo	VideoStagingLEGACY	IsDeleted	-7	bit	1	1	None	None	1	None	((0))	-7	None	None	17	YES	50

index_name	index_description	index_keys
PK_VideoStagingLEGACY	clustered, unique, primary key located on PRIMARY	ContentID
