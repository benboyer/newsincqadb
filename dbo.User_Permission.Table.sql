USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User_Permission](
	[UserID] [int] NOT NULL,
	[PermissionID] [smallint] NOT NULL,
 CONSTRAINT [PK_User_Permission] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[PermissionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[User_Permission]  WITH CHECK ADD  CONSTRAINT [FK_User_Permission_Permission] FOREIGN KEY([PermissionID])
REFERENCES [dbo].[Permission] ([PermissionID])
GO
ALTER TABLE [dbo].[User_Permission] CHECK CONSTRAINT [FK_User_Permission_Permission]
GO
ALTER TABLE [dbo].[User_Permission]  WITH CHECK ADD  CONSTRAINT [FK_User_Permission_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[User_Permission] CHECK CONSTRAINT [FK_User_Permission_User]
GO
