TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Asset	AssetID	-5	bigint identity	19	8	0	10	0	None	None	-5	None	None	1	NO	63
NewsincQA	dbo	Asset	AssetTypeID	4	int	10	4	0	10	1	None	None	4	None	None	2	YES	38
NewsincQA	dbo	Asset	MimeTypeID	4	int	10	4	0	10	1	None	None	4	None	None	3	YES	38
NewsincQA	dbo	Asset	Filename	12	varchar	200	200	None	None	1	None	None	12	None	200	4	YES	39
NewsincQA	dbo	Asset	FileSize	-5	bigint	19	8	0	10	1	None	None	-5	None	None	5	YES	108
NewsincQA	dbo	Asset	FilePath	12	varchar	200	200	None	None	1	None	None	12	None	200	6	YES	39
NewsincQA	dbo	Asset	ImportStatusID	5	smallint	5	2	0	10	1	None	None	5	None	None	7	YES	38
NewsincQA	dbo	Asset	Height	4	int	10	4	0	10	1	None	None	4	None	None	8	YES	38
NewsincQA	dbo	Asset	Width	4	int	10	4	0	10	1	None	None	4	None	None	9	YES	38
NewsincQA	dbo	Asset	Bitrate	3	decimal	10	12	2	10	1	None	None	3	None	None	10	YES	106
NewsincQA	dbo	Asset	Duration	3	decimal	20	22	2	10	1	None	None	3	None	None	11	YES	106
NewsincQA	dbo	Asset	Active	-7	bit	1	1	None	None	0	None	((0))	-7	None	None	12	NO	50
NewsincQA	dbo	Asset	isArchived	-7	bit	1	1	None	None	1	None	None	-7	None	None	13	YES	50
NewsincQA	dbo	Asset	isDeleted	-7	bit	1	1	None	None	1	None	None	-7	None	None	14	YES	50
NewsincQA	dbo	Asset	FeedContentAssetID	-5	bigint	19	8	0	10	1	None	None	-5	None	None	15	YES	108
NewsincQA	dbo	Asset	AltFeedContentAssetID	-5	bigint	19	8	0	10	1	None	None	-5	None	None	16	YES	108
NewsincQA	dbo	Asset	EncodingID	12	varchar	40	40	None	None	1	None	None	12	None	40	17	YES	39
NewsincQA	dbo	Asset	DownloadTries	4	int	10	4	0	10	0	None	((0))	4	None	None	18	NO	56
NewsincQA	dbo	Asset	DownloadMessage	12	varchar	300	300	None	None	1	None	None	12	None	300	19	YES	39
NewsincQA	dbo	Asset	StagingPath	12	varchar	200	200	None	None	1	None	None	12	None	200	20	YES	39
NewsincQA	dbo	Asset	RetryTimes	4	int	10	4	0	10	0	None	((0))	4	None	None	21	NO	56
NewsincQA	dbo	Asset	RetryMessage	12	varchar	300	300	None	None	1	None	None	12	None	300	22	YES	39
NewsincQA	dbo	Asset	RetryStart	11	datetime	23	16	3	None	1	None	None	9	3	None	23	YES	111
NewsincQA	dbo	Asset	EncodingProgress	4	int	10	4	0	10	0	None	((0))	4	None	None	24	NO	56
NewsincQA	dbo	Asset	EncodingErrorMessage	12	varchar	300	300	None	None	1	None	None	12	None	300	25	YES	39

index_name	index_description	index_keys
IX_Asset_filename	nonclustered located on PRIMARY	Filename
IX_Asset_mimetypeid	nonclustered located on PRIMARY	MimeTypeID
IX_Asset_StatusID	nonclustered located on PRIMARY	ImportStatusID
NDX_asset_MTidAIDfname	nonclustered located on PRIMARY	MimeTypeID
PK_Asset	clustered, unique, primary key located on PRIMARY	AssetID
