USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[allowimpersonate] @partnerID int
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'allowimpersonate'
	select	ac.ContentProviderPartnerID, ac.DistributorPartnerID, p.Name
	from	AllowContent ac, [Partner] p
	where	ac.DistributorPartnerID = p.partnerid
	and ac.ContentProviderPartnerID = @partnerID
	set nocount off
END
GO
