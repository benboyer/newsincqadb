USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetFullTextSearchResult_VideoStaging]
(
	@UserID INT,
	@SearchString NVARCHAR(1000),
	@Count INT = 100,
	@OrderClause NVARCHAR(MAX) = '',
	@StatusType INT = 0,
	@IsAdmin BIT = 0

)

AS
BEGIN

exec SprocTrackingUpdate 'GetFullTextSearchResult_VideoStaging'

DECLARE @queryString NVARCHAR(MAX)
DECLARE @WhereClause NVARCHAR(MAX)

SET @queryString=
	' SELECT TOP ' + CONVERT(NVARCHAR(20),@Count)+ ' vs.[VideoStagingID]
		,vs.Title
		,vs.UserID
		,vs.UserType
		,vs.ClipURL
		,vs.Description
		,vs.EventDate
		,vs.Height
		,vs.Width
		,vs.Duration
		,vs.CreatedOn
		,vs.ThumbnailURL
		,vs.PublishDate
		,vs.Keywords
		,vs.Guid
		,vs.ReadyForEncoding
		,vs.OriginalName
		,ISNULL(vs.EncodingStatus,0) AS EncodingStatus
		,vs.EncodingPercentCompleted
		,vs.EncodingStarted
		,vs.EncodingCompleted
		,vs.ExpiryDate
		,vs.Status
		,vs.Active
		,vs.IsEnabled
		,vs.IsDeleted
		,vs.IngestionSource
		,vs.FileSize
		,vs.StillFrameURL
		,vs.AudioTrackURL
		,vs.EncodedURL
		,KT.[Rank]
		,org.[Name]
		,us.[OrganizationId] FROM VideoStaging vs
		INNER JOIN FREETEXTTABLE(VideoStaging, ([Description], Title, Keywords), ''' + @SearchString+''') AS
		KT ON vs.VideoStagingID = KT.[KEY]
		INNER JOIN NDNUsers us ON vs.UserId = us.UserId
		INNER JOIN Organizations org ON org.OrganizationID = us.OrganizationID	'

SELECT @WhereClause = 'Where IsDeleted = 0 AND EncodingStatus > 0 '

SELECT @WhereClause = @WhereClause + (CASE CONVERT(NVARCHAR(1),@StatusType)
							WHEN '0' THEN ' AND EncodingStatus in (1,2,3,4,5,6,12) '
							WHEN '1' THEN ' AND (EncodingStatus in (1,2,3,4,5,6,12) OR EncodingStatus  in (9,10,11) ) AND ((Title IS NULL OR RTRIM(LTRIM(Title)) = '''') OR (Description IS NULL OR RTRIM(LTRIM(Description)) = '''' )) '
							WHEN '2' THEN ' AND ((Title IS NULL OR RTRIM(LTRIM(Title)) = '''') OR (Description IS NULL OR RTRIM(LTRIM(Description)) = '''' )) AND EncodingStatus = 7 '
							WHEN '3' THEN ' AND EncodingStatus in (9,10,11) '
							END)

IF (@IsAdmin = 0 )
    SELECT @WhereClause = @WhereClause + ' AND vs.UserId = ' + CONVERT(NVARCHAR(20),@UserID)  ;

SET @queryString = @queryString + @WhereClause

IF( @OrderClause IS NOT NULL AND @OrderClause != '')
	SET @queryString = @queryString +  ' Order By ' + @OrderClause


EXEC (@queryString );


END
GO
