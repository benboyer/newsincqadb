CREATE procedure [dbo].[GetLogoTypes]
	@LogoTypeID int = null
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetLogoTypes'

	select LogoTypeID, Name, Description, Comment
	from	LogoType
	where	LogoTypeID = isnull(@LogoTypeID, LogoTypeID)

	set nocount off
END
