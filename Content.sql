TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Content	ContentID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	Content	ContentTypeID	5	smallint	5	2	0	10	0	None	None	5	None	None	2	NO	52
NewsincQA	dbo	Content	PartnerID	4	int	10	4	0	10	0	None	None	4	None	None	3	NO	56
NewsincQA	dbo	Content	Version	4	int	10	4	0	10	1	None	None	4	None	None	4	YES	38
NewsincQA	dbo	Content	ContentSourceID	5	smallint	5	2	0	10	0	None	None	5	None	None	5	NO	52
NewsincQA	dbo	Content	Name	12	varchar	500	500	None	None	0	None	None	12	None	500	6	NO	39
NewsincQA	dbo	Content	Description	12	varchar	1000	1000	None	None	0	None	None	12	None	1000	7	NO	39
NewsincQA	dbo	Content	Category	12	varchar	250	250	None	None	1	None	None	12	None	250	8	YES	39
NewsincQA	dbo	Content	Keyword	12	varchar	1000	1000	None	None	1	None	None	12	None	1000	9	YES	39
NewsincQA	dbo	Content	Duration	3	decimal	20	22	2	10	1	None	None	3	None	None	10	YES	106
NewsincQA	dbo	Content	EffectiveDate	11	datetime	23	16	3	None	0	None	None	9	3	None	11	NO	61
NewsincQA	dbo	Content	ExpirationDate	11	smalldatetime	16	16	0	None	1	None	None	9	3	None	12	YES	111
NewsincQA	dbo	Content	CreatedDate	11	smalldatetime	16	16	0	None	0	None	(getdate())	9	3	None	13	NO	58
NewsincQA	dbo	Content	CreatedUserID	4	int	10	4	0	10	0	None	None	4	None	None	14	NO	56
NewsincQA	dbo	Content	UpdatedDate	11	smalldatetime	16	16	0	None	1	None	(getdate())	9	3	None	15	YES	111
NewsincQA	dbo	Content	UpdatedUserID	4	int	10	4	0	10	1	None	None	4	None	None	16	YES	38
NewsincQA	dbo	Content	Active	-7	bit	1	1	None	None	0	None	((0))	-7	None	None	17	NO	50
NewsincQA	dbo	Content	IsFileArchived	-7	bit	1	1	None	None	1	None	((0))	-7	None	None	18	YES	50
NewsincQA	dbo	Content	isDeleted	-7	bit	1	1	None	None	1	None	((0))	-7	None	None	19	YES	50
NewsincQA	dbo	Content	FeedID	4	int	10	4	0	10	1	None	((0))	4	None	None	20	YES	38
NewsincQA	dbo	Content	SourceGUID	12	varchar	250	250	None	None	1	None	None	12	None	250	21	YES	39
NewsincQA	dbo	Content	ContentImportStatus	4	int	10	4	0	10	1	None	None	4	None	None	22	YES	38
NewsincQA	dbo	Content	TranscriptPath	12	varchar	500	500	None	None	1	None	None	12	None	500	23	YES	39
NewsincQA	dbo	Content	TimesUpdated	4	int	10	4	0	10	0	None	((0))	4	None	None	24	NO	56
NewsincQA	dbo	Content	OnlyOwnerView	-7	bit	1	1	None	None	0	None	('0')	-7	None	None	25	NO	50
NewsincQA	dbo	Content	UUID	-11	uniqueidentifier	36	16	None	None	0	None	(newid())	-11	None	None	26	NO	37
NewsincQA	dbo	Content	StoryID	12	varchar	64	64	None	None	1	None	None	12	None	64	27	YES	39
NewsincQA	dbo	Content	antikw	12	varchar	200	200	None	None	1	None	None	12	None	200	28	YES	39

index_name	index_description	index_keys
IX_Content_PidActEffDt	nonclustered located on PRIMARY	PartnerID, Active
IX_Content_PIDverEffSGUID	nonclustered located on PRIMARY	PartnerID
IX_ContentActive	nonclustered located on PRIMARY	Active
IX_ContentEffCrtDTM	nonclustered located on PRIMARY	CreatedDate, ContentID
IX_ContentEffDt	nonclustered located on PRIMARY	EffectiveDate
IX_ContentExpDTM	nonclustered located on PRIMARY	ExpirationDate
IX_ContentFeed	nonclustered located on PRIMARY	FeedID
IX_CreatedDate_Desc	nonclustered located on PRIMARY	CreatedDate(-)
IX_UpdatedDate_Desc	nonclustered located on PRIMARY	UpdatedDate(-)
NDX_CON_cdtm_cidNdesKW	nonclustered located on PRIMARY	CreatedDate
NDX_Con_CDTMimpstatCIDcuie	nonclustered located on PRIMARY	CreatedDate, ContentID, CreatedUserID, ContentImportStatus
NDX_Con_EffCrtStat_CIDfid	nonclustered located on PRIMARY	EffectiveDate, CreatedDate, ContentImportStatus, ContentID, FeedID
NDX_Con_oov	nonclustered located on PRIMARY	OnlyOwnerView, ContentID
NDX_CON_pid_etc	nonclustered located on PRIMARY	PartnerID
NDX_CON_pidKitSnk	nonclustered located on PRIMARY	PartnerID, Active, isDeleted, ContentImportStatus, ContentID, Name, EffectiveDate, ExpirationDate, CreatedDate, CreatedUserID, OnlyOwnerView
NDX_CON_pidSGuid	nonclustered located on PRIMARY	PartnerID, SourceGUID
NDX_Con_SrcCID	nonclustered located on PRIMARY	ContentSourceID
NDX_Con_UITstatCID	nonclustered located on PRIMARY	CreatedUserID, ContentImportStatus
NDX_CONactPIDcdtm	nonclustered located on PRIMARY	Active, PartnerID, CreatedDate
NDX_Content_CUIDaExpDtETC	nonclustered located on PRIMARY	CreatedUserID, Active, ExpirationDate
NDX_ContentPIDcidEffCrt	nonclustered located on PRIMARY	PartnerID
NDX_PART_cis_cid_dtms	nonclustered located on PRIMARY	PartnerID, ContentImportStatus
PK_Content_1	clustered, unique, primary key located on PRIMARY	ContentID
