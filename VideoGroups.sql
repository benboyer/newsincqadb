CREATE VIEW [dbo].[VideoGroups]
AS
SELECT     ContentVideoGroupId AS VideoGroupId, ContentVideoGroupName AS VideoGroupName
FROM         dbo.ContentVideoGroup