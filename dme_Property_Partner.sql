CREATE VIEW dbo.dme_Property_Partner
AS
SELECT     p.ZoneID, p.PartnerID, p.Name, o.Phone, a.Address1, a.Address2, a.City, s.Abbreviation AS StateCd, a.Zip, c.Name AS Country, pdma.DMACode, o.IsMediaSource, 
                      p.LogoURL AS ContentLogo, p.DefaultEmbedWidth, p.DefaultEmbedHeight, p.isContentPrivate
FROM         dbo.Partner AS p WITH (nolock) LEFT OUTER JOIN
                      dbo.Address_Partner AS ap ON p.PartnerID = ap.PartnerID LEFT OUTER JOIN
                      dbo.Address AS a WITH (nolock) ON ap.AddressID = a.AddressID LEFT OUTER JOIN
                      dbo.State AS s WITH (nolock) ON a.StateID = s.StateID LEFT OUTER JOIN
                      dbo.Country AS c WITH (nolock) ON a.CountryID = c.CountryID LEFT OUTER JOIN
                      dbo.Partner_DMA AS pdma WITH (nolock) ON p.PartnerID = pdma.PartnerID INNER JOIN
                      dbo.Organizations AS o WITH (nolock) ON p.PartnerID = o.OrganizationId
