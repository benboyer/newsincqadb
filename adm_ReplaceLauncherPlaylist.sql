create procedure dbo.adm_ReplaceLauncherPlaylist
	-- declare 
	@oldplaylist int, 
	@newplaylist int
as
BEGIN
	declare @lplist table(launcherid int, oldplaylistid int, newplaylist int, sorder int)
	declare @listcount table(RemovedFromLaunchers int, AddedToLaunchers int)

	-- 	set @oldplaylist = 11299 
	-- 	set @newplaylist = 4930 

	insert into @lplist(launcherid, oldplaylistid, newplaylist, sorder)
	select LauncherID, @oldplaylist, @newplaylist, [Order]
	from Launcher_Playlist where PlaylistID = @oldplaylist 

	insert into @listcount(removedfromlaunchers)
	select count(*) from @lplist

	-- select *
	delete lp 
	from @lplist a 
	join Launcher_Playlist lp
	on	a.launcherid = lp.LauncherID
	and	a.oldplaylistid = lp.PlaylistID

	update @listcount set addedtolaunchers = 
	(select count(*)
	from @lplist a
	where not exists (select 1
					from	launcher_playlist b
					where	b.launcherid = a.launcherid
					and		b.playlistid = a.newplaylist))

	update b
	set b.[order] = b.[order] + 1
	from (select pl.launcherid, p.newplaylist, pl.[order] sorder
		from @lplist p
		join Launcher_Playlist pl
		on	p.launcherid = pl.LauncherID
		and	p.newplaylist = pl.playlistid
		and  p.sorder = pl.[Order]) a
	join launcher_playlist b
	on		a.launcherid = b.launcherid
	and		a.newplaylist = b.playlistid
	where	b.[order] >= a.sorder

	insert into launcher_playlist(launcherid, playlistid, [order])
	select launcherid, newplaylist, isnull(sorder, 0)
	from @lplist a
	where not exists (select 1
					from	launcher_playlist b
					where	b.launcherid = a.launcherid
					and		b.playlistid = a.newplaylist)

	select * from @listcount
END
