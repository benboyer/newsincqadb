TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	UserInterface_Setting	UserInterface_SettingID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	UserInterface_Setting	UserInterface	12	varchar	20	20	None	None	1	None	None	12	None	20	2	YES	39
NewsincQA	dbo	UserInterface_Setting	Setting	12	varchar	20	20	None	None	1	None	None	12	None	20	3	YES	39
NewsincQA	dbo	UserInterface_Setting	DefaultValue	12	varchar	50	50	None	None	1	None	None	12	None	50	4	YES	39

index_name	index_description	index_keys
IX_UIS_UIsV	nonclustered located on PRIMARY	UserInterface, Setting, DefaultValue
PK_UserInterface_Setting	clustered, unique, primary key located on PRIMARY	UserInterface_SettingID
