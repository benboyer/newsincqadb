USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[dmev_Partner_TargetPlatformProfile]
	@PartnerID int = null,
	@TargetPlatformID int = null
as
select * 
from dme_Partner_TargetPlatformProfile (nolock) ptpp
where	ptpp.PartnerID = ISNULL(@PartnerID, ptpp.PartnerID)
and		ptpp.TargetPlatformID = ISNULL(@TargetPlatformID, ptpp.TargetPlatformID)
GO
