CREATE VIEW [dbo].[WidgetExcludedOrgs]
AS
SELECT     NULL AS ContentItemID, LauncherID AS WidgetId, PartnerID AS OrganizationId
FROM         dbo.Launcher_ContentExclusion