CREATE PROCEDURE [dbo].[BuildLauncherPlaylistVideosMetadata]
--  declare
	@widgetID int = null,
	@playlistID int = null,
	@trackingGroup int = null,
	@contentid bigint,
	@devicetype int = 1
AS
	-- Updated 9/17/2013 (switched to allowedlaunchercontentP)
	set FMTONLY OFF
	set nocount on
	exec SprocTrackingUpdate 'BuildLauncherPlaylistVideosMetadata'

	Declare	@now datetime,
			@iwidgetID int,
			@iplaylistID int,
			@itrackinggroup int,
			@iContentID int,
			@idevicetype int
	set		@now = GETDATE()
	select @iContentid = @contentid, @iwidgetID = isnull(@widgetID, 1), @iplaylistID = isnull(@playlistID, (select value from vw_sysDefaultSettings (nolock) where entity = 'Playlist' and Setting = 'Number')), @itrackinggroup = isnull(@trackingGroup, 10557), @idevicetype = isnull(@devicetype, 1), @iContentID = @contentid

	if	@iplaylistID = 9999999
	begin
		exec GetSingleContentMetadata @icontentid, @iTrackingGroup, @ideviceType
		return
	end


	select
				a.PlaylistID,
				a.ContentID, a.Name, a.Description, a.TrackingGroup ContentPartnerID, a.VideoGroupName, a.Duration, convert(date, a.PubDate) PubDate, a.Keyword, convert(varchar(120), null) Timeline,
				case	when a.AssetTypeName like 'Video%' then 'src'
						--when a.AssetTypeName like 'Thumb%' then 'thumbnail'
						--when a.AssetTypeName like 'Still%' then 'stillFrame'
						-- when a.AssetTypeName in ('ExternalContentID', 'ExternalImageURL') then a.AssetTypeName
						else a.AssetTypeName --'unk'
				end as AssetType,
				case when a.AssetTypeName like 'External%' then a.Filename
				else
					a.DeliveryPath
				end as AssetLocation,
				a.ProducerName, a.logourl ProducerLogo, a.[Order]+1 ContentOrder, a.ProducerCategory
	from	(select	pt.partnerid, pt.TrackingGroup, pt.isContentPrivate, p.playlistid, pc.ContentID, c.Name,
				c.Description, c.EffectiveDate PubDate, pc.[Order], aa.FilePath, aa.Filename, aa.Duration, c.Keyword,
				VG.VideoGroupName, at.Name AssetTypeName, mt.FileExtension, pt.logourl, pt.Name ProducerName, isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'news') ProducerCategory,
				dbo.GetAssetDeliveryPath(mt.FileExtension, c.partnerid, ca.contentid, ca.assetid) DeliveryPath
		from	(select * from Playlist (nolock) where PlaylistID = @iplaylistID) p
		join	(select * from Playlist_Content (nolock) where playlistid = @iplaylistid and contentid = @icontentid) pc
		on		p.PlaylistID = pc.PlaylistID
		join	Content c (nolock)
		on		pc.ContentID = c.ContentID
		join	Content_Asset ca
		on		c.ContentID = ca.ContentID
		and		@idevicetype = ca.TargetPlatformID
		join	Asset aa
		on		ca.AssetID = aa.AssetID
		join	AssetType at
		on		ca.AssetTypeID = at.AssetTypeID
		join	MimeType mt
		on		at.MimeTypeID = mt.MimeTypeID
		and		aa.MimeTypeID = mt.MimeTypeID
		join	Partner_TargetPlatformProfile ptp
		on		c.PartnerID = ptp.PartnerID
		and		@idevicetype = ptp.TargetPlatformID
		JOIN	partner pt (nolock)
		ON		c.PartnerID = pt.PartnerID
		left join	VideoGroups VG
		on		p.PlaylistID = vg.VideoGroupId
		left join	Playlist_Content_PartnerMap pm
		on			c.PartnerID = pm.PartnerID
		and			c.FeedID = pm.FeedID
		left join	NDNCategory nc
		on			pm.NDNCategoryID = nc.NDNCategoryID
		where	p.PlaylistID = @iplaylistID
		and		isnull(aa.isDeleted, 0) = 0
		and		dbo.AllowedLauncherContent(@iwidgetID, c.PartnerID) = 1
		) a
		left join mTrackingGroup pd (nolock)-- partner pd
		on	@itrackinggroup = pd.TrackingGroup
		where		dbo.AllowedContentID(a.contentid, @itrackinggroup, @devicetype) = 1
		--and			dbo.AllowedLauncherContent(@iwidgetID, @itrackinggroup) = 1

	ORDER BY a.[Order]+1, a.ContentID

	set nocount off
