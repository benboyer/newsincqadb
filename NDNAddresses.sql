CREATE VIEW dbo.NDNAddresses
AS
SELECT DISTINCT 
                      TOP (100) PERCENT dbo.Address.AddressID, dbo.NDNAddressesLegacy.Phone, dbo.Address.Address1, dbo.Address.Address2, dbo.Address.City, 
                      dbo.NDNAddressesLegacy.State, dbo.Address.Zip, dbo.Address.CountryID
FROM         dbo.Address LEFT OUTER JOIN
                      dbo.NDNAddressesLegacy ON dbo.Address.AddressID = dbo.NDNAddressesLegacy.AddressID
ORDER BY dbo.Address.AddressID
