create procedure dbo.mon_listlandingurltypeids
as
begin
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'mon_listlandingurltypeids'
	select * from landingurltype
	set nocount off
end	
