USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ContentLink_GetStoryLinks]
	--declare 
	@partnerid int = null, 
	@GetMatched bit = 0,
	@limit int = 100
as
BEGIN
	set		fmtonly off
	set		nocount on
	exec	SprocTrackingUpdate 'ContentLink_GetStoryLinks'
	select	top (@limit) PartnerID, StoryGUID, VideoGUID, ContentID, StoryStorageLink, MatchRank, TimesUpdated
	-- select *
	from	contentlink
	where	(@partnerid is null or partnerid = @partnerid)
	and		((@GetMatched = 0 and ContentID is null)
			or
			@GetMatched = 1)
	order by LastUPdated desc
	set nocount off
END
GO
