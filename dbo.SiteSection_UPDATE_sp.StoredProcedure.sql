USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SiteSection_UPDATE_sp]
	@SiteSectionID int,
	@Name nvarchar(50),
	@CreatedDT smalldatetime,
	@IsEnabled bit,
	@IsDefault bit,
	@UserID int = null
AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'SiteSection_UPDATE_sp'

	Declare @PartnerID int;

	Select @PartnerID = PartnerID from [User] where UserID = @UserID

    UPDATE Section Set PartnerID=@PartnerID, Name=@Name, [Default]=@IsDefault, Active=@IsEnabled
    WHERE SectionID = @SiteSectionID


END
GO
