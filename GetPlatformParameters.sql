
CREATE procedure [dbo].[GetPlatformParameters]
	--declare
	@ContentID bigint -- = 23571856
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate ''

	select distinct c.PartnerID, c.ContentID, ca.TargetPlatformID, ptpp.isImportEnabled, ptpp.isDistributionEnabled, ptpp.ActivateOnImport
	from content c
	join Content_Asset ca
	on	c.ContentID = ca.ContentID
	join Partner_TargetPlatformProfile ptpp
	on	c.partnerid = ptpp.PartnerID
	and ca.TargetPlatformID = ptpp.TargetPlatformID
	where ca.ContentID = @ContentID

	set nocount on
END