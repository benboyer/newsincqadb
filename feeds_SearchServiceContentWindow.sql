CREATE PROCEDURE [dbo].[feeds_SearchServiceContentWindow]
 -- Add the parameters for the stored procedure here
 @StartDate DATETIME

AS
BEGIN

SET NOCOUNT ON;

DECLARE @MyStartDate DATETIME
IF @StartDate IS NULL
BEGIN
 SET @MyStartDate = GETDATE()
END
ELSE
BEGIN
 SET @MyStartDate = @StartDate
END

 -- Resolve Content
DECLARE @TempContent TABLE(
 ContentID INT PRIMARY KEY,
 UUID UNIQUEIDENTIFIER,
 PartnerID INT NOT NULL,
 PartnerName NVARCHAR(50),
 Title VARCHAR(500) NULL,
 [Description] VARCHAR(1000) NULL,
 EffectiveDate DATETIME NULL,
 CreatedDate DATETIME NULL,
 UpdatedDate DATETIME NULL,
 Category VARCHAR(250) NULL,
 Keyword VARCHAR(1000) NULL,
 Active BIT NULL
)

INSERT INTO @TempContent
SELECT
 C.ContentID, C.UUID, P.PartnerID, P.Name, C.Name, C.[Description], C.EffectiveDate, C.CreatedDate, C.UpdatedDate, C.Category, C.Keyword, C.Active
 FROM Content C
 INNER JOIN [Partner] P ON P.PartnerID = C.PartnerID
  WHERE  C.UpdatedDate >= @StartDate
  ORDER BY C.UpdatedDate DESC, C.ContentID DESC

 -- Resolve Video Meta
 DECLARE @TempVideoMeta TABLE(
 ContentID INT PRIMARY KEY,
 Url VARCHAR(1000) NULL,
 [Type] VARCHAR(20) NULL,
 [Medium] VARCHAR(20) NULL,
 Duration INT NULL,
 Height INT NULL,
 Width INT NULL
 )
INSERT INTO @TempVideoMeta
SELECT C.ContentID, dbo.GetPathToStreamAsset(CA.AssetID), MT.MimeType, MT.MediaType, A.Duration, A.Height, A.Width
 FROM @TempContent C
 INNER JOIN Content_Asset CA ON CA.ContentID = C.ContentID
 INNER JOIN Asset A ON A.AssetID = CA.AssetID
 INNER JOIN MimeType MT ON A.MimeTypeID = MT.MimeTypeID
  WHERE MT.MimeTypeID IN (3,10,13)

-- Resolve Thumbnail Meta
DECLARE @TempThumbnailMeta TABLE(
 ContentID INT PRIMARY KEY,
 Url VARCHAR(1000) NULL,
 Height INT NULL,
 Width INT NULL
)
INSERT INTO @TempThumbnailMeta
SELECT C.ContentID, dbo.GetPathToServeAsset(A.AssetID), A.Height, A.Width
 FROM @TempContent C
 INNER JOIN Content_Asset CA ON CA.ContentID = C.ContentID
 INNER JOIN Asset A ON A.AssetID = CA.AssetID
 INNER JOIN AssetType AT ON AT.AssetTypeID = CA.AssetTypeID
  WHERE AT.AssetTypeID = 3

-- Pull it all together
SELECT
 C.ContentID, C.UUID, C.PartnerID, C.PartnerName, C.Title, C.[Description], C.Category, C.Keyword, C.Active, C.EffectiveDate, C.CreatedDate, C.UpdatedDate,
 VM.Url AS [ContentUrl], VM.[Type] AS [MimeType], VM.Medium, VM.Height, VM.Width, VM.Duration,
 TM.Url AS [ThumbnailUrl], TM.Height AS [ThumbnailHeight], TM.Width AS [ThumbnailWidth]
 FROM @TempContent C
  LEFT OUTER JOIN @TempVideoMeta VM ON VM.ContentID = C.ContentID
  LEFT OUTER JOIN @TempThumbnailMeta TM ON TM.ContentID = C.ContentID
 ORDER BY C.EffectiveDate DESC, C.ContentID DESC

END
