TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	UploadContent	UploadContentID	-5	bigint identity	19	8	0	10	0	None	None	-5	None	None	1	NO	63
NewsincQA	dbo	UploadContent	PartnerID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	UploadContent	GUID	12	varchar	250	250	None	None	0	None	None	12	None	250	3	NO	39
NewsincQA	dbo	UploadContent	FeedContentID	-5	bigint	19	8	0	10	1	None	None	-5	None	None	4	YES	108
NewsincQA	dbo	UploadContent	PublishDate	11	datetime	23	16	3	None	1	None	None	9	3	None	5	YES	111
NewsincQA	dbo	UploadContent	ExpirationDate	11	datetime	23	16	3	None	1	None	None	9	3	None	6	YES	111
NewsincQA	dbo	UploadContent	Name	12	varchar	500	500	None	None	1	None	None	12	None	500	7	YES	39
NewsincQA	dbo	UploadContent	Description	12	varchar	1000	1000	None	None	1	None	None	12	None	1000	8	YES	39
NewsincQA	dbo	UploadContent	Keywords	12	varchar	1000	1000	None	None	1	None	None	12	None	1000	9	YES	39
NewsincQA	dbo	UploadContent	Categories	12	varchar	250	250	None	None	1	None	None	12	None	250	10	YES	39
NewsincQA	dbo	UploadContent	Version	4	int	10	4	0	10	0	None	None	4	None	None	11	NO	56
NewsincQA	dbo	UploadContent	ImportComplete	-7	bit	1	1	None	None	0	None	None	-7	None	None	12	NO	50
NewsincQA	dbo	UploadContent	UpdatedDate	11	datetime	23	16	3	None	0	None	(getdate())	9	3	None	13	NO	61
NewsincQA	dbo	UploadContent	OnlyOwnerView	-7	bit	1	1	None	None	0	None	((0))	-7	None	None	14	NO	50
NewsincQA	dbo	UploadContent	ContentSourceID	4	int	10	4	0	10	0	None	((0))	4	None	None	15	NO	56

index_name	index_description	index_keys
PK_UploadContent	clustered, unique, primary key located on PRIMARY	UploadContentID
