CREATE procedure [dbo].[GetPartnerStatments]
	--declare
	@userid int = 8155
as
BEGIN
	set fmtonly OFF
	set nocount on
	exec sproctrackingupdate 'GetPartnerStatments'

	if exists (select 1
			from	(select * from [user] where userid = @userid) u
			join	user_permission up
			on		u.userid = up.userid
			join	permission pm
			on		up.permissionid = pm.permissionid
			where	pm.name = 'Access Financial Statements'
			and		pm.Value = 1)
	begin
		select	replace(ps.TypePath, '\', '') StatementType,  left(ps.YearMonthPath, 4) StatementYear, replace(right(ps.YearMonthPath, 3), '\', '') StatementMonth, convert(varchar(200), ps.RootPath + ps.YearMonthPath + ps.TypePath + FileName) as StatementPath
		from	[user] u
		join	user_permission up
		on		u.userid = up.userid
		join	permission pm
		on		up.permissionid = pm.permissionid
		join	partner p
		on		u.partnerid = p.partnerid
		join	PartnerStatements ps
		on		p.TrackingGroup = ps.DPID
		where	u.userid = @userid
		and		pm.name = 'Access Financial Statements'
		and		pm.Value = 1
	end
	else
	begin
		select convert(varchar(200),'User Not Authorized') as StatementPath
	end

	set nocount on
END
