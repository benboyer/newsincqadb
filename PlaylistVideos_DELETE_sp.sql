CREATE PROCEDURE [dbo].[PlaylistVideos_DELETE_sp]  
   @VideoID int,  
      @SortOrder int,  
      @Sharing int,  
      @PlaylistID int,  
      @ContentItemID int  
AS  
BEGIN  
 SET NOCOUNT ON;  
 exec SprocTrackingUpdate 'PlaylistVideos_DELETE_sp'  
  
 BEGIN TRANSACTION  
  
 DELETE Playlist_Content  
 WHERE PlaylistID = @PlaylistID and ContentID = @VideoID  
 IF @@ERROR = 0  
 BEGIN  
  DELETE PlaylistVideosLEGACY  
  WHERE PlaylistID = @PlaylistID and ContentID = @VideoID  
    
  UPDATE Playlist  
  SET ContentUpdatedDate = GETDATE()  
  WHERE PlaylistID = @PlaylistID  
 END  
  
 IF @@ERROR = 0  
  COMMIT TRANSACTION  
 ELSE  
  ROLLBACK TRANSACTION  
  
 RETURN @@ERROR  
  
  
END  

