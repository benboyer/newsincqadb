USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetDeliverySettings]
	@LauncherID int = null,
	@TrackingGroup int = null
as
BEGIN
	-- How it works:
	--		If the DeliverySettings.Value is set to the default, then specific settings for the launchertypesid or trackinggroup will override them.
	--		If the DeliverySettings.Value is not set to the default then it will be the value returned.
	--		Logic is that if the global settings are not at the default, we are implementing a system-wide condition that takes precedence over individual partner/launcher settings.
	--		Logic is based on the assumption that default global settings are promiscious but non-default settings are not.
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetDeliverySettings'

	-- declare @LauncherID int = 7010, 	@TrackingGroup int = 90121--10557 -- select * from deliverysettings
	select ds.PlayerVariable as Name, case when ds.value = ds.defaultvalue then isnull(isnull(dslt.Value, dstg.Value), ds.Value) else ds.value end Value,
	 convert(varchar(5), ds.DeliverySettingsID) + '=' + convert(varchar(5), case when ISNUMERIC(case when ds.value = ds.defaultvalue then isnull(isnull(dslt.Value, dstg.Value), ds.Value) else ds.value end) = 1
			then case when ds.value = ds.defaultvalue then isnull(isnull(dslt.Value, dstg.Value), ds.Value) else ds.value end
			else case when case when ds.value = ds.defaultvalue then isnull(isnull(dslt.Value, dstg.Value), ds.Value) else ds.value end = 'true' then 1
			when case when ds.value = ds.defaultvalue then isnull(isnull(dslt.Value, dstg.Value), ds.Value) else ds.value end = 'false' then 0 end end) as ShortVariable

	from DeliverySettings ds
	left join DeliverySetting_TrackingGroup dstg
	on	ds.deliverysettingsid = dstg.deliverysettingsid
	and @TrackingGroup = dstg.trackinggroup
	left join DeliverySetting_LauncherTypes dslt
	on	ds.deliverysettingsid = dslt.deliverysettingsid
	and	@LauncherID = dslt.LauncherTypesID
	left join Launcher l
	on		@LauncherID = l.LauncherID
	left join LauncherTemplate lt
	on		l.LauncherTemplateID = lt.LauncherTemplateID

	set nocount off
END
GO
