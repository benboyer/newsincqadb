CREATE PROCEDURE [dbo].[WidgetPlaylist_ADD_sp]
	@WidgetId int,
	@PlaylistId int,
	@Order int = null
AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'WidgetPlaylist_ADD_sp'

	INSERT Launcher_Playlist(LauncherID, PlaylistID, [Order])
	VALUES(@WidgetId, @PlaylistId, @Order);

END
