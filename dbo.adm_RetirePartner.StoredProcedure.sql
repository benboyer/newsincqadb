USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[adm_RetirePartner]
	--declare 
	@partnerid int,  
	@Statusid int,
	@DeleteContent bit = 0
as
BEGIN
	declare @messages as table(messageback varchar(200))
	declare @reccnt varchar(10)

	set @reccnt = (select convert(varchar(10), COUNT(*)) from [User] where PartnerID = @partnerid and StatusID < @Statusid)
	update [User] set StatusID = @Statusid, UpdatedDate = GETDATE(), UpdatedUserID = 7534 where PartnerID = @partnerid and StatusID < @Statusid
	insert into @messages(messageback)
	select @reccnt + ' User accounts updated'

	update Partner set StatusID = @Statusid, UpdatedDate = GETDATE(), UpdateUserID = 7534, isMediaSource = 0 where (PartnerID = @partnerid and StatusID < @Statusid)
	insert into @messages(messageback)
	select 'Partner record updated'

	if (select COUNT(*) from OrganizationsLEGACY where partnerid = @partnerid and IsMediaSource = 1) > 0
	begin
		update OrganizationsLEGACY set IsMediaSource = 0 where partnerid = @partnerid and IsMediaSource = 1
		select 'OrganizationsLegacy record updated'
	end
	
	set @reccnt = (select convert(varchar(10), COUNT(*)) from Partner_TargetPlatformProfile where PartnerID = @partnerid)
	update Partner_TargetPlatformProfile set isImportEnabled = 0, isDistributionEnabled = 0 where PartnerID = @partnerid 
	insert into @messages(messageback)
	select @reccnt + ' Target Platform Profiles updated'

	set @reccnt = (select convert(varchar(10), COUNT(*)) from Feed where PartnerID = @partnerid)
	update Feed set Active = 0 where (PartnerID = @partnerid and Active = 1)
	insert into @messages(messageback)
	select @reccnt + ' inbound RSS Feeds updated'

	set @reccnt = (select convert(varchar(10), COUNT(*)) from PartnerContentImportFolder where PartnerID = @partnerid)
	update PartnerContentImportFolder set Enabled = 0 where (PartnerID = @partnerid and Enabled = 1)
	insert into @messages(messageback)
	select @reccnt + ' WatchFolder Feeds updated'

	set @reccnt = (select convert(varchar(10), COUNT(*)) from partner where PartnerID = @partnerid and isFeedGUIDActive = 1)
	update Partner set isFeedGUIDActive = 0  where (PartnerID = @partnerid and isFeedGUIDActive = 1)
	insert into @messages(messageback)
	select @reccnt + ' FeedGuids set Inactive'
		
	set @reccnt = (select convert(varchar(10), COUNT(*)) from NDNUsersLEGACY where UserID in (select UserID 
						from [User] 
						where PartnerID = @partnerid) 
						and AccountStatus < @Statusid)
	update NDNUsersLEGACY set AccountStatus = @Statusid 
	where UserID in (select UserID 
						from [User] 
						where PartnerID = @partnerid) 
						and AccountStatus < @Statusid
	insert into @messages(messageback)
	select @reccnt + ' Legacy Users accounts updated'

	if @DeleteContent = 1
	begin
		set @reccnt =	(select convert(varchar(10), COUNT(*)) 
						from (select contentid from content (nolock) where PartnerID = @partnerid) ref
						join Content c on		ref.ContentID = c.ContentID
						join Content_Asset ca on c.ContentID = ca.ContentID 
						join Asset a on ca.AssetID = a.AssetID
						where c.PartnerID = @partnerid)
		update a set a.active = 0, a.isdeleted = 1
		from (select contentid from content (nolock) where PartnerID = @partnerid) ref
		join Content c on		ref.ContentID = c.ContentID
		join Content_Asset ca on c.ContentID = ca.ContentID 
		join Asset a on ca.AssetID = a.AssetID
		where c.PartnerID = @partnerid 
		insert into @messages(messageback)
		select @reccnt + ' Assets set inactive and isDeleted'

		set @reccnt =	(select convert(varchar(10), COUNT(*)) 
						from (select contentid from content (nolock) where PartnerID = @partnerid) ref
						join Content c on		ref.ContentID = c.ContentID
						join Content_Asset ca on c.ContentID = ca.ContentID 
						where c.PartnerID = @partnerid)
		update ca set ca.active = 0
		from (select contentid from content (nolock) where PartnerID = @partnerid) ref
		join Content c on	ref.ContentID = c.ContentID
		join Content_Asset ca on c.ContentID = ca.ContentID 
		where c.PartnerID = @partnerid 
		insert into @messages(messageback)
		select @reccnt + ' Content_Assets set inactive and isDeleted'

		set @reccnt = (select convert(varchar(10), COUNT(*)) 
						from (select contentid from content (nolock) where PartnerID = @partnerid) ref
						join VideosLEGACY c on		ref.ContentID = c.ContentID)
		update c set c.IsDeleted  = 1
		from (select contentid from content (nolock) where PartnerID = @partnerid) ref
		join VideosLEGACY c on		ref.ContentID = c.ContentID
		insert into @messages(messageback)
		select @reccnt + ' VideosLegacy records set isDeleted'

		set @reccnt = (select convert(varchar(10), COUNT(*)) from (select contentid from content (nolock) where PartnerID = @partnerid) ref
						join Content c on		ref.ContentID = c.ContentID
						where c.PartnerID = @partnerid)
		update c set c.active = 0, c.isdeleted = 1, c.updateddate = getdate(), c.updateduserid = 7534
		from (select contentid from content (nolock) where PartnerID = @partnerid) ref
		join Content c on		ref.ContentID = c.ContentID
		where c.PartnerID = @partnerid 
		insert into @messages(messageback)
		select @reccnt + ' Content records set inactive and isDeleted'
	end
	select messageback from @messages
END
GO
