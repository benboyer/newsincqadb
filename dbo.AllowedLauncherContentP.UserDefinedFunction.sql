USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION [dbo].[AllowedLauncherContentP]
(
	 @LauncherID bigint,
	 @PartnerID int
)
RETURNS bit
AS
BEGIN
	DECLARE @allowed bit

	set @Allowed = isnull((select top 1 0 -- select top 100 *
		from	Launcher_ContentExclusion (nolock) lce
		where	lce.LauncherID = @LauncherID
		and		lce.PartnerID = @PartnerID), 1)

	RETURN @Allowed

END
GO
