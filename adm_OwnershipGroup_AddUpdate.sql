CREATE procedure [dbo].[adm_OwnershipGroup_AddUpdate]
	@ownershipgroupid int = null,
	@organizationname varchar(100) = null,
	@adunit varchar(100) = null,
	@user varchar(20) = null
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'adm_OwnershipGroup_AddUpdate'
/*	
	if @ownershipgroupid is null and @organizationname is null
	begin
		select 'Must have an ID or a name' as MessageBack
		return
	end
*/	
	if @ownershipgroupid is not null and @organizationname is not null
	begin
		if exists (select 1 from ownershipgroup where ownershipgroupid = @ownershipgroupid)
		begin
			if @user is not null
			begin
				update OwnershipGroup set OwnershipGroup = @organizationname, AdUnit = @adunit, UpdatedBy = @user, UpdatedDate = GETDATE()
				where OwnershipGroupID = @ownershipgroupid
			end
			else
			begin
				select 'UID is required to update' as MessageBack
				return
			end
		end
		else
		begin
			select 'ID is not valid.  Try again.' as MessageBack
			return
		end
	end
	
	if @ownershipgroupid is null and @organizationname is not null
	begin
		if not exists (select 1 from ownershipgroup where ownershipgroup = @organizationname)
		begin
			if @user is not null
			begin
				insert into OwnershipGroup(OwnershipGroup, AdUnit, UpdatedBy)
				select @organizationname, @adunit, @user
			end
			else
			begin
				select 'UID is required to insert' as MessageBack
				return
			end
		end
		set @ownershipgroupid = (select ownershipgroupid from ownershipgroup where ownershipgroup = @organizationname)
	end

	select OwnershipGroupID, OwnershipGroup, AdUnit
	from OwnershipGroup
	where ownershipgroupid = isnull(@ownershipgroupid, ownershipgroupid)
	order by Ownershipgroup	
	set nocount off
END
