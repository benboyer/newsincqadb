CREATE procedure [dbo].[adm_ContentCategoryToDMAInclude]
	(
	-- declare 
	@partnerid int, @lastContentID bigint, @user varchar(20) = 'admin', @Exproc bit = 1
	)
as
BEGIN
	set FMTONLY OFF
	set NOCOUNT ON
	exec SprocTrackingUpdate 'CategoryToDMAInclude'
	
	-- declare @partnerid int = 1882, @lastContentID bigint = (select lastcontentid from admExceptionalProcessing), @user varchar(20) = 'admin', @Exproc bit = 1
	declare @workme bigint,
			@workcat varchar(200)
	declare @CIDs table (ContentID bigint)
	insert into @CIDs(ContentID)
	-- declare @partnerid int = 1882, @lastContentID bigint, @user varchar(20) = 'admin', @Exproc bit = 1
	select top 100 Contentid 
	from	Content 
	where	PartnerID = @partnerid
	and		ContentID > isnull(@lastContentID, 0)
	order by ContentID
		--select * from @CIDs --  select @workme, @workcat

	while (select COUNT(*) from @CIDs ) > 0
	begin
		set @workme = (select MIN(contentid) from @CIDs)
		set @workcat = (select category from Content where ContentID = @workme)
		if	@workcat is null
		begin
			update Content set Category = '99' where ContentID = @workme
			set @workcat = '99'
			-- select 'updated category to 99'
		end 
		if	(select COUNT(*) --  @workme ContentID, a.value DMSCode
			from	dbo.fn_Rows_fmCommaSeparated(@workcat) a
			join	DMA b
			on		case when isnumeric(a.value) = 1 then a.value else null end = b.DMACode
			where	ISNUMERIC(a.value)= 1) > 0
			begin
				select	@partnerid, @workme ContentID, a.value DMACode, @Exproc, @user
				from	dbo.fn_Rows_fmCommaSeparated(@workcat) a
				join	DMA b
				on		case when isnumeric(a.value) = 1 then a.value else null end = b.DMACode
				where	ISNUMERIC(a.value)= 1
				exec	RefTableInserts @workme, @workcat, 'CIDtoDMAInclude'
			end	
		if @Exproc = 1
		begin
			update	admExceptionalProcessing
			set		LastContentID = @workme,
					UpdatedDTM = GETDATE(),
					UpdatedUser = @user 
			where	PartnerID = @partnerid
			and		ProcedureName = 'adm_ContentCategoryToDMAInclude'
		end
		delete from @CIDs where ContentID = @workme
	end

		update	admExceptionalProcessing
		set		LastRunDTM = GETDATE(),
				UpdatedUser = @user 
		where	PartnerID = @partnerid
		and		ProcedureName = 'adm_ContentCategoryToDMAInclude'

	set NOCOUNT OFF
END
