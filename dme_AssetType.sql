create view dme_AssetType
as
select AssetTypeID, Name, MimeTypeID,MinHeight, MaxHeight, MinWidth, MaxWidth, MinBitrate, MaxBitrate, IdealWidth, IdealBitrate, case when Comment = 'test data' then null else Comment end as Comment, Process, Transcode, CreateFile, CreateFileExtension
from AssetType (nolock)
