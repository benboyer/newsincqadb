USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetFullTextSearchResult_3]
(	-- declare
	@SearchString NVARCHAR(4000),
	@WidgetId INT = NULL,
	@UserId INT = 0,
	@OrgId INT =0,
	@Count INT = 100,
	@OrderClause NVARCHAR(MAX) = null,
	@StatusType INT = 0,
	@IsAdmin BIT = 0,
	@WhereCondition NVARCHAR(MAX) = ''
	, @StartDTM datetime = null
	, @EndDTM datetime = null,
	 @MinVideoID BIGINT = 0, -- max value of the current external record set: used for pagination
	 @MaxVideoID BIGINT = 0 -- min value of the current external record set: used for pagination
)
AS
BEGIN
	-- updated 2013-09-18
	-------------------------------------------------
	--set @UserId = 7534
	--set @SearchString = 'president obama'
	--set @startdtm = '2013-03-16'
	--set @enddtm = '2013-03-19'
	-------------------------------------------------
	exec SprocTrackingUpdate 'GetFullTextSearchResult_3'

		if @WhereCondition like '%partnerid%' and isnull(@OrgId, 0) = 0
		begin
			set @OrgId = (select convert(int, right(@wherecondition, charindex('=',reverse(@WhereCondition))-1)))
		end

	--insert into sprocparamtracker(sproc, param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12, param13)
	--select 'GetFullTextSearchResult_3', LEFT(@SearchString, 200),@WidgetId,@UserId,@OrgId,@Count,@OrderClause,@StatusType,@IsAdmin,@WhereCondition,@StartDTM,@EndDTM,@MinVideoID,@MaxVideoID

	if @MaxVideoID = 0 set @MaxVideoID = 0
	if @MinVideoID = 0 set @MinVideoID = 0

	create table #tmpsrch(VideoID bigint, UserID int, UserType int, ClipURL varchar(500), Title varchar(1000), Description varchar(5000), EventDate datetime, NumberofReviews int, Height int, Width int, Duration decimal(10,2), ClipPopularity int, IsAdvertisement bit, CreatedOn datetime, ThumbnailURL varchar(200), PublishDate datetime, Keywords varchar(1000), NumberOfPlays int, OriginalName varchar(200), ExpiryDate datetime, IsEnabled bit, IsDeleted bit, OnlyOwnerView bit, UserName varchar(200), OrganizationID int, Rank int, OrganizationName  varchar(200), IsMediaSource bit, IsAdmin bit)

	if @StatusType = -1 set @StatusType = 0
	if @Count is null set @Count = 100
	if @Count > 100 set @Count = 100

	insert into #tmpsrch(VideoID, UserID, UserType, ClipURL, Title, Description, EventDate, NumberofReviews, Height, Width, Duration, ClipPopularity, IsAdvertisement, CreatedOn, ThumbnailURL, PublishDate, Keywords, NumberOfPlays, OriginalName, ExpiryDate, IsEnabled, IsDeleted, OnlyOwnerView, UserName, OrganizationID, Rank, OrganizationName, IsMediaSource,IsAdmin)
	exec GetSearchResults @searchString, @widgetid, @userid, @orgid, @count, @orderclause, @statustype, @isadmin, @wherecondition, @startdtm, @enddtm,@minvideoid, @maxvideoid,
	0, 'all', 0, 1

	if (select COUNT(*) from #tmpsrch (nolock)) = 0
	insert into #tmpsrch(VideoID, UserID, UserType, ClipURL, Title, Description, EventDate, NumberofReviews, Height, Width, Duration, ClipPopularity, IsAdvertisement, CreatedOn, ThumbnailURL, PublishDate, Keywords, NumberOfPlays, OriginalName, ExpiryDate, IsEnabled, IsDeleted, OnlyOwnerView, UserName, OrganizationID, Rank, OrganizationName, IsMediaSource,IsAdmin)
	exec GetSearchResults @searchString, @widgetid, @userid, @orgid, @count, @orderclause, @statustype, @isadmin, @wherecondition, @startdtm, @enddtm,@minvideoid, @maxvideoid,
	1, 'all', 0, 1

	if (select COUNT(*) from #tmpsrch (nolock)) = 0
	insert into #tmpsrch(VideoID, UserID, UserType, ClipURL, Title, Description, EventDate, NumberofReviews, Height, Width, Duration, ClipPopularity, IsAdvertisement, CreatedOn, ThumbnailURL, PublishDate, Keywords, NumberOfPlays, OriginalName, ExpiryDate, IsEnabled, IsDeleted, OnlyOwnerView, UserName, OrganizationID, Rank, OrganizationName, IsMediaSource,IsAdmin)
	exec GetSearchResults @searchString, @widgetid, @userid, @orgid, @count, @orderclause, @statustype, @isadmin, @wherecondition, @startdtm, @enddtm,@minvideoid, @maxvideoid,
	2, 'all', 0, 1

	select top (@Count) * from #tmpsrch order by
			case when @OrderClause like '%Post%' OR @OrderClause like '%created%' then CreatedOn end desc,
			--case when @OrderClause like '%Event%' or @OrderClause like '%publish%' then PublishDate end desc,
			case when @OrderClause like '%EventDate desc%' or @OrderClause like '%publish%' then PublishDate end desc,
			case when @OrderClause like '%EventDate asc%' or @OrderClause like '%publish%' then PublishDate end asc,
			case when @OrderClause like '%Update%' then CreatedOn end desc,
			CreatedOn desc

	drop table #tmpsrch
END
GO
