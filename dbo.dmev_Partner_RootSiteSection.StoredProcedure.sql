USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[dmev_Partner_RootSiteSection]
	@PartnerID int = null
as
select * 
from dme_Partner_RootSiteSection (nolock) prss
where	prss.PartnerID = ISNULL(@partnerid, prss.PartnerID)
GO
