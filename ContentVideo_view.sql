CREATE VIEW dbo.ContentVideo_view
AS
SELECT     dbo.[Content].ContentID, dbo.[Content].ContentTypeID, dbo.[Content].PartnerID, dbo.[Content].ContentSourceID, dbo.[Content].Name, dbo.[Content].Description, 
                      dbo.[Content].Category, dbo.[Content].Keyword, dbo.[Content].EffectiveDate, dbo.[Content].ExpirationDate, dbo.[Content].Active, dbo.[Content].CreatedDate, 
                      dbo.[Content].CreatedUserID, dbo.[Content].UpdatedDate, dbo.[Content].UpdatedUserID, dbo.[Content].IsFileArchived, dbo.ContentVideo.GUID, 
                      dbo.ContentVideo.ContentVideoImportStatusID, dbo.ContentVideo.FileName, dbo.ContentVideo.ImportFilePath, dbo.ContentVideo.Height, dbo.ContentVideo.Width, 
                      dbo.ContentVideo.Duration, dbo.ContentVideo.ThumbnailImportFilePath, dbo.ContentVideo.ThumbnailFileName, dbo.ContentVideo.StillframeImportFilePath, 
                      dbo.ContentVideo.StillframeFileName
FROM         dbo.[Content] INNER JOIN
                      dbo.ContentVideo ON dbo.[Content].ContentID = dbo.ContentVideo.ContentID
