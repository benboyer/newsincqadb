CREATE procedure GetContentIDByNameDescriptionKeyword @name varchar(100), @page int = 1, @size int = 100 as    
    
if LEN(@name) < 4    
 return    
    
select * into #temp from CONTAINSTABLE (content, (name, description, keyword), @name)    
    
select *, @page as page, ( select COUNT(*) from #temp ) as cnt from     
(    
select contentid, name, [description], keyword, antikw, partnerid, ( select name from partner where partnerid = content.partnerid) as partnername, RANK ( ) OVER ( order by contentid desc ) as rnk from Content    
 inner join     
  (select * from #temp) sub    
 on content.contentid = sub.[key]    
) data    
where rnk between ( @page - 1 ) * @size + 1 and ( ( @page - 1 ) * @size ) + @size    
order by ContentID desc 