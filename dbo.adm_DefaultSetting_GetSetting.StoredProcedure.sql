USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[adm_DefaultSetting_GetSetting]
as
BEGIN
	set fmtonly off
	set nocount on
	select Name from setting
	order by name
	set nocount off
END
GO
