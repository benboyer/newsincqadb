USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_Categories_fmContentCategories]
(
	@ContentID bigint
)
RETURNS @Cattable TABLE
(Categories varchar(200))
AS
BEGIN
		declare @wholecat varchar(120)
		declare @lastCat varchar(120)
		declare @PartnerID bigint

		set		@wholecat = (select Category from Content where ContentID = @ContentID)
		set		@PartnerID = (select partnerid from content where ContentID = @ContentID)

		while	(select CHARINDEX(',', @wholecat, 1)) > 0
		begin
			set @lastCat = (select	left(@wholecat, CHARINDEX(',', @wholecat, 1) -1))
			insert @Cattable select rtrim(ltrim(@lastCat))
			set @lastCat = @lastCat + ','
			set @wholecat = replace(@wholecat, @lastcat, '')
		end
		insert @Cattable select rtrim(ltrim(@wholecat))
			declare @title varchar(100)
			set @title = (select [name] from content where contentid = @ContentID)
			insert @Cattable
			select	SourceCategory
			from	playlist_content_partnermap
			where	partnerid = @PartnerID
			and		@title like SourceCategory + '%'
		insert @Cattable select rtrim(ltrim(Category)) from Playlist_Content_PartnerMapDefaultCategory where PartnerID = @PartnerID
		insert @Cattable select rtrim(ltrim(Category)) from Playlist_Content_PartnerMapDefaultCategory2 where PartnerID = @PartnerID
	RETURN
END;
GO
