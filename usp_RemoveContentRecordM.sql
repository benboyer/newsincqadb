CREATE procedure [dbo].[usp_RemoveContentRecordM]
--	declare
	@contentID bigint
as
BEGIN
	set fmtonly off
	exec SprocTrackingUpdate 'usp_RemoveContentRecordM'

--	declare @contentID bigint = 23628831
	select COUNT(*) as Asset_Count
		from	Content c
		join	Content_Asset ca
		on		c.contentid = ca.ContentID
		join	Asset a
		on		ca.AssetID = a.AssetID
		where	c.contentid = @contentID

		select distinct a.AssetID
		into	#deletableassets -- drop table #deletableassets
		from	Content c
		join	Content_Asset ca
		on		c.contentid = ca.ContentID
		join	Asset a
		on		ca.AssetID = a.AssetID
		where	c.contentid = @contentID

--	declare @contentID bigint = 23526763
	select COUNT(*) as ContentAsset_Count
		from	Content c
		join	Content_Asset ca
		on		c.contentid = ca.ContentID
		where	c.contentid = @contentID

		delete	ca
		from	Content c
		join	Content_Asset ca
		on		c.contentid = ca.ContentID
		where	c.contentid = @contentID

		delete	a
		from	#deletableassets ca
		join	Asset a
		on		ca.AssetID = a.AssetID


--	declare @contentID bigint = 23526763
	select COUNT(*) as ContentVideo_Count
		from	Content c
		join	ContentVideo cv
		on		c.contentid = cv.ContentID
		where	c.contentid = @contentID

		delete	cv
		from	Content c
		join	ContentVideo cv
		on		c.contentid = cv.ContentID
		where	c.contentid = @contentID

--	declare @contentID bigint = 23526763
	select COUNT(*) as VideosLegacy_Count
		from	VideosLEGACY
		where	contentid = @contentID

		delete
		from	VideosLEGACY
		where	contentid = @contentID

--	declare @contentID bigint = 23526763
	select COUNT(*) as VideoStagingLegacy_Count
		from	VideoStagingLegacy
		where	contentid = @contentID

		delete
		from	VideoStagingLegacy
		where	contentid = @contentID

	select COUNT(*) as Playlist_Content_Count
		from	Playlist_Content
		where	ContentID = @contentID

		delete
		from	Playlist_Content
		where	ContentID = @contentID

--	declare @contentID bigint = 23526763
	select COUNT(*) as PlaylistVideoData_Count
		from	PlaylistVideoData
		where	VideoID = @contentID

		delete
		from	PlaylistVideoData
		where	VideoID = @contentID

	select COUNT(*) as PlaylistVideosLegacy_Count
		from	PlaylistVideosLegacy
		where	ContentID = @contentID

		delete
		from	PlaylistVideosLegacy
		where	ContentID = @contentID

	select COUNT(*) as ContentKeyword_Count
		from	Content c
		join	Content_Keyword cv
		on		c.contentid = cv.ContentID
		where	c.contentid = @contentID

		delete	cv
		from	Content c
		join	Content_Keyword cv
		on		c.contentid = cv.ContentID
		where	c.contentid = @contentID

	select COUNT(*) as DMAExclude_Count
		from	Content c
		join	DMAExclude cv
		on		c.contentid = cv.ContentID
		where	c.contentid = @contentID

		delete	cv
		from	Content c
		join	DMAExclude cv
		on		c.contentid = cv.ContentID
		where	c.contentid = @contentID

	select COUNT(*) as DMAInclude_Count
		from	Content c
		join	DMAInclude cv
		on		c.contentid = cv.ContentID
		where	c.contentid = @contentID

		delete	cv
		from	Content c
		join	DMAInclude cv
		on		c.contentid = cv.ContentID
		where	c.contentid = @contentID

	select count(*) FeedCategory_count
		from	FeedContent fc

	join	FeedCategory fcat
		on		fc.FeedContentID = fcat.FeedContentID
		where	fc.ContentID = @contentID

	delete fcat
		from	FeedContent fc
		join	FeedCategory fcat
		on		fc.FeedContentID = fcat.FeedContentID
		where	fc.ContentID = @contentID


	select COUNT(*) UploadContentAssetCount
		from	FeedContent fc
		join	UploadContent uc
		on		fc.PartnerID = uc.PartnerID
		and		fc.GUID = uc.GUID
		join	UploadContentAsset uca
		on		uc.UploadContentID = uca.UploadContentID
		where	fc.ContentID = @contentID
--	declare @contentid bigint = 23642441
	delete	uca
		from	FeedContent fc
		join	UploadContent uc
		on		fc.PartnerID = uc.PartnerID
		and		fc.GUID = uc.GUID
		join	UploadContentAsset uca
		on		uc.UploadContentID = uca.UploadContentID
		where	fc.ContentID = @contentID

	select COUNT(*) UploadContentCount
		from	FeedContent fc
		join	UploadContent uc
		on		fc.PartnerID = uc.PartnerID
		and		fc.GUID = uc.GUID
		where	fc.ContentID = @contentID

	delete	uc
		from	FeedContent fc
		join	UploadContent uc
		on		fc.PartnerID = uc.PartnerID
		and		fc.GUID = uc.GUID
		where	fc.ContentID = @contentID


--	declare @contentID bigint = 23526763
	select COUNT(*) FeedContentAsset_Asset_Count
		from FeedContent fc
		join FeedContentAsset fca
		on	 fc.FeedContentID = fca.FeedContentID
		join	Asset a
		on		fca.FeedContentAssetID = a.FeedContentAssetID
		where fc.ContentID = @contentID

		delete a
		from FeedContent fc
		join FeedContentAsset fca
		on	 fc.FeedContentID = fca.FeedContentID
		join	Asset a
		on		fca.FeedContentAssetID = a.FeedContentAssetID
		where fc.ContentID = @contentID




--	declare @contentID bigint = 23526763
	select COUNT(*) FeedContentAsset_Count
		from FeedContent fc
		join FeedContentAsset fca
		on	 fc.FeedContentID = fca.FeedContentID
		where fc.ContentID = @contentID

--	declare @contentID bigint = 23526763
		delete fca
		from FeedContent fc (nolock)
		join FeedContentAsset fca
		on	 fc.FeedContentID = fca.FeedContentID
		where fc.ContentID = @contentID

	select COUNT(*) FeedContent_Count
		from FeedContent fc
		where fc.ContentID = @contentID

	delete fc
		from FeedContent fc
		where fc.ContentID = @contentID

	select COUNT(*) FeedContent_Count
		from FeedContent fc
		where fc.ContentID = @contentID

	delete fc
		from FeedContent fc
		where fc.ContentID = @contentID


	select COUNT(*) as Content_Count
		from	Content
		where	contentid = @contentID

	delete
		from	Content
		where	contentid = @contentID

END
