USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmailAlertConfig](
	[EmailAlertConfigID] [int] IDENTITY(1,1) NOT NULL,
	[EmailAlertName] [varchar](42) NULL,
	[CheckFrequency_minutes] [int] NULL,
	[SendAlertFrequency_minutes] [int] NULL,
	[AddressList] [varchar](500) NULL,
	[LastCheckedDTM] [datetime] NULL,
	[LastSentDTM] [datetime] NULL,
	[ProcedureToRun] [varchar](200) NULL,
	[EmailMessage] [varchar](500) NULL,
	[EmailProfile] [varchar](200) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_EmailAlertConfig] PRIMARY KEY CLUSTERED 
(
	[EmailAlertConfigID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[EmailAlertConfig] ADD  CONSTRAINT [DF_EmailAlertConfig_Active]  DEFAULT ((1)) FOR [Active]
GO
