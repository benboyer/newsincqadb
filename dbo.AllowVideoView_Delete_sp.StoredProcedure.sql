USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AllowVideoView_Delete_sp]
	@ContentProviderOrgID int,
	@DistributorOrgID int
AS
BEGIN
	set fmtonly off
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'AllowVideoView_Delete_sp'

	Delete from AllowContent where ContentProviderPartnerID = @ContentProviderOrgID
	and DistributorPartnerID = @DistributorOrgID

	SET NOCOUNT OFF
END
GO
