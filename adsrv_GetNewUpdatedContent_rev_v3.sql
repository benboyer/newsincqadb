CREATE PROCEDURE [dbo].[adsrv_GetNewUpdatedContent_rev_v3]        
-- -- declare        
--    @datefrom datetime = null,        
--    @dateto datetime = null,        
--    @numbertoreturn int = 500, -- cap the superset size by adding this to a Top statement        
--    @numberinpage int = 100, -- the number to return for a page        
--    @page int = 0,    -- page number        
--    @Updates bit = 0   -- 1=only consider updateddate, 0=consider createddate and updateddate        
AS        
-- -- select @datefrom = '2010-01-01 00:00:00', @dateto = '2012-12-31 23:59:59'        
BEGIN      
  
truncate table archives.dbo.dfpdump  
      
IF OBJECT_ID('tempdb..#MyConRecord') IS NOT NULL      
 DROP TABLE #MyConRecord      
          
declare       
-- @datefrom datetime = getdate() - 14,        
-- @datefrom datetime = '2010-03-07 19:58:00',      
@datefrom datetime = '2010-01-01 00:00:00',  
@dateto datetime = getdate() + 1,        
@numbertoreturn int = 5000000, -- cap the superset size by adding this to a Top statement        
@numberinpage int = 100, -- the number to return for a page        
@page int = 0,    -- page number        
@Updates bit = 0   --       
        
 SET FMTONLY OFF        
 SET NOCOUNT ON;        
 exec SprocTrackingUpdate 'adsrv_GetNewUpdatedContent_rev_v3'        
 if @numbertoreturn > 500 set @numbertoreturn = 500        
-- declare @SortOrder varchar(10) = 'DESC'        
        
 if @numbertoreturn is null set @numbertoreturn = 500        
 if @numberinpage is null set @numberinpage = 100        
 if @page is null set @page = 0        
 if @Updates is null set @Updates = 0        
        
if @datefrom is null or @dateto is null        
begin        
 if @datefrom is null and @dateto is null        
 begin        
  set @dateto = GETDATE()        
  set @datefrom = DATEADD(hh, -1, @dateto)        
 end        
 else        
 begin        
  if @dateto is null and @datefrom is not null        
  begin        
   set @dateto = dateadd(hh, 1, @datefrom)        
  end        
  else        
  begin        
   if @dateto is not null and @datefrom is null        
   set @datefrom = DATEADD(hh, -1, @dateto)        
  end        
 end        
end        
        
-- declare @updates bit = 0, @numbertoreturn int = 400, @numberinpage int = 100, @datefrom datetime = '2013-05-01 00:11:22', @dateto datetime = '2013-06-01 00:11:22', @page int = 1-- , @curURL varchar(200), @nextURL varchar(200)        
-- declare @updates bit = 0, @numbertoreturn int = 400, @numberinpage int = 100, @datefrom datetime = '2013-05-01 00:11:22', @dateto datetime = '2013-06-01 00:11:22', @page int = 0        
        
create table #MyConRecord(id int not null identity(1,1) primary key, contentid bigint)        
insert into #MyConRecord(contentid)        
select ContentID        
 FROM Content (nolock) c        
 where ContentImportStatus = 5        
 and exists (select 1 from Content_Asset ca join Asset a on ca.AssetID = a.AssetID where ca.ContentID = c.ContentID and ca.AssetTypeID = 1)        
 and exists (select 1 from Content_Asset ca join Asset a on ca.AssetID = a.AssetID where ca.ContentID = c.ContentID and ca.AssetTypeID = 2)        
 and exists (select 1 from Content_Asset ca join Asset a on ca.AssetID = a.AssetID where ca.ContentID = c.ContentID and ca.AssetTypeID = 3)        
 and  ((@updates = 0 and (CreatedDate between @datefrom and @dateto or UpdatedDate  between @datefrom and @dateto))        
   or        
   (@Updates = 1 and UpdatedDate  between @datefrom and @dateto))        
 and  PartnerID not in (select partnerid from vw_partnerDefaultSettings (nolock) where Entity = 'BlockContentFromAdServer' and Value = 'true')        
 order by UpdatedDate desc, ContentID desc        
        
      
-- declare @numberinpage int = 100, @page int = 0, @numbertoreturn int = 500, @datefrom datetime = '2013-05-01 00:00:00'        
declare @offset int = @page, @resultset int = @numbertoreturn, @sendset int = @numberinpage, @TotalSet int, @curURL varchar(200), @nextURL varchar(200)        
declare @serviceURL varchar(100) = (select Value from vw_DefaultSettings where Entity = 'AdServer' and Setting = 'serviceURL' )        
        
insert into archives.dbo.dfpdump(link,linkNext,itemsPerPage,startIndex,totalResults,content,mediatype,medium,duration,title,description,category,keywords,status,thumbnail,ThumbnailHeight,ThumbnailWidth,contentID,owner,monetizeable,pubdate,
videoWidth,videoHeight,OwnerName, UUID)        
  select      
  -- COUNT(*)       
 --   replace(@curURL, '{pageno}', CONVERT(varchar(10), @page)) link,         
   convert(varchar(20),@datefrom,20) + '/' +  convert(varchar(10),@offset) as link,        
   case when @TotalSet - (@offset * @sendset) < @sendset then null        
    else convert(varchar(20),@datefrom,20) + '/' + CONVERT(varchar(10), @offset+1)         
    end linkNext,         
   1000 itemsPerPage,        
   (@offset * @sendset) + 1 startIndex, @TotalSet totalResults,         
   dbo.GetPathToStreamAsset(cav.assetid) content, dbo.GetMimeTypeToProgressiveAsset(cav.AssetID) mediatype, 'video' medium, avid.duration, c.Name title, c.description,         
   nc.NDNCategory category, c.Keyword keywords, /*c.effectivedate pubdate,*/        
   case c.Active when 1 then 'active' else 'inactive' end status, dbo.GetPathToServeAsset(ca.AssetID) thumbnail,         
   a.Height ThumbnailHeight, a.Width ThumbnailWidth,        
   c.contentID, c.PartnerID owner, --, c.EffectiveDate, c.ExpirationDate, c.CreatedDate,         
   -- case when noads.contentid is null then 0 else 1 end as         
   convert(int, 1) as         
   monetizeable        
   ,c.effectivedate pubdate        
   ,avid.width videoWidth        
   ,avid.Height videoHeight        
   ,p.Name OwnerName
   ,c.UUID        
 --  ,avid.Width videoWidth, avid.Height videoHeight        
 -- declare @sendset int = 12, @offset int = 0 select *  -- select top 10 * from asset        
  from #MyConRecord ref  --#MyConRecord (nolock) ref        
  join Content (nolock) c        
  on  ref.contentid = c.ContentID        
  join Partner (nolock) p        
  on  c.PartnerID = p.PartnerID        
  join Content_Asset (nolock) ca        
  on  c.ContentID = ca.ContentID        
  and  3 = ca.AssetTypeID        
  join Asset (nolock) a        
  on  ca.assetid = a.AssetID        
  join Content_Asset (nolock) cav        
  on  c.ContentID = cav.ContentID        
  and  1 = cav.AssetTypeID        
  join Asset (nolock) avid        
  on  cav.AssetID = avid.AssetID        
  left join (select * from Playlist_Content_PartnerMap (nolock) where NDNCategoryID is not null) pcpm        
  on  c.PartnerID = pcpm.PartnerID        
  left join NDNCategory (nolock) nc        
  on  pcpm.NDNCategoryID = nc.NDNCategoryID        
  --left join archives.dbo.VideoIDsToFilterAds noads        
  --on  ref.contentid = noads.contentid        
  order by c.effectivedate    
      
  drop table #MyConRecord        
  
declare @cnt int     
declare @max int  
set @cnt = ( select COUNT(*) from archives.dbo.dfpdump )   
update d   
 set d.startindex = sub.si, totalResults = @cnt,   
 link = SUBSTRING(link,1,19) + '/' + CONVERT(varchar,b),  
 linknext = SUBSTRING(linknext,1,19) + '/' + CONVERT(varchar,b+1)  
from     
(    
select ((( RANK() OVER (ORDER BY dfpdumpid) - 1) / 1000 ) * 1000 ) + 1 as si,   
(( RANK() OVER (ORDER BY dfpdumpid) - 1) / 1000 ) as b,  
dfpdumpid from archives.dbo.dfpdump     
) sub, archives.dbo.dfpdump d where sub.dfpdumpid = d.dfpdumpid    
  
set @max = ( select MAX(startindex) from archives.dbo.dfpdump )  
update archives.dbo.dfpdump set linknext = null where startindex = @max      
      
end  
  
