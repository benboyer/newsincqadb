
CREATE Procedure [dbo].[RefTableInserts]
(
--	declare 
	@refnumber bigint,
	@inputstring varchar(max),
	@params varchar(200) = null
)
as
BEGIN
	set FMTONLY OFF
	set NOCOUNT ON
	
	exec SprocTrackingUpdate 'RefTableInserts'
	/*
	select	@refnumber = MAX(FeedContentID) from FeedContent -- where Active = 1 -- select MAX(feedcontentid) from FeedContent
	select @inputstring = '881,999999'
	select @params =  'FDMAItoDMAInclude'
	select	@refnumber = MAX(feedcontentid) from FeedContent
	select	@inputstring = '678,890,abc'
	select * from dbo.fn_Rows_fmCommaSeparated(@inputstring)
	select * from FeedContent where feedcontentid = @refnumber
	*/
	if @params = 'FDMAItoDMAInclude'
	begin
		if exists (select 1
				from	FeedContent
				where	feedcontentid = @refnumber
				and		contentid is not null
				and		importcomplete = 1)
		begin
				-- select 'hi' -- declare @inputstring varchar(200) = '805,999999', @refnumber bigint = 1745933
				insert into DMAInclude(ContentID, DMACode)
				select	a.ContentID, value 
				from	dbo.fn_Rows_fmCommaSeparated(@inputstring),
						FeedContent a
				where	a.FeedContentID = @refnumber
				and	not exists 
						(select 1
						from	DMAInclude r
						where	r.ContentID = a.ContentID
						and		r.DMACode in (select * from dbo.fn_Rows_fmCommaSeparated(@inputstring)))
		end 
	END
	if @params = 'CIDtoDMAInclude'
	begin
		-- select 'hi'
		insert into DMAInclude(ContentID, DMACode)
		select	@refnumber, value from dbo.fn_Rows_fmCommaSeparated(@inputstring)
		where	not exists 
				(select 1
				from	DMAInclude r
				where	r.ContentID = @refnumber
				and		r.DMACode in (select * from dbo.fn_Rows_fmCommaSeparated(@inputstring)))
	end
	set NOCOUNT OFF
END
