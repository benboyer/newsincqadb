TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	PlayerType	PlayerTypeID	5	smallint	5	2	0	10	0	None	None	5	None	None	1	NO	52
NewsincQA	dbo	PlayerType	Name	-9	nvarchar	50	100	None	None	0	None	None	-9	None	100	2	NO	39
NewsincQA	dbo	PlayerType	Description	-9	nvarchar	200	400	None	None	0	None	None	-9	None	400	3	NO	39
NewsincQA	dbo	PlayerType	Order	5	smallint	5	2	0	10	0	None	None	5	None	None	4	NO	52
NewsincQA	dbo	PlayerType	ConfigurationFileName	-9	nvarchar	100	200	None	None	1	None	None	-9	None	200	5	YES	39
NewsincQA	dbo	PlayerType	Height	4	int	10	4	0	10	1	None	None	4	None	None	6	YES	38
NewsincQA	dbo	PlayerType	Width	4	int	10	4	0	10	1	None	None	4	None	None	7	YES	38
NewsincQA	dbo	PlayerType	CreatedDate	11	smalldatetime	16	16	0	None	0	None	(getutcdate())	9	3	None	8	NO	58

index_name	index_description	index_keys
PK_PlayerType	clustered, unique, primary key located on PRIMARY	PlayerTypeID
