TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	ContactDetail	ContactDetailID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	ContactDetail	UserID_Conv	4	int	10	4	0	10	1	None	None	4	None	None	2	YES	38
NewsincQA	dbo	ContactDetail	ContactTypeID	5	smallint	5	2	0	10	0	None	None	5	None	None	3	NO	52
NewsincQA	dbo	ContactDetail	Value	-9	nvarchar	50	100	None	None	0	None	None	-9	None	100	4	NO	39
NewsincQA	dbo	ContactDetail	CreatedDate	11	smalldatetime	16	16	0	None	0	None	(getutcdate())	9	3	None	5	NO	58
NewsincQA	dbo	ContactDetail	CreatedUserID	4	int	10	4	0	10	1	None	None	4	None	None	6	YES	38

index_name	index_description	index_keys
PK_ContactDetail	clustered, unique, primary key located on PRIMARY	ContactDetailID
