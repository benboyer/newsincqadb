TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	PlaylistVideosLEGACY	PlaylistID	4	int	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	PlaylistVideosLEGACY	ContentID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	PlaylistVideosLEGACY	Sharing	4	int	10	4	0	10	1	None	None	4	None	None	3	YES	38
NewsincQA	dbo	PlaylistVideosLEGACY	ContentItemID	4	int	10	4	0	10	1	None	None	4	None	None	4	YES	38

index_name	index_description	index_keys
PK_PlaylistVideosLEGACY	clustered, unique, primary key located on PRIMARY	PlaylistID, ContentID
