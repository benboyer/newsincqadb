create procedure [dbo].[medgen_UpdateLastRunDTM]
	@NewDTM datetime
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'medgen_UpdateLastRunDTM'

	update ConfigLookup set Value = @NewDTM where [KEY] = 'LastMedGenTime'
	set nocount off
END
