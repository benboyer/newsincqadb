CREATE procedure [dbo].[cr_ListMediaSources]
	@PartnerID int = null
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'cr_ListMediaSources'
	-- declare @PartnerID int = 499
	select p.PartnerID, p.Name, p.TrackingGroup
	from OrganizationsLEGACY ol (nolock)
	join Partner p (nolock)
	on		ol.PartnerID = p.PartnerID
	where ol.IsMediaSource = 1
	and		dbo.AllowedDistributorID(p.PartnerID, @PartnerID) = 1
	union
	select partnerid, name, trackinggroup
	from Partner
	where	PartnerID = @PartnerID
	order by p.Name

	set nocount off
END
