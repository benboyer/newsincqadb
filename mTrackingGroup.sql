CREATE view mTrackingGroup
as 
SELECT     plm.partnerid, pl.Name, pl.TrackingGroup, pl.StatusID
FROM         dbo.Partner AS pl WITH (nolock) INNER JOIN
                          (SELECT     CASE WHEN trackinggroup = 10557 THEN 478 
											WHEN trackinggroup = 22790 THEN 156 
											WHEN TrackingGroup = 90450 then 831
											ELSE MAX(partnerid) END AS partnerid, TrackingGroup
                            FROM          dbo.Partner WITH (nolock)
                            GROUP BY TrackingGroup) AS plm ON pl.PartnerID = plm.partnerid