CREATE VIEW dbo.dme_OwnershipGroup_Partner
AS
SELECT     a.ZoneID AS OwnershipGroupID_ZoneID, p.PartnerID, p.Name, p.TrackingGroup, p.CreatedDate
FROM         (SELECT DISTINCT ZoneID
                       FROM          dbo.Partner WITH (nolock)) AS a INNER JOIN
                      dbo.Partner AS p WITH (nolock) ON a.ZoneID = p.ZoneID
