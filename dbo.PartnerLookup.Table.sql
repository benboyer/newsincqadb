USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PartnerLookup](
	[PartnerID] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Constant] [nvarchar](100) NULL,
	[Description] [nvarchar](1000) NULL,
	[DisplayConstant] [nvarchar](100) NULL,
 CONSTRAINT [PK_PartnerLookup] PRIMARY KEY CLUSTERED 
(
	[PartnerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
