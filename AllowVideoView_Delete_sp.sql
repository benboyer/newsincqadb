CREATE PROCEDURE [dbo].[AllowVideoView_Delete_sp]
	@ContentProviderOrgID int,
	@DistributorOrgID int
AS
BEGIN
	set fmtonly off
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'AllowVideoView_Delete_sp'

	Delete from AllowContent where ContentProviderPartnerID = @ContentProviderOrgID
	and DistributorPartnerID = @DistributorOrgID

	SET NOCOUNT OFF
END
