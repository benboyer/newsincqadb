CREATE VIEW [dbo].[States]
AS
SELECT     CONVERT(int, StateID) AS StateID, Name, Abbreviation AS Abbr
FROM         dbo.State