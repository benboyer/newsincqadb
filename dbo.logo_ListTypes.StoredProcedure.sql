USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[logo_ListTypes]
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'logo_ListTypes'
	select * from LogoType
	set nocount off
END
GO
