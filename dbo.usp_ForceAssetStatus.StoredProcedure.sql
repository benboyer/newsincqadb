USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_ForceAssetStatus]
	@assetid bigint,
	@Status int
as
BEGIN
	update Asset
	set ImportStatusID = @Status
	where AssetID = @assetid

	select AssetID, ImportStatusID, Active
	from Asset
	where AssetID = @assetid

END
GO
