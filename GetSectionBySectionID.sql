CREATE procedure [dbo].[GetSectionBySectionID]
	-- declare
	@SectionID int
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetSectionBySectionID'

	select SectionID, PartnerID, Name, OldID, [default], Active, CreatedDate, CreatedUserID, UpdatedDate, UpdatedUserID, SectionPartnerID, SectionSectionID, SectionSubSectionID, SectionPageTypeID
	from Section
	where SectionID = @SectionID

	set nocount off
END
