USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetSearchResult_VideoStaging]
(
	@UserID INT,
	@Count INT = 100,
	@OrderClause NVARCHAR(MAX) = '',
	@StatusType INT = 0,
	@SearchType NVARCHAR(20) = '',
	@SearchText NVARCHAR(MAX) ='',
	@IsAdmin BIT = 0,
	@WhereCondition NVARCHAR(MAX) = ''

)

AS
BEGIN

exec SprocTrackingUpdate 'GetSearchResult_VideoStaging'

DECLARE @queryString NVARCHAR(MAX)
DECLARE @WhereClause NVARCHAR(MAX)

SET @queryString=
	' SELECT TOP ' + CONVERT(NVARCHAR(20),@Count)+ ' ISNULL(vs.[VideoStagingID],0) as VideoStagingID
		,vs.Title
		,vs.UserID
		,vs.UserType
		,vs.ClipURL
		,vs.Description
		,vs.EventDate
		,vs.Height
		,vs.Width
		,vs.Duration
		,vs.CreatedOn
		,vs.ThumbnailURL
		,vs.PublishDate
		,vs.Keywords
		,vs.Guid
		,vs.ReadyForEncoding
		,vs.OriginalName
		,ISNULL(vs.EncodingStatus,0) AS EncodingStatus
		,vs.EncodingPercentCompleted
		,vs.EncodingStarted
		,vs.EncodingCompleted
		,vs.ExpiryDate
		,vs.Status
		,vs.Active
		,vs.IsEnabled
		,vs.IsDeleted
		,vs.IngestionSource
		,vs.FileSize
		,vs.StillFrameURL
		,vs.AudioTrackURL
		,vs.EncodedURL
		,org.[Name]
		,us.[OrganizationId]	FROM VideoStaging vs
		INNER JOIN NDNUsers us ON vs.UserId = us.UserId
		INNER JOIN Organizations org ON org.OrganizationID = us.OrganizationID	'


SELECT @WhereClause = 'Where EncodingStatus > 0 '

SELECT @WhereClause = @WhereClause + (CASE CONVERT(NVARCHAR(1),@StatusType)
							WHEN '0' THEN ' AND EncodingStatus in (1,2,3) '
							WHEN '1' THEN ' AND (EncodingStatus in (1,2,3) ) AND ((Title IS NULL OR RTRIM(LTRIM(Title)) = '''') OR (Description IS NULL OR RTRIM(LTRIM(Description)) = '''' )) '
							WHEN '2' THEN ' AND ((Title IS NULL OR RTRIM(LTRIM(Title)) = '''') OR (Description IS NULL OR RTRIM(LTRIM(Description)) = '''' )) AND EncodingStatus = 4 '
							WHEN '3' THEN ' AND EncodingStatus in (5) '
							END)

IF(@SearchType IS NOT NULL AND @SearchType != '' AND @SearchType != '7' AND @SearchType != 'None' AND @SearchText IS NOT NULL AND @SearchText != '')
SELECT @WhereClause = @WhereClause + (CASE CONVERT(NVARCHAR(20),@SearchType)
							WHEN 'Title' THEN ' AND Title like ''%'+ @SearchText + '%'''
							WHEN 'FileName' THEN ' AND OriginalName like ''%'+ @SearchText + '%'''
							WHEN 'DateAdded' THEN ' AND DateDiff(dd, CreatedOn, '''+ @SearchText + ''') =  0 '
							WHEN 'EventDate' THEN ' AND DateDiff(dd, EventDate, '''+ @SearchText + ''') =  0 '
							WHEN 'ExpirationDate' THEN ' AND DateDiff(dd, ExpiryDate, '''+ @SearchText + ''') =  0 '
							WHEN 'Keywords' THEN ' AND Keywords like ''%'+ @SearchText + '%'''
							END)

IF (@IsAdmin = 0)
    SELECT @WhereClause = @WhereClause + ' AND vs.UserId = ' + CONVERT(NVARCHAR(20),@UserID) ;

IF (@WhereCondition IS NOT NULL AND @WhereCondition != '')
    SELECT @WhereClause = @WhereClause + ' AND '+ @WhereCondition ;

SET @queryString = @queryString + @WhereClause

IF( @OrderClause IS NOT NULL AND @OrderClause != '')
	SET @queryString = @queryString + ' Order By ' + @OrderClause

EXEC ( @queryString );


END
GO
