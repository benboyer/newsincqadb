USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[mon_GetNullFeedContentGUIDs]
as
BEGIN
	select	p.name, a.PartnerID, max(fc.FeedContentID) maxFeedContentID, a.MaxNullFCID MaxFeedContentIDwNullGUID, a.PublishDate, a.Name, a.Description, a.Version, a.Retries, a.LastUploadDate, a.Uploads CurrentUploads, COUNT(*) TotalTimesUploaded
	from	(select Partnerid, PublishDate, Name, Description, Version, Retries, MAX(RecordAddedDTM) LastUploadDate, COUNT(*) Uploads, MAX(FeedContentID) MaxNullFCID
			from FeedContent (nolock)
			where GUID is null
			and	ImportComplete = 0
			and	RecordAddedDTM > dateadd(dd, -30, GETDATE())
			group by Partnerid, PublishDate, Name, Description, Version, Retries) a
	join	FeedContent fc
	on		a.PartnerID = fc.PartnerID
	and		a.Name = fc.Name
	and		a.Description = fc.Description
	and		a.Version = fc.Version
	and		a.Retries = fc.Retries
	join	Partner p
	on		a.PartnerID = p.PartnerID
	-- where fc.ImportComplete = 0
	group by p.name, a.PartnerID, a.MaxNullFCID, a.PublishDate, a.Name, a.Description, a.Version, a.Retries, a.LastUploadDate, a.Uploads
	order by Uploads Desc, a.LastUploadDate desc, p.Name  
END
GO
