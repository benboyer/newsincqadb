USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetWidgetPlaylistVideos]
	@widgetID int,
	@playlistID int,
	@trackingGroup int
AS
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetWidgetPlaylistVideos'

	Declare	@now datetime,
			@iwidgetID int,
			@iplaylistID int,
			@itrackinggroup int
	set		@now = GETDATE()
	select @iwidgetID = @widgetID, @iplaylistID = @playlistID, @itrackinggroup = @trackingGroup
	-- Revision 2011-05-26, for performance
	select distinct a.ContentID as VideoID, a.Name as Title, a.[Order]
	from	(select  c.partnerid, ol.iscontentprivate, p.playlistid, pc.ContentID, c.Name, pc.[Order]
			from	Playlist p (nolock)
			join	Playlist_Content pc (nolock)
			on		p.PlaylistID = pc.PlaylistID
			join	Content c (nolock)
			on		pc.ContentID = c.ContentID
			JOIN	VideosLEGACY vL (nolock)
			ON		pc.ContentID = vL.ContentID
			and		c.ContentID = vl.ContentID
			JOIN	OrganizationsLEGACY oL (nolock)
			ON		c.PartnerID = oL.PartnerID
			where	p.PlaylistID = @iplaylistID
			--and		c.active = 1
			--and		vl.IsDeleted = 0
			and		dbo.AllowedLauncherContentP(@iwidgetID, c.PartnerID) = 1) a
			left join partner pd (nolock)
			on	@itrackinggroup = pd.TrackingGroup
		WHERE dbo.AllowedContentID(a.contentid, @trackinggroup, 1) = 1
		-- and		dbo.AllowedLauncherContent(@iwidgetID, @itrackinggroup) = 1
		ORDER BY a.[Order], a.Name
		set nocount off
GO
