USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[mTrackingGroupPartnerDefaultHW] as

SELECT		plm.partnerid, pl.Name, CAST(pl.TrackingGroup AS NVARCHAR(255)) AS TrackingGroup, pl.StatusID, pl.DefaultEmbedWidth,
            pl.DefaultEmbedHeight, pl.DefaultEmbedRatio, lurl.LandingURL, lurl.Active AS LandingURLActive, lurl.LandingURLTypeID, lt.Name LandingUrlType
FROM			Partner AS pl WITH (nolock)
INNER JOIN		(SELECT     CASE WHEN trackinggroup = 10557 THEN 478 WHEN trackinggroup = 22790 THEN 156 WHEN TrackingGroup = 90450 THEN 831 ELSE MAX(partnerid) END AS partnerid, TrackingGroup
                FROM        Partner WITH (nolock)
                GROUP BY TrackingGroup) AS plm
ON				pl.PartnerID = plm.partnerid
LEFT OUTER JOIN	Partner_LandingURL AS plu
ON				pl.PartnerID = plu.PartnerID
LEFT OUTER JOIN	LandingURL AS lurl
ON				plu.LandingURLID = lurl.LandingURLID
LEFT JOIN		LandingURLType lt
on			lurl.LandingURLTypeID = lt.LandingURLTypeID
GO
