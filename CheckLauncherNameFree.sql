CREATE procedure [dbo].[CheckLauncherNameFree]
--declare
		@userid int,
		@LauncherName varchar(max)
as
BEGIN
	SET FMTONLY OFF
	SET NOCOUNT ON
	exec SprocTrackingUpdate 'CheckLauncherNameFree'

	declare @partnerid int,
			@launcheravailable bit

	set		@partnerid = (select top 1 partnerid from [User] (nolock) where UserID = @userid)

	if	(select COUNT(*) launchercount
				from Launcher (nolock)
				where PartnerID = @partnerid
				and	Name = @LauncherName) > 0
		set @launcheravailable = 0
	else
		set @launcheravailable = 1
	select @launcheravailable as NameFree

	SET NOCOUNT OFF
END
