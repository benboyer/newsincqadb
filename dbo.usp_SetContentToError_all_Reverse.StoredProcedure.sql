USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SetContentToError_all_Reverse]
	-- Add the parameters for the stored procedure here
	@contentid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    update Asset set ImportStatusID=3 where AssetID in (select AssetID from Asset where EncodingID = 'error' and AssetID in (select AssetID from Content_Asset where ContentID=@contentid))
	update Asset set ImportStatusID=5 where AssetID in (select AssetID from Asset where EncodingID is null and AssetID in (select AssetID from Content_Asset where ContentID=@contentid))
	update Content_Asset set ImportStatusID=7 where ContentID =@contentid 
	update Content set ContentImportStatus=7 where ContentID =@contentid
	update ContentVideo set ContentVideoimportstatusid=7 where ContentID =@contentid
	
	select 'success';
END
GO
