CREATE PROCEDURE [dbo].[usp_GetContentFeedContentDME]
	-- declare
	@feedtoken uniqueidentifier = null,
    @providerid int = 0,
    @datefrom datetime = null,
    @dateto datetime = null,
    @numbertoreturn int = 500,
    @Updates bit = 0,
    @MinContentID int = null,
    @SortOrder varchar(10) = 'DESC'

AS
BEGIN
	SET FMTONLY OFF
	SET NOCOUNT ON;
	--	set @numbertoreturn = 55
	--	set @SortOrder = 'DESC'
	-- set @mincontentid = 24747900
	-- set @updates = 0
	-- Update 1012-10-22 for performance - this is adopted from usp_getcontentfeedcontent (optimized version)
	exec SprocTrackingUpdate 'usp_GetContentFeedContentDME'
	if @providerid is null
		set @providerid = 0
	if @numbertoreturn = 0 or @numbertoreturn is null
		set @numbertoreturn = 500
	if @feedtoken is null
	    set @feedtoken = '00000000-0000-0000-0000-000000000000'

	if @numbertoreturn > 500 set @numbertoreturn = 500

	if @SortOrder not in ('ASC', 'DESC')
		set @SortOrder = 'DESC'

        DECLARE @accesspartnerid int, @landingurl varchar(120), @minUpdatedDate datetime
		--	select @sortorder

			select	@accesspartnerid = PartnerID,
					@landingurl = LandingURL
			from Partner where FeedGUID = @feedtoken
		if @accesspartnerid is null set @accesspartnerid = 0
		if @MinContentID is not null set @minUpdatedDate = (select CreatedDate from Content where ContentID = @MinContentID)

	--select @MinContentID, @minUpdatedDate
    SELECT TOP(@numbertoreturn) c.ContentID, c.ContentTypeID, c.PartnerID, c.ContentSourceID, c.Name, c.Description, c.Category, c.Keyword, c.EffectiveDate, c.ExpirationDate, c.CreatedDate, c.CreatedUserID, c.UpdatedDate, c.UpdatedUserID, c.Active,
			c.FeedID, partners.PartnerID, partners.ParentPartnerID, partners.Name, partners.ContactID, partners.Website, partners.StatusID, partners.CreatedDate, partners.CreatedDate, partners.CreatedUserID, Partners.UpdatedDate, partners.UpdateUserID, partners.TrackingGroup, partners.ShortName, /*@landingurl LandingURL,*/partners.LandingURL, partners.isContentPrivate, partners.ZoneID, partners.FeedGUID, partners.isFeedGUIDActive, c.IsFileArchived,
			c.ContentID, c.ContentID, 0 NumberOfReviews, aa.Duration, null ClipPopularity, 0 IsAdvertisement, null CandidateOfInterest, null NumberOfPlays, 0 SentToFreeWheel, null FileExtension, null IngestionStatusMessage, case when c.Active = 1 then 0 else 1 end IsDeleted,
			partners.PartnerID, c.ContentID ContentItemId, null Phone, pt.PartnerTypeID OrganizationType, partners.shortname Acronym, null CandidateOfficePlaceholder, 0 AllowUploadingVideo, partners.LogoUrl, 0 IsMediaSource, partners.IsContentPrivate,
			null AllowContentID, partners.partnerid ContentProviderPartnerID, pd.partnerid DistributorPartnerID, null AC_CreatedDate
			, aa.FilePath + '/' + aa.Filename as TranscriptPath, case when @Updates = 1 then 'Update' else 'No Update' end as Updated
	FROM	Content c (nolock)
    JOIN	Partner AS Partners (nolock)
    ON		c.PartnerID = Partners.PartnerID
    join	Content_Asset ca (nolock)
    on		c.ContentID = ca.contentid
    and		1 = ca.TargetPlatformID
    and		1 = ca.AssetTypeID -- (1 = ca.AssetTypeID or 17 = ca.assettypeid)
    join	Asset a (nolock)
    on		ca.assetid = a.AssetID
    left join Asset aa (nolock)
    on		ca.AssetID = aa.AssetID
    and		ca.AssetTypeID = 17
    left join (select max(partnertypeid) PartnerTypeID, partnerid from Partner_PartnerType (nolock) group by partnerid) ppt
    on		Partners.partnerid = ppt.PartnerID
    left join PartnerType pt (nolock)
    on		ppt.PartnerTypeID = pt.PartnerTypeID
	left	join mTrackingGroup pd -- select top 1 * from AllowContent
	on		@accesspartnerid = pd.TrackingGroup
	WHERE	c.ContentImportStatus = 5 -- c.Active = 1 -- select * from vw_partnerDefaultSettings
	-- and		c.PartnerID not in (select partnerid from vw_partnerDefaultSettings where Entity = 'BlockContentFromDME' and Value = 'true')
    AND		(@providerid = 0 OR c.PartnerID = @providerid)
	and		(
				(@datefrom IS NULL OR case when @updates = 1 then c.UpdatedDate else c.CreatedDate end >= @datefrom)
				AND
				(@dateto IS NULL OR case when @updates = 1 then c.updateddate else c.CreatedDate end <= @dateto)
			)
	and		(
			@MinContentID is null
			or
				(--@MinContentID is not null
				--	and
					(c.ContentID > @MinContentID or c.UpdatedDate > @minUpdatedDate))
			)
    ORDER BY 
		case when @MinContentID is not null and @sortorder = 'DESC' then c.ContentID end desc,
		case when @MinContentID is not null and @sortorder = 'ASC' then c.Contentid end asc,
		case when @updates = 1 and @sortorder = 'ASC' then isnull(c.updatedDate, c.CreatedDate) end asc,
		case when @updates = 1 and @sortorder = 'DESC' then isnull(c.updatedDate, c.CreatedDate) end desc,
		--case when @updates = 1 and @sortorder = 'DESC' then isnull(c.ContentID, c.ContentID) end desc,
		case when @datefrom is null and @dateto is null and @updates = 0 and @SortOrder = 'ASC' then c.ContentID end asc,		 
		case when @datefrom is null and @dateto is null and @updates = 0 and @SortOrder = 'DESC' then c.ContentID end desc,
		case when @datefrom is not null or @dateto is not null and @sortorder = 'ASC' and @updates = 0  then c.CreatedDate end asc,
		case when @datefrom is not null or @dateto is not null and @sortorder = 'DESC' and @updates = 0  then c.CreatedDate end desc,
    c.ContentID
END


