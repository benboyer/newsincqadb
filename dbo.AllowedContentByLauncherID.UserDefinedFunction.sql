USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      
-- THIS LOGIC NEEDS TO BE UPDATED IN FOUR PLACES      
-- AllowedContentByPartnerID      
-- AllowedContentByLauncherID      
-- AllowedContentByPlaylistID    
-- AllowedContentID      
-- updated 9/11/2013 (and -> or in dmaexclude check)
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      
CREATE FUNCTION [dbo].[AllowedContentByLauncherID](@launcherID int, @partnerID int)          
RETURNS Table          
as           
 Return          
 (          
  select /*pc.PlaylistID,*/ pc.playlistID, c.ContentID as ContentID--, p.PartnerID                
  from        
  dbo.Launcher_Playlist lp        
  join dbo.Playlist_Content pc (nolock)          
  on lp.LauncherID = @launcherID and lp.PlaylistID = pc.PlaylistID        
  inner join dbo.Content c (nolock)        
  on pc.ContentID = c.ContentID            
  join dbo.Partner p (nolock)                   
  on c.partnerid = p.partnerid and c.active = 1                  
  join dbo.Partner_TargetPlatformProfile ptp (nolock)            
  on  c.PartnerID = ptp.PartnerID and isDistributionEnabled = 1 and ptp.TargetPlatformID = 1                
  -- join playlist_content pc                   
  -- on pc.playlistid = @iplaylistid and pc.ContentID = c.ContentID                   
                 
  LEFT JOIN dbo.AllowContent ac (nolock)            
  ON c.PartnerID = ac.ContentProviderPartnerID and @partnerID = ac.DistributorPartnerID                
                  
  /*                
  LEFT JOIN AllowContent ac                      
  ON  c.PartnerID = ac.ContentProviderPartnerID                    
  left join mtrackinggroup pd                     
  on pd.partnerid = ac.DistributorPartnerID                
  */                 
                
  left join dbo.DMAExclude dmaEx (nolock)  -- select * from DMAExclude -- select top 100 * from mtrackinggroup                    
  on  c.ContentID = dmaEx.contentid                    
  left join dbo.Partner_DMA pdma (nolock)                     
  on  dmaex.DMACode = pdma.DMACode                    
  and  @partnerID = pdma.PartnerID                    
  left join dbo.DMAInclude dmaIn (nolock)                     
  on  c.ContentID = dmain.ContentID                    
  left join dbo.Partner_DMA pdmai (nolock)                     
  on  dmain.dmacode = pdmai.dmacode                    
  and  @partnerID = pdmai.PartnerID                    
  where dbo.AllowedLauncherContentP(@launcherID, c.partnerid) = 1 and
  ( c.PartnerID = @partnerID  
  or  (c.OnlyOwnerView <> 1              
    and  (p.isContentPrivate = 0              
   or  ((p.isContentPrivate = 1 and ac.AllowContentID is not null              
     and (pdma.PartnerID is null              
   /*            
   and  (dmaEx.StartDTM IS NULL AND dmaEx.EndDTM IS NULL)              
     OR (dmaEx.EndDTM IS NULL AND dmaEx.StartDTM > GETDATE())              
     OR (dmaEx.StartDTM IS NULL AND dmaEx.EndDTM < GETDATE())              
     OR (GETDATE() not between dmaEx.StartDTM AND dmaEx.EndDTM)))              
   */            
--   and  (dmaEx.StartDTM IS NULL AND dmaEx.EndDTM IS NULL)                        
   or  (dmaEx.StartDTM IS NULL AND dmaEx.EndDTM IS NULL)                        
     OR (dmaEx.StartDTM > GETDATE())                        
     OR (dmaEx.EndDTM < GETDATE())))                           
   or  (p.isContentPrivate = 1 -- and ac.AllowContentID is null              
     and (pdmai.PartnerID is not null              
   /*            
   and  (dmaIn.StartDTM IS NULL AND dmaIn.EndDTM IS NULL)              
     OR (dmaIn.EndDTM IS NULL AND dmaIn.StartDTM <= GETDATE())              
     OR (dmaIn.StartDTM IS NULL AND dmaIn.EndDTM >= GETDATE())              
     OR (GETDATE() between dmaIn.StartDTM AND dmaIn.EndDTM))))))              
     )            
   */            
   and  (dmaIn.StartDTM IS NULL AND dmaIn.EndDTM IS NULL)                        
     OR (GETDATE() between ISNULL(dmaIn.StartDTM, GETDATE()) and ISNULL(dmaIn.EndDTM, GETDATE())))))))                
 ) )
GO
