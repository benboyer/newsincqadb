USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LauncherTemplate](
	[LauncherTemplateID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[LauncherTypesID] [int] NOT NULL,
	[Name] [varchar](60) NULL,
	[Description] [varchar](200) NULL,
	[Height] [int] NULL,
	[Width] [int] NULL,
	[ImageLocation] [varchar](120) NULL,
	[SortOrder] [int] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_LauncherTemplate] PRIMARY KEY CLUSTERED 
(
	[LauncherTemplateID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_LauncherTemplate_sort] ON [dbo].[LauncherTemplate] 
(
	[SortOrder] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LauncherTemplate]  WITH CHECK ADD  CONSTRAINT [FK_LauncherTemplate_LauncherTypes] FOREIGN KEY([LauncherTypesID])
REFERENCES [dbo].[LauncherTypes] ([LauncherTypesID])
GO
ALTER TABLE [dbo].[LauncherTemplate] CHECK CONSTRAINT [FK_LauncherTemplate_LauncherTypes]
GO
ALTER TABLE [dbo].[LauncherTemplate] ADD  CONSTRAINT [DF_LauncherTemplate_Active]  DEFAULT ((1)) FOR [Active]
GO
