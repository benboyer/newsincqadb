CREATE PROCEDURE [dbo].[GetWidgetPlaylist]
-- declare
	@widgetID int,
	@trackingGroup int,
	@DeviceType int = 1
AS
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetWidgetPlaylist'

	-- declare @widgetID int = 3233, @trackinggroup int = 90121, @devicetype int = 2 -- exec GetWidgetPlaylist @widgetid, @trackinggroup, @devicetype
	declare @now datetime,
			@iwidgetID int,
			@itrackingGroup int,
			@iDeviceType int,
			@dpartnerid int
	set		@now = GETDATE()

	select @iwidgetID = @widgetID, @itrackingGroup = @trackingGroup, @iDeviceType = @DeviceType
	set		@dpartnerid = (select partnerid from mTrackingGroup (nolock) where TrackingGroup = @itrackingGroup)
	-- Revision 2011-07-19, for mobile platforms
	SELECT a.PlaylistID, a.Name as Title, COUNT(DISTINCT a.ContentID) as CountVideo
	from
			(select  c.partnerid, ol.iscontentprivate, p.playlistid, pc.ContentID, p.Name, lp.[Order] -- , ca.AssetID, ca.AssetTypeID, ca.TargetDeviceClassID
			from	(select * from Launcher_Playlist (nolock) where Launcherid = @iwidgetID) lp
			join	Playlist p (nolock)
			ON		lp.PlaylistID = p.PlaylistID
			join	Playlist_Content pc (nolock)
			on		p.PlaylistID = pc.PlaylistID
			join	Content c (nolock)
			on		pc.ContentID = c.ContentID
			and		1 = c.Active
			join	Content_Asset ca (nolock)
			on		c.ContentID = ca.ContentID
			and		ca.TargetPlatformID = @iDeviceType
			and		1 = ca.Active
			--JOIN	VideosLEGACY vL (nolock)
			--ON		pc.ContentID = vL.ContentID
			--and		c.ContentID = vl.ContentID
			JOIN	Partner oL (nolock)
			ON		c.PartnerID = oL.PartnerID
			where	--c.active = 1
			dbo.AllowedLauncherContentP(@iwidgetID, c.PartnerID) = 1) a
			--join	mTrackingGroup (nolock) mt
			--on		a.PartnerID = mt.partnerid
			--join	mTrackingGroup (nolock) mtd
			--on		@itrackingGroup = mtd.TrackingGroup
		WHERE	dbo.AllowedContentID(a.contentid, @itrackinggroup, 1) = 1
		--and		dbo.AllowedLauncherContent(@iwidgetID, @itrackingGroup) = 1
		GROUP BY a.PlaylistID, a.Name, a.[Order]
		ORDER BY a.[Order], a.Name
	set nocount off
