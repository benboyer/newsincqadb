CREATE VIEW dbo.AllowVideoView
AS
SELECT     ContentProviderPartnerID AS ContentProviderOrgID, DistributorPartnerID AS DistributorOrgID
FROM         dbo.AllowContent