CREATE PROCEDURE [dbo].[WidgetExcludedOrgs_DELETE_sp]
	@WidgetId int,
	@OrganizationId int
AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'WidgetExcludedOrgs_DELETE_sp'

	DELETE Launcher_ContentExclusion Where LauncherID =@WidgetId and PartnerID=@OrganizationId

END
