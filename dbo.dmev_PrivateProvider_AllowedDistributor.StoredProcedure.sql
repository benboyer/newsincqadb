USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[dmev_PrivateProvider_AllowedDistributor]
	@ProviderPartnerID int = null,
	@DistributorPartnerID int = null
as
--select *
--from dme_PrivateProvider_AllowedDistributor (nolock)ppad
	--where	ppad.PrivateProviderPartnerID = ISNULL(@ProviderPartnerID, ppad.PrivateProviderPartnerID)
	--and		ppad.AllowedDistributorPartnerID = ISNULL(@DistributorPartnerID, ppad.AllowedDistributorPartnerID)

--where	ppad.PrivateProviderPartnerID = case when @ProviderPartnerID is null then ppad.PrivateProviderPartnerID else @ProviderPartnerID end
--and		ppad.AllowedDistributorPartnerID = case when @DistributorPartnerID is null then ppad.AllowedDistributorPartnerID else @DistributorPartnerID end

if @ProviderPartnerID is null and @DistributorPartnerID is not null
begin
	select *
	from dme_PrivateProvider_AllowedDistributor (nolock)ppad
	where	ppad.AllowedDistributorPartnerID = @DistributorPartnerID
end
if @ProviderPartnerID is not null and @DistributorPartnerID is null
begin
	select *
	from dme_PrivateProvider_AllowedDistributor (nolock)ppad
	where	ppad.PrivateProviderPartnerID = @ProviderPartnerID
end
if @ProviderPartnerID is not null and @DistributorPartnerID is not null
begin
	select *
	from dme_PrivateProvider_AllowedDistributor (nolock)ppad
	where	ppad.PrivateProviderPartnerID = @ProviderPartnerID
	and		ppad.AllowedDistributorPartnerID = @DistributorPartnerID
end
GO
