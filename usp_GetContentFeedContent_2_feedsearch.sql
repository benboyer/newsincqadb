CREATE PROCEDURE [dbo].[usp_GetContentFeedContent_2_feedsearch]
	-- declare
	@feedtoken uniqueidentifier = null, --'00000000-0000-0000-0000-000000000000',
    @providerid varchar(200) = '',
    @datefrom datetime = null,
    @dateto datetime = null,  --'2011-09-30 23:59:59',
    @numbertoreturn int = 100,
    @updates bit = 0,
    @SearchText varchar(100) = null, --'michelle obama',
    @SearchType varchar(16) = null, -- 'title', -- '0'=all, otherwise 'title', 'keywords', 'description' may be used
    @SortOrder varchar(10) = null,
    @videoid bigint = null

AS
BEGIN
	set fmtonly off;
    SET NOCOUNT ON;
	exec SprocTrackingUpdate 'usp_GetContentFeedContent_2_feedsearch'

    declare @MatchType int = 1, -- 1= exact match, 0= near (all words), 2= gimme something (any words)
	@ReturnSet int = 1

	-- set @feedtoken = '0C033971-7586-4BD3-8BB3-6CE8990AF08F' -- select * from partner where feedguid = '0C033971-7586-4BD3-8BB3-6CE8990AF08F'
	-- set @numbertoreturn = 14
	-- set @searchtext = 'obama'
	--	set @searchtype = 'all'

	set @searchtype = replace(@SearchType, '0', 'all')

	if @numbertoreturn = 0 or @numbertoreturn is null
		set @numbertoreturn = 100
	if @numbertoreturn > 500
		set @numbertoreturn = 500
	if @feedtoken is null
	    set @feedtoken = '00000000-0000-0000-0000-000000000000'
    DECLARE @accesspartnerid int, @landingurl varchar(120), @itrackinggroup int, @now datetime
		select	@accesspartnerid = PartnerID,
				@landingurl = LandingURL,
				@itrackinggroup = trackinggroup
		from Partner where FeedGUID = @feedtoken
		select @now = GETDATE()
	if @accesspartnerid is null
		set @accesspartnerid = 0

	declare @userid int = (select contactid from Partner (nolock) where PartnerID = @accesspartnerid)

	create table #ftreturn (contentid bigint, relevance int)

	insert into #ftreturn(contentid, relevance)
	exec GetSearchResults
			@SearchText, --@SearchString NVARCHAR(4000),
			null, -- @WidgetId INT = NULL,
			@UserId,
			null, -- @Providerid, -- @OrgId,
			@numbertoreturn, -- @Count INT = 100,
			null, -- @sortorder, -- @OrderClause NVARCHAR(MAX) = null,
			null, -- @StatusType INT = 0,
			null, -- @IsAdmin BIT = 0,
			null, -- @WhereCondition NVARCHAR(MAX) = null
			null, --, @StartDTM datetime = null
			null, -- , @EndDTM datetime = null,
			null, --@VideoID, -- @MinVideoID BIGINT = null, -- max value of the current external record set: used for pagination
			null, -- @VideoID, --@MaxVideoID BIGINT = null, -- min value of the current external record set: used for pagination
			@MatchType, -- int = 2, -- 1= exact match, 0= near (all words), 2= gimme something (any words)
			@searchType, -- @SearchType varchar(16) = null, -- 'title', -- '0'=all, otherwise 'title', 'keywords', 'description' may be used
			@updates,
			2 -- @ReturnSet -- int = 1

	    SELECT	 TOP(@numbertoreturn)
			c.ContentID, c.ContentTypeID, c.PartnerID, c.ContentSourceID, c.Name, c.Description, c.Category, c.Keyword, c.EffectiveDate, c.ExpirationDate, c.CreatedDate, c.CreatedUserID, c.UpdatedDate, c.UpdatedUserID, c.Active,
			c.FeedID, partners.PartnerID, partners.ParentPartnerID, partners.Name, partners.ContactID, partners.Website, partners.StatusID, partners.CreatedDate, partners.CreatedDate, partners.CreatedUserID, Partners.UpdatedDate, partners.UpdateUserID, partners.TrackingGroup, partners.ShortName, @landingurl LandingURL, partners.isContentPrivate, partners.ZoneID, partners.FeedGUID, partners.isFeedGUIDActive, c.IsFileArchived,
			c.ContentID, c.ContentID, 0 NumberOfReviews, a.Duration, 0 as ClipPopularity, 0 as IsAdvertisement, null as CandidateOfInterest, 0 as NumberOfPlays, 1 as SentToFreeWheel, null as FileExtension, null as IngestionStatusMessage, a.IsDeleted,
			c.PartnerID, null as ContentItemId, null as Phone, pt.PartnerTypeID as OrganizationType, Partners.ShortName, null as CandidateOfficePlaceholder, case when pt.PartnerTypeID IN (2,3) then 1 else 0 end as AllowUploadingVideo, Partners.LogoUrl, case when pt.PartnerTypeID IN (2,3) then 1 else 0 end as IsMediaSource, partners.isContentPrivate,
			0  /*ac.AllowContentID */ as AllowContentID,
			c.partnerid as  ContentProviderPartnerID,
			partners.Partnerid as DistributorPartnerID, c.CreatedDate
			, aa.FilePath + '/' + aa.Filename as TranscriptPath
			,ftr.relevance
		FROM	Content c
		join (select max(relevance) relevance, contentid from #ftreturn (nolock) group by contentid) ftr
		on		c.ContentID = ftr.contentid
		join	Content_Asset ca (nolock)
		on		c.ContentID = ca.contentid
		and		1 = ca.TargetPlatformID
		and		1 = ca.AssetTypeID --or 17 = ca.assettypeid)
		join	Asset a (nolock)
		on		ca.assetid = a.AssetID
		left join Content_Asset ccaa
		on		c.ContentID = ccaa.ContentID
		and		17 = ccaa.AssetTypeID
		left join Asset aa (nolock)
		on		ccaa.AssetID = aa.AssetID
		JOIN	Partner AS Partners (nolock)
		ON		c.PartnerID = Partners.PartnerID
		left join (select max(partnertypeid) PartnerTypeID, partnerid from Partner_PartnerType (nolock) group by partnerid) ppt
		on		Partners.partnerid = ppt.PartnerID
		left join PartnerType pt (nolock)
		on		ppt.PartnerTypeID = pt.PartnerTypeID
		WHERE  ((isnull(@videoid, 0) <> 0 and c.ContentID = @videoid)
				or
				(isnull(@videoid, 0) = 0 and
					(@SearchType is null or ftr.contentid is not null)  and
			(@datefrom IS NULL OR case when @updates = 1 then c.UpdatedDate else c.EffectiveDate end >= @datefrom)
			AND (@dateto IS NULL OR case when @updates = 1 then c.updateddate else c.EffectiveDate end <= @dateto)
			AND (@providerid = '' OR c.PartnerID in (select Number from dbo.fn_numbers_fmCSnumbersList(@providerid)))))
		ORDER BY case
					when @SortOrder like 'PostedDate%' then c.CreatedDate
					when @SortOrder like 'EventDate%' then c.EffectiveDate
					when @SortOrder like 'UpdatedDate%' then c.UpdatedDate
					else 1
				end		DESC ,
				isnull(ftr.relevance, 0) desc ,
				isnull(c.updateddate, c.CreatedDate) DESC, c.contentid desc

	drop table #ftreturn
END
