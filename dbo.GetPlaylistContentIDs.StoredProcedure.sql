USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPlaylistContentIDs]
--  declare
	@widgetID int = null,
	@playlistID int = null,
	@trackingGroup int = null,
	@contentid bigint = null,
	@devicetype int = 1
AS
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetPLaylistContentIDs'

	if @devicetype = 3
	begin
		exec ps_PlaylistContentIDs @widgetID, @playlistID, @trackingGroup, @contentid, @devicetype
		return
	end

	Declare	@now datetime,
			@iwidgetID int,
			@iplaylistID int,
			@itrackinggroup int,
			@idevicetype int
	set		@now = GETDATE()
	select @iwidgetID = isnull(@widgetID, 1), @iplaylistID = isnull(@playlistID, 507), @itrackinggroup = @trackingGroup, @idevicetype = isnull(@devicetype, 1)

	if	@PlaylistID = 9999999
	begin
		exec GetSingleContent @contentid, @iTrackingGroup, @deviceType
		return
	end
	-- declare @iwidgetid int = 6516, @iplaylistid int = 10920, @itrackinggroup int = 99999, @idevicetype int = 1, @now datetime = getdate()

	-- declare @iwidgetid int = 3233, @iplaylistid int = 14296, @itrackinggroup int = 12699, @idevicetype int = 1, @now datetime = getdate()
	select distinct a.PlaylistID, a.ContentID, a.Name, (a.[Order] + 1) as ContentOrder
	from	(-- declare @iwidgetid int = 3233, @iplaylistid int = 10203, @itrackinggroup int = 10557, @idevicetype int = 1, @now datetime = getdate()
			select	pt.partnerid, pt.TrackingGroup, pt.isContentPrivate, p.playlistid, pc.ContentID, c.Name,
				c.Description, c.EffectiveDate PubDate, pc.[Order], aa.FilePath, aa.Filename, aa.Duration, c.Keyword,
				VG.VideoGroupName, at.Name AssetTypeName, mt.FileExtension, pt.logourl, pt.Name ProducerName, pt.ShortName ProducerNameAlt
		-- declare @iwidgetid int = 3233, @iplaylistid int = 14296, @itrackinggroup int = 45981, @idevicetype int = 1, @now datetime = getdate() select top 1000 *
		from	(select * from Playlist (nolock) where PlaylistID = @iplaylistID) p
		join	Playlist_Content pc (nolock)
		on		p.PlaylistID = pc.PlaylistID
		join	Content c (nolock)
		on		pc.ContentID = c.ContentID
		join	Content_Asset ca
		on		c.ContentID = ca.ContentID
		and		@idevicetype = ca.TargetPlatformID
		join	Asset aa
		on		ca.AssetID = aa.AssetID
		join	AssetType at
		on		ca.AssetTypeID = at.AssetTypeID
		join	MimeType mt
		on		at.MimeTypeID = mt.MimeTypeID
		--and		'video' = mt.MediaType
		and		aa.MimeTypeID = mt.MimeTypeID
		join	Partner_TargetPlatformProfile ptp
		on		c.PartnerID = ptp.PartnerID
		and		@idevicetype = ptp.TargetPlatformID
		JOIN	partner pt (nolock)
		ON		c.PartnerID = pt.PartnerID
		left join	VideoGroups VG
		on		p.PlaylistID = vg.VideoGroupId
		where	p.PlaylistID = @iplaylistID
		and		(@widgetID is null or dbo.AllowedLauncherContentP(@widgetID, c.ContentID) = 1)
		) a
	join	DistributionFilePath dfp
	on		a.FileExtension = dfp.MimeFileExtension
		left join mTrackingGroup pd (nolock)
		on	@itrackinggroup = pd.TrackingGroup
	where dbo.AllowedContentID(a.contentid, @itrackinggroup, @idevicetype) = 1
	ORDER BY a.[Order]+1

set nocount off
GO
