CREATE procedure [dbo].[GetLaunchersForUserID]
	--declare
		@userid int
as
BEGIN
	set fmtonly OFF
	set nocount ON
	exec SprocTrackingUpdate 'GetLaunchersForUserID'

	select --select top 100
			l.LauncherID WidgetID, convert(int, 0) ContentItemID, convert(int, 0) DCId, convert(varchar(32), null) VanityName, l.Name, isnull(lltt.LauncherTypesID, 0) WidgetType, convert(varchar(100), null) WidgetIdentificationName,
			isnull(l.isDefault, 0) isDefault, convert(int, 0) DefaultTab, convert(bit, case l.Active when 0 then 1 else 0 end) as isDeleted, l.UpdatedDate LastUpdated, l.GenerateNow, convert(varbinary(max), null) TemplateZip,
			isnull(pt.PlayerTypesID, 1) as Layout, isnull(pt.PlayerTypesID, 0) as PlayerID, lt.Name/* pt.name */PlayerDescription, convert(varchar(100), null) as Location,
			isnull(l.Height, 0) Height, isnull(l.Width, 0) Width,  isnull(s.SectionID, 0) SiteSectionID, convert(smalldatetime, l.CreatedDate) EnterDTM, l.UpdatedDate LastChgDTM, l.CreatedUserID UserID,
			convert(bit, case when lt.LauncherTemplateID is null then 1 else 0 end) as Deprecated, l.LaunchInLandingPage LandingPageAllowed
	-- select top 10 *
	from Launcher l
	left join LauncherTemplate lt
	on	l.LauncherTemplateID = lt.LauncherTemplateID
	left join LauncherTypes lltt
	on	lt.LauncherTypesID = lltt.LauncherTypesID
	left join PlayerTypes pt
	on	l.PlayerTypeID = pt.PlayerTypesID
	left join Section s
	on	l.SectionID = s.SectionID
	where l.CreatedUserID = @userid

	and	lt.LauncherTemplateID is not null

	set nocount OFF
END
