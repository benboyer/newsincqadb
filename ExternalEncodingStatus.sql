TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	ExternalEncodingStatus	VideoID	4	int	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	ExternalEncodingStatus	ThumbnailMediaId	4	int	10	4	0	10	1	None	None	4	None	None	2	YES	38
NewsincQA	dbo	ExternalEncodingStatus	ThumbnailFinished	-7	bit	1	1	None	None	1	None	None	-7	None	None	3	YES	50
NewsincQA	dbo	ExternalEncodingStatus	StillFrameMediaId	4	int	10	4	0	10	1	None	None	4	None	None	4	YES	38
NewsincQA	dbo	ExternalEncodingStatus	StillFrameFinished	-7	bit	1	1	None	None	1	None	None	-7	None	None	5	YES	50
NewsincQA	dbo	ExternalEncodingStatus	VideoMediaId	4	int	10	4	0	10	1	None	None	4	None	None	6	YES	38
NewsincQA	dbo	ExternalEncodingStatus	VideoFinished	-7	bit	1	1	None	None	1	None	None	-7	None	None	7	YES	50

