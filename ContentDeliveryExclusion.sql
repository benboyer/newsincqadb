TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	ContentDeliveryExclusion	ContentDeliveryExclusionID	-5	bigint identity	19	8	0	10	0	None	None	-5	None	None	1	NO	63
NewsincQA	dbo	ContentDeliveryExclusion	SourcePartnerID	4	int	10	4	0	10	1	None	None	4	None	None	2	YES	38
NewsincQA	dbo	ContentDeliveryExclusion	SourceLauncherID	4	int	10	4	0	10	1	None	None	4	None	None	3	YES	38
NewsincQA	dbo	ContentDeliveryExclusion	SourceContentID	-5	bigint	19	8	0	10	1	None	None	-5	None	None	4	YES	108
NewsincQA	dbo	ContentDeliveryExclusion	ExcludePartnerID	4	int	10	4	0	10	1	None	None	4	None	None	5	YES	38
NewsincQA	dbo	ContentDeliveryExclusion	ExcludeLauncherID	4	int	10	4	0	10	1	None	None	4	None	None	6	YES	38
NewsincQA	dbo	ContentDeliveryExclusion	ExcludeDMACode	4	int	10	4	0	10	1	None	None	4	None	None	7	YES	38
NewsincQA	dbo	ContentDeliveryExclusion	StartDTM	11	datetime	23	16	3	None	1	None	None	9	3	None	8	YES	111
NewsincQA	dbo	ContentDeliveryExclusion	EndDTM	11	datetime	23	16	3	None	1	None	None	9	3	None	9	YES	111

index_name	index_description	index_keys
IX_ContentDeliveryExclusion_DTM	nonclustered located on PRIMARY	StartDTM, EndDTM
IX_ContentDeliveryExclusion_exDMA	nonclustered located on PRIMARY	ExcludeDMACode
IX_ContentDeliveryExclusion_ExPID	nonclustered located on PRIMARY	ExcludePartnerID
IX_ContentDeliveryExclusion_exWid	nonclustered located on PRIMARY	ExcludeLauncherID
IX_ContentDeliveryExclusion_SrcPID	nonclustered located on PRIMARY	SourcePartnerID
IX_ContentDeliveryExclusion_srcWID	nonclustered located on PRIMARY	SourceLauncherID
IX_ContentDeliveryExclusion_VID	nonclustered located on PRIMARY	SourceContentID
PK_ContentDeliveryExclusion	clustered, unique, primary key located on PRIMARY	ContentDeliveryExclusionID
