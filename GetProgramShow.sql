
CREATE PROCEDURE [dbo].[GetProgramShow]
	@contentID int,
	@iDeviceType int = 1
AS
/*
This is what uses the new logo table structure, but should be corrected to only pull the producer logo (not 43 or 169)
begin
	-- if	@iDeviceType = 1
	exec SprocTrackingUpdate 'GetProgramShow'
	-- declare @contentid int = 23454277, @iDeviceType int = 1
		SELECT TOP 1
			c.ContentID, c.Name, c.[Description], c.PartnerID, c.Keyword,
			cv.ThumbnailFileName, cv.[FileName], cv.Duration,
			part.Name AS PartnerName,  isnull(palo43.logourl, ' http://assets.newsinc.com/newsinconebyone.png') LogoUrl, isnull(palo169.logourl, ' http://assets.newsinc.com/newsinconebyone.png') LogoURL169
		FROM Content c
		INNER JOIN ContentVideo cv ON cv.ContentID = c.ContentID
		INNER JOIN [Partner] part ON part.PartnerID = c.PartnerID
		--INNER JOIN OrganizationsLEGACY oL ON oL.PartnerID = part.PartnerID
			left join	(select pl.partnerid, l.LogoURL
						from	partner_logo pl (nolock)
						join	Logo l (nolock)
						on		pl.logoid = l.LogoID
						join	LogoType lt (nolock)
						on		l.logotypeid = lt.LogoTypeID
						and		1 = l.playertypesid) palo43
			on			c.PartnerID = palo43.partnerid
			left join	(select pl.partnerid, l.LogoURL
						from	partner_logo pl (nolock)
						join	Logo l (nolock)
						on		pl.logoid = l.LogoID
						join	LogoType lt (nolock)
						on		l.logotypeid = lt.LogoTypeID
						and		2 = l.playertypesid) palo169
			on			c.PartnerID = palo169.partnerid


		WHERE c.ContentID = @contentID
end
*/

/*
This is what we were going to use before logos raised their ugly heads
	-- declare @contentid int = 23454277, @iDeviceType int = 1
	SELECT TOP 1
		c.ContentID, c.Name, c.[Description], c.PartnerID, c.Keyword,
		thumbnailFileName, vid.[Filename], c.Duration,
		part.Name AS PartnerName, oL.LogoUrl
	from	(select ca.ContentID, a.*
			from	Content_Asset ca
			join	AssetType at
			on		ca.AssetTypeID = at.AssetTypeID
			join	Asset a
			on		ca.AssetID = a.AssetID
			join	MimeType mt
			on		at.MimeTypeid = mt.MimeTypeID
			and		mt.MediaType ='Video'
			where	ca.ContentID = @contentID
			and		ca.TargetPlatformID = @iDeviceType) vid
	join	(select ca.ContentID, a.*
			from	Content_Asset ca
			join	AssetType at
			on		ca.AssetTypeID = at.AssetTypeID
			join	Asset a
			on		ca.AssetID = a.AssetID
			join	MimeType mt
			on		at.MimeTypeid = mt.MimeTypeID
			and		mt.MediaType ='image'
			where	ca.ContentID = @contentID
			and		ca.TargetPlatformID = @iDeviceType) thumb
	on		vid.ContentID = thumb.ContentID
	join	Content c
	on		vid.contentid = c.ContentID
	INNER JOIN [Partner] part ON part.PartnerID = c.PartnerID
	INNER JOIN OrganizationsLEGACY oL ON oL.PartnerID = part.PartnerID
	WHERE c.ContentID = @contentID
*/

begin
-- declare @contentid bigint = 23454277, @iDeviceType int = 1
--	This is what is in production as of 1/12/2012
	-- if	@iDeviceType = 1
	set FMTONLY OFF
	set NOCOUNT ON
	exec SprocTrackingUpdate 'GetProgramShow'
	-- declare @contentid int = 23454277, @iDeviceType int = 1
		SELECT TOP 1
			c.ContentID, c.Name, c.[Description], c.PartnerID, c.Keyword,
			cv.ThumbnailFileName, cv.[FileName], cv.Duration,
			part.Name AS PartnerName, part.ShortName as ProducerNameAlt, oL.LogoUrl, isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'News Distribution Network, Inc.') ProducerCategory
		FROM Content c (nolock)
		INNER JOIN ContentVideo cv  (nolock) ON cv.ContentID = c.ContentID
		INNER JOIN [Partner] part  (nolock) ON part.PartnerID = c.PartnerID
		INNER JOIN OrganizationsLEGACY oL  (nolock) ON oL.PartnerID = part.PartnerID
		left join (select * from Playlist_Content_PartnerMap where NDNCategoryID is not null) pcpm
		on		c.PartnerID = pcpm.PartnerID
		left join NDNCategory nc
		on		pcpm.NDNCategoryID = nc.NDNCategoryID
		WHERE c.ContentID = @contentID
		and	c.Active = 1
		-- and cv.ContentVideoImportStatusID = 4
	set NOCOUNT OFF
end

/*


	-- declare @contentid int = 23454277, @iDeviceType int = 1
	SELECT TOP 1
		c.ContentID, c.Name, c.[Description], c.PartnerID, c.Keyword,
		thumbnailFileName, vid.[Filename], c.Duration,
		part.Name AS PartnerName, oL.LogoUrl
	from	(select ca.ContentID, a.*
			from	Content_Asset ca
			join	AssetType at
			on		ca.AssetTypeID = at.AssetTypeID
			join	Asset a
			on		ca.AssetID = a.AssetID
			join	MimeType mt
			on		at.MimeTypeid = mt.MimeTypeID
			and		mt.MediaType ='Video'
			where	ca.ContentID = @contentID
			and		ca.TargetPlatformID = @iDeviceType) vid
	join	(select ca.ContentID, a.*
			from	Content_Asset ca
			join	AssetType at
			on		ca.AssetTypeID = at.AssetTypeID
			join	Asset a
			on		ca.AssetID = a.AssetID
			join	MimeType mt
			on		at.MimeTypeid = mt.MimeTypeID
			and		mt.MediaType ='image'
			where	ca.ContentID = @contentID
			and		ca.TargetPlatformID = @iDeviceType) thumb
	on		vid.ContentID = thumb.ContentID
	join	Content c
	on		vid.contentid = c.ContentID
	INNER JOIN [Partner] part ON part.PartnerID = c.PartnerID
	INNER JOIN OrganizationsLEGACY oL ON oL.PartnerID = part.PartnerID
	WHERE c.ContentID = @contentID
*/
