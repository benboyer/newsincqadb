create procedure dbo.usp_SetAssetActive
	@assetid bigint
as
BEGIN
	update Asset 
	set Active = 1, 
		ImportStatusID = 5
	where AssetID = @assetid
	select AssetID, ImportStatusID, Active
	from Asset
	where AssetID = @assetid
	 
END
