USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[DeletedVideosNotArchived] as
select c.contentid
from Content (nolock) c
join VideosLEGACY (nolock) vl
on	c.ContentID = vl.ContentID
where	c.Active = 0
and		c.isDeleted = 1
and		c.IsFileArchived = 0
and		vl.IsDeleted = 1
GO
