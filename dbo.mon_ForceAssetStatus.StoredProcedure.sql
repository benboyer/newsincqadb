USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[mon_ForceAssetStatus]
	@assetid bigint,
	@Status int
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'mon_ForceAssetStatus'

	update Asset
	set ImportStatusID = @Status
	where AssetID = @assetid

	select AssetID, ImportStatusID, Active
	from Asset
	where AssetID = @assetid
	set nocount off

END
GO
