USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BlockedSites_listInternal]
	@includeInactive bit = 0,
	@Host varchar(1000) = null
AS
BEGIN
	set nocount on
	set fmtonly off
	exec SprocTrackingUpdate 'BlockedSites_ListInternal'

	SELECT	BlockedSiteID, Host, Active, isnull(DateUpdatedGMT, DateCreatedGMT) LastUpdatedDate, Comment
	  FROM	BlockedSite
	  WHERE ([Active] = 1 or @includeInactive = 1 or Host = @Host)
	  and	 Host = ISNULL(@Host, Host)
	  order by host
	  
	set nocount off
END
GO
