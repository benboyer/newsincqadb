USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetLauncherPlaylistVideos]
	--  declare
	@widgetID int = 1, -- = 6292, -- 3237,
	@playlistID int = null, -- null,
	@trackingGroup int = null,
	@contentid int = null,
	@devicetype int = 1
 AS
BEGIN
	-- updated 2013-09-18
	set nocount on
	SET FMTONLY OFF
	exec SprocTrackingUpdate 'GetLauncherPlaylistVideos'

	--  declare	@widgetID int = 2, 	@playlistID int = 507, @trackingGroup int = 10449, @contentid bigint = 23515411, @devicetype int = 1
	if @playlistID is null
		set @playlistID = 507
	if @trackingGroup is null
		set @trackingGroup = 10557
	if not exists
		(select 1 from Partner where TrackingGroup = @trackingGroup)
		set @trackingGroup = 10557
	Declare	@now datetime,
			@distPartnerID int
	set		@now = GETDATE()

	create table #playlists(playlistid int, title varchar(max), playlistorder int)
	--  declare	@widgetID int = 3233, 	@playlistID int = 10202, @trackingGroup int = 90121, @contentid bigint, @devicetype int = 2
	insert into #playlists(playlistid, title, playlistorder)
	--  declare	@widgetID int = 905, 	@playlistID int = 7910, @trackingGroup int = 10449, @contentid bigint = null, @devicetype int = 1
	exec getlauncherplaylist @widgetid, @trackinggroup, @devicetype

	if @Playlistid is not null
	begin
		delete -- declare	@widgetID int = 905, 	@playlistID int = 7910, @trackingGroup int = 10449, @contentid bigint, @devicetype int = 1 select *
			from #playlists where PlaylistID <> @PlaylistID
		if (select COUNT(*) from #Playlists	) = 0
			insert into #playlists(playlistid, title, playlistorder)
			select PlaylistID, Name, 1
			from	Playlist where PlaylistID = @playlistid
	end
	insert into #playlists(playlistid, title, playlistorder)
	select 9999999 as PlaylistID, 'SelectedContent' as Title, 0 as PlaylistOrder

	-- select * from #playlists
	select distinct playlistid
	into #plids
	from #playlists

	-- declare @widgetid int = 2, @trackinggroup int = 10557, @devicetype int = 1
	declare @pid int
	-- drop table #content
	create table #content(PlaylistID int, ContentID int, Name varchar(max), Description varchar(max), ContentPartnerID int, VideoGroupName varchar(26), Duration decimal(20, 4), PubDate datetime, Keyword varchar(max), Timeline int, AssetType varchar(max), AssetLocation varchar(max), assetmimetype varchar(20), assetsortorder int, AssetLocation2 varchar(max), assetmimetype2 varchar(20), assetsortorder2 int, ProducerName varchar(max), ProducerNameAlt varchar(max), ProducerLogo varchar(max), ContentOrder int, ProducerCategory varchar(26), SEOFriendlyTitle varchar(500), embedCodeAccessible bit)
	while (select count(*) from	#plids) > 0
	begin
	--	declare @pid int, @WidgetID int = 2, @TrackingGroup int = 10557, @DeviceType int = 1
		set @pid = (select min(playlistid) from #plids)
		--  declare	@widgetID int = 2, 	@pID int = 507, @trackingGroup int = 10557, @contentid bigint = null, @devicetype int = 1
		insert into #content(PlaylistID, ContentID, Name, Description, ContentPartnerID, VideoGroupName, Duration, PubDate, Keyword, Timeline, AssetType, AssetLocation, assetmimetype, assetsortorder, AssetLocation2, assetmimetype2, assetsortorder2, ProducerName, ProducerNameAlt, ProducerLogo, ContentOrder, ProducerCategory, SEOFriendlyTitle, embedCodeAccessible)
		-- declare @widgetid int, @trackinggroup int, @PID int, @contentid bigint, @devicetype int = 1
		exec BuildlauncherplaylistVideos @WidgetID, @PID, @TrackingGroup, null, @DeviceType
		delete from #plids where playlistid = @pid
	end
	drop table #plids

	--  declare	@widgetID int, 	@playlistID int, @trackingGroup int, @contentid bigint = 23515411, @devicetype int = 1
	declare @Minplid int, @minconorder int
	if @contentid is null
	begin
		set @Minplid = (select playlistid
			from #playlists
			where playlistorder =

				(select MIN(playlistorder)
						from #playlists p
						join #content c
						on	p.playlistid = c.playlistid))

		set @minconorder =
			(select MIN(ContentOrder)

			from #content
			where PlaylistID = @Minplid)

		set @contentid = (select distinct contentid from #content where PlaylistID = @Minplid and ContentOrder = @minconorder)
	end
	insert into #content(PlaylistID, ContentID, Name, Description, ContentPartnerID, VideoGroupName, Duration, PubDate, Keyword, Timeline, AssetType, AssetLocation, assetmimetype, assetsortorder, AssetLocation2, assetmimetype2, assetsortorder2, ProducerName, ProducerNameAlt, ProducerLogo, ContentOrder, ProducerCategory, SEOFriendlyTitle, embedCodeAccessible)

	exec getSingleContent @ContentID, @TrackingGroup, @DeviceType

	select distinct pl.PlaylistID, pl.title PlaylistTitle, c.ContentID, c.Name, c.Description, c.ContentPartnerID, c.VideoGroupName, c.Duration, c.PubDate, c.Keyword, c.Timeline, c.AssetType, c.AssetLocation, c.AssetMimeType, c.AssetSortOrder, c.AssetLocation2, c.AssetMimeType2, c.AssetSortOrder2, c.ProducerName, null ProducerNameAlt,c.ProducerLogo, pl.PlaylistOrder, convert(int, c.ContentOrder) ContentOrder, null ProducerCategory,null SEOFriendlyTitle, null embedCodeAccessible
	from	#playlists pl
	join	#content  c
	on		pl.playlistid = c.playlistid
	order by pl.playlistorder, contentorder

	drop table #playlists
	drop
	 table #content
	set nocount off
END
GO
