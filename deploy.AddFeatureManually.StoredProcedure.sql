USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [deploy].[AddFeatureManually]
	@type varchar(20),
	@name varchar(60),
	@schema varchar(20),
	@installation_code varchar(max),
	@dependsOn int = null,
	@definer varchar(60) = null,
	@tickets varchar(120) = null,
	@comments varchar(200) = null
AS
BEGIN
	if exists (select 1 from deploy.schema_feature where feature_type = @type and feature_name = @name and for_schema = @schema and installation_code = @installation_code )
	BEGIN
		update	deploy.schema_feature 
		set		defined_by = ISNULL(@definer, SUSER_NAME()), depends_on = @dependsOn, tickets_applicable = isnull(@tickets, tickets_applicable), description = @comments
		where	feature_type = @type and feature_name = @name and for_schema = @schema
	END
	insert into deploy.schema_feature(feature_type, feature_name, defined_on, defined_by, installation_code, for_schema, depends_on, tickets_applicable, description)
	select	@type, @name, GETDATE(), @definer, @installation_code, @schema, @dependsOn, @tickets, @comments
	where	not exists (select 1 from deploy.schema_feature where feature_type = @type and feature_name = @name and for_schema = @schema and installation_code = @installation_code )
END
GO
