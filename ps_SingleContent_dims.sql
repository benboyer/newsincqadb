CREATE PROCEDURE [dbo].[ps_SingleContent_dims]
--  declare
	@ContentID bigint,
	@trackingGroup int = null,
	@devicetype int = 1,
	@playlistid int = 9999999
AS
BEGIN
	set FMTONLY OFF
	set NOCOUNT ON
	exec SprocTrackingUpdate 'ps_SingleContent_dims'
	--  select @ContentID = 24746973, @trackinggroup = 90121, @devicetype = 1
	Declare	@now datetime,
			@iContentID bigint,
			@itrackinggroup int,
			@idevicetype int,
			@iplaylistid int,
			@iPartnerid int,
			@logourl varchar(400)
	set		@now = GETDATE()

	select	@iContentID = @ContentID, @itrackinggroup = @trackingGroup, @idevicetype = @devicetype, @iplaylistid = isnull(@playlistid, 9999999)
	set		@iPartnerid = (select partnerid from Content (nolock) where ContentID = @ContentID)
	set		@logourl = coalesce((select lo.logourl from Partner_Logo pl (nolock) left join Logo (nolock) lo on pl.LogoID = lo.LogoID where pl.PartnerID = @iPartnerid and lo.LogoTypeID = 5 and lo.Active = 1),
			(select logourl from Partner (nolock) where PartnerID = @iPartnerid),
			'http://assets.newsinc.com/newsinconebyone.png')

	select  distinct
			@iplaylistid as PlaylistID,	-- convert(int, 9999999) as PlaylistID,
				a.ContentID, a.Name, a.Description, a.TrackingGroup ContentPartnerID, a.VideoGroupName, a.Duration, a.Width, a.Height,
				convert(date, a.PubDate) PubDate, a.Keyword, convert(varchar(120), null) Timeline,
				case	when a.AssetTypeName like 'Video%' then 'src'
						else a.AssetTypeName
				end as AssetType,
					case when a.AssetTypeName like 'External%' then a.Filename
				else
					a.AssetLocation
				end as AssetLocation, a.AssetMimeType, a.AssetSortOrder,
					a.AssetLocation2, a.AssetMimeType2, a.AssetSortOrder2,
				a.DeliveryPath2 as AssetLocation3,
				a.ProducerName, a.ProducerNameAlt,
				-- isnull(a.logourl,'http://assets.newsinc.com/newsinconebyone.png')
				@logourl ProducerLogo, convert(int, 0) as ContentOrder,
				isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'news') ProducerCategory,
				REPLACE(a.Name, ' ', '-') + + '-' +CONVERT(varchar(20), a.ContentID) as SEOFriendlyTitle,
				case when dse.partnerid is null then 1
						when dse.partnerid is not null then 0
				end as embedCodeAccessible--,
/*				case when dsm.partnerid is null then 1
						when dsm.partnerid is not null then 0
				end as shareEmailAccessible,
				case when dss.partnerid is null then 1
						when dss.partnerid is not null then 0
				end as shareSocialAccessible
*/
	from (select	pt.partnerid, pt.TrackingGroup, pt.isContentPrivate, c.ContentID, c.Name,
				c.Description, c.EffectiveDate PubDate, null as [order], aa.FilePath, aa.Filename, aa.Duration, aa.Width, aa.Height, c.Keyword,
				null as VideoGroupName, at.Name AssetTypeName, mt.FileExtension, pt.logourl, pt.Name ProducerName, pt.ShortName ProducerNameAlt,
				case when mt.mediatype in ('video', 'image', 'text') then dbo.getpathtostreamasset(aa.AssetID) else null end as AssetLocation,
				case when mt.mediatype in ('video', 'image', 'text') then dbo.GetMimeTypeToStreamAsset(aa.AssetID) else null end as AssetMimeType,
				case when mt.mediatype in ('video', 'image', 'text') then convert(int,dbo.GetSortOrderToStreamAsset(aa.AssetID)) else null end as AssetSortOrder,
				case when mt.mediatype in ('video', 'image', 'text') then dbo.getpathtoprogressiveasset(aa.AssetID) else null end as AssetLocation2,
				case when mt.mediatype in ('video', 'image', 'text') then dbo.GetMimeTypeToProgressiveAsset(aa.AssetID) else null end as AssetMimeType2,
				case when mt.mediatype in ('video', 'image', 'text') then convert(int,dbo.GetSortOrderToProgressiveAsset(aa.AssetID)) else null end as AssetSortOrder2,
				-- dbo.GetAssetDeliveryPath(mt.FileExtension, c.partnerid, ca.contentid, ca.assetid) DeliveryPath,
				dbo.GetPathToServeAsset(aa.assetid) DeliveryPath2, c.FeedID
		from	(select * from Content (nolock) where contentid = @iContentID) c
		join	Partner_TargetPlatformProfile (nolock) ptp
		on		c.PartnerID = ptp.PartnerID
--		and		@idevicetype = ptp.TargetPlatformID
		join	TargetPlatform_AssetType (nolock) tpat
		on		ptp.TargetPlatformID = tpat.TargetPlatformID
		join	Content_Asset (nolock) ca
		on		c.ContentID = ca.ContentID
		and		(ca.TargetPlatformID = 1 or ca.AssetTypeID = 9)
		join	AssetType (nolock) at
		on		tpat.AssetTypeID = at.AssetTypeID
		and		ca.AssetTypeID = at.AssetTypeID
		join	Asset (nolock) aa
		on		ca.AssetID = aa.AssetID
		join	MimeType (nolock) mt
		on		at.MimeTypeid = mt.MimeTypeID
		JOIN	partner (nolock) pt
		ON		c.PartnerID = pt.PartnerID) a
		left join	Playlist_Content_PartnerMap pm   (nolock)
		on
		a.PartnerID = pm.PartnerID
		and			a.FeedID = pm.FeedID
		left join	NDNCategory nc (nolock)
		on			pm.NDNCategoryID = nc.NDNCategoryID
		left join (select partnerid from vw_partnerDefaultSettings (nolock) where Entity = 'BlockShareForProviderID' and Setting = 'shareEmbed' and Value = 'true') dse
		on	a.PartnerID = dse.PartnerID
		left join (select partnerid from vw_partnerDefaultSettings (nolock) where Entity = 'BlockShareForProviderID' and Setting = 'shareEmail' and Value = 'true') dsm
		on	a.PartnerID = dsm.PartnerID
		left join (select partnerid from vw_partnerDefaultSettings (nolock) where Entity = 'BlockShareForProviderID' and Setting = 'shareSocial' and Value = 'true') dss
		on	a.PartnerID = dss.PartnerID
		where		dbo.AllowedContentID(a.contentid, @itrackinggroup, 1) = 1
		order by case	when a.AssetTypeName like 'Video%' then 'src'
						else a.AssetTypeName
					end
	SET NOCOUNT OFF
END
