USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_GetBestFeedContentAsset_code]
	(
--		-- declare
	@feedcontentid bigint = null,
	@assettyperid int = null
	)
as
BEGIN
	set fmtonly OFF
	set Nocount ON
	exec SprocTrackingUpdate 'usp_GetBestFeedContentAsset_code'

	--	set @feedcontentid = 1921821
	--	set @assettyperid = 12
		--	declare @feedcontentid bigint = 1921821 , @assettyperid int = 9 -- , @feedcontentassetid int, @width int, @bitrate int, @encode bit = 0, @fileextension varchar(120)
	declare @feedcontentassetid bigint, @width int, @bitrate int, @encode bit = 0, @fileextension varchar(120), @CreateFile bit = 0

	if @feedcontentid  is null or @assettyperid is null
	begin
		-- select 'Got nuthin to work with here'
		set @feedcontentassetid = 0
		set @width = 0
		set @bitrate = 0
		set	@fileextension = ''
		set @encode = 0
		set @CreateFile = 0
		SELECT @FeedContentAssetID FeedContentAssetID, @width Width, @bitrate Bitrate, @fileextension FileExtension, @encode Encode, @CreateFile CreateFile

		return
	end

	-- declare @feedcontentid bigint = 324955, @assettyperid int = 14
	select	distinct /*tp.targetplatformid, */at.*, mt.fileextension, mt.MediaType
	into	#wewant -- drop table #wewant -- select * from #wewant -- select * from #wehave -- select * from assettype
	from	Partner p (nolock)
	join	Partner_TargetPlatformProfile tdp (nolock)
	on		p.PartnerID = tdp.PartnerID
	join	targetplatform tp (nolock)
	on		tdp.targetplatformid = tp.targetplatformid
	join	targetplatform_assettype tpat (nolock)
	on		tdp.targetplatformid = tpat.targetplatformid
	join	(select * from  AssetType (nolock) where AssetTypeID = @assettyperid )at
	on		tpat.assettypeid = at.AssetTypeID
	join	MimeType mt (nolock)
	on		at.mimetypeid = mt.mimetypeid
	join	(select f.FeedContentAssetID, m.*
			from	FeedContentAsset F (nolock)
			join 	MimeType m (nolock)
			on		f.mimetypeid = m.mimetypeid
			where FeedContentID = @feedcontentid)  fca
	on		mt.mediatype = fca.mediatype
	where	at.AssetTypeID = @assettyperid

	-- declare @feedcontentid bigint = 329262, @assettyperid int = 16
	select distinct fca.*
	into	#wehave -- drop table #wehave -- select * from #wehave -- delete from #wehave where mimetypeid = 1
	from	Partner p (nolock)
	join	Partner_TargetPlatformProfile tdp (nolock)
	on		p.PartnerID = tdp.PartnerID
	join	targetplatform tp (nolock)
	on		tdp.targetplatformid = tp.targetplatformid
	join	targetplatform_assettype tpat (nolock)
	on		tdp.targetplatformid = tpat.targetplatformid
	join	AssetType at (nolock)
	on		tpat.assettypeid = at.AssetTypeID
	join	MimeType mt (nolock)
	on		at.mimetypeid = mt.mimetypeid
	join	(select f.FeedContentAssetID, f.width, f.bitrate, f.filepath,m.*
			from	FeedContentAsset F (nolock)
			join 	MimeType m (nolock)
			on		f.mimetypeid = m.mimetypeid
			where FeedContentID = @feedcontentid
			and		f.FilePath is not null)  fca
	on		mt.mediatype = fca.mediatype
	where	at.AssetTypeID = @assettyperid

	-- select 'created temp tables'
	-- select * from #wewant select * from #wehave
	if (select COUNT(*) from #wewant) = 0
		and
		(select COUNT(*) from #wehave) = 0
		-- There is no source assets that match the mediatype we need
		BEGIN
			select @feedcontentassetid = 0, @width = 0, @bitrate = 0, @fileextension = '', @encode = 0, @CreateFile = 0
			SELECT @FeedContentAssetID FeedContentAssetID, @width Width, @bitrate Bitrate, @fileextension FileExtension, @encode Encode, @CreateFile CreateFile

				drop table #wehave
				drop table #wewant
			return
		END

		select @feedcontentassetid = MAX(g.FeedContentAssetID) --FeedContentAssetID -- select g.* -- select MAX(g.FeedContentAssetID)
		from (select MAX(d.bitrate) bitrate, c.width, c.fileextension
			from (select MAX(a.width) width, a.fileextension
					from #wehave a,
						#wewant b
					where (a.width between b.minwidth and b.maxwidth or b.Name =  'ExternalContentID')
					-- where a.width between b.minwidth and b.maxwidth
					and	a.fileextension = b.fileextension
					and	(a.bitrate between b.MinBitrate and b.maxbitrate or b.MediaType <> 'video')
					group by a.fileextension) c
			join #wehave d
			on	c.width = d.width,
				#wewant e
			where	c.width = d.width
			and		d.fileextension = e.fileextension
			and	(d.bitrate between e.MinBitrate and e.maxbitrate or e.MediaType <> 'video')
			group by c.width, c.fileextension) f
		join	#wehave g
		on		f.width = g.width
		and		(f.bitrate = g.bitrate or (select MediaType from #wewant) <> 'video')
		and		f.fileextension = g.fileextension

		if	@feedcontentassetid is null
			and
			(select COUNT(*) from #wewant) = 1
			and
			(select COUNT(*) from #wehave) = 1

		begin
			if	(select idealwidth from #wewant) is null
				and
				(select idealbitrate from #wewant) is null
				and
				(select ISNULL(transcode, 1) from #wewant) = 0
				and
				(select isnull(createfile, 0) from #wewant) = 1
				and
				(select FileExtension from #wehave) = (select CreateFileExtension from #wewant)
			begin
				set @feedcontentassetid = (select feedcontentassetid from #wehave)
			end
		end
		if @feedcontentassetid is not null
		BEGIN
			--	select 'Found exact match'
			select @width = width, @bitrate = bitrate, @fileextension = fileextension, @encode = 0
			from	#wehave where FeedContentAssetID = @feedcontentassetid
			set @CreateFile = (select createfile from #wewant)
			SELECT @FeedContentAssetID FeedContentAssetID, @width Width, @bitrate Bitrate, @fileextension FileExtension, @encode Encode, @CreateFile CreateFile

				drop table #wehave
				drop table #wewant
			return
		END
		if (select ISNULL(transcode, 1) from #wewant) = 0
		begin -- declare @feedcontentassetid bigint, @width int, @bitrate int, @encode bit = 0, @fileextension varchar(120), @CreateFile bit = 0
			-- select 'can not transcode this one'
			select @feedcontentassetid = null, @width = null, @bitrate = null, @fileextension = null, @encode = null, @CreateFile = null
			SELECT @FeedContentAssetID FeedContentAssetID, @width Width, @bitrate Bitrate, @fileextension FileExtension, @encode Encode, @CreateFile CreateFile
			drop table #wehave
			drop table #wewant
			return
		end
--------------------------------------------------
		-- If we make it this far, there is not a perfect match.  Therefore we will have to transcode, so set the flag now.
		set @encode = 1
		-- select 'we must encode'

		-- do we have a match on width/bitrate,  but with different extension?  If so we'll transcode it to the same params, new extension
		select @feedcontentassetid = MAX(g.FeedContentAssetID) --FeedContentAssetID -- select MAX(g.FeedContentAssetID) -- select *
		from (select MAX(d.bitrate) bitrate, c.width, c.fileextension -- select *
			from (select MAX(a.width) width, a.fileextension
					from #wehave a,
						#wewant b
					where a.width between b.minwidth and b.maxwidth
					--and	a.fileextension = b.fileextension
					and	(isnull(a.bitrate, b.idealbitrate) between b.minbitrate and b.maxbitrate or b.MediaType <> 'video')
					group by a.fileextension) c
			join #wehave d
			on	c.width = d.width,
				#wewant e
			where	c.width = d.width
			and		d.mediatype = e.mediatype --	c.fileextension = e.fileextension
			group by c.width, c.fileextension) f
		join	#wehave g
		on		f.width = g.width
		and		(f.bitrate = isnull(g.bitrate, f.bitrate) or (select MediaType from #wewant) <> 'video')
		and		f.fileextension <> g.fileextension

		if @feedcontentassetid is not null
		BEGIN -- declare @feedcontentassetid bigint = 1159154, @width int, @bitrate int, @encode bit = 1, @fileextension varchar(120), @CreateFile bit = 0
		-- select 'found match on width/bitrate,  but with different extension'
			select @width = width, @bitrate = bitrate from #wehave where FeedContentAssetid = @feedcontentassetid
			select @fileextension = fileextension, @CreateFile = createfile from #wewant
			if @width > (select maxwidth from #wewant)
				set @width = (select idealwidth from #wewant)
			if @bitrate > (select maxbitrate from #wewant)
				set @bitrate = (select idealbitrate from #wewant)
			SELECT @FeedContentAssetID FeedContentAssetID, @width Width, @bitrate Bitrate, @fileextension FileExtension, @encode Encode, @CreateFile CreateFile
			drop table #wehave
			drop table #wewant
			return
		END
		--	select * from #wewant select * from #wehave
----------------------------------------------
		-- do we have a higher width/bitrate  with same extension?  If so we'll transcode it to the ideal params
		select @feedcontentassetid = (select MAX(g.FeedContentAssetID) --FeedContentAssetID -- select MAX(g.FeedContentAssetID) -- select *
		from (select MIN(d.bitrate) bitrate, c.width, c.fileextension -- select *
			from (select MIN(a.width) width, a.fileextension
					from #wehave a,
						#wewant b
					where a.fileextension = b.fileextension
					and	((a.width > b.minwidth
						or	(isnull(a.bitrate, b.idealbitrate) >= b.minbitrate)) or b.MediaType <> 'video')
					group by a.fileextension) c
			join #wehave d
			on	c.width = d.width,
				#wewant e
			where	c.width = d.width
			and		d.mediatype = e.mediatype --	c.fileextension = e.fileextension
			group by c.width, c.fileextension) f
		join	#wehave g
		on		f.width = g.width
		and		(f.bitrate = isnull(g.bitrate, f.bitrate) or (select MediaType from #wewant) <> 'video')
		and		f.fileextension = g.fileextension)

		if @feedcontentassetid is not null
		BEGIN -- declare @feedcontentassetid bigint = 1208890, @width int, @bitrate int, @encode bit = 1, @fileextension varchar(120), @CreateFile bit = 0
		-- select 'found match higher width/bitrate  with same extension'
			select @width = idealwidth, @bitrate = idealbitrate, @fileextension = fileextension, @CreateFile = createfile from #wewant
			if @width > (select maxwidth from #wewant)
				set @width = (select idealwidth from #wewant)
			if @bitrate > (select maxbitrate from #wewant)
				set @bitrate = (select idealbitrate from #wewant)
			SELECT @FeedContentAssetID FeedContentAssetID, @width Width, @bitrate Bitrate, @fileextension FileExtension, @encode Encode, @CreateFile CreateFile
			drop table #wehave
			drop table #wewant
			return
		END

----------------------------------------------
		-- do we have a higher width/bitrate  with other extension?  If so we'll transcode it to the ideal params
		select @feedcontentassetid = (select MAX(g.FeedContentAssetID) --FeedContentAssetID -- select MAX(g.FeedContentAssetID) -- select *
		from (select MAX(d.bitrate) bitrate, c.width, c.fileextension -- select *
			from (select MAX(a.width) width, a.fileextension -- select *
					from #wehave a,
						#wewant b
					where (a.width > b.minwidth
					--and	a.fileextension = b.fileextension
					or	a.bitrate > b.maxbitrate or a.bitrate is null) or b.MediaType <> 'video'
					group by a.fileextension) c
			join #wehave d
			on	c.width = d.width,
				#wewant e
			where	c.width = d.width
			and		d.mediatype = e.mediatype --	c.fileextension = e.fileextension
			group by c.width, c.fileextension) f
		join	#wehave g
		on		f.width = g.width
		and		(f.bitrate = g.bitrate or f.bitrate is null or (select MediaType from #wewant) <> 'video')
		and		f.fileextension <> g.fileextension)

		if @feedcontentassetid is not null
		BEGIN -- declare @feedcontentassetid bigint = 1204468, @width int, @bitrate int, @encode bit = 1, @fileextension varchar(120), @CreateFile bit = 0
		-- select 'found a match on higher width/bitrate  with other extension'
			select @width = idealwidth, @bitrate = idealbitrate, @fileextension = fileextension, @CreateFile = createfile from #wewant
			if @width > (select maxwidth from #wewant)
				set @width = (select idealwidth from #wewant)
			if @bitrate > (select maxbitrate from #wewant)
				set @bitrate = (select idealbitrate from #wewant)
			SELECT @FeedContentAssetID FeedContentAssetID, @width Width, @bitrate Bitrate, @fileextension FileExtension, @encode Encode, @CreateFile CreateFile
			drop table #wehave
			drop table #wewant
			return
		END

---------------------------------------------- select * from #wehave select * from #wewant
		-- Okay.  We have no ideal match, no ideal match with a different extension, no match to higher width/bitrate with same extension, no match to higher width/bitrate with other extension.  So, now,
		-- do we have a match on the mediatype at all?  If so we'll grab one and transcode it to the ideal params
		select @feedcontentassetid = (select top 1 a.feedcontentassetid
		from	#wehave a
		join	#wewant b
		on		a.mediatype = b.mediatype
		--where	((a.width between b.minwidth and b.maxwidth or isnull(a.width, 0) = 0)
		--		and
		--		(a.bitrate between b.minbitrate and b.maxbitrate or isnull(a.bitrate, 0) = 0))
		order by isnull(ISNULL(a.width, a.bitrate), a.feedcontentassetid) desc)

		if @feedcontentassetid is not null
		BEGIN -- declare @feedcontentassetid bigint = 1504106, @width int, @bitrate int, @encode bit = 1, @fileextension varchar(120), @CreateFile bit = 0
		-- select 'found a match on the mediatype, at lease'
			--if (select width from #wehave) is null and (select bitrate from #wehave) is null and (select fileextension from #wehave) = (select fileextension from #wewant)
			--	set @encode = 0
			select @width = idealwidth, @bitrate = isnull(idealbitrate, 0), @fileextension = fileextension, @CreateFile = isnull(createfile, 0) from #wewant

			SELECT @FeedContentAssetID FeedContentAssetID, @width Width, @bitrate Bitrate, @fileextension FileExtension, @encode Encode, @CreateFile CreateFile
			drop table #wehave
			drop table #wewant
			return
		END

	drop table #wehave
	drop table #wewant

	set Nocount OFF

END
GO
