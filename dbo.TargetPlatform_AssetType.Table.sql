USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TargetPlatform_AssetType](
	[TargetPlatform_AssetTypeID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[TargetPlatformID] [smallint] NOT NULL,
	[AssetTypeID] [int] NOT NULL,
 CONSTRAINT [PK_TargetDevice_AssetType] PRIMARY KEY CLUSTERED 
(
	[TargetPlatform_AssetTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TargetPlatform_AssetType]  WITH CHECK ADD  CONSTRAINT [FK_TargetPlatform_AssetType_AssetType] FOREIGN KEY([AssetTypeID])
REFERENCES [dbo].[AssetType] ([AssetTypeID])
GO
ALTER TABLE [dbo].[TargetPlatform_AssetType] CHECK CONSTRAINT [FK_TargetPlatform_AssetType_AssetType]
GO
ALTER TABLE [dbo].[TargetPlatform_AssetType]  WITH CHECK ADD  CONSTRAINT [FK_TargetPlatform_AssetType_TargetPlatform] FOREIGN KEY([TargetPlatformID])
REFERENCES [dbo].[TargetPlatform] ([TargetPlatformID])
GO
ALTER TABLE [dbo].[TargetPlatform_AssetType] CHECK CONSTRAINT [FK_TargetPlatform_AssetType_TargetPlatform]
GO
