USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_GetPartnerInfoByPartnerID]
--	declare
	@Partnerid int
as
begin
	SET FMTONLY OFF
	set nocount on
	exec SprocTrackingUpdate 'usp_GetPartnerInfoByPartnerID'
	
	select p.PartnerID, p.Name, p.ZoneID, p.LandingURL, p.LogoURL, p.FeedGUID
	from	partner p
	where	p.PartnerID = @partnerid

	set nocount off
end
GO
