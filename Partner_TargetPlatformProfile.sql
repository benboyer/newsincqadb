TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Partner_TargetPlatformProfile	Partner_TargetPlatformProfileID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	Partner_TargetPlatformProfile	PartnerID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	Partner_TargetPlatformProfile	ProfileDescriptionID	4	int	10	4	0	10	1	None	None	4	None	None	3	YES	38
NewsincQA	dbo	Partner_TargetPlatformProfile	TargetPlatformID	5	smallint	5	2	0	10	0	None	None	5	None	None	4	NO	52
NewsincQA	dbo	Partner_TargetPlatformProfile	isImportEnabled	-7	bit	1	1	None	None	1	None	None	-7	None	None	5	YES	50
NewsincQA	dbo	Partner_TargetPlatformProfile	isDistributionEnabled	-7	bit	1	1	None	None	1	None	None	-7	None	None	6	YES	50
NewsincQA	dbo	Partner_TargetPlatformProfile	ActivateOnImport	-7	bit	1	1	None	None	0	None	((1))	-7	None	None	7	NO	50

index_name	index_description	index_keys
IX_Partner_TargetDeviceProfile_PID_DID	nonclustered located on PRIMARY	PartnerID, TargetPlatformID
PK_Partner_TargetDeviceProfile	clustered, unique, primary key located on PRIMARY	Partner_TargetPlatformProfileID
