USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[setAntiKeywordbyContentID] @contentID int, @newantikw  varchar(1000) as    
    
declare @antikw varchar(1000)     
set @antikw = ( select ISNULL(LTRIM(RTRIM(antikw)),'') from Content where ContentID = @contentid )    
    
if ( len(@antikw) = 0 )    
 update Content set antikw = @newantikw where ContentID = @contentid    
else if ( SUBSTRING(@antikw, len(@antikw)-1, 1) = ',' )    
 update Content set antikw = antikw + @newantikw where ContentID = @contentid     
else     
 update Content set antikw = antikw + ',' + @newantikw where ContentID = @contentid
GO
