CREATE PROCEDURE [dbo].[usp_GetLastContentPublishDate]
	@PartnerID int
AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'usp_GetLastContentPublishDate'

	DECLARE @PublishDate datetime;

	Select TOP 1 @PublishDate = Content.EffectiveDate
	FROM Content
	WHERE PartnerID = @PartnerID
	ORDER BY EffectiveDate DESC

    Select @PublishDate;

END
