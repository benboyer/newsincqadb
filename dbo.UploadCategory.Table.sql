USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UploadCategory](
	[UploadCategoryID] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[UploadContentID] [bigint] NOT NULL,
	[Category] [varchar](40) NOT NULL,
 CONSTRAINT [PK_UploadCategory] PRIMARY KEY CLUSTERED 
(
	[UploadCategoryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[UploadCategory]  WITH CHECK ADD  CONSTRAINT [FK_UploadCategory_UploadContent] FOREIGN KEY([UploadContentID])
REFERENCES [dbo].[UploadContent] ([UploadContentID])
GO
ALTER TABLE [dbo].[UploadCategory] CHECK CONSTRAINT [FK_UploadCategory_UploadContent]
GO
