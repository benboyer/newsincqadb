create procedure dbo.dmev_SiteSection
	@partnerid int
as
begin
	select PartnerID, SectionID, CreatedUserID, FullSiteSection, SectionPartnerID, SectionPartnerUnique, DefaultUnique, Section, SubSection, PageType
	from dme_SiteSection (nolock)
	where partnerid = isnull(@partnerid, partnerid)
end

