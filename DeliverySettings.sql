TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	DeliverySettings	DeliverySettingsID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	DeliverySettings	Name	12	varchar	26	26	None	None	1	None	None	12	None	26	2	YES	39
NewsincQA	dbo	DeliverySettings	Value	12	varchar	20	20	None	None	1	None	None	12	None	20	3	YES	39
NewsincQA	dbo	DeliverySettings	Description	12	varchar	200	200	None	None	1	None	None	12	None	200	4	YES	39
NewsincQA	dbo	DeliverySettings	DefaultValue	12	varchar	20	20	None	None	1	None	None	12	None	20	5	YES	39
NewsincQA	dbo	DeliverySettings	PlayerVariable	12	varchar	100	100	None	None	1	None	None	12	None	100	6	YES	39

index_name	index_description	index_keys
PK_DeliverySettings	clustered, unique, primary key located on PRIMARY	DeliverySettingsID
