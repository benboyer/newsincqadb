USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Playlist_Content_PartnerMap](
	[Playlist_Content_PartnerMapID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PartnerID] [int] NOT NULL,
	[FeedID] [int] NULL,
	[PlaylistID] [int] NULL,
	[SourceCategory] [varchar](50) NULL,
	[DestinationCategory] [varchar](50) NULL,
	[NDNCategoryID] [int] NULL,
	[isDefault] [bit] NOT NULL,
	[MaxVideoCount] [int] NULL,
	[LastContentID] [bigint] NULL,
 CONSTRAINT [PK_Playlist_Content_PartnerMap_1] PRIMARY KEY CLUSTERED 
(
	[Playlist_Content_PartnerMapID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_C_PL_PartnerMap] ON [dbo].[Playlist_Content_PartnerMap] 
(
	[PartnerID] ASC,
	[SourceCategory] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Playlist_Content_PartnerMap]  WITH CHECK ADD  CONSTRAINT [Partner_Playlist_Content_PartnerMap] FOREIGN KEY([PartnerID])
REFERENCES [dbo].[Partner] ([PartnerID])
GO
ALTER TABLE [dbo].[Playlist_Content_PartnerMap] CHECK CONSTRAINT [Partner_Playlist_Content_PartnerMap]
GO
ALTER TABLE [dbo].[Playlist_Content_PartnerMap] ADD  CONSTRAINT [DF_Content_Playlist_PartnerMap_isDefault]  DEFAULT ((0)) FOR [isDefault]
GO
ALTER TABLE [dbo].[Playlist_Content_PartnerMap] ADD  CONSTRAINT [DF_Playlist_Content_PartnerMap_MaxVideoCount]  DEFAULT ((25)) FOR [MaxVideoCount]
GO
