USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[adm_PartnerDefaultSetting_Add_Update]
	@PartnerID int,
	@iSettingType varchar(36),
	@iSetting varchar(36),
	@value varchar(120),
	@deleteit bit = 0,
	@comment varchar(120) = null
as
BEGIN
	set nocount on
	set FMTONLY OFF
	exec SprocTrackingUpdate 'DefaultSingleEmbed'

	declare @SettingTypeID int,
			@SettingID int

	set @SettingTypeID = (select DefaultSettingTypeID from DefaultSettingType where name = @iSettingType)
	set @SettingID = (select SettingID from Setting where Name = @iSetting )
	if @SettingTypeID is null or @SettingID is null
	begin
		select 'Can not find an input parameter(s).  Please check and try again' as MessageBack
		return
	end

	if @deleteit = 1
	and	exists
		(select 1 -- select *
		from	PartnerDefaultSetting
		where	PartnerID = @PartnerID
		and		DefaultSettingTypeID = @SettingTypeID
		and		SettingID = @SettingID
		and		Value = @value)
	begin
		delete
		from	PartnerDefaultSetting
		where	PartnerID = @PartnerID
		and		DefaultSettingTypeID = @SettingTypeID
		and		SettingID = @SettingID
		and		Value = @value
	end
	else
	BEGIN
		if exists
			(select 1 -- select *
			from	PartnerDefaultSetting
			where	PartnerID = @PartnerID
			and		DefaultSettingTypeID = @SettingTypeID
			and		SettingID = @SettingID)
		begin
			update	PartnerDefaultSetting
			set		Value = @value,
					Comment = @comment
			where	PartnerID = @PartnerID
			and		DefaultSettingTypeID = @SettingTypeID
			and		SettingID = @SettingID
		end
		else
		BEGIN
			insert into 	PartnerDefaultSetting (PartnerID, DefaultsettingTypeID, SettingID, Value, Comment)
			select @PartnerID, @SettingTypeID, @SettingID, @value, @comment
		END
	END
	set nocount off
END
GO
