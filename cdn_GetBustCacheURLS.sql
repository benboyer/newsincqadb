
CREATE procedure [dbo].[cdn_GetBustCacheURLS]
	-- declare 
	@contentid bigint = null,
	@Type int = 0,
	@filename varchar(200) = null
as
BEGIN
	-- @Type 0 = All Content urls - ContentImport
	-- @Type 1 = vid.catalog only - Metadata Generator
	-- @tyype 2 = assets [producer logos]
	
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'akamai_BustCacheURLS'
	-- declare 	@contentid bigint = 23801844, @type int = 0, @filename varchar(120)

	declare @list table(urls varchar(200))
	if @Type = 0
	begin
		insert into @list(urls)
			select distinct 'http://' +  a.filepath + '/' + a.filename
			from content_asset ca
			join	asset a
			on		ca.assetid = a.assetid
			where ca.contentid = @contentid
			union all
			select distinct cv.FileName
			from content c
			join content_asset ca
			on		c.contentid = ca.contentid
			join	asset a
			on		ca.assetid = a.assetid
			left join contentvideo cv
			on	c.contentid = cv.contentid
			where c.contentid = @contentid
			union all
			select distinct cv.thumbnailfilename
			from contentvideo cv
			where cv.contentid = @contentid
			union all
			select distinct cv.stillframefilename
			from contentvideo cv
			where cv.contentid = @contentid
			union all
			select 'http://vid.catalog.newsinc.com/' + convert(varchar(20), @contentid) + '.xml'
			union all
			select 'rtmp://cp98516.edgefcs.net/ondemand/videos/' + convert(varchar(20), @contentid) + '.flv'
			union all
			select 'http://Video.newsinc.com/' + convert(varchar(20), @contentid) + '.flv'
		end
		if @Type = 1
		begin
			insert into @list(urls)
			select 'http://vid.catalog.newsinc.com/' + convert(varchar(20), @contentid) + '.xml'
		end
		if @Type = 2
		begin
			insert into @list(urls)
			select 'http://assets.newsinc.com/' + @filename
		end
		select urls from @list where urls is not null
		set nocount OFF
END

