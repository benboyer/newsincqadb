USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_partnerDefaultSettings]
AS
SELECT     ds.PartnerID, dst.Name AS Entity, s.Name AS Setting, ds.Value
FROM         dbo.Setting AS s INNER JOIN
                      dbo.PartnerDefaultSetting AS ds ON s.SettingID = ds.SettingID INNER JOIN
                      dbo.DefaultSettingType AS dst ON ds.DefaultsettingTypeID = dst.DefaultSettingTypeID
WHERE     (dst.Active = 1)
GO
