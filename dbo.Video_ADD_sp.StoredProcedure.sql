USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Video_ADD_sp]
           @VideoID int,
           @ContentItemID int = null,
           @UserID int,
           @ClipURL nvarchar(50) = null,
           @Title nvarchar(100),
           @Description nvarchar(1000),
           @EventDate smalldatetime,
           @NumberOfReviews int = null,
           @Height decimal(10,2) = null,
           @Width decimal(10,2) = null,
           @Duration decimal(10,2) = null,
           @CreatedOn smalldatetime,
           @ClipPopularity float = null,
           @CandidateOfInterest float = null,
           @ThumbnailURL nvarchar(50) = null,
           @PublishDate smalldatetime,
           @Keywords nvarchar(500) = null,
           @NumberOfPlays int = null,
           @SentToFreeWheel bit,
           @IsEnabled bit,
           @IsDeleted bit,
           @ExpiryDate smalldatetime = null,
           @OriginalName nvarchar(200),
           @StillFrameURL nvarchar(50) = null,
           @FileExtension nvarchar(5) = null,
           @Category nvarchar(50) = null,
           @EnterDTM smalldatetime,
           @LastChgDTM datetime = null,
           @IngestionStatusMessage nvarchar(2000) = null,
           @IsAdvertisement bit



AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'Video_ADD_sp'

	Declare @NewID int;
	Declare @PartnerID int;

	Select @PartnerID = PartnerID
	From [User]
	Where UserID = @UserID;

	BEGIN TRANSACTION

    -- Insert statements for procedure here
	INSERT INTO Content (ContentTypeID, PartnerID, Name, [Description], Category, Keyword, EffectiveDate, ExpirationDate, Active, CreatedUserID, CreatedDate, UpdatedDate)
	Values (1, @PartnerID, @Title, @Description, @Category, @Keywords, @PublishDate, @ExpiryDate, @IsDeleted, @UserID, @CreatedOn, @LastChgDTM)

	if @@ERROR = 0
	BEGIN

		Select @NewID = SCOPE_IDENTITY();
		INSERT INTO ContentVideo (ContentID, [FileName], ImportFilePath, Height, Width, Duration, ThumbnailFileName, StillframeFileName)
		VALUES(@NewID, @ClipURL, @ClipURL, @Height, @Width, @Duration,@ThumbnailURL, @StillFrameURL)

	END
	if @@ERROR = 0
	BEGIN

		INSERT INTO VideosLEGACY(ContentID, NumberOfReviews, ClipPopularity, CandidateOfInterest, NumberOfPlays, SentToFreeWheel, FileExtension, IngestionStatusMessage)
		VALUES(@NewID, @NumberOfReviews, @ClipPopularity, @CandidateOfInterest, @NumberOfPlays, @SentToFreeWheel, @FileExtension, @IngestionStatusMessage)

	END

	if @@ERROR =0
		COMMIT TRANSACTION
	ELSE
		ROLLBACK TRANSACTION

	Select @NewID as ID

END
GO
