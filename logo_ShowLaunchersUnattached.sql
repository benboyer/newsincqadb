CREATE procedure [dbo].[logo_ShowLaunchersUnattached]
	@PartnerID int,
	@LogoTypeID int = null,
	@LogoID int = null
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'logo_ShowLaunchersUnattached'

--if @LogoTypeID is null and @LogoID is not null
--	set @LogoTypeID = (select logotypeid from Logo where LogoID = @LogoID)
--	 declare @partnerid int, @logoid int set @PartnerID = 538 set @logoid = 3293
--	 declare @partnerid int, @logoid int, @logotypeid int set @PartnerID = 468 set @logoid = 2304 set @logotypeid = null
	select	p.PartnerID, p.Name, p.TrackingGroup, l.LauncherID, l.Name LauncherName, lo.LogoID LogoIDAttached, lt.Name LogoType, lt.Description LogoDescription, lo.PartnerDefault, lo.LogoURL
	from	partner p
	join	Launcher l
	on		p.PartnerID = l.PartnerID
	left join	Partner_Logo pl
	on		p.PartnerID = pl.PartnerID
	and		l.LauncherID = pl.LauncherID
	left join	Logo lo
	on		pl.LogoID = lo.LogoID
	left join	LogoType lt
	on		lo.LogoTypeID = lt.LogoTypeID
	where	p.PartnerID = @PartnerID
	and		isnull(lt.Name, 'Header') = 'Header'
	and not	exists(select 1 from Partner_Logo plz join Logo lz on plz.LogoID = lz.LogoID
			where plz.PartnerID = @PartnerID
			and		plz.LauncherID = l.LauncherID
			and		lz.LogoTypeID = (select LogoTypeID from Logo where LogoID = @LogoID) )
	order by p.PartnerID, lt.LogoTypeID desc, lo.PartnerDefault desc, lo.LogoURL
	set nocount off
END
