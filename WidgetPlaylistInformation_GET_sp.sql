CREATE PROCEDURE [dbo].[WidgetPlaylistInformation_GET_sp]
	@WidgetID int
AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'WidgetPlaylistInformation_GET_sp'

	select pl.PlaylistID as ID, pl.Title as Name, Count(plv.VideoID) as NumVideos
	FROM Playlists pl
	INNER JOIN PlaylistVideos plv on pl.PlaylistID = plv.PlaylistID
	INNER JOIN WidgetPlaylist wpl on pl.PlaylistID = wpl.PlaylistId
	WHERE WidgetID = @WidgetID and pl.IsDeleted = 0
	GROUP BY pl.PlaylistID, pl.Title, wpl.SortOrder
	ORDER BY wpl.SortOrder asc
END
