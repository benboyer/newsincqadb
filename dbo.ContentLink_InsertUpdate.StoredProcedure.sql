USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ContentLink_InsertUpdate]
	-- declare
	@PartnerID int,
	@StoryGUID varchar(64),
	@VideoGUID varchar(64) = null,
	@StorageLocation varchar(200),
	@ContentID bigint = null,
	@Method int = 2,
	@Rank float = 0.5
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'ContentLink_InsertUpdate'
	-- Valid input check
	if @Method is null set @Method = 2
	if @Rank is null set @Rank = 0.5
	if @StoryGUID is null and @VideoGUID is null
	begin
		return
	end	
		-- PartnerID/StoryGUID is the primary entity, so updates are relevant to that combination
		if exists (select 1 from ContentLink where PartnerID = @PartnerID and StoryGUID = @StoryGUID)
		begin
			-- log the old values
			insert into ContentLinkLog(PartnerID,StoryGUID,VideoGUID,ContentID,StoryStorageLink,LastUpdated,UpdateMethod,MatchRank)
			select	PartnerID,StoryGUID,VideoGUID,ContentID,StoryStorageLink,LastUpdated,UpdateMethod,MatchRank
			from	ContentLink 
			where	PartnerID = @PartnerID 
			and		StoryGUID = @StoryGUID
			-- update settings
			update	cl
			set		cl.ContentID = isnull(@ContentID, cl.ContentID),
					cl.VideoGUID = @VideoGUID, --isnull(@VideoGUID, cl.VideoGUID),
					cl.StoryStorageLink = @StorageLocation, --isnull(@StorageLocation, cl.StoryStorageLink),
					cl.LastUpdated = GETDATE(),
					cl.UpdateMethod = @Method,
					cl.MatchRank = isnull(@Rank, cl.MatchRank)
			from	ContentLink cl
			where	cl.PartnerID = @PartnerID 
			and		cl.StoryGUID = @StoryGUID
			-- increment the updates counter
			update contentlink
			set		timesupdated = isnull(timesupdated, 0) + 1
			where	PartnerID = @PartnerID 
			and		StoryGUID = @StoryGUID

		end
		else
		begin
			insert into ContentLink(PartnerID,StoryGUID,VideoGUID,ContentID,StoryStorageLink,LastUpdated,UpdateMethod,MatchRank)
			select	@PartnerID, @StoryGUID, @VideoGUID, @ContentID, @StorageLocation, GETDATE(), @Method, @Rank
		end
		if @VideoGUID is not null
		begin
			Update	cl 
			set		cl.ContentID = c.ContentID, MatchRank = @Rank, cl.timesupdated = isnull(cl.timesupdated, 0) + 1
			from	contentlink cl
			join	FeedContent c
			on		cl.partnerid = @PartnerID
			and		right(cl.StoryGUID, 32) = @StoryGUID
			and		(cl.contentid is null or cl.contentid <> c.ContentID)
		end
	set nocount off

END
GO
