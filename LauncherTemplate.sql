TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	LauncherTemplate	LauncherTemplateID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	LauncherTemplate	LauncherTypesID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	LauncherTemplate	Name	12	varchar	60	60	None	None	1	None	None	12	None	60	3	YES	39
NewsincQA	dbo	LauncherTemplate	Description	12	varchar	200	200	None	None	1	None	None	12	None	200	4	YES	39
NewsincQA	dbo	LauncherTemplate	Height	4	int	10	4	0	10	1	None	None	4	None	None	5	YES	38
NewsincQA	dbo	LauncherTemplate	Width	4	int	10	4	0	10	1	None	None	4	None	None	6	YES	38
NewsincQA	dbo	LauncherTemplate	ImageLocation	12	varchar	120	120	None	None	1	None	None	12	None	120	7	YES	39
NewsincQA	dbo	LauncherTemplate	SortOrder	4	int	10	4	0	10	1	None	None	4	None	None	8	YES	38
NewsincQA	dbo	LauncherTemplate	Active	-7	bit	1	1	None	None	1	None	((1))	-7	None	None	9	YES	50

index_name	index_description	index_keys
IX_LauncherTemplate_sort	nonclustered, unique located on PRIMARY	SortOrder
PK_LauncherTemplate	clustered, unique, primary key located on PRIMARY	LauncherTemplateID
