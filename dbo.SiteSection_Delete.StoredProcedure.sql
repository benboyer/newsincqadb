USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SiteSection_Delete]
		-- declare
			@SectionID int
as
begin
	set fmtonly off
	SET NOCOUNT ON
	exec SprocTrackingUpdate 'SiteSection_Delete'
	delete from Section where SectionID = @sectionid

	set nocount off

END
GO
