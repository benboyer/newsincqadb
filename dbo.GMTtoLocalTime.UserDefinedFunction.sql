USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GMTtoLocalTime]
(
	@MyTime datetime
)
RETURNS datetime
AS
BEGIN
	declare @GMTtime datetime
	declare @diff int = (select DATEDIFF(HH, GETDATE(), GETUTCDATE()))

	-- Add the T-SQL statements to compute the return value here
	SELECT @GMTtime = DATEADD(HH, -@diff, @MyTime) 


	-- Return the result of the function
	RETURN @GMTtime
END
GO
