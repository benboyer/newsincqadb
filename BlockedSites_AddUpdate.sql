CREATE procedure [dbo].[BlockedSites_AddUpdate]
	--	declare
	@ID int = null,
	@domain varchar(1000),
	@user varchar(20),
	@active bit,
	@Comment varchar(200) = null
AS
	-- select @user = 'me', @active = 1, @domain = 'myfourthsite'
	-- select @user = 'A wonderful guy', @ID = 4, @active = 1, @domain = 'myFourthsite'
BEGIN
	set fmtonly OFF
	set nocount ON
	exec SprocTrackingUpdate 'BlockedSites_AddUpdate'

	if	@user is null
	begin
		select 'Nice Try.  Identify yourself!' as MessageBack
		return
	End

	if	ISNULL(@domain, '') = '' or len(@domain) < 5 or @active is null
	begin
		select 'Nice Try.  Use real values' as MessageBack
		return
	End

	if exists (select 1 from BlockedSites (nolock) where BlockedSitesID = @ID)
	begin

		insert into BlockedSitesLog(BlockedSitesID, LastAction, LastActionDateGMT, ActionTakenBy, FieldName, FieldValue)
		select	BlockedSitesID, 'Updated', GETUTCDATE(), @user, 'Domain', @domain
		from	BlockedSites (nolock)
		where	BlockedSitesID = @ID
		and		Domain <> @domain

		insert into BlockedSitesLog(BlockedSitesID, LastAction, LastActionDateGMT, ActionTakenBy, FieldName, FieldValue)
		select	BlockedSitesID, 'Updated', GETUTCDATE(), @user, 'Active', @active
		from	BlockedSites (nolock)
		where	BlockedSitesID = @ID
		and		Active <> @active

		insert into BlockedSitesLog(BlockedSitesID, LastAction, LastActionDateGMT, ActionTakenBy, FieldName, FieldValue)
		select	BlockedSitesID, 'Updated', GETUTCDATE(), @user, 'Comment', @Comment
		from	BlockedSites (nolock)
		where	BlockedSitesID = @ID
		and		isnull(Comment, '') <> isnull(@Comment, '')

		if exists	(select 1
					from	BlockedSites (nolock)
					where	BlockedSitesID = @ID
					and		(Domain <> @domain
							or
							Active <> @active)
							or
							Comment <> @Comment)
		begin
			update BlockedSites set Active = @active, DateUpdatedGMT = GETUTCDATE(), UpdatedBy = @user where BlockedSitesID = @ID and Active <> @active
			update BlockedSites set Domain = @domain, DateUpdatedGMT = GETUTCDATE(), UpdatedBy = @user  where BlockedSitesID = @ID and isnull(Domain, '') <> @domain
			update BlockedSites set Comment = @Comment, DateUpdatedGMT = GETUTCDATE(), UpdatedBy = @user  where BlockedSitesID = @ID and isnull(Comment, '') <> @Comment
		end

	end
	else
	begin
		if not exists (select 1
							from BlockedSites bs
							where	bs.Domain = @domain)
		begin
			insert into BlockedSites(Domain, Active, CreatedBy, DateCreatedGMT, Comment)
			select @domain, @active, @user, GETUTCDATE(), @Comment
			where not exists (select 1
							from BlockedSites bs
							where	bs.Domain = @domain)

			insert into BlockedSitesLog(BlockedSitesID, LastAction, LastActionDateGMT, ActionTakenBy)
			select BlockedSitesID, 'Created', GETUTCDATE(), @user
			from	BlockedSites
			where	Domain = @domain
			and		CreatedBy = @user
			and		Active = @active
		end
		else
		begin
			insert into BlockedSitesLog(BlockedSitesID, LastAction, LastActionDateGMT, ActionTakenBy)
			select BlockedSitesID, 'PreviouslyCreated', GETUTCDATE(), @user
			from	BlockedSites
			where	Domain = @domain
		end
	end
	set nocount OFF
END

