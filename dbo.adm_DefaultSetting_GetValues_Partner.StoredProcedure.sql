USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[adm_DefaultSetting_GetValues_Partner]
	-- declare
	@partnerid int = null
as
begin
	set nocount on
	set fmtonly off
	exec sproctrackingupdate 'adm_DefaultSetting_GetValues_Partner'
	select isnull(convert(varchar(20),b.PartnerID), 'All') PartnerID, a.entity, isnull(b.setting, a.Setting) Setting, isnull(b.value, a.value) Value-- , case when b.value is null then 'System' else 'Partner' end as ValueLevel
	from vw_sysdefaultsettings a
	left join (select * from vw_partnerdefaultsettings where partnerid = @partnerid ) b
	on a.entity = b.entity
	and	a.setting = b.setting
	set nocount off
end
GO
