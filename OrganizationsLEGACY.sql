TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	OrganizationsLEGACY	PartnerID	4	int	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	OrganizationsLEGACY	ContentItemId	4	int	10	4	0	10	1	None	None	4	None	None	2	YES	38
NewsincQA	dbo	OrganizationsLEGACY	Phone	-8	nchar	10	20	None	None	1	None	None	-8	None	20	3	YES	39
NewsincQA	dbo	OrganizationsLEGACY	OrganizationType	4	int	10	4	0	10	0	None	None	4	None	None	4	NO	56
NewsincQA	dbo	OrganizationsLEGACY	Acronym	-9	nvarchar	50	100	None	None	1	None	None	-9	None	100	5	YES	39
NewsincQA	dbo	OrganizationsLEGACY	CandidateOfficePlaceholder	-9	nvarchar	100	200	None	None	1	None	None	-9	None	200	6	YES	39
NewsincQA	dbo	OrganizationsLEGACY	AllowUploadingVideo	-7	bit	1	1	None	None	0	None	None	-7	None	None	7	NO	50
NewsincQA	dbo	OrganizationsLEGACY	LogoUrl	-9	nvarchar	1000	2000	None	None	1	None	None	-9	None	2000	8	YES	39
NewsincQA	dbo	OrganizationsLEGACY	IsMediaSource	-7	bit	1	1	None	None	1	None	None	-7	None	None	9	YES	50
NewsincQA	dbo	OrganizationsLEGACY	IsContentPrivate	-7	bit	1	1	None	None	0	None	None	-7	None	None	10	NO	50

index_name	index_description	index_keys
PK_OrganizationsLEGACY	clustered, unique, primary key located on PRIMARY	PartnerID
