TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	LandingURL	LandingURLID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	LandingURL	LandingURL	-1	text	2147483647	2147483647	None	None	1	None	None	-1	None	2147483647	2	YES	39
NewsincQA	dbo	LandingURL	LandingURLTypeID	4	int	10	4	0	10	1	None	None	4	None	None	3	YES	38
NewsincQA	dbo	LandingURL	Active	-7	bit	1	1	None	None	1	None	None	-7	None	None	4	YES	50

index_name	index_description	index_keys
IX_LandingURL_Active	nonclustered located on PRIMARY	Active
PK_LandingURL	clustered, unique, primary key located on PRIMARY	LandingURLID
