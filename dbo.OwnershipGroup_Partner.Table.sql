USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OwnershipGroup_Partner](
	[OwnershipGroupID] [int] NOT NULL,
	[PartnerID] [int] NOT NULL,
	[OwnershipAdUnitName] [varchar](200) NULL,
	[TrackingGroupAdUnitName] [varchar](200) NULL,
 CONSTRAINT [PK_OwnershipGroup_Partner] PRIMARY KEY CLUSTERED 
(
	[OwnershipGroupID] ASC,
	[PartnerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
