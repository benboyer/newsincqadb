TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	ContentVideo	ContentID	4	int	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	ContentVideo	GUID	-9	nvarchar	250	500	None	None	0	None	None	-9	None	500	2	NO	39
NewsincQA	dbo	ContentVideo	ContentVideoImportStatusID	5	smallint	5	2	0	10	0	None	None	5	None	None	3	NO	52
NewsincQA	dbo	ContentVideo	FileName	-9	nvarchar	500	1000	None	None	1	None	None	-9	None	1000	4	YES	39
NewsincQA	dbo	ContentVideo	ImportFilePath	-9	nvarchar	1000	2000	None	None	0	None	None	-9	None	2000	5	NO	39
NewsincQA	dbo	ContentVideo	FileSizeBytes	-5	bigint	19	8	0	10	1	None	None	-5	None	None	6	YES	108
NewsincQA	dbo	ContentVideo	Height	3	decimal	10	12	2	10	1	None	None	3	None	None	7	YES	106
NewsincQA	dbo	ContentVideo	Width	3	decimal	10	12	2	10	1	None	None	3	None	None	8	YES	106
NewsincQA	dbo	ContentVideo	Duration	3	decimal	10	12	2	10	1	None	None	3	None	None	9	YES	106
NewsincQA	dbo	ContentVideo	ThumbnailImportFilePath	-9	nvarchar	1000	2000	None	None	1	None	None	-9	None	2000	10	YES	39
NewsincQA	dbo	ContentVideo	ThumbnailFileName	-9	nvarchar	500	1000	None	None	1	None	None	-9	None	1000	11	YES	39
NewsincQA	dbo	ContentVideo	StillframeImportFilePath	-9	nvarchar	1000	2000	None	None	1	None	None	-9	None	2000	12	YES	39
NewsincQA	dbo	ContentVideo	StillframeFileName	-9	nvarchar	500	1000	None	None	1	None	None	-9	None	1000	13	YES	39
NewsincQA	dbo	ContentVideo	CopyFileMessage	12	varchar	500	500	None	None	1	None	None	12	None	500	14	YES	39

index_name	index_description	index_keys
IX_ContentVideo_ImpStatusID	nonclustered located on PRIMARY	ContentVideoImportStatusID
IX_CVID_GUID	nonclustered located on PRIMARY	GUID
NDX_CV_FN	nonclustered located on PRIMARY	FileName
PK_ContentVideo	clustered, unique, primary key located on PRIMARY	ContentID
