USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Launcher_Playlist_DeletePlaylists]
	@LauncherID int

AS
BEGIN
	set fmtonly off
	SET NOCOUNT ON
	exec SprocTrackingUpdate 'Launcher_Playlist_DeletePlaylists'

	DELETE
	from	Launcher_Playlist
	WHERE	LauncherID = @LauncherID

	SET NOCOUNT OFF
END
GO
