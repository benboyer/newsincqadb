TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	ContentSource	ContentSourceID	5	smallint identity	5	2	0	10	0	None	None	5	None	None	1	NO	52
NewsincQA	dbo	ContentSource	Name	-9	nvarchar	50	100	None	None	0	None	None	-9	None	100	2	NO	39

index_name	index_description	index_keys
PK_ContentSource	clustered, unique, primary key located on PRIMARY	ContentSourceID
