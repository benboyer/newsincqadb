USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_GetContentCreatedByDateRange]
	--declare 
	@startdtm datetime = null,  
	@enddtm datetime = null 
as	
	if @startdtm is null
	begin
		set @startdtm = (select dateadd(dd, -10, getdate()))
		set @enddtm = (select dateadd(dd, -3, getdate()))
	end
-- select @startdtm, @enddtm

SELECT     distinct c.partnerid, c.ContentID, c.CreatedDate, c.UpdatedDate, c.Name--, c.Description, at.Name AssetType, a.FilePath, a.Filename-- ISNULL(a.Filename, cv.FileName) AS Filename
			--, cv.FileName LegacyFileName, cv.ThumbnailFileName LegacyThumbnaileFileName, cv.StillframeFileName LegacyStillframeFileName, cv.ImportFilePath LegacyImportFilePath, cv.ThumbnailImportFilePath LegacyThumbnailImportFilePath, cv.StillframeImportFilePath LegacyStillframeImportFilePath
FROM		dbo.[Content] c (nolock)
LEFT JOIN	dbo.ContentVideo cv  (nolock)
ON			c.ContentID = cv.ContentID 
LEFT JOIN	dbo.Content_Asset ca  (nolock)
ON			c.ContentID = ca.ContentID 
left join dbo.AssetType at
on			ca.AssetTypeID = at.AssetTypeID
LEFT JOIN	dbo.Asset a  (nolock)
ON			ca.AssetID = a.AssetID
where		ISNULL(c.updateddate, c.createddate) between @startdtm and @enddtm
GO
