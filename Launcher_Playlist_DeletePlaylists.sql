create PROCEDURE [dbo].[Launcher_Playlist_DeletePlaylists]
	@LauncherID int

AS
BEGIN
	set fmtonly off
	SET NOCOUNT ON
	exec SprocTrackingUpdate 'Launcher_Playlist_DeletePlaylists'

	DELETE
	from	Launcher_Playlist
	WHERE	LauncherID = @LauncherID

	SET NOCOUNT OFF
END
