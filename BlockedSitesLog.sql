TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	BlockedSitesLog	BlockedSitesLogID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	BlockedSitesLog	BlockedSitesID	4	int	10	4	0	10	1	None	None	4	None	None	2	YES	38
NewsincQA	dbo	BlockedSitesLog	LastAction	12	varchar	20	20	None	None	1	None	None	12	None	20	3	YES	39
NewsincQA	dbo	BlockedSitesLog	LastActionDateGMT	11	datetime	23	16	3	None	1	None	None	9	3	None	4	YES	111
NewsincQA	dbo	BlockedSitesLog	ActionTakenBy	12	varchar	60	60	None	None	1	None	None	12	None	60	5	YES	39
NewsincQA	dbo	BlockedSitesLog	FieldName	12	varchar	20	20	None	None	1	None	None	12	None	20	6	YES	39
NewsincQA	dbo	BlockedSitesLog	FieldValue	12	varchar	1000	1000	None	None	1	None	None	12	None	1000	7	YES	39

index_name	index_description	index_keys
IX_BlockedSitesID	nonclustered located on PRIMARY	BlockedSitesID
PK__BlockedS__EFC17B9C74FF1B0F	clustered, unique, primary key located on PRIMARY	BlockedSitesLogID
