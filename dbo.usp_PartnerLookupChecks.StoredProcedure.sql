USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_PartnerLookupChecks]
-- declare
	@Name varchar(60),
	@DPID bigint,
	@Constant varchar(60) = null
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'usp_PartnerLookupChecks'
	--set @Name = replace(@Name, CHAR(39), '/'+ CHAR(39))
	set @Name = LTRIM(rtrim(@Name))
	set @DPID = LTRIM(rtrim(@DPID))
	set @Constant = LTRIM(rtrim(@constant))

	--select @name = 'Suburban Newspapers Inc.', @DPID = 90974, @constant = 'suburbannews'
	declare @partnerid int,
			@PartnerCount int,
			@PartnerLookupCount int,
			@sql varchar(max),
			@dpidcount int,
			@dpidname varchar(max)
	set @PartnerCount = (Select COUNT(*) from partner where Name = @Name)
	set @PartnerLookupCount = (select Count(*) from PartnerLookup where Name like '' + @Name + '' or PartnerID like CONVERT(varchar(10), @DPID))
	set @partnerid = (Select top 1 PartnerID from partner where Name = @Name)

	if @PartnerCount = 1 and @PartnerLookupCount = 0
	
	begin
		set @sql = 'exec usp_partnerlookupadditions ' + '''' + @Name + '''' + ', ' + CONVERT(varchar(10), @DPID) + ', ' + '''' + @Constant + '''' + '
		'
		set @sql = @sql + 'UPDATE partner set TrackingGroup = 	' + CONVERT(varchar(10), @DPID) + ' where partnerid = ' + CONVERT(varchar(10), @partnerid) + '
			and isnull(trackinggroup, 0) = 0
			'
		exec (@sql)
		select 'Added: ' + @Name  as Messageback
		-- select 'exec usp_partnerlookupchecks ' + '''' + + '''' + ', , ' + '''' + ''''
	end
	else
	begin
		set @dpidcount = (select COUNT(*) from partner where TrackingGroup = @DPID)
		if @dpidcount > 0 set @dpidname = (select top 1 name from mTrackingGroup where TrackingGroup = @DPID)
		set @sql = 'Something is wrong. There are ' + CONVERT(varchar(10), @partnercount) + ' partners with this name, '
				+ case when @dpidcount > 0 then 'and ' + convert(varchar(3), @dpidcount) + ' partner has this DPID.  A partner name with this DPID is: ' + @dpidname + '. 'else '' end
				+ 'There are ' + CONVERT(varchar(10), isnull(@partnerlookupcount, 0)) + ' records for this DPID in PartnerLookup.'
		select @sql
		--Select * from partner where Name like '%' + @Name + '%' or TrackingGroup like CONVERT(varchar(10), @DPID)
		--select * from PartnerLookup where Name like '' + @Name + '' or PartnerID like CONVERT(varchar(10), @DPID)
		--select 'exec usp_partnerlookupchecks ' + '''' + + '''' + ', , ' + '''' + ''''
	end
	set nocount off
END
GO
