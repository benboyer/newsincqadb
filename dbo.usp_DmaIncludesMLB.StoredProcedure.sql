USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_DmaIncludesMLB]
	-- declare
	@contentid bigint
as
BEGIN
	set nocount ON
	set fmtonly OFF
	exec sproctrackingupdate 'usp_DmaIncludesMLB'

	-- declare @contentid bigint = 24746543 -- select top 100 * from content where partnerid = 1882 order by contentid desc
	if (select isnull(category, '') from Content where ContentID = @contentid) = ''
	begin
		return
	end
	declare @cats varchar(200)
	set @cats = (select category from Content where ContentID = @contentid)
		--	select * from dbo.fn_Rows_fmCommaSeparated(@cats)
	select distinct c.ContentID, pd.PartnerID
	-- declare @contentid bigint = 24341292 select *
	from	(select * from Content (nolock) where ContentID = @contentid) c,
			(select Value from dbo.fn_Rows_fmCommaSeparated(@cats) where isnumeric(value) = 1)  d
	join	DMA dm
	on		d.Value = dm.DMACode
	join	Partner_DMA pd
	on		dm.DMACode = pd.DMACode

	set nocount off
END
GO
