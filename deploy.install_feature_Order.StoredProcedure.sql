USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [deploy].[install_feature_Order]
	--	declare 
	@FeatureOrder int -- = 8
as
BEGIN
	-- select * from deploy.schema_feature
	-- declare @featureorder int = 17 
	-- declare @marker int = 17
	--declare @name varchar(255)
	--declare @source_code varchar(max)
	declare @marker int, @sql varchar(max)
	declare @installme table (forder int, type varchar(20), name varchar(60), schem varchar(20))

	IF (select 1 from deploy.installed_features where feature_order = @FeatureOrder) > 0
	BEGIN
			select feature_name + ', feature_order ' + CONVERT(varchar(10), feature_order) + ', has already been installed.'
			-- select *
			from	deploy.installed_features
			where	feature_order = @FeatureOrder
			RETURN
	END
	
	insert into @installme (forder, type, name, schem)
	-- declare @featureorder int = 17
	select feature_order, feature_type, feature_name, for_schema 
	from	deploy.uninstalled_features
	where	feature_order = @FeatureOrder
	--and		completed_on is null
	--and		not exists (select 1 from deploy.uninstalled_dependencies where feature_name = @name)
	--select * from @installme



	WHILE (select COUNT(*) from @installme) > 0
	BEGIN
		select @marker = (select MIN(forder) from @installme)
			-- declare @name varchar(60) = 'ConfigLookup'
		IF exists (select 1	-- select *
				from	deploy.uninstalled_dependencies 
				where	feature_order = @marker)
		BEGIN
				select feature_name + ' step ' + CONVERT(varchar(10), feature_order) + ' depends upon ' + requires_name + ' step ' + convert(varchar(10), requires_order) + '.  Please  install it first.'
				-- select *
				from	deploy.uninstalled_dependencies 
				where	feature_order = @marker
				RETURN
		END
--			if (select 1 from deploy.uninstalled_features where feature_order = @FeatureOrder) is not null
--			BEGIN
		select @sql = (select installation_Code from deploy.uninstalled_features where feature_order = @marker)
				--select 'installation code is: ' + @sql
		if 	(select COUNT(*)-- s.name, sf.for_schema, m.definition, sf.installation_code, sf.for_schema, REPLACE(REPLACE(REPLACE(sf.installation_code, 'Create View ', 'ALTER View '), 'Create Function ', 'ALTER Function '), 'Create Procedure ', 'ALTER Procedure ')
			--update	sf set		sf.installation_code = REPLACE(REPLACE(REPLACE(sf.installation_code, 'Create View ', 'Update View '), 'Create Function ', 'Update Function '), 'Create Procedure ', 'Update Procedure ')
			from	NewsincQA.sys.schemas s
			join	NewsincQA.sys.objects p 
			on		s.schema_id = p.schema_id 
			join	NewsincQA.sys.sql_modules m 
			on		p.object_id = m.object_id 
			join	(select * from deploy.schema_feature where feature_order = @FeatureOrder) sf 
			on		s.name = sf.for_schema and p.name = sf.feature_name
			join	@installme im
			on		s.name = im.schem
			and		p.name = im.name
			where	sf.installation_code <> m.definition
			and		((p.type = 'P' and sf.feature_type = 'procedure')
					or (p.type = 'V' and sf.feature_type = 'view')
					or (p.type = 'FN' and sf.feature_type = 'function'))) > 0
		BEGIN
			select @sql = REPLACE(REPLACE(REPLACE(@sql, 'Create View ', 'ALTER View '), 'Create Function ', 'ALTER Function '), 'Create Procedure ', 'ALTER Procedure ')
			--select @sql
		END
		if not exists ( -- declare @featureOrder int = 17
				select 1 from NewsincQA.sys.schemas s
				join	NewsincQA.sys.objects p 
				on		s.schema_id = p.schema_id 
				join	NewsincQA.sys.sql_modules m 
				on		p.object_id = m.object_id 
				join	(select * from deploy.schema_feature where feature_order = @FeatureOrder) sf 
				on		s.name = sf.for_schema and p.name = sf.feature_name
				join	@installme im
				on		sf.feature_order = im.forder
				where	sf.installation_code = m.definition
				and		((p.type = 'P' and sf.feature_type = 'procedure')
						or (p.type = 'V' and sf.feature_type = 'view')
						or (p.type = 'FN' and sf.feature_type = 'function')))
		BEGIN
			--select	'install stuff here'
			exec	(@sql)
		END
		else
		begin
			select 'Appears to be the same as the installed version'
		end
				-- mark complete
		IF @@ERROR = 0
		BEGIN
			--select 'i am updating schema_feature here'
			update deploy.schema_feature set completed_on = GETDATE() where feature_order = @marker
		END
--		end
		delete from @installme where forder = @marker
	END
END
GO
