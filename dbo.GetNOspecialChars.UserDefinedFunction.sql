USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION [dbo].[GetNOspecialChars]
(
	@String varchar(1500)
)
RETURNS varchar(1500)
AS

BEGIN
	-- declare @string varchar(1500) = 'e! 79087u)(*_+++online'
	DECLARE @ResultVar varchar(1500)
	declare @len int, @pos int, @curchar char, @nameout varchar(200) = ''
	set @len = LEN(@String)
	set @pos = 1
	--  select @name, @len, @pos
	while @pos < @len + 1
	begin
		set @curchar = (SUBSTRING(@String, @pos, 1))
		if  ascii(@curchar) between 48 and 57
			or
			ascii(@curchar) between 65 and 90
			or
			ascii(@curchar) between 97 and 122
			set @nameout = @nameout + @curchar
		set @pos = @pos + 1
	end
	-- select @nameout
	RETURN @nameout

END
GO
