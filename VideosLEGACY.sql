TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	VideosLEGACY	ContentID	4	int	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	VideosLEGACY	ContentItemID	4	int	10	4	0	10	1	None	None	4	None	None	2	YES	38
NewsincQA	dbo	VideosLEGACY	NumberOfReviews	4	int	10	4	0	10	1	None	None	4	None	None	3	YES	38
NewsincQA	dbo	VideosLEGACY	Duration	6	float	15	8	None	10	1	None	None	6	None	None	4	YES	109
NewsincQA	dbo	VideosLEGACY	ClipPopularity	6	float	15	8	None	10	1	None	None	6	None	None	5	YES	109
NewsincQA	dbo	VideosLEGACY	IsAdvertisement	-7	bit	1	1	None	None	1	None	None	-7	None	None	6	YES	50
NewsincQA	dbo	VideosLEGACY	CandidateOfInterest	6	float	15	8	None	10	1	None	None	6	None	None	7	YES	109
NewsincQA	dbo	VideosLEGACY	NumberOfPlays	4	int	10	4	0	10	1	None	None	4	None	None	8	YES	38
NewsincQA	dbo	VideosLEGACY	SentToFreeWheel	-7	bit	1	1	None	None	0	None	None	-7	None	None	9	NO	50
NewsincQA	dbo	VideosLEGACY	FileExtension	-9	nvarchar	5	10	None	None	1	None	None	-9	None	10	10	YES	39
NewsincQA	dbo	VideosLEGACY	IngestionStatusMessage	-9	nvarchar	2000	4000	None	None	1	None	None	-9	None	4000	11	YES	39
NewsincQA	dbo	VideosLEGACY	IsDeleted	-7	bit	1	1	None	None	1	None	None	-7	None	None	12	YES	50

index_name	index_description	index_keys
PK_VideosLEGACY	clustered, unique, primary key located on PRIMARY	ContentID
