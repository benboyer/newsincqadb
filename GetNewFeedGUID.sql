CREATE PROCEDURE [dbo].[GetNewFeedGUID]
	@PartnerID int = null
AS
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetNewFeedGUID'
	if	@partnerID is not null
	begin
		if (select name from partner where PartnerID = @partnerid) is not null
		begin
			update Partner
			set		FeedGUID = NEWID()
			where	PartnerID = @PartnerID
			Select FeedGUID
			from	Partner
			where	PartnerID = @PartnerID
		end
	end
	set nocount off
END
