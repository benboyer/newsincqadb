CREATE FUNCTION [dbo].[GetPathToStreamAsset]
(
		@assetid bigint
)

RETURNS varchar(500)
AS
BEGIN
	--select getdate() declare	@assetid bigint = 4892208 -- 4397250 -- 4397259
	declare @returnvar varchar(500),
		@ContentID bigint,
		@cContentID varchar(20),
		@templateid int,
		@ext varchar(10),
		@partnerid int,
		@cpartnerid varchar(20),
		@cassetid varchar(20)

	set		@ContentID = (select contentid from Content_Asset (nolock) where AssetID = @assetid)
	set		@partnerid = (select partnerid from Content (nolock) where ContentID = @ContentID)
	set		@ext = (select RIGHT(filename, (CHARINDEX('.', reverse(filename)) - 1)) from Asset (nolock) where AssetID = @assetid)
	set		@cContentID = CONVERT(varchar(20), @contentid)
	set		@cpartnerid = CONVERT(varchar(20), @partnerid)
	set		@cassetid = CONVERT(varchar(20), @assetid)

	if @ext = 'flv'
	begin
		set @templateid = (select ISNULL((select Value from vw_partnerDefaultSettings (nolock) where Entity = 'PlayerServiceDeliveryTemplate' and Setting = 'FLVstream' and PartnerID = @cpartnerid), (select Value from vw_sysDefaultSettings (nolock) where Entity = 'PlayerServiceDeliveryTemplate' and Setting = 'FLVstream')))
		if @templateid is not null
			set @returnvar = (select REPLACE(replace(replace(urlline, '{assetid}', @cassetid), '{partnerid}', @cpartnerid), '{contentid}', @cContentID) from templates (nolock) where TemplatesID = @templateid)
	end
	if @ext = 'mp4'
	begin
		set @templateid = (select ISNULL((select Value from vw_partnerDefaultSettings (nolock) where Entity = 'PlayerServiceDeliveryTemplate' and Setting = 'mp4stream' and PartnerID = @partnerid), (select Value from vw_sysDefaultSettings (nolock) where Entity = 'PlayerServiceDeliveryTemplate' and Setting = 'mp4stream')))
		if @templateid is not null
			set @returnvar = (select REPLACE(replace(replace(urlline, '{assetid}', @cassetid), '{partnerid}', @cpartnerid) , '{contentid}', @cContentID) from templates where TemplatesID = @templateid)
	end
		if @returnvar is null
			set @returnvar = (select case when isnull(filepath, '') <> '' then FilePath + '/' end  + Filename from Asset where AssetID = @assetid)
		if @ext not in ('txt') and @ext <> ''
		begin
			if LEFT(@returnvar, 7) <> 'http://' and LEFT(@returnvar, 7) <> 'rtmp://'
				set @returnvar = 'http://'+@returnvar
		end
	-- select @partnerid, @returnvar, getdate()
	RETURN @returnvar

END
