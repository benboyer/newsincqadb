USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adsrv_GetNewUpdatedContent_rev] @datefrom datetime = null,    
    @dateto datetime = null,    
    @numbertoreturn int = 500, -- cap the superset size by adding this to a Top statement    
    @numberinpage int = 100, -- the number to return for a page    
 @page int = 0,    -- page number    
    @Updates bit = 0   -- 1=only consider updateddate, 0=consider createddate and updateddate    
AS    
 -- select @datefrom = '2013-09-26 11:30:00'--, @dateto = '2013-05-10 12:00:00'    
BEGIN    
 SET FMTONLY OFF    
 SET NOCOUNT ON;    
 exec SprocTrackingUpdate 'adsrv_GetNewUpdatedContent'    
 if @numbertoreturn > 500 set @numbertoreturn = 500    
 declare @SortOrder varchar(10) = 'DESC'    
    
  -- init load to google
  if ( @datefrom is null and @dateto is null )
  begin
  set @datefrom = dateadd(year,-10,GETDATE())
  set @dateto = GETDATE()
  end
  
-- if @page > 0 set @page = @page -1    
  -- select @sortorder    
if @datefrom is null or @dateto is null    
begin    
 if @datefrom is null and @dateto is null    
 begin    
  set @dateto = GETDATE()    
  set @datefrom = DATEADD(hh, -1, @dateto)  
  -- ACM  
  -- set @datefrom = DATEADD(year, -4, @dateto)    
 end    
 else    
 begin    
  if @dateto is null and @datefrom is not null    
  begin    
   set @dateto = dateadd(hh, 1, @datefrom)    
  end    
  else    
  begin    
   if @dateto is not null and @datefrom is null    
   set @datefrom = DATEADD(hh, -1, @dateto)  
   -- ACM  
   -- set @datefrom = DATEADD(year, -4, @dateto)    
  end    
 end    
end    
-- select @datefrom asFrom, @dateto asTo    
    
-- declare @updates bit = 0, @numbertoreturn int = 400, @numberinpage int = 100, @datefrom datetime = '2013-05-01 00:11:22', @dateto datetime = '2013-06-01 00:11:22', @page int = 1-- , @curURL varchar(200), @nextURL varchar(200)    
declare @offset int = @page, @resultset int = @numbertoreturn, @sendset int = @numberinpage, @TotalSet int, @curURL varchar(200), @nextURL varchar(200)    
declare @serviceURL varchar(100) = (select Value from vw_DefaultSettings where Entity = 'AdServer' and Setting = 'serviceURL' )    
-- declare @updates bit = 0, @numbertoreturn int = 400, @numberinpage int = 100, @datefrom datetime = '2013-05-01 00:11:22', @dateto datetime = '2013-06-01 00:11:22', @page int = 0    
create table #MyConRecord(id int not null identity(1,1) primary key, contentid bigint)    
insert into #MyConRecord(contentid)    
select ContentID    
 FROM Content    
 where ContentImportStatus = 5    
 and  ((@updates = 0 and (CreatedDate between @datefrom and @dateto or UpdatedDate  between @datefrom and @dateto))    
   or    
   (@Updates = 1 and UpdatedDate  between @datefrom and @dateto))    
 order by UpdatedDate desc, ContentID desc    
    
 set @TotalSet = (select COUNT(*) from #MyConRecord (nolock))    
    
 -- declare @sendset int = 12, @offset int = 0    
-- declare @updates bit = 0, @totalset int = 19566, @sendset int = 100, @datefrom datetime = '2013-05-01 00:11:22', @dateto datetime = '2013-06-01 00:11:22', @offset int =180    
    SELECT TOP(@sendset) -- ref.id,    
--   replace(@curURL, '{pageno}', CONVERT(varchar(10), @page)) link,     
   convert(varchar(20),@datefrom,20) + '/' +  convert(varchar(10),@offset) as link,    
   case when @TotalSet - (@offset * @sendset) < @sendset then null    
    else convert(varchar(20),@datefrom,20) + '/' + CONVERT(varchar(10), @offset+1)     
    end linkNext,     
   @sendset itemsPerPage,    
   (@offset * @sendset) + 1 startIndex, @TotalSet totalResults,     
   dbo.GetPathToStreamAsset(cav.assetid) content, dbo.GetMimeTypeToProgressiveAsset(cav.AssetID) mediatype, 'video' medium, avid.duration, c.Name title, c.description,     
   nc.NDNCategory category, c.Keyword keywords, /*c.effectivedate pubdate,*/    
   case c.Active when 1 then 'active' else 'inactive' end status, dbo.GetPathToServeAsset(ca.AssetID) thumbnail,     
   -- case c.Active when 1 then 'active' else case when c.isDeleted = 1 then 'deleted' else 'inactive' end end as status, dbo.GetPathToServeAsset(ca.AssetID) thumbnail,     
   a.Height ThumbnailHeight, a.Width ThumbnailWidth,    
   c.contentID, c.PartnerID owner, --, c.EffectiveDate, c.ExpirationDate, c.CreatedDate,     
   -- case when noads.contentid is null then 0 else 1 end as     
   convert(int, 1) as     
   monetizeable    
   ,c.effectivedate pubdate    
   ,avid.width videoWidth    
   ,avid.Height videoHeight    
   ,p.Name OwnerName    
 --  ,avid.Width videoWidth, avid.Height videoHeight    
 -- declare @sendset int = 12, @offset int = 0 select *  -- select top 10 * from asset    
 from #MyConRecord (nolock) ref    
 join Content (nolock) c    
 on  ref.contentid = c.ContentID    
 join Partner (nolock) p    
 on  c.PartnerID = p.PartnerID    
 join Content_Asset (nolock) ca    
 on  c.ContentID = ca.ContentID    
 and  3 = ca.AssetTypeID    
 join Asset (nolock) a    
 on  ca.assetid = a.AssetID    
 join Content_Asset (nolock) cav    
 on  c.ContentID = cav.ContentID    
 and  1 = cav.AssetTypeID    
 join Asset (nolock) avid    
 on  cav.AssetID = avid.AssetID    
 left join (select * from Playlist_Content_PartnerMap (nolock) where NDNCategoryID is not null) pcpm    
 on  c.PartnerID = pcpm.PartnerID    
 left join NDNCategory (nolock) nc    
 on  pcpm.NDNCategoryID = nc.NDNCategoryID    
 --left join archives.dbo.VideoIDsToFilterAds noads    
 --on  ref.contentid = noads.contentid    
 WHERE ref.id > (@offset * @sendset)    
 and  c.PartnerID not in (select partnerid from vw_partnerDefaultSettings (nolock) where Entity = 'BlockContentFromAdServer' and Value = 'true')    
    ORDER BY     
    ref.id    
 drop table #MyConRecord    
    
END
GO
