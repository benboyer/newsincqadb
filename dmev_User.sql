CREATE procedure dbo.dmev_User
	@PartnerID int = null,
	@UserID int = null,
	@CreatedDate datetime = null,
	@UpdatedDate datetime = null
as
select * 
from dme_User (nolock) u
where	u.PartnerID = ISNULL(@PartnerID, u.PartnerID)
and		u.UserID = ISNULL(@UserID, u.UserID)
and		u.CreatedDate >= ISNULL(@CreatedDate, u.CreatedDate)
and		u.UpdatedDate >= ISNULL(@UpdatedDate, u.UpdatedDate)
