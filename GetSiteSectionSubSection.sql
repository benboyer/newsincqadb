CREATE procedure [dbo].[GetSiteSectionSubSection]
	-- declare
	@sectionid int = null,
	@subsectionid int = null
as
begin
	--	select @sectionid = 1,  @subsectionid = 19
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetSiteSectionSubSection'
/*
We tried, we failed.
	if exists (select 1 from SectionSection_SectionSubSection where SectionSectionID = @sectionid and SectionSubSectionID = @subsectionid)
	begin
			select	b.SectionSubSectionID, b.Value, b.Description, a.AllowPageTypes
			from	SectionSection_SectionSubSection a
			join	SectionSubSection b
			on		a.SectionSubSectionID = b.SectionSubSectionID
			where	a.SectionSectionID = @sectionid
			and		a.SectionSubSectionID = @subsectionid
			return
	end

	if not exists (select 1 from SectionSection_SectionSubSection where SectionSectionID = @sectionid and SectionSubSectionID = @subsectionid)
		and
		exists (select 1 from SectionSection_SectionSubSection where SectionSectionID = @sectionid)
	begin
			select	b.SectionSubSectionID, b.Value, b.Description, a.AllowPageTypes
			from	SectionSection_SectionSubSection a
			join	SectionSubSection b
			on		a.SectionSubSectionID = b.SectionSubSectionID
			where	a.SectionSectionID = @sectionid
			--and		a.SectionSubSectionID = @subsectionid
			return
	end
*/

	if @sectionid is not null
	begin -- declare @sectionid int = 1
		select	b.SectionSubSectionID, b.Value, b.Description, a.AllowPageTypes
		from	SectionSection_SectionSubSection a
		join	SectionSubSection b
		on		a.SectionSubSectionID = b.SectionSubSectionID
		where	a.SectionSectionID = @sectionid
		order by case when a.SectionSectionID = 13 then 1 else 2 end, a.SectionSectionID, a.SectionSubSectionID
		return
	end

	-- declare @subsectionid int = 6
	select distinct SectionSubSectionID, Value, Description, CONVERT(int, null) as AllowPageTypes
	from SectionSubSection
	where SectionSubSectionID = ISNULL(@subsectionid, SectionSubSectionID)
	order by Value

	set nocount off
end
