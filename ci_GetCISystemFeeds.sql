create PROCEDURE dbo.ci_GetCISystemFeeds
	@ContentImportSystemID int
as
BEGIN
	set		fmtonly off
	set		nocount on
	exec	SprocTrackingUpdate 'ci_GetCISystemFeeds'
	select	FeedID, FeedPriorityID, PartnerID
	from	Feed 
	where	ContentImportSystemID = @ContentImportSystemID
	set		nocount off
END
