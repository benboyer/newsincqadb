USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[cr_VideoMetadataGet]
	-- DECLARE
		@VideoID bigint,
		@userid int
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'cr_VideoMetadataGet'
	-- declare @videoid int = 24747904, @userid int = 8482
	declare @thumbnailurl varchar(200),
			@stillframeurl varchar(200)
	set @stillframeurl = (select case
		when a.filepath is not null and LEFT(a.filepath, 7) = 'http://' and a.Filename is not null then a.FilePath + '/' + a.Filename + case when SUBSTRING(reverse(a.filename), 4,1) <> '.' then '.' + mt.FileExtension else '' end
		when a.filepath is not null and LEFT(a.filepath, 7) <> 'http://' and a.Filename is not null then 'http://' + a.FilePath + '/' + a.Filename + case when SUBSTRING(reverse(a.filename), 4,1) <> '.' then '.' + mt.FileExtension else '' end
		when a.filepath is null and LEFT(a.Filename, 7) = 'http://' then a.Filename + case when SUBSTRING(reverse(a.filename), 4,1) <> '.' then '.' + mt.FileExtension else '' end
		when a.filepath is null and LEFT(a.Filename, 7) <> 'http://' then 'http://' + a.Filename + case when SUBSTRING(reverse(a.filename), 4,1) <> '.' then '.' + mt.FileExtension else '' end
		end
	from Content_Asset ca
	join Asset a
	on	ca.AssetID = a.AssetID
	join mimetype mt
	on	a.mimetypeid = mt.mimetypeid
	where ca.ContentID = @VideoID
	and	ca.AssetTypeID = 2)

	set @thumbnailurl = (select case
		when a.filepath is not null and LEFT(a.filepath, 7) = 'http://' and a.Filename is not null then a.FilePath + '/' + a.Filename + case when SUBSTRING(reverse(a.filename), 4,1) <> '.' then '.' + mt.FileExtension else '' end
		when a.filepath is not null and LEFT(a.filepath, 7) <> 'http://' and a.Filename is not null then 'http://' + a.FilePath + '/' + a.Filename + case when SUBSTRING(reverse(a.filename), 4,1) <> '.' then '.' + mt.FileExtension else '' end
		when a.filepath is null and LEFT(a.Filename, 7) = 'http://' then a.Filename + case when SUBSTRING(reverse(a.filename), 4,1) <> '.' then '.' + mt.FileExtension else '' end
		when a.filepath is null and LEFT(a.Filename, 7) <> 'http://' then 'http://' + a.Filename  + case when SUBSTRING(reverse(a.filename), 4,1) <> '.' then '.' + mt.FileExtension else '' end
		end
	from Content_Asset ca
	join Asset a
	on	ca.AssetID = a.AssetID
	join mimetype mt
	on	a.mimetypeid = mt.mimetypeid
	where ca.ContentID = @VideoID
	and	ca.AssetTypeID = 3)

	select
		c.contentid VideoID,
		@Userid UserID,
		c.name Title,
		c.Description,
		c.effectivedate EventDate,
		c.createddate CreatedOn,
		c.effectivedate PublishDate,
		c.keyword Keywords,
		c.active IsEnabled,
		c.isdeleted IsDeleted,
		c.expirationdate ExpiryDate,
		c.category Category,
		c.OnlyOwnerView,
		c.updateddate LastChgDTM,
		@stillframeurl StillFrameURL,
		@thumbnailurl ThumbnailURL

		from Content c
		join Content_Asset ca
		on	c.ContentID = ca.ContentID
		join Asset a on ca.AssetID = a.AssetID
		join mimetype mt on a.mimetypeid = mt.mimetypeid
		where c.contentid = @videoid
		and	ca.assettypeid = 1
	set nocount off
END
GO
