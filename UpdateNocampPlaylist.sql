CREATE procedure dbo.UpdateNocampPlaylist
as
BEGIN
	update c set keyword = isnull(c.Keyword, '') + CASE when LEN(keyword) > 0 then ',' when Keyword is null then '' else '' end +  'ndn789',
		UpdatedDate = GETDATE(), CreatedDate = GETDATE()
	-- select c.keyword, *
	from Playlist_Content pc (nolock)
	join Content c
	on	pc.ContentID = c.ContentID
	where PlaylistID = 13377
	and		(c.Keyword not like '%ndn789%'
			or 
			c.Keyword is null)
END
