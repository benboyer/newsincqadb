TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	FeedFilter	FeedFilterID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	FeedFilter	FeedID	4	int	10	4	0	10	1	None	None	4	None	None	2	YES	38
NewsincQA	dbo	FeedFilter	Active	-7	bit	1	1	None	None	1	None	None	-7	None	None	3	YES	50
NewsincQA	dbo	FeedFilter	FilterField	12	varchar	60	60	None	None	1	None	None	12	None	60	4	YES	39
NewsincQA	dbo	FeedFilter	FieldValue	12	varchar	60	60	None	None	1	None	None	12	None	60	5	YES	39

index_name	index_description	index_keys
IX_FeedFilter_Feedid_active	nonclustered located on PRIMARY	FeedID, Active
PK_FeedFilter	clustered, unique, primary key located on PRIMARY	FeedFilterID
