USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[playlist_MostWatchedParams](
	[PartnerID] [bigint] NOT NULL,
	[PlaylistID] [bigint] NOT NULL,
	[LookbackHours] [int] NULL,
	[LookbackDuration] [int] NULL,
	[RefreshHours] [int] NULL,
	[MaxRows] [int] NULL,
	[lastrun] [datetime] NULL,
	[ProviderOrDistributor] [char](1) NULL,
	[isEnabled] [bit] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
