USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ads_PhrasesToBlockAds](
	[PhrasesToFilterAds] [int] IDENTITY(1,1) NOT NULL,
	[phrase] [varchar](200) NOT NULL,
	[filteraddeddate] [datetime] NOT NULL,
	[filteractive] [bit] NOT NULL,
	[updateddate] [datetime] NULL,
	[KeywordAddPhrase] [varchar](200) NULL,
	[AddUpdateUser] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PhrasesToFilterAds] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ads_PhrasesToBlockAds] ADD  DEFAULT ('admin') FOR [AddUpdateUser]
GO
