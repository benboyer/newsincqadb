USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetSingleContentAssetsMetadata]
--  declare
	@playlistid int,
	@ContentID bigint,
	@trackingGroup int = null,
	@devicetype int = 1
AS
BEGIN
	set FMTONLY OFF
	set nocount on
	exec SprocTrackingUpdate 'GetSingleContentAssetsMetadata'

	if @devicetype = 3
	begin
		exec ps_SingleContentAssetsMetadata @playlistid, @ContentID, @trackingGroup, @devicetype
		return
	end
	Declare	@now datetime,
			@iplaylistid int,
			@iContentID bigint,
			@itrackinggroup int,
			@idevicetype int
	set		@now = GETDATE()
	select	@iContentID = @ContentID, @itrackinggroup = @trackingGroup, @idevicetype = @devicetype, @iplaylistid = @playlistid

	exec GetSingleContent @icontentid, @itrackinggroup, @idevicetype, @iplaylistid
	/*
	if @playlistid = 0 and @trackinggroup = 0
	begin
		select	convert(int, @iplaylistid) as PlaylistID,
				c.ContentID, c.Name, c.Description, mt.TrackingGroup ContentPartnerID, convert(varchar(100), null) as VideoGroupName, a.Duration, c.EffectiveDate PubDate, c.Keyword, convert(varchar(120), null) Timeline,
				case	when at.Name like 'Video%' then 'src'
						--when at.Name like 'Thumb%' then 'thumbnail'
						--when at.Name like 'Still%' then 'stillFrame'
						-- when at.Name in ('ExternalContentID', 'ExternalImageURL') then at.Name
						else at.Name  -- 'unk'
				end as AssetType,
					case when at.Name like 'External%' then a.Filename
				else
					dbo.GetAssetDeliveryPath(mtt.FileExtension, c.partnerid, ca.contentid, ca.assetid)
				end as AssetLocation,
				mt.Name ProducerName, p.shortname ProducerNameAlt,  isnull(p.logourl, 'http://assets.newsinc.com/newsinconebyone.png') ProducerLogo, convert(int, 0) as ContentOrder, isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'news') ProducerCategory
		from	(select * from Content where ContentID = @ContentID) c
		join	mTrackingGroup (nolock) mt
		on		c.PartnerID = mt.partnerid
		join	Partner (nolock) p
		on		mt.partnerid = p.PartnerID
		join	Content_Asset (nolock) ca
		on		c.ContentID = ca.contentid
		join	AssetType (nolock) at
		on		ca.AssetTypeID = at.AssetTypeID
		join	Asset (nolock) a
		on		ca.AssetID = a.AssetID
		join	MimeType (nolock) mtt
		on		a.MimeTypeID = mtt.MimeTypeID
		left join	Playlist_Content_PartnerMap (nolock) pm  -- select * from playlist_content_partnermap where feedid is not null
		on			c.PartnerID = pm.PartnerID
		and			c.FeedID = pm.FeedID
		left join	NDNCategory nc
		on			pm.NDNCategoryID = nc.NDNCategoryID
	where	ca.TargetPlatformID = @devicetype
		-- and		c.Active = 1
		and
		ca.Active = 1
		and		a.Active = 1
		-- and		dbo.AllowedContentID(c.contentid, @itrackinggroup, @devicetype) = 1
		 order by case	when at.Name like 'Video%' then 'src'
						else at.Name  -- 'unk'
				end

	end

	ELSE
	begin
		select  -- distinct -- a.iscontentprivate, a.partnerid OwningPartnerid, pd.partnerid DistribPartnerid,
				--a.PlaylistID,
				convert(int, @iplaylistid) as PlaylistID,
				a.ContentID, a.Name, a.Description, a.TrackingGroup ContentPartnerID, a.VideoGroupName, a.Duration, a.PubDate, a.Keyword, convert(varchar(120), null) Timeline,
				case	when a.AssetTypeName like 'Video%' then 'src'
						--when a.AssetTypeName like 'Thumb%' then 'thumbnail'
						--when a.AssetTypeName like 'Still%' then 'stillFrame'
						--when a.AssetTypeName in ('ExternalContentID', 'ExternalImageURL') then a.AssetTypeName
						else a.AssetTypeName
				end as AssetType,
					case when a.assettypeName like 'External%' then a.Filename
				else
					a.DeliveryPath
				end as AssetLocation,
				a.ProducerName, a.ProducerNameAlt, a.logourl ProducerLogo, convert(int, 0) as ContentOrder, a.ProducerCategory

		from
			(select	distinct pt.partnerid, pt.TrackingGroup, pt.isContentPrivate, c.ContentID, c.Name,
					c.Description, c.EffectiveDate PubDate, null as [order], aa.FilePath, aa.Filename, aa.Duration, c.Keyword,
					null as VideoGroupName,at.Name AssetTypeName, mt.FileExtension, pt.logourl, pt.Name ProducerName, pt.shortname ProducerNameAlt, isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'news') ProducerCategory,
					dbo.GetAssetDeliveryPath(mt.FileExtension, c.partnerid, ca.contentid, ca.assetid) DeliveryPath
			from	(select * from Content where contentid = @iContentID) c
			join	Partner_TargetPlatformProfile ptp
			on		c.PartnerID = ptp.PartnerID
			and		@idevicetype = ptp.TargetPlatformID
			join	TargetPlatform_AssetType tpat
			on		ptp.TargetPlatformID = tpat.TargetPlatformID

			join	Content_Asset ca
			on		c.ContentID = ca.ContentID
			join	AssetType at
			on		tpat.AssetTypeID = at.AssetTypeID
			and		ca.AssetTypeID = at.AssetTypeID
			join	Asset aa
			on		ca.AssetID = aa.AssetID

			join	MimeType mt
			on		at.MimeTypeid = mt.MimeTypeID
			JOIN	partner pt (nolock)
			ON		c.PartnerID = pt.PartnerID
			left join	Playlist_Content_PartnerMap pm  -- select * from playlist_content_partnermap where feedid is not null
			on			c.PartnerID = pm.PartnerID
			and			c.FeedID = pm.FeedID
			left join	NDNCategory nc
			on			pm.NDNCategoryID = nc.NDNCategoryID
			where	/*ptp.isDistributionEnabled = 1
			and		c.Active = 1
			and		*/ca.active = 1
			and		aa.Active = 1
			-- and		isnull(aa.isDeleted, 0) = 0
			) a
		left	join partner pd
		on		@itrackinggroup = pd.TrackingGroup
		where		dbo.AllowedContentID(a.contentid, @itrackinggroup, @devicetype) = 1
		order by case	when a.AssetTypeName like 'Video%' then 'src'
						else a.AssetTypeName
				end
	END

	set nocount off
*/
END
GO
