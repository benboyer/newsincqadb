TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	ContentVideoImportException	FeedID	4	int	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	ContentVideoImportException	GUID	12	varchar	500	500	None	None	0	None	None	12	None	500	2	NO	39
NewsincQA	dbo	ContentVideoImportException	ExceptionArea	5	smallint	5	2	0	10	0	None	None	5	None	None	3	NO	52
NewsincQA	dbo	ContentVideoImportException	ContentID	4	int	10	4	0	10	1	None	None	4	None	None	4	YES	38
NewsincQA	dbo	ContentVideoImportException	DisplayErrorMessage	-9	nvarchar	1000	2000	None	None	1	None	None	-9	None	2000	5	YES	39
NewsincQA	dbo	ContentVideoImportException	Exception	-9	nvarchar	1000	2000	None	None	1	None	None	-9	None	2000	6	YES	39
NewsincQA	dbo	ContentVideoImportException	InnerException	-9	nvarchar	1000	2000	None	None	1	None	None	-9	None	2000	7	YES	39
NewsincQA	dbo	ContentVideoImportException	StackTrace	-9	nvarchar	2000	4000	None	None	1	None	None	-9	None	4000	8	YES	39

index_name	index_description	index_keys
PK_ContentVideoImportException	clustered, unique, primary key located on PRIMARY	FeedID, GUID
