USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[cr_GetSelectedPlaylistVideos]
	--	declare
	@userid int,
	@playlistid int
as
BEGIN
	set FMTONLY OFF
	set	nocount on

	--	declare @userid int = 7545, @playlistid int = 15781
	exec SprocTrackingUpdate 'cr_GetSelectedPlaylistVideos'
	declare @TG int = (select trackinggroup from mTrackingGroup (nolock) where partnerid = (select partnerid from [User](nolock) where UserID = @userid))
	if @TG is null set @TG = 10557
	declare @UserIsAdmin bit = (select convert(bit,isadmin) from NDNUsersLEGACY where UserID = @userid)
	select
		replace(case when a.FilePath is not null then a.FilePath + '/' + a.Filename
			when a.FilePath is null and a.Filename is not null then a.Filename
			else ''
			end, 'http://', '') as ClipURL,
         c.EffectiveDate PublishedDate,
         c.CreatedDate CreatedOn,
		replace(case when ath.FilePath is not null then ath.FilePath + '/' + ath.Filename
			when ath.FilePath is null and ath.Filename is not null then ath.Filename
			else ''
			end, 'http://', '') as ThumbnailURL,
         c.name Title,
         c.Description,
         isnull(a.Duration, 0) Duration,
         c.contentid VideoId,
         c.EffectiveDate EventDate,
         c.CreatedUserID  UserID,
         --@UserId UserID,
         pc.[order] Rank,
         null ClipPopularity,
         0 NumberOfPlays,
         p.Name Publisher,
         p.partnerid OrganizationId,
         c.expirationdate ExpiryDate,
         l.LogoUrl,
         C.OnlyOwnerView
		from	(select * from Playlist_content (nolock) where playlistid = @playlistid) pc
		join	content (nolock) c
		on		pc.contentid = c.contentid
		join	content_asset (nolock) ca
		on		c.contentid = ca.contentid
		and		ca.TargetPlatformID = 1
		join	asset (nolock) a
		on		ca.assetid = a.assetid
		and		ca.AssetTypeID = 1

		join	content_asset (nolock) ca2
		on		c.contentid = ca2.contentid
		and		ca.TargetPlatformID = 1
		join	Asset (nolock) ath
		on		ca2.AssetID = ath.AssetID
		and		ca2.AssetTypeID = 2
		join	partner (nolock) p
		on		c.partnerid = p.partnerid
		left join (select pl.PartnerID, lo.*
			from Partner_Logo (nolock) pl
			join logo (nolock) lo
			on	pl.logoid = lo.logoid
			where LogoTypeID = 5 and Active = 1) l
		on c.partnerid = l.partnerid
	where	pc.playlistid = @playlistid
	and		ca.targetplatformid = 1
	and		c.Active = 1
	and		(dbo.AllowedContentID(c.contentid, @TG, 1) = 1 or @UserIsAdmin = 1)
	order by pc.[Order]
	set	nocount off
END
GO
