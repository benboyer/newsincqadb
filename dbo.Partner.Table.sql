USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Partner](
	[PartnerID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ParentPartnerID] [int] NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ContactID] [int] NULL,
	[Website] [nvarchar](200) NULL,
	[StatusID] [smallint] NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[CreatedUserID] [int] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdateUserID] [int] NULL,
	[TrackingGroup] [int] NULL,
	[ShortName] [varchar](60) NULL,
	[LandingURL] [varchar](120) NULL,
	[LogoURL] [varchar](400) NULL,
	[isContentPrivate] [bit] NULL,
	[ZoneID] [int] NULL,
	[FeedGUID] [uniqueidentifier] NOT NULL,
	[isFeedGUIDActive] [bit] NULL,
	[DefaultEmbedWidth] [int] NULL,
	[DefaultEmbedRatio] [varchar](10) NULL,
	[DefaultEmbedHeight] [int] NULL,
	[isProvider] [bit] NOT NULL,
	[isDistributor] [bit] NOT NULL,
	[isMediaSource] [bit] NOT NULL,
	[isProgressive] [bit] NOT NULL,
	[UUID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Partner] PRIMARY KEY CLUSTERED 
(
	[PartnerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_Partner_private] ON [dbo].[Partner] 
(
	[isContentPrivate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Partner_TrkGrp] ON [dbo].[Partner] 
(
	[TrackingGroup] ASC,
	[ZoneID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDEX_fedguid] ON [dbo].[Partner] 
(
	[FeedGUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Partner]  WITH CHECK ADD  CONSTRAINT [FK_Partner_Partner] FOREIGN KEY([ParentPartnerID])
REFERENCES [dbo].[Partner] ([PartnerID])
GO
ALTER TABLE [dbo].[Partner] CHECK CONSTRAINT [FK_Partner_Partner]
GO
ALTER TABLE [dbo].[Partner] ADD  CONSTRAINT [DF_Partner_StatusID]  DEFAULT ((1)) FOR [StatusID]
GO
ALTER TABLE [dbo].[Partner] ADD  CONSTRAINT [DF__Partner__Created__5748DA5E]  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Partner] ADD  CONSTRAINT [DF_Partner_FeedGUID]  DEFAULT (newid()) FOR [FeedGUID]
GO
ALTER TABLE [dbo].[Partner] ADD  CONSTRAINT [DF_Partner_DefaultEmbedWidth]  DEFAULT ((425)) FOR [DefaultEmbedWidth]
GO
ALTER TABLE [dbo].[Partner] ADD  CONSTRAINT [DF_Partner_DefaultEmbedRatio]  DEFAULT ('custom') FOR [DefaultEmbedRatio]
GO
ALTER TABLE [dbo].[Partner] ADD  CONSTRAINT [DF_Partner_DefaultEmbedHeight]  DEFAULT ((320)) FOR [DefaultEmbedHeight]
GO
ALTER TABLE [dbo].[Partner] ADD  CONSTRAINT [DF_Partner_isProvider]  DEFAULT ((0)) FOR [isProvider]
GO
ALTER TABLE [dbo].[Partner] ADD  CONSTRAINT [DF_Partner_isDistributor]  DEFAULT ((1)) FOR [isDistributor]
GO
ALTER TABLE [dbo].[Partner] ADD  CONSTRAINT [DF_Partner_isMediaSource]  DEFAULT ((0)) FOR [isMediaSource]
GO
ALTER TABLE [dbo].[Partner] ADD  CONSTRAINT [DF_Partner_isProgressive]  DEFAULT ((0)) FOR [isProgressive]
GO
ALTER TABLE [dbo].[Partner] ADD  CONSTRAINT [DF_Partner_UUID]  DEFAULT (newid()) FOR [UUID]
GO
