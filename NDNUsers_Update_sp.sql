CREATE PROCEDURE [dbo].[NDNUsers_Update_sp]
		   @UserID int,
		   @UserTypeID tinyint = null,
		   @UserName nvarchar(150),
		   @EmailID nvarchar(50),
           @PasswordFormat int,
           @PasswordSalt varbinary(128),
           @Password varbinary(128),
           @ReceiveDailyIQEmail bit,
           @FirstName nvarchar(50)='',
           @MI nvarchar(60),
           @LastName nvarchar(50)='',
           @Gender char(1),
           @DOB datetime,
           @AddressID int,
           @OrganizationID int,
           @ContentItemID int,
           @PartnerId nvarchar(255),
           @FreeWheelType nvarchar(20),
           @Active bit,
           @IsFtpEnabled bit,
           @XSLT text,
           @AccountStatus int,
           @LastModified smalldatetime,
           @IsAdmin bit,
           @PasswordToken nvarchar(255),
           @AllowVideoUpload bit,
           @ProposedOrganization nvarchar(500),
           @EnableRssFeed bit,
           @LandingURL nvarchar(2083),
           @CustomRssFeedXSLT nvarchar(255)
AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'NDNUsers_Update_sp'

	UPDATE [User]
           Set [PartnerID] = @OrganizationID,
            [FirstName] = (Case When @FirstName IS NULL then '' else @FirstName End),
			[LastName] = (Case When @LastName IS NULL then '' else @LastName End),
			[Login] = @UserName,
			[Password] = @Password,
			[Active] = @Active,
			UpdatedDate = @LastModified,
			[PasswordFormat] = @PasswordFormat,
            [PasswordSalt] = @PasswordSalt,
            [PasswordToken] = @PasswordToken
            Where UserID = @UserID


        Update NDNUsersLEGACY
        SET
        EmailID = @EmailID,
        ReceiveDailyIQEmail = @ReceiveDailyIQEmail,
        MI = @MI,
        Gender = @Gender,
        DOB = @DOB,
        AddressID = @AddressID,
        ContentItemID = @ContentItemID,
        PartnerId = @PartnerId,
        FreeWheelType = @FreeWheelType,
        IsAdmin = @IsAdmin,
        UserTypeID = @UserTypeID,
        IsFtpEnabled = @IsFtpEnabled,
        XSLT = @XSLT,
        AccountStatus = @AccountStatus,
        AllowVideoUpload = @AllowVideoUpload,
        ProposedOrganization = @ProposedOrganization,
        EnableRssFeed = @EnableRssFeed,
        LandingURL = @LandingURL,
        CustomRssFeedXSLT = @CustomRssFeedXSLT
		Where UserID = @UserID



END
