TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Feed	FeedId	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	Feed	FeedUrl	-9	nvarchar	1000	2000	None	None	1	None	None	-9	None	2000	2	YES	39
NewsincQA	dbo	Feed	PartnerID	4	int	10	4	0	10	0	None	None	4	None	None	3	NO	56
NewsincQA	dbo	Feed	XSLT	-1	text	2147483647	2147483647	None	None	0	None	None	-1	None	2147483647	4	NO	35
NewsincQA	dbo	Feed	UploadXSLT	-9	nvarchar	1000	2000	None	None	1	None	None	-9	None	2000	5	YES	39
NewsincQA	dbo	Feed	Active	-7	bit	1	1	None	None	0	None	((1))	-7	None	None	6	NO	50
NewsincQA	dbo	Feed	UploadActive	-7	bit	1	1	None	None	1	None	None	-7	None	None	7	YES	50
NewsincQA	dbo	Feed	UserName	-9	nvarchar	255	510	None	None	1	None	None	-9	None	510	8	YES	39
NewsincQA	dbo	Feed	Password	-9	nvarchar	255	510	None	None	1	None	None	-9	None	510	9	YES	39
NewsincQA	dbo	Feed	NotifyEmail	12	varchar	100	100	None	None	1	None	None	12	None	100	10	YES	39
NewsincQA	dbo	Feed	SMILSXLT	-8	nchar	200	400	None	None	1	None	None	-8	None	400	11	YES	39
NewsincQA	dbo	Feed	SourceID	5	smallint	5	2	0	10	0	None	None	5	None	None	12	NO	52
NewsincQA	dbo	Feed	FilterIncludes	-7	bit	1	1	None	None	1	None	None	-7	None	None	13	YES	50
NewsincQA	dbo	Feed	FilterValues	-3	varbinary	150	150	None	None	1	None	None	-3	None	150	14	YES	37
NewsincQA	dbo	Feed	GetInterval	12	varchar	10	10	None	None	1	None	None	12	None	10	15	YES	39
NewsincQA	dbo	Feed	IntervalValue	4	int	10	4	0	10	1	None	None	4	None	None	16	YES	38
NewsincQA	dbo	Feed	TranscriptXslt	-9	nvarchar	1000	2000	None	None	1	None	None	-9	None	2000	17	YES	39
NewsincQA	dbo	Feed	MaxUpdates	4	int	10	4	0	10	0	None	((5))	4	None	None	18	NO	56
NewsincQA	dbo	Feed	MaxUpdatesExceptions	12	varchar	200	200	None	None	1	None	None	12	None	200	19	YES	39
NewsincQA	dbo	Feed	MaxDownloads	4	int	10	4	0	10	0	None	((3))	4	None	None	20	NO	56
NewsincQA	dbo	Feed	ContentImportSystemID	4	int	10	4	0	10	0	None	((0))	4	None	None	21	NO	56
NewsincQA	dbo	Feed	FeedPriorityID	4	int	10	4	0	10	0	None	((2))	4	None	None	22	NO	56
NewsincQA	dbo	Feed	MaxItems	4	int	10	4	0	10	1	None	None	4	None	None	23	YES	38
NewsincQA	dbo	Feed	ControlDate	11	datetime	23	16	3	None	1	None	None	9	3	None	24	YES	111
NewsincQA	dbo	Feed	ControlDateEnd	11	datetime	23	16	3	None	1	None	None	9	3	None	25	YES	111

index_name	index_description	index_keys
IX_FeedPID	nonclustered located on PRIMARY	PartnerID
PK_RSSFeeds	clustered, unique, primary key located on PRIMARY	FeedId
