USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetFeedFilters]
 -- declare
	@feedid int
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetFeedFilters'

	if exists (select 1 from FeedFilter where FeedID = @feedid and Active = 1)
	begin
	declare @fieldlist varchar(max) = '', @include bit, @field varchar(60), @f1 varchar(max)

		select	distinct ff.FilterField, ff.FieldValue, f.FilterIncludes
		into #flist
		from	feed f
		join	FeedFilter ff
		on		f.FeedId = ff.FeedID
		where	f.FeedId = @feedid
		and		ff.Active = 1

		set @field = (select distinct FilterField from #flist)
		set @include = (select distinct Filterincludes from #flist)

		while (select COUNT(*) from #flist) > 0
		begin
			set @f1 = (select top 1 FieldValue from #flist)
			set @fieldlist = @fieldlist + @f1
			delete from #flist where FieldValue = @f1
			if (select COUNT(*) from #flist) > 0
				set @fieldlist = @fieldlist + ','
		end
		select @field as FilterField, @fieldlist as FieldValue, @include as FilterIncludes
		drop table #flist
	end
	else
	begin
		select convert(varchar(60), 'none') as FilterField, convert(varchar(60), 'none') as FieldValue, CONVERT(bit, 0) as FilterIncludes
	end

	set nocount off
END
GO
