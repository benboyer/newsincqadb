USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[AllowedLauncherContent]
(
	 @LauncherID bigint,
	 @trackinggroup int
)
RETURNS bit
AS
BEGIN
--	This funtion should not be used.  Instead use AllowedLauncherContentP.  The correct methodology check content-provider's PartnerID in the l-c-e table, NOT the distributor's!
	DECLARE @allowed bit
	set @Allowed = isnull((select top 1 0 -- select top 100 *
		from	Launcher_ContentExclusion (nolock) lce
		where	lce.LauncherID = @LauncherID
		and		lce.PartnerID = (select PartnerID from mTrackingGroup (nolock) where TrackingGroup = @trackinggroup)), 1)

	RETURN @Allowed

END
GO
