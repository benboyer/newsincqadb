TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	FileContentVideoImportConfig	FeedID	4	int	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	FileContentVideoImportConfig	PartnerID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	FileContentVideoImportConfig	ContentFilePath	-9	nvarchar	200	400	None	None	0	None	None	-9	None	400	3	NO	39

index_name	index_description	index_keys
PK_FileContentVideoImportConfig	clustered, unique, primary key located on PRIMARY	FeedID, PartnerID, ContentFilePath
