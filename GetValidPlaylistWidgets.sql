CREATE PROCEDURE [dbo].[GetValidPlaylistWidgets]
	@userId int,
	@showSharedPlaylist bit
AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'GetValidPlaylistWidgets'

	declare @tg int = (select trackinggroup from mTrackingGroup where partnerid = (select partnerid from [User] where UserID = @userId))
		-- Insert statements for procedure here
	SELECT isnull([v].[Name], '') as WidgetName, [p].[PlaylistID],
	case when [u].UserID != [p].UserID then '(' + isnull([u].FirstName + ' ' + [u].LastName, [u].UserName) + ')' else '' end as UserName
	FROM [dbo].NDNUsers AS [u], [dbo].[Playlists] AS [p]
	left outer JOIN [dbo].[WidgetPlaylist] AS [pv] ON [p].[PlaylistID] = [pv].[PlaylistID]
	left outer join [dbo].[Widgets] AS [v] ON [pv].[WidgetID] = [v].[WidgetID]
	WHERE (([p].[UserID] = @userId or ([p].SharingOption = 2 and @showSharedPlaylist = 1) ) and [p].isdeleted=0)
	AND ((isnull([v].[IsDeleted],0) = 0) ) AND [u].UserID = [v].UserID
	and dbo.AllowedLauncherContent(v.WidgetId, @tg) = 1
END
