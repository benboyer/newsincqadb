
CREATE PROCEDURE [dbo].[ps_SingleContent2]        
--  declare        
@PartnerId int,
@StoryId varchar(64)
      
AS        
BEGIN        
 set FMTONLY OFF        
 set NOCOUNT ON        

exec SprocTrackingUpdate 'ps_SingleContent2'        
  
declare @devicetype int = 1
declare @trackingGroup int = 10557
        
 select  
    -- @playlistid as PlaylistID,  
    c.ContentID, c.Name, c.Description, pt.TrackingGroup ContentPartnerID,       
    null as VideoGroupName, fa.Duration, convert(date, c.EffectiveDate) PubDate, c.Keyword, convert(varchar(120), null) Timeline,        
    case when fa.Name like 'Video%' then 'src'        
      else fa.Name  
    end as AssetType,  
 fa.AssetLocation,  
 fa.AssetMimeType,  
 fa.AssetSortOrder,  
 fa.AssetLocation2,  
 fa.AssetMimeType2,  
 fa.AssetSortOrder2,  
 dbo.GetPathToServeAsset(fa.assetid) AssetLocation3,  
    pt.name as ProducerName, pt.shortname ProducerNameAlt, isnull(pt.logourl,'http://assets.newsinc.com/newsinconebyone.png') ProducerLogo, convert(int, 0) as ContentOrder,        
    isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'news') ProducerCategory,        
    REPLACE(c.Name, ' ', '-') + + '-' +CONVERT(varchar(20), c.ContentID) as SEOFriendlyTitle,        
    case when dsm.partnerid is null then 1        
      when dsm.partnerid is not null then 0        
    end as embedCodeAccessible        
       
  from Content (nolock) c  
  join partner (nolock) pt  
  --on c.contentid = @ContentID 
  --and
  on pt.PartnerID = c.PartnerID      
  
  left join Playlist_Content_PartnerMap pm (nolock)  
  on pt.PartnerID = pm.PartnerID and c.FeedID = pm.FeedID        
  left join NDNCategory nc (nolock)        
  on   pm.NDNCategoryID = nc.NDNCategoryID        
  left join vw_partnerDefaultSettings (nolock) dsm        
  on pt.PartnerID = dsm.PartnerID and Entity = 'BlockShareForProviderID'      

 cross apply dbo.ps_fn_getAssetDataMultiAsset(c.PartnerID, c.contentid) fa      
  where c.PartnerID = @PartnerId AND c.StoryID = @StoryId and dbo.AllowedContentID(c.contentid, @trackinggroup, 1) = 1  
  order by replace(fa.Name, 'Video', 'src'), fa.Assetsortorder, fa.AssetMimeType    
 set NOCOUNT OFF  
  
end 