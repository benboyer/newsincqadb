TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	FeedContent	FeedContentID	-5	bigint identity	19	8	0	10	0	None	None	-5	None	None	1	NO	63
NewsincQA	dbo	FeedContent	PartnerID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	FeedContent	GUID	12	varchar	250	250	None	None	1	None	None	12	None	250	3	YES	39
NewsincQA	dbo	FeedContent	ContentID	4	int	10	4	0	10	1	None	None	4	None	None	4	YES	38
NewsincQA	dbo	FeedContent	PublishDate	11	datetime	23	16	3	None	1	None	None	9	3	None	5	YES	111
NewsincQA	dbo	FeedContent	ExpirationDate	11	datetime	23	16	3	None	1	None	None	9	3	None	6	YES	111
NewsincQA	dbo	FeedContent	Name	12	varchar	500	500	None	None	1	None	None	12	None	500	7	YES	39
NewsincQA	dbo	FeedContent	Description	12	varchar	1000	1000	None	None	1	None	None	12	None	1000	8	YES	39
NewsincQA	dbo	FeedContent	Keywords	12	varchar	1000	1000	None	None	1	None	None	12	None	1000	9	YES	39
NewsincQA	dbo	FeedContent	Categories	12	varchar	250	250	None	None	1	None	None	12	None	250	10	YES	39
NewsincQA	dbo	FeedContent	Version	4	int	10	4	0	10	0	None	None	4	None	None	11	NO	56
NewsincQA	dbo	FeedContent	ImportComplete	-7	bit	1	1	None	None	0	None	None	-7	None	None	12	NO	50
NewsincQA	dbo	FeedContent	Retries	-6	tinyint	3	1	0	10	0	None	None	-6	None	None	13	NO	48
NewsincQA	dbo	FeedContent	PendingPublishDate	-7	bit	1	1	None	None	0	None	None	-7	None	None	14	NO	50
NewsincQA	dbo	FeedContent	network	12	varchar	120	120	None	None	1	None	None	12	None	120	15	YES	39
NewsincQA	dbo	FeedContent	FeedID	4	int	10	4	0	10	0	None	None	4	None	None	16	NO	56
NewsincQA	dbo	FeedContent	RecordAddedDTM	11	datetime	23	16	3	None	0	None	(getdate())	9	3	None	17	NO	61
NewsincQA	dbo	FeedContent	OnlyOwnerView	-7	bit	1	1	None	None	0	None	((0))	-7	None	None	18	NO	50

index_name	index_description	index_keys
NDX_FC_guidICaddeddtm	nonclustered located on PRIMARY	GUID, ImportComplete, RecordAddedDTM
NDX_FeedCID	nonclustered located on PRIMARY	ContentID
NDX_FeedContent_CID	nonclustered located on PRIMARY	ContentID
PK_FeedContent_1	clustered, unique, primary key located on PRIMARY	FeedContentID
