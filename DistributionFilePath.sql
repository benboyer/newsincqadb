TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	DistributionFilePath	DistributionFilePathID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	DistributionFilePath	MimeFileExtension	12	varchar	20	20	None	None	0	None	None	12	None	20	2	NO	39
NewsincQA	dbo	DistributionFilePath	StoragePath	12	varchar	200	200	None	None	1	None	None	12	None	200	3	YES	39
NewsincQA	dbo	DistributionFilePath	DeliveryPath	12	varchar	200	200	None	None	1	None	None	12	None	200	4	YES	39
NewsincQA	dbo	DistributionFilePath	StorageLevel1	12	varchar	100	100	None	None	1	None	None	12	None	100	5	YES	39
NewsincQA	dbo	DistributionFilePath	DeliveryLevel1	12	varchar	100	100	None	None	1	None	None	12	None	100	6	YES	39
NewsincQA	dbo	DistributionFilePath	StorageLevel2	12	varchar	100	100	None	None	1	None	None	12	None	100	7	YES	39
NewsincQA	dbo	DistributionFilePath	DeliveryLevel2	12	varchar	100	100	None	None	1	None	None	12	None	100	8	YES	39
NewsincQA	dbo	DistributionFilePath	StorageLevel3	12	varchar	100	100	None	None	1	None	None	12	None	100	9	YES	39
NewsincQA	dbo	DistributionFilePath	DeliveryLevel3	12	varchar	100	100	None	None	1	None	None	12	None	100	10	YES	39
NewsincQA	dbo	DistributionFilePath	StorageLevel4	12	varchar	100	100	None	None	1	None	None	12	None	100	11	YES	39
NewsincQA	dbo	DistributionFilePath	DeliveryLevel4	12	varchar	100	100	None	None	1	None	None	12	None	100	12	YES	39
NewsincQA	dbo	DistributionFilePath	IncludeStorageExtension	-7	bit	1	1	None	None	0	None	((1))	-7	None	None	13	NO	50
NewsincQA	dbo	DistributionFilePath	IncludeDeliveryExtension	-7	bit	1	1	None	None	0	None	((1))	-7	None	None	14	NO	50
NewsincQA	dbo	DistributionFilePath	active	-7	bit	1	1	None	None	0	None	((1))	-7	None	None	15	NO	50
NewsincQA	dbo	DistributionFilePath	StorageLevel5	12	varchar	100	100	None	None	1	None	None	12	None	100	16	YES	39
NewsincQA	dbo	DistributionFilePath	DeliveryLevel5	12	varchar	100	100	None	None	1	None	None	12	None	100	17	YES	39
NewsincQA	dbo	DistributionFilePath	StorageLevel6	12	varchar	100	100	None	None	1	None	None	12	None	100	18	YES	39
NewsincQA	dbo	DistributionFilePath	DeliveryLevel6	12	varchar	100	100	None	None	1	None	None	12	None	100	19	YES	39

index_name	index_description	index_keys
IX_DistributionFilePath_ext	nonclustered located on PRIMARY	MimeFileExtension, StoragePath, DeliveryPath
PK_DistributionFilePath	clustered, unique, primary key located on PRIMARY	DistributionFilePathID
