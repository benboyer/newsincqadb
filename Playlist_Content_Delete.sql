CREATE Procedure [dbo].[Playlist_Content_Delete]  
 @PlaylistID int,  
 @ContentID int  
AS  
Begin  
 SET NOCOUNT ON  
 exec SprocTrackingUpdate 'Playlist_Content_Delete'  
  
 delete from Playlist_Content  
 where  
  PlaylistID = @PlaylistID  
  and ContentID = @ContentID  
   
 Update Playlist   
 set ContentUpdatedDate = GETDATE()  
 where PlaylistID = @PlaylistID  
End  
  
