USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetLauncherShow]
	@widgetID int = null,
	@trackingGroup int = null,
	@deviceTypeID int = 1
AS
BEGIN
	set fmtonly off
	set nocount on

	exec SprocTrackingUpdate 'GetLauncherShow'
	if @deviceTypeID = 3
	begin
		set @deviceTypeID = 1
--		exec ps_LauncherShow @widgetID, @trackingGroup, @deviceTypeID
--		return
	end

	declare @partnerid int,
			@distPartnerid int,
			@AdServer varchar(26)


	if isnull(@deviceTypeID, 0) = 0 --is null
		set @deviceTypeID = 1
	if @widgetID is null
		set @widgetID = 1
	if isnull(@trackingGroup, 0) = 0-- is null
		set @trackingGroup = 10557
	if not exists (select 1 from partner where TrackingGroup = @trackingGroup)
		set @trackingGroup = 10557

	declare @iwidgetID int,
			@itrackinggroup int,
			@iDeviceTypeID int

				-- select * from launcher where launcherid =2033
	select @iwidgetID = @widgetID, @itrackinggroup = @trackingGroup, @iDeviceTypeID = @deviceTypeID
	set @partnerid = (select partnerid from Launcher where LauncherID = @iwidgetID)
	set @distPartnerid = (select partnerid from mTrackingGroup (nolock) where TrackingGroup = @trackinggroup)
	set @AdServer = isnull((select value from vw_partnerDefaultSettings where PartnerID = @distPartnerid and Entity = 'AdServer'), (select value from vw_sysDefaultSettings where Entity = 'AdServer'))

	select top 1 *
	from
		(-- declare @iwidgetid int, @itrackinggroup int, @iDeviceTypeID int select @iwidgetID = 1294, @itrackinggroup = 58285, @idevicetypeid = 1
		SELECT   l.LauncherID, isnull(isnull(l.SectionName, s2.name), 'ndn') SiteSectionName, convert(smallint, 1) AllowFullScreen, @itrackinggroup TrackingGroup,
				--l.DisableEmailButton, l.DisableAdsOnPlayer, l.DisableAutoPlay, l.DisableShareButton, l.DisableAutoPlay,
				convert(bit,case when blk.launcherid is null then wpl.DisableSocialNetworking else 1 end) as DisableSocialNetworking, wpl.DisableAds, wpl.DisableVideoPlayOnLoad, wpl.DisableSingleEmbed, wpl.ContinuousPlay,
				coalesce(l.Width, ltem.Width, pt.Width) Width, coalesce(l.Height, ltem.height, pt.height) Height ,
				-- pt.Width, pt.Height,
				case when isnull(p.ZoneID, 0) = 0 then 50974 else p.ZoneID end as ZoneID
				 , isnull(lu2.LandingURL, 'http://www.newsinc.com') LandingUrl_Hulu , isnull(lu.LandingURL,
				 isnull(p.LandingURL, 'http://landing.newsinc.com/shared/video.html')) LandingURL,
				 isnull(isnull(palo43.logourl, pld43.LogoURL), 'http://assets.newsinc.com/partnerlogo.jpg') LogoURL43,
				 isnull(isnull(palo169.logourl, pld169.logourl), 'http://assets.newsinc.com/partnerlogo169.jpg') LogoURL169,
				 ISNULL(isnull(paloD.LogoURL, plddisplay.logourl), 'no logo') LogoURLDisplay,
				 m.Name DistributorName,  isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'News Distribution Network, Inc.') ProducerCategory, l.LauncherID WidgetID, m.ShortName DistributorNameAlt,
				 l.LaunchInLandingPage LandingPageAllowed, l.TransitionSeconds as VideoInterval, @AdServer AdServer
		-- declare @partnerid int = 101, @iwidgetID int = 2033, @itrackinggroup int = 90057, @iDeviceTypeID int = 1 select *
		FROM		(select la.*, sa.name SectionName from Launcher (nolock) la left join Section  (nolock) sa on la.SectionID = sa.SectionID where la.LauncherID = @iwidgetID) l
		INNER JOIN	WidgetPlayerLegacy wpl (nolock)
		ON			l.LauncherID = wpl.LauncherID
		left join	PlayerType pt (nolock)
		on			l.PlayerTypeID = pt.PlayerTypeID
		left join	(select pl.partnerid, pl.launcherid, l.LogoURL
					from	partner_logo pl (nolock)
					join	Logo l (nolock)
					on		pl.logoid = l.LogoID
					where	pl.PartnerID = @partnerid
					and		l.LogoTypeID = 1
					and		l.Active = 1) palo43
--		on			l.PartnerID = palo43.partnerid
--		and			l.LauncherID = palo43.LauncherID
		on			@distPartnerid = palo43.PartnerID
		and			l.LauncherID = palo43.LauncherID
		left join	(select top 1 pl.PartnerID, logourl
					from	Partner_Logo pl  (nolock)
					join	Logo lo  (nolock)
					on		pl.LogoID = lo.LogoID
					where	pl.PartnerID = @partnerid
					and		lo.LogoTypeID = 1
					and		lo.PartnerDefault = 1
					and		lo.Active = 1) pld43
--		on			l.PartnerID = pld43.partnerid
		on			@distPartnerid = pld43.PartnerID
		left join	(select pl.partnerid, pl.LauncherID, l.LogoURL
					from	partner_logo pl (nolock)
					join	Logo l (nolock)
					on		pl.logoid = l.LogoID
					where	pl.PartnerID = @partnerid
					and		l.LogoTypeID = 2
					and		l.Active = 1) palo169
--		on			l.PartnerID = palo169.partnerid
--		and			l.LauncherID = palo169.LauncherID
		on			@distPartnerid = palo169.PartnerID
		and			l.LauncherID = palo169.LauncherID
		left join	(select top 1 pl.PartnerID, logourl
					from	Partner_Logo pl  (nolock)
					join	Logo lo  (nolock)
					on		pl.LogoID = lo.LogoID
					where	pl.PartnerID = @partnerid
					and		lo.LogoTypeID = 2
					and		lo.PartnerDefault = 1
					and		lo.Active = 1) pld169
--		on			l.PartnerID = pld169.partnerid
		on			@distPartnerid = pld169.PartnerID
		left join	(select pl.partnerid, pl.LauncherID, l.LogoURL
					from	partner_logo pl (nolock)
					join	Logo l (nolock)
					on		pl.logoid = l.LogoID
					where	l.LogoTypeID = 6
					and		l.Active = 1) paloD
--		on			l.PartnerID = paloD.partnerid
--		and			l.LauncherID = paloD.LauncherID
		on			@distPartnerid = paloD.PartnerID
		and			l.LauncherID = paloD.LauncherID
		left join	(select top 1 pl.PartnerID, logourl
					from	Partner_Logo pl  (nolock)
					join	Logo lo  (nolock)
					on		pl.LogoID = lo.LogoID
					where	pl.PartnerID = @partnerid
					and		lo.LogoTypeID = 6
					and		lo.PartnerDefault = 1
					and		lo.Active = 1) plddisplay
--		on			l.PartnerID = plddisplay.partnerid
		on			@distPartnerid = plddisplay.PartnerID
		left join LauncherTemplate ltem (nolock)
		on		l.LauncherTemplateID = ltem.LauncherTemplateID
		left join LauncherTypes lt (nolock)
		on		ltem.Launchertypesid = lt.LauncherTypesID
		-- select top 100 * from launchertemplate
		join	mTrackingGroup mt (nolock)
		on		@itrackinggroup = mt.TrackingGroup
		join	Partner m (nolock)
		on		mt.partnerid = m.PartnerID
		left join	(select m.partnerid, lu.LandingURL
					from	(select partnerid from mTrackingGroup where TrackingGroup = @itrackinggroup) m
					join	Partner_LandingURL lu2 (nolock)
					on		m.partnerid = lu2.PartnerID
					join	LandingURL lu (nolock)
					on		lu2.LandingURLID = lu.LandingURLID
					join	LandingURLType lt  (nolock)-- select * from LandingURLType
					on		lu.LandingURLTypeID = lt.LandingURLTypeID
					where	lt.Name = 'Partner Default' and lu.active = 1) lu
--		on	l.PartnerID = lu.partnerid
		on	@distPartnerid = lu.partnerid
		left join	(select m.partnerid, lu.LandingURL
					from	(select partnerid from mTrackingGroup (nolock) where TrackingGroup = @itrackinggroup) m
					join	Partner_LandingURL lu2 (nolock)
					on		m.partnerid = lu2.PartnerID
					join	LandingURL lu (nolock)
					on		lu2.LandingURLID = lu.LandingURLID
					join	LandingURLType lt (nolock)
					on		lu.LandingURLTypeID = lt.LandingURLTypeID
					where	lt.Name = 'hulu' and lu.active = 1) lu2
--		on	m.PartnerID = lu2.partnerid
		on	@distPartnerid = lu2.partnerid
		left join (select lp.LauncherID
					from	Launcher_Playlist (nolock) lp
					join	Playlist_Content (nolock) pc
					on		lp.PlaylistID = pc.PlaylistID
					join	Content (nolock) c
					on		pc.ContentID = c.ContentID
					where	c.PartnerID in (select partnerid
											from vw_partnerdefaultsettings
											where entity = 'BlockShareForProviderID' and value = 'maybe')) blk
											--where entity = 'BlockShareForProviderID' and value = 'true')) blk
		on		l.LauncherID = blk.launcherid
------------
		left join  Playlist_Content_PartnerMap (nolock) pm
--		on			l.PartnerID = pm.PartnerID
		on			@distPartnerid = pm.PartnerID
		and			pm.NDNCategoryID is not null
		left join	NDNCategory (nolock) nc
		on			pm.NDNCategoryID = nc.NDNCategoryID
		left join	 Partner p  (nolock)
		-- left join	mTrackingGroup (nolock) p --  Partner p  (nolock)-- select top 100 [default] from section
		on			@itrackinggroup = p.TrackingGroup
		left join (select * from section (nolock) where sectionid = (select max(sectionid)  from Section (nolock) where PartnerID = @partnerid and [default] = 1)) s2
		on			m.partnerid = s2.PartnerID
		-- where		dbo.AllowedLauncherContent(@widgetID, @itrackinggroup) = 1
		) z
	order by LauncherID desc

	set nocount off
END
GO
