TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	AssetType	AssetTypeID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	AssetType	Name	-9	nvarchar	40	80	None	None	0	None	None	-9	None	80	2	NO	39
NewsincQA	dbo	AssetType	MimeTypeID	4	int	10	4	0	10	1	None	None	4	None	None	3	YES	38
NewsincQA	dbo	AssetType	MinHeight	4	int	10	4	0	10	1	None	None	4	None	None	4	YES	38
NewsincQA	dbo	AssetType	MaxHeight	4	int	10	4	0	10	1	None	None	4	None	None	5	YES	38
NewsincQA	dbo	AssetType	MinWidth	4	int	10	4	0	10	1	None	None	4	None	None	6	YES	38
NewsincQA	dbo	AssetType	MaxWidth	4	int	10	4	0	10	1	None	None	4	None	None	7	YES	38
NewsincQA	dbo	AssetType	MinBitrate	4	int	10	4	0	10	1	None	None	4	None	None	8	YES	38
NewsincQA	dbo	AssetType	MaxBitrate	4	int	10	4	0	10	1	None	None	4	None	None	9	YES	38
NewsincQA	dbo	AssetType	IdealWidth	4	int	10	4	0	10	1	None	None	4	None	None	10	YES	38
NewsincQA	dbo	AssetType	IdealBitrate	4	int	10	4	0	10	1	None	None	4	None	None	11	YES	38
NewsincQA	dbo	AssetType	PlayerAssetType	12	varchar	20	20	None	None	1	None	None	12	None	20	12	YES	39
NewsincQA	dbo	AssetType	Comment	-1	text	2147483647	2147483647	None	None	1	None	None	-1	None	2147483647	13	YES	39
NewsincQA	dbo	AssetType	Process	-7	bit	1	1	None	None	1	None	None	-7	None	None	14	YES	50
NewsincQA	dbo	AssetType	Transcode	-7	bit	1	1	None	None	1	None	None	-7	None	None	15	YES	50
NewsincQA	dbo	AssetType	CreateFile	-7	bit	1	1	None	None	1	None	None	-7	None	None	16	YES	50
NewsincQA	dbo	AssetType	CreateFileExtension	12	varchar	10	10	None	None	1	None	None	12	None	10	17	YES	39
NewsincQA	dbo	AssetType	CustomProcessingID	4	int	10	4	0	10	1	None	None	4	None	None	18	YES	38

index_name	index_description	index_keys
IX_AssetType_Mimetype	nonclustered located on PRIMARY	MimeTypeID
PK_AssetType	clustered, unique, primary key located on PRIMARY	AssetTypeID
