USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Content_AdBlockKeyword_AddUpdate]
as
BEGIN
	set fmtonly off
	set nocount on

	exec sprocTrackingUpdate 'Content_AdBlockKeyword_AddUpdate'
	
	insert into archives.dbo.VideoIDsToFilterAds(VideoID, KeywordAddPhrase)
	select distinct ContentID, b.KeywordAddPhrase
			-- select * -- ContentID, PartnerID, Name, CreatedDate
	from	archives.dbo.PhrasesToFilterAds b
	join	(select * from Content (nolock) where CreatedDate > '2013-04-17') c
	on		(c.Name like b.phrase
			or
			c.Description like b.phrase
			or
			c.Keyword like b.phrase)
	and		not exists(
				select 1
				from	archives.dbo.VideoIDsToFilterAds (nolock) v
				where	v.videoid = c.ContentID)

	update c set c.Keyword = case when c.Keyword is null then a.keywordaddphrase
									when c.Keyword is not null and LTRIM(rtrim(c.keyword)) = '' then a.keywordaddphrase
									when LEN(c.keyword) > 1 then c.Keyword + ',' + a.keywordaddphrase
									end,
					c.UpdatedDate = GETDATE(),
					c.UpdatedUserID = 7534 
	-- select *
	from	(select * from archives.dbo.VideoIDsToFilterAds (nolock) where filteraddeddate is null)  a
	join	Content (nolock) c
	on		a.videoid = c.ContentID
	where	isnull(c.keyword, '') not like '%' + a.KeywordAddPhrase + '%'
	and LEN(c.keyword) < 980

	update x 
	set		x.FilterAddedDate = GETDATE(),
			x.UpdatedDate = GETDATE(),
			x.FilterActive = 1
	-- select *
	from	(select * from archives.dbo.VideoIDsToFilterAds (nolock) where filteraddeddate is null)  a
	join	content c 
	on		a.videoid = c.contentid
	join	archives.dbo.VideoIDsToFilterAds x
	on		a.VideoIDsToFilterAdsID = x.VideoIDsToFilterAdsID
	where	isnull(c.keyword, '') like '%' + a.KeywordAddPhrase + '%'

	set nocount off
END
GO
