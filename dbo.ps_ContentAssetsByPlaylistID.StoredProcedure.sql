USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ps_ContentAssetsByPlaylistID]  
--  declare  
 @widgetID int = null,  
 @playlistID int = null,  
 @trackingGroup int = null,  
 @contentid bigint = null,  
 @devicetype int = 3  
AS  
BEGIN  
  set FMTONLY OFF  
  set nocount on  
  exec SprocTrackingUpdate 'ps_ContentAssetsByPlaylistID2'  
  -- declare  @widgetID int = 2, @playlistID int = 11320, @trackingGroup int = 10557, @contentid bigint = null, @devicetype int = 1  
  -- declare  @widgetID int = 2, @playlistID int = 10203, @trackingGroup int = 10557, @contentid bigint = null, @devicetype int = 1  
  declare @PlaylistCap int = 50  
  
  Declare @iwidgetID int,  
    @iplaylistID int,  
    @itrackinggroup int,  
    @idevicetype int  
  if @devicetype = 3 set @devicetype = 1  
    
  select @iwidgetID = isnull(@widgetID, 1), @iplaylistID = isnull(@playlistID, (select value from vw_sysDefaultSettings (nolock) where entity = 'Playlist' and Setting = 'Number')), @itrackinggroup = isnull(@trackingGroup, 10557), @idevicetype =   
  isnull(@devicetype, 1)  
  
--  if  dbo.AllowedLauncherContent(@iwidgetID, @itrackinggroup) =  0   
--  begin  
--   return  
--  end  
  
  -- declare  @widgetID int = 3233, @playlistID int = 9999999, @itrackingGroup int = 10557, @contentid bigint = 23766629, @devicetype int = 3  
  if @PlaylistID = 9999999  
  begin  
   exec ps_SingleContent @contentid, @iTrackingGroup, @deviceType  
   return  
  end  
  
  declare @cids as table(ContentID bigint, partnerid int, feedid int, contentorder int)  
  insert into @cids(ContentID, partnerid, feedid, contentorder)  
  select top (@PlaylistCap) pc.ContentID, c.PartnerID, c.FeedID, pc.[Order]   
    from playlist_content (nolock) pc   
    join Content c on pc.ContentID = c.ContentID   
    -- join Partner p (nolock) on c.PartnerID = p.PartnerID  
    where pc.PlaylistID = @iplaylistid   
    and dbo.AllowedContentID(pc.ContentID, @itrackinggroup, 1) = 1   
    and dbo.AllowedLauncherContentP(@widgetID, c.PartnerID)= 1 order by [ORDER] ASC  
  
  select @iplaylistID PlaylistID, core.ContentID, core.ContentName Name, core.Description, p.TrackingGroup ContentPartnerID, vg.VideoGroupName, core.Duration,  
    core.effectivedate PubDate, core.Keyword, convert(varchar(120), null) Timeline,  
    --case when core.Name like 'video%' then 'src'  
    -- else   
      core.AssetType,  
    -- end as AssetType,  
     case when core.AssetType like 'External%' then core.Filename  
      else core.AssetLocation  
      end as AssetLocation,  
     core.AssetMimeType,      
     core.AssetSortOrder,      
     core.AssetLocation2,      
     core.AssetMimeType2,      
     core.AssetSortOrder2,       
     p.Name ProducerName, p.ShortName ProducerNameAlt,   
     coalesce (lo.logourl, p.Logourl) ProducerLogo, core.contentorder+1 ContentOrder, isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'news') ProducerCategory,  
     REPLACE(core.ContentName, ' ', '-') + + '-' +CONVERT(varchar(20), core.ContentID) as SEOFriendlyTitle,  
    case when dse.partnerid is null then 1  
      when dse.partnerid is not null then 0  
    end as embedCodeAccessible--,  
  from (select ref.PartnerID, ref.ContentID, ref.contentorder, c.Name ContentName, c.Description, c.EffectiveDate, c.Keyword, ref.feedid, replace(fa.Name, 'video', 'src') AssetType 
  --, fa.AssetID, fa.Duration, fa.FilePath, fa.Filename-- , fa.MediaType, mt.MimeType   
    , fa.*  
   from @cids ref  
   join Content (nolock) c  
   on  ref.ContentID = c.ContentID  
    cross apply dbo.ps_fn_getAssetDataMultiAsset(ref.PartnerID, ref.contentid) fa      
    ) core  
  join Partner (nolock) p  
  on  core.partnerid = p.PartnerID  
  left join (select pl.PartnerID, l.logourl   
     from Partner_Logo (nolock) pl  
     join Logo (nolock) l  
     on  pl.LogoID = l.LogoID  
     where l.LogoTypeID = 5  
     and  l.Active = 1) lo 
  on  core.PartnerID = lo.PartnerID  
  left join VideoGroups VG (nolock)  
  on  @iplaylistID  = vg.VideoGroupId  
  left join Playlist_Content_PartnerMap pm   (nolock)-- select * from playlist_content_partnermap where feedid is not null  
  on   core.PartnerID = pm.PartnerID  
  and   core.feedid = pm.FeedID  
  left join NDNCategory nc (nolock)  
  on   pm.NDNCategoryID = nc.NDNCategoryID  
  left join (select partnerid from vw_partnerDefaultSettings (nolock) where Entity = 'BlockShareForProviderID' and Setting = 'shareEmbed' and Value = 'true') dse  
  on core.PartnerID = dse.PartnerID  
  order by core.contentorder +1, core.ContentID, core.AssetType  
  
  set nocount off  
  
END
GO
