TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	DMA	DMACode	4	int	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	DMA	DMAName	12	varchar	32	32	None	None	0	None	None	12	None	32	2	NO	39
NewsincQA	dbo	DMA	CreatedDate	11	smalldatetime	16	16	0	None	1	None	None	9	3	None	3	YES	111
NewsincQA	dbo	DMA	UpdatedDate	11	datetime	23	16	3	None	1	None	None	9	3	None	4	YES	111

index_name	index_description	index_keys
PK_DMA	clustered, unique, primary key located on PRIMARY	DMACode
