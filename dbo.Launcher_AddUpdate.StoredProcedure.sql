USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Launcher_AddUpdate]
	--	declare
		@PartnerID int = null,
		@UserID int = null,
		@LauncherID int = null,
		@LauncherName varchar(50),
		@DisableAutoPlay bit,
		@DisableAdsOnPlayer bit,
		@DisableEmailButton bit,
		@DisableShareButton bit,
		@SectionPartnerValue varchar(26),
		@SectionSectionValue varchar(10),
		@SectionSubSectionValue varchar(10),
		@SectionPageTypeValue varchar(4),
		@Height int,
		@Width int,
		@active bit,
		@ContinuousPlay int = null,
		@LauncherTemplateID int,
		@LandingPageAllowed bit = 0
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'Launcher_AddUpdate'

	-- insert into analytics.dbo.LauncherAddParamGrab(Partnerid, Userid, LauncherID, LauncherName, DisableAutoPlay,DisableAdsOnPlayer,	DisableEmailButton,	DisableShareButton,	SectionPartnerValue,SectionSectionValue,SectionSubSectionValue,	SectionPageTypeValue,Height,Width,active,ContinuousPlay,LauncherTemplateID)
	-- select	@PartnerID,@UserID,@LauncherID,@LauncherName,@DisableAutoPlay,@DisableAdsOnPlayer,@DisableEmailButton,@DisableShareButton,@SectionPartnerValue,@SectionSectionValue,@SectionSubSectionValue,@SectionPageTypeValue,@Height,@Width,@active,@ContinuousPlay,@LauncherTemplateID

	declare @SectionID int

	if @ContinuousPlay is null -- or @ContinuousPlay = 0
	begin
		set @ContinuousPlay = 6
	end

	if @PartnerID is null and @UserID is null
	begin
		select 'Must have @Partnerid or @Userid' as LauncherID
		return
	end

		if @partnerid is null set @partnerid = (select Max(partnerid) from [user] where UserID = @UserID)
		if @UserID is null set @UserID = (select contactid from Partner where PartnerID = @partnerid)

		EXEC	SiteSection_Create_Update null, @partnerid, null, @sectionPartnerValue, @SectionSectionValue, @SectionSubSectionValue, @SectionPageTypeValue, 1
		set @SectionID = (select top 1 sectionid from Section where PartnerID = @PartnerID and Name = @SectionPartnerValue + @SectionSectionValue + @SectionSubSectionValue + @SectionPageTypeValue)

	if isnull(@LauncherID, 0) = 0
	begin
		insert into Launcher(PartnerID, Name, SectionID, Active, CreatedDate, CreatedUserID, UpdatedDate, UpdatedUserID, GenerateNow, Height, Width, ContinuousPlay, DisableEmailButton, DisableAdsOnPlayer, DisableShareButton, DisableAutoPlay, LauncherTemplateID, LaunchInLandingPage)
		select @PartnerID, @LauncherName, @SectionID, @active, GETDATE() CreatedDate, @UserID, getdate() as UpdatedDate, @UserID as UpdatedUserID, 1 as GenerateNow, @Height, @Width, @ContinuousPlay, @DisableEmailButton, @DisableAdsOnPlayer, @DisableShareButton, @DisableAutoPlay, @LauncherTemplateID, @LandingPageAllowed
		where not exists
			(select 1
			from Launcher
			where	PartnerID = @PartnerID
			and		Name = @LauncherName)
		set @LauncherID = (select LauncherID from Launcher where PartnerID = @PartnerID and Name = @LauncherName)
		--select @launcherid
		insert into WidgetsLEGACY(LauncherID, ContentItemID, DCId, VanityName, IsDefault, GenerateNow, TemplateZip, Height, Width)
		select @LauncherID, null ContentItemID, null DCID, null VanityName, 0 isDefault, 1 GenerateNow, null TemplateZip, @Height, @Width
		where not exists
			(select 1
			from WidgetsLEGACY
			where	LauncherID = @LauncherID)

		insert into WidgetPlayerLegacy(LauncherID, DisableSocialNetworking, DisableAds, DisableVideoPlayOnLoad, DisableSingleEmbed, StyleSheet, DisableKeyWords, ContinuousPlay)
		select @LauncherID, @DisableEmailButton, @DisableAdsOnPlayer, @DisableAutoPlay, @DisableShareButton, null stylesheet, null disablekeywords, @ContinuousPlay
		where not exists
			(select 1
			from WidgetPlayerLegacy
			where LauncherID = @LauncherID)

	end
	else
	begin
		update Launcher
			set PartnerID = @PartnerID, Name = @LauncherName, SectionID = @SectionID, Active = @active, UpdatedDate = GETDATE(), UpdatedUserID = @UserID, GenerateNow = 1, Height = @Height,
				Width = @Width, ContinuousPlay = @ContinuousPlay, DisableEmailButton = @DisableEmailButton, DisableAdsOnPlayer = @DisableAdsOnPlayer, DisableShareButton = @DisableShareButton,
				DisableAutoPlay = @DisableAutoPlay, LauncherTemplateID = @LauncherTemplateID, LaunchInLandingPage = @LandingPageAllowed
			where LauncherID = @LauncherID

		update WidgetPlayerLegacy
		set DisableVideoPlayOnLoad = @DisableAutoPlay, DisableSocialNetworking = @DisableEmailButton, DisableAds = @DisableAdsOnPlayer, DisableSingleEmbed = @DisableShareButton, StyleSheet = null, DisableKeywords = null, ContinuousPlay = @ContinuousPlay
		where LauncherID = @LauncherID

--		update WidgetPlayerLegacy
--		set DisableSocialNetworking = @DisableEmailButton, DisableAds = @DisableAdsOnPlayer, DisableSingleEmbed = @DisableShareButton, StyleSheet = null, DisableKeywords = null, ContinuousPlay = @ContinuousPlay
--		where LauncherID = @LauncherID

	end
	select convert(varchar(20), @LauncherID) as LauncherID

	set nocount off
END
GO
