USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Content](
	[ContentID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ContentTypeID] [smallint] NOT NULL,
	[PartnerID] [int] NOT NULL,
	[Version] [int] NULL,
	[ContentSourceID] [smallint] NOT NULL,
	[Name] [varchar](500) NOT NULL,
	[Description] [varchar](1000) NOT NULL,
	[Category] [varchar](250) NULL,
	[Keyword] [varchar](1000) NULL,
	[Duration] [decimal](20, 2) NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[ExpirationDate] [smalldatetime] NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[CreatedUserID] [int] NOT NULL,
	[UpdatedDate] [smalldatetime] NULL,
	[UpdatedUserID] [int] NULL,
	[Active] [bit] NOT NULL,
	[IsFileArchived] [bit] NULL,
	[isDeleted] [bit] NULL,
	[FeedID] [int] NULL,
	[SourceGUID] [varchar](250) NULL,
	[ContentImportStatus] [int] NULL,
	[TranscriptPath] [varchar](500) NULL,
	[TimesUpdated] [int] NOT NULL,
	[OnlyOwnerView] [bit] NOT NULL,
	[UUID] [uniqueidentifier] NOT NULL,
	[StoryID] [varchar](64) NULL,
	[antikw] [varchar](200) NULL,
 CONSTRAINT [PK_Content_1] PRIMARY KEY CLUSTERED 
(
	[ContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_Content_PidActEffDt] ON [dbo].[Content] 
(
	[PartnerID] ASC,
	[Active] ASC
)
INCLUDE ( [EffectiveDate]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Content_PIDverEffSGUID] ON [dbo].[Content] 
(
	[PartnerID] ASC
)
INCLUDE ( [Version],
[EffectiveDate],
[SourceGUID]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ContentActive] ON [dbo].[Content] 
(
	[Active] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ContentEffCrtDTM] ON [dbo].[Content] 
(
	[CreatedDate] ASC,
	[ContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ContentEffDt] ON [dbo].[Content] 
(
	[EffectiveDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ContentExpDTM] ON [dbo].[Content] 
(
	[ExpirationDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ContentFeed] ON [dbo].[Content] 
(
	[FeedID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_CreatedDate_Desc] ON [dbo].[Content] 
(
	[CreatedDate] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_UpdatedDate_Desc] ON [dbo].[Content] 
(
	[UpdatedDate] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_CON_cdtm_cidNdesKW] ON [dbo].[Content] 
(
	[CreatedDate] ASC
)
INCLUDE ( [ContentID],
[Name],
[Description],
[Keyword]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_Con_CDTMimpstatCIDcuie] ON [dbo].[Content] 
(
	[CreatedDate] ASC,
	[ContentID] ASC,
	[CreatedUserID] ASC,
	[ContentImportStatus] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_Con_EffCrtStat_CIDfid] ON [dbo].[Content] 
(
	[EffectiveDate] ASC,
	[CreatedDate] ASC,
	[ContentImportStatus] ASC,
	[ContentID] ASC,
	[FeedID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_Con_oov] ON [dbo].[Content] 
(
	[OnlyOwnerView] ASC,
	[ContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_CON_pid_etc] ON [dbo].[Content] 
(
	[PartnerID] ASC
)
INCLUDE ( [ContentID],
[ContentTypeID],
[ContentSourceID],
[Name],
[Description],
[Category],
[Keyword],
[EffectiveDate],
[ExpirationDate],
[CreatedDate],
[CreatedUserID],
[UpdatedDate],
[UpdatedUserID],
[Active],
[IsFileArchived],
[FeedID]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_CON_pidKitSnk] ON [dbo].[Content] 
(
	[PartnerID] ASC,
	[Active] ASC,
	[isDeleted] ASC,
	[ContentImportStatus] ASC,
	[ContentID] ASC,
	[Name] ASC,
	[EffectiveDate] ASC,
	[ExpirationDate] ASC,
	[CreatedDate] ASC,
	[CreatedUserID] ASC,
	[OnlyOwnerView] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_CON_pidSGuid] ON [dbo].[Content] 
(
	[PartnerID] ASC,
	[SourceGUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_Con_SrcCID] ON [dbo].[Content] 
(
	[ContentSourceID] ASC
)
INCLUDE ( [ContentID]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_Con_UITstatCID] ON [dbo].[Content] 
(
	[CreatedUserID] ASC,
	[ContentImportStatus] ASC
)
INCLUDE ( [ContentID]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_CONactPIDcdtm] ON [dbo].[Content] 
(
	[Active] ASC,
	[PartnerID] ASC,
	[CreatedDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_Content_CUIDaExpDtETC] ON [dbo].[Content] 
(
	[CreatedUserID] ASC,
	[Active] ASC,
	[ExpirationDate] ASC
)
INCLUDE ( [ContentID],
[Name],
[Description],
[Keyword],
[EffectiveDate],
[CreatedDate]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_ContentPIDcidEffCrt] ON [dbo].[Content] 
(
	[PartnerID] ASC
)
INCLUDE ( [ContentID],
[EffectiveDate],
[CreatedDate]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_PART_cis_cid_dtms] ON [dbo].[Content] 
(
	[PartnerID] ASC,
	[ContentImportStatus] ASC
)
INCLUDE ( [ContentID],
[EffectiveDate],
[CreatedDate]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Content]  WITH CHECK ADD  CONSTRAINT [FK_Content_ContentSource] FOREIGN KEY([ContentSourceID])
REFERENCES [dbo].[ContentSource] ([ContentSourceID])
GO
ALTER TABLE [dbo].[Content] CHECK CONSTRAINT [FK_Content_ContentSource]
GO
ALTER TABLE [dbo].[Content]  WITH NOCHECK ADD  CONSTRAINT [FK_Content_FeedID] FOREIGN KEY([FeedID])
REFERENCES [dbo].[Feed] ([FeedId])
GO
ALTER TABLE [dbo].[Content] CHECK CONSTRAINT [FK_Content_FeedID]
GO
ALTER TABLE [dbo].[Content]  WITH CHECK ADD  CONSTRAINT [Partner_Content] FOREIGN KEY([PartnerID])
REFERENCES [dbo].[Partner] ([PartnerID])
GO
ALTER TABLE [dbo].[Content] CHECK CONSTRAINT [Partner_Content]
GO
ALTER TABLE [dbo].[Content] ADD  CONSTRAINT [DF__Content__Created__452A2A23]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Content] ADD  CONSTRAINT [DF_Content_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[Content] ADD  CONSTRAINT [DF_Content_Active]  DEFAULT ((0)) FOR [Active]
GO
ALTER TABLE [dbo].[Content] ADD  CONSTRAINT [DF_Content_IsFileArchived]  DEFAULT ((0)) FOR [IsFileArchived]
GO
ALTER TABLE [dbo].[Content] ADD  CONSTRAINT [DF_Content_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [dbo].[Content] ADD  CONSTRAINT [DF_Content_FeedID]  DEFAULT ((0)) FOR [FeedID]
GO
ALTER TABLE [dbo].[Content] ADD  CONSTRAINT [DF_Content_TimesUpdated]  DEFAULT ((0)) FOR [TimesUpdated]
GO
ALTER TABLE [dbo].[Content] ADD  CONSTRAINT [DF_Content_OnlyOwnerView]  DEFAULT ('0') FOR [OnlyOwnerView]
GO
ALTER TABLE [dbo].[Content] ADD  DEFAULT (newid()) FOR [UUID]
GO
