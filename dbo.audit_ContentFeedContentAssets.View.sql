USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[audit_ContentFeedContentAssets]
AS
SELECT     dbo.[Content].ContentID, dbo.[Content].Name, dbo.[Content].PartnerID, dbo.FeedContentAsset.FeedContentAssetID, dbo.FeedContentAsset.FeedContentID, 
                      dbo.MimeType.MimeType, dbo.MimeType.FileExtension, dbo.MimeType.MediaType, dbo.FeedContentAsset.FilePath, dbo.FeedContentAsset.Height, 
                      dbo.FeedContentAsset.Width, dbo.FeedContentAsset.Bitrate, dbo.FeedContentAsset.Duration
FROM         dbo.Asset INNER JOIN
                      dbo.Content_Asset ON dbo.Asset.AssetID = dbo.Content_Asset.AssetID INNER JOIN
                      dbo.[Content] ON dbo.Content_Asset.ContentID = dbo.[Content].ContentID INNER JOIN
                      dbo.FeedContentAsset ON dbo.Asset.FeedContentAssetID = dbo.FeedContentAsset.FeedContentAssetID INNER JOIN
                      dbo.MimeType ON dbo.FeedContentAsset.MimeTypeID = dbo.MimeType.MimeTypeID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Asset"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 357
               Right = 228
            End
            DisplayFlags = 280
            TopColumn = 12
         End
         Begin Table = "Content_Asset"
            Begin Extent = 
               Top = 6
               Left = 266
               Bottom = 125
               Right = 438
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Content"
            Begin Extent = 
               Top = 82
               Left = 514
               Bottom = 201
               Right = 705
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "FeedContentAsset"
            Begin Extent = 
               Top = 213
               Left = 262
               Bottom = 332
               Right = 452
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MimeType"
            Begin Extent = 
               Top = 76
               Left = 884
               Bottom = 195
               Right = 1044
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'audit_ContentFeedContentAssets'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'       Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'audit_ContentFeedContentAssets'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'audit_ContentFeedContentAssets'
GO
