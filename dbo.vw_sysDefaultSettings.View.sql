USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_sysDefaultSettings]
AS
SELECT     dst.Name AS Entity, s.Name AS Setting, ds.Value
FROM         dbo.Setting AS s INNER JOIN
                      dbo.DefaultSetting AS ds ON s.SettingID = ds.SettingID INNER JOIN
                      dbo.DefaultSettingType AS dst ON ds.DefaultsettingTypeID = dst.DefaultSettingTypeID
WHERE     (dst.Active = 1)
GO
