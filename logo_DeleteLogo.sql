CREATE procedure [dbo].[logo_DeleteLogo]
	@PartnerID int,
	@LogoID int
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'logo_DeleteLogo'

	if (select COUNT(distinct launcherid) from Partner_Logo where PartnerID = @PartnerID and LogoID = @LogoID and LauncherID is not null) = 0
	begin
		delete from Partner_Logo where PartnerID = @partnerid and LogoID = @logoid
		delete from Logo where LogoID = @logoid
		if @@ERROR = 0
		select convert(varchar(200), 'Logo ' + convert(varchar(20), @logoid) + ' deleted') as MessageBack
	end
	else
	begin
		select 'There are launchers attached to Logoid ' + convert(varchar(20), @logoid) + '.
			Detach the launcher before deleting the logo.' as MessageBack
	end
	set nocount off
END
