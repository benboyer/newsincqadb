USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TargetPlatform](
	[TargetPlatformID] [smallint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [varchar](20) NOT NULL,
	[Format] [varchar](20) NULL,
	[Comments] [varchar](max) NULL,
	[CopyBack] [bit] NOT NULL,
 CONSTRAINT [PK_TargetDeviceClass] PRIMARY KEY CLUSTERED 
(
	[TargetPlatformID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[TargetPlatform] ADD  CONSTRAINT [DF_TargetPlatform_CopyBack]  DEFAULT ((0)) FOR [CopyBack]
GO
