CREATE PROCEDURE [dbo].[ps_LauncherPlaylistVideos]  
--  declare  
 @widgetID int = null,  
 @playlistID int = null,  
 @trackingGroup int = null,  
 @contentid bigint = null,  
 @devicetype int = 1  
AS  
BEGIN  
 set nocount on  
 set fmtonly off  
 exec SprocTrackingUpdate 'ps_LauncherPlaylistVideos'  
  
 Declare @now datetime,  
   @iwidgetID int,  
   @iplaylistID int,  
   @itrackinggroup int,  
   @idevicetype int  
 set  @now = GETDATE()  
 select @iwidgetID = isnull(@widgetID, 1), @iplaylistID = isnull(@playlistID, 507), @itrackinggroup = @trackingGroup, @idevicetype = isnull(@devicetype, 1)  
  
 if @PlaylistID = 9999999  
 begin  
  exec ps_SingleContent @contentid, @iTrackingGroup, @deviceType  
  return  
 end  
  
 select  distinct -- a.iscontentprivate, a.partnerid OwningPartnerid, pd.partnerid DistribPartnerid,  
     a.PlaylistID,  
     a.ContentID, a.Name, a.Description, a.TrackingGroup ContentPartnerID, a.VideoGroupName, a.Duration, convert(date, a.PubDate) PubDate, a.Keyword, convert(varchar(120), null) Timeline,  
     case when a.AssetTypeName like 'Video%' then 'src'  
       else a.AssetTypeName  
     end as AssetType,  
     a.AssetLocation, a.AssetMimeType, a.AssetSortOrder, a.AssetLocation2, a.AssetMimeType2, a.AssetSortOrder2,  
     a.ProducerName, null as ProducerNameAlt, a.logourl ProducerLogo, a.[Order]+1 ContentOrder, null ProducerCategory, null SEOFriendlyTitle, null embedCodeAccessible  
  from (-- declare @iwidgetid int = 3233, @iplaylistid int = 10203, @itrackinggroup int = 90121, @idevicetype int = 3  
     select pt.partnerid, pt.TrackingGroup, pt.isContentPrivate, p.playlistid, pc.ContentID, c.Name,  
     c.Description, c.EffectiveDate PubDate, pc.[Order], aa.FilePath, aa.Filename, aa.Duration, c.Keyword,  
     VG.VideoGroupName, at.Name AssetTypeName, mt.FileExtension, pt.logourl, pt.Name ProducerName, c.OnlyOwnerView,  
     case when mt.mediatype in ('video', 'image', 'text') then dbo.getpathtostreamasset(aa.AssetID) else null end as AssetLocation,  
     case when mt.mediatype in ('video', 'image', 'text') then dbo.GetMimeTypeToStreamAsset(aa.AssetID) else null end as AssetMimeType,  
     case when mt.mediatype in ('video', 'image', 'text') then convert(int,dbo.GetSortOrderToStreamAsset(aa.AssetID)) else null end as AssetSortOrder,  
     case when mt.mediatype in ('video', 'image', 'text') then dbo.getpathtoprogressiveasset(aa.AssetID) else null end as AssetLocation2,  
     case when mt.mediatype in ('video', 'image', 'text') then dbo.GetMimeTypeToProgressiveAsset(aa.AssetID) else null end as AssetMimeType2,  
     case when mt.mediatype in ('video', 'image', 'text') then convert(int,dbo.GetSortOrderToProgressiveAsset(aa.AssetID)) else null end as AssetSortOrder2  
   -- declare @iwidgetid int = 3233, @iplaylistid int = 10203, @itrackinggroup int = 90121, @idevicetype int = 3 select top 100 *  
   from Playlist p (nolock)  
   join Playlist_Content pc (nolock)  
   on  p.PlaylistID = pc.PlaylistID  
   join Content c  
   on  pc.ContentID = c.ContentID  
   join Partner_TargetPlatformProfile ptp  
   on  c.PartnerID = ptp.PartnerID  
   and  1 = ptp.targetplatformid -- (@idevicetype = 3 or @idevicetype = ptp.TargetPlatformID)  
   join TargetPlatform_AssetType tpat  
   on  ptp.TargetPlatformID = tpat.TargetPlatformID  
  
   join Content_Asset ca  
   on  c.ContentID = ca.ContentID  
   join AssetType at  
   on  tpat.AssetTypeID = at.AssetTypeID  
   and  ca.AssetTypeID = at.AssetTypeID  
   join Asset aa  
   on  ca.AssetID = aa.AssetID  
  
   join MimeType mt  
   on  at.MimeTypeid = mt.MimeTypeID  
   JOIN partner pt (nolock)  
   ON  c.PartnerID = pt.PartnerID  
   left join VideoGroups VG  
   on  p.PlaylistID = vg.VideoGroupId  
   where p.PlaylistID = @iplaylistID  
   and  dbo.AllowedContentID(c.contentid, @itrackinggroup, 3) = 1  
   and  dbo.AllowedLauncherContentP(@iwidgetID, c.PartnerID) = 1  
   ) a  
--   left join partner pd  
--   on @itrackinggroup = pd.TrackingGroup  
  ORDER BY a.[Order]+1  
 set nocount off  
END  

