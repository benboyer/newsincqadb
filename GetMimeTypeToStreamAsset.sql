create FUNCTION [dbo].[GetMimeTypeToStreamAsset]
(
		@assetid bigint
)

RETURNS varchar(500)
AS
BEGIN
	--select getdate() declare	@assetid bigint = 4892500 -- 4397250 -- 4397259
	declare @returnvar varchar(500),
		@ContentID bigint,
		@templateid int,
		@cpartnerid varchar(20),
		@ext varchar(10),
		@partnerid int

	set		@ContentID = (select contentid from Content_Asset (nolock) where AssetID = @assetid)
	set		@partnerid = (select partnerid from Content (nolock) where ContentID = @ContentID)
	set		@ext = (select RIGHT(filename, (CHARINDEX('.', reverse(filename)) - 1)) from Asset (nolock) where AssetID = @assetid)
	set		@cpartnerid = CONVERT(varchar(20), @partnerid)

	if @ext = 'flv'
	begin
		set @templateid = (select ISNULL((select Value from vw_partnerDefaultSettings (nolock) where Entity = 'PlayerServiceDeliveryTemplate' and Setting = 'FLVstream' and PartnerID = @cpartnerid), (select Value from vw_sysDefaultSettings (nolock) where Entity = 'PlayerServiceDeliveryTemplate' and Setting = 'FLVstream')))
		if @templateid is not null
			set @returnvar = (select MimeType from templates (nolock) where TemplatesID = @templateid)
	end
	if @ext = 'mp4'
	begin
		set @templateid = (select ISNULL((select Value from vw_partnerDefaultSettings (nolock) where Entity = 'PlayerServiceDeliveryTemplate' and Setting = 'mp4stream' and PartnerID = @partnerid), (select Value from vw_sysDefaultSettings (nolock) where Entity = 'PlayerServiceDeliveryTemplate' and Setting = 'mp4stream')))
		if @templateid is not null
			set @returnvar = (select MimeType from templates (nolock) where TemplatesID = @templateid)
	end
	-- select @partnerid, @returnvar, getdate()
	RETURN @returnvar

END
