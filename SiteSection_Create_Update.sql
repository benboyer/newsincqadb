CREATE procedure [dbo].[SiteSection_Create_Update]
		-- declare
				@SectionID int = null,
				@partnerid int = null,
				@userid int = null,
				@SectionPartnerValue varchar(20),
				@SectionValue varchar(10),
				@SubSectionValue varchar(10),
				@SectionPageType varchar(4),
				@active bit = 1,
				@returnSectionID bit = 0
as
begin
	set fmtonly off
	SET NOCOUNT ON
	exec SprocTrackingUpdate 'SiteSection_Create_Update'
			declare @ipartnerid int,
					@iUserID int,
					@iSectionPartnerValue varchar(20),
					@iSectionValue varchar(10),
					@iSubSectionValue varchar(10),
					@iSectionPageType varchar(4),

					@sspid int,		-- SectionPartnerID
					@ssid int,		-- SectionSectionID
					@spid int,		-- SectionPageTypeID ???
					@sssid int,		-- SectionSubSectionID
					@sptid int,		-- SectionPageTYpeID
					@iactive bit,
					@isectionid int	-- SectionID ???

			if @SectionValue is null set @SectionValue = '_non'
			if @SubSectionValue is null set @SubSectionValue   = '_non'
			if @SectionPageType is null set @SectionPageType  = '_fro'

			select	@iactive = @active,
					@isectionid = @sectionid,
					@ipartnerid = @partnerid,
					@iUserID = @Userid,
					@iSectionPartnerValue = @SectionPartnerValue,
					@iSectionValue = @SectionValue,
					@iSubSectionValue = @SubSectionValue,
					@iSectionPageType = @SectionPageType

			if @ipartnerid is null set @ipartnerid = (select Max(partnerid) from [user] where UserID = @iUserID)
			if @iUserID is null set @iUserID = (select top 1 userid from [User] where PartnerID = @ipartnerid)


			select @sspid = (select top 1 sectionpartnerid from SectionPartner where partnerid = @partnerid and Value = @iSectionPartnerValue)
			select @ssid = (select top 1 sectionsectionid from SectionSection where Value = @iSectionValue)
			select @sssid = (select top 1 sectionsubsectionid from SectionSubSection where Value = @iSubSectionValue)
			select @sptid = (select top 1 sectionpagetypeid from Sectionpagetype where Value = @iSectionPageType)

	if @iUserID is null or @iPartnerID is null or @sspid is null
	begin
		select 'Invalid UserID, PartnerID, or SectionPartnerID' as MessageBack
		return
	end

	if @isectionid is null
	begin
			insert into Section(PartnerID, Name, Active, [default], CreatedDate, CreatedUserID, SectionPartnerID, SectionSectionID, SectionSubSectionID, SectionPageTypeID)
			select	@iPartnerID, @iSectionPartnerValue + @iSectionValue + @iSubSectionValue + @iSectionPageType as Name,
					@iactive, 0 as 'Default', GETDATE() as CreatedDate, @iUserID as CreateduserID,
					@sspid, @ssid, @sssid, @sptid
			where not exists
					(select 1
					from	Section
					where	PartnerID = @ipartnerid
					and		Name = @iSectionPartnerValue + @iSectionValue + @iSubSectionValue + @iSectionPageType)

/*
			insert into SectionPartner(SectionID, Value)
			select SectionID, @iSectionPartnerValue
			from	Section s
			where	Name = isnull(@iSectionPartnerValue, '') + @iSectionValue + @iSubSectionValue + @iSectionPageType
			and		not exists
				(select 1
				from SectionPartner sp
				where	sp.SectionID = s.SectionID
				and		sp.Value = @iSectionPartnerValue)
*/
	end

	else
	begin
				update Section
				set Name = @iSectionPartnerValue + @iSectionValue + @iSubSectionValue + @iSectionPageType,
					Active = @iactive,
					UpdatedDate = GETDATE(),
					UpdatedUserID = @iUserID,
					SectionPartnerID = @sspid,
					SectionSectionID = @ssid,
					SectionSubSectionID = @sssid,
					SectionPageTypeID = @sptid
				where SectionID = @iSectionID

/*
			update SectionPartner
			set		Value
= @iSectionPartnerValue
			where	SectionID = @isectionID
*/

	end

	if @returnSectionID = 1
	begin
		set @isectionid = (select sectionid from Section where Name = @iSectionPartnerValue + @iSectionValue + @iSubSectionValue + @iSectionPageType)

		select @isectionid as SectionID
	end

	set nocount off

END
