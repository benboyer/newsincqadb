USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[usp_GetContentFeedContentAuditude]
    @datefrom datetime = null,
    @dateto datetime = null
AS
BEGIN
	SET FMTONLY OFF
	set nocount on
	exec SprocTrackingUpdate 'usp_GetContentFeedContentAuditude'

    DECLARE @accesspartnerid int, @landingurl varchar(120)

  
    SELECT top 100 percent c.ContentID, c.ContentTypeID, c.PartnerID, c.ContentSourceID, c.Name, c.Description, c.Category, c.Keyword, c.EffectiveDate, c.ExpirationDate, 
    c.CreatedDate, c.CreatedUserID, c.UpdatedDate, c.UpdatedUserID, c.Active, c.FeedID, partners.PartnerID, partners.ParentPartnerID, partners.Name, partners.ContactID, partners.Website, partners.StatusID, partners.CreatedDate, partners.CreatedDate, partners.CreatedUserID, Partners.UpdatedDate, partners.UpdateUserID, partners.TrackingGroup, partners.ShortName, '' as LandingURL, partners.isContentPrivate, partners.ZoneID, partners.FeedGUID, partners.isFeedGUIDActive, c.IsFileArchived, VideosLEGACY.ContentID, VideosLEGACY.ContentID, VideosLEGACY.NumberOfReviews, VideosLEGACY.Duration, VideosLEGACY.ClipPopularity, VideosLEGACY.IsAdvertisement, VideosLEGACY.CandidateOfInterest, VideosLEGACY.NumberOfPlays, VideosLEGACY.SentToFreeWheel, VideosLEGACY.FileExtension, VideosLEGACY.IngestionStatusMessage, VideosLEGACY.IsDeleted
    FROM Content c (nolock)
    INNER JOIN [Partner] AS Partners  (nolock) ON c.PartnerID = Partners.PartnerID
    INNER JOIN VideosLEGACY  (nolock) ON c.ContentID = VideosLEGACY.ContentID   
	--where c.CreatedDate > DATEADD(HH, -1, getdate())
	 WHERE (
			((c.UpdatedDate >= @datefrom AND c.UpdatedDate <= @dateto)
			OR
			(c.CreatedDate >= @datefrom AND c.CreatedDate <= @dateto))
			AND
			c.Active = 1
			AND 
			VideosLEGACY.IsDeleted = 0
			)	
    ORDER BY c.CreatedDate DESC
    set nocount off
END
GO
