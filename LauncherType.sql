TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	LauncherType	LauncherTypeID	4	int	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	LauncherType	Name	-9	nvarchar	50	100	None	None	0	None	None	-9	None	100	2	NO	39
NewsincQA	dbo	LauncherType	Description	-9	nvarchar	500	1000	None	None	1	None	None	-9	None	1000	3	YES	39
NewsincQA	dbo	LauncherType	CreatedDate	11	smalldatetime	16	16	0	None	0	None	(getutcdate())	9	3	None	4	NO	58
NewsincQA	dbo	LauncherType	UpdatedDate	11	smalldatetime	16	16	0	None	1	None	None	9	3	None	5	YES	111

index_name	index_description	index_keys
PK_LauncherType	clustered, unique, primary key located on PRIMARY	LauncherTypeID
