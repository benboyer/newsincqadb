CREATE procedure [dbo].[adm_DefaultSetting_GetTypes]
as
BEGIN
	set fmtonly off
	set nocount on
	select Name, Active, Comment from defaultsettingtype
	order by name
	set nocount off
END
