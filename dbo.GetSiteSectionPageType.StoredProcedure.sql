USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetSiteSectionPageType]
	@SectionPageTypeID int = null
as
begin
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetSiteSectionPageType'

	-- declare @SectionPageTypeID int = null
	select SectionPageTypeID, Value, Description
	from SectionPageType
	where SectionPageTypeID = ISNULL(@SectionPageTypeID, SectionPageTypeID)

	set nocount off
end
GO
