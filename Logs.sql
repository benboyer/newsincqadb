TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Logs	LogID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	Logs	LogEntry	11	datetime	23	16	3	None	0	None	None	9	3	None	2	NO	61
NewsincQA	dbo	Logs	EventDetails	-9	nvarchar	255	510	None	None	0	None	None	-9	None	510	3	NO	39
NewsincQA	dbo	Logs	EventType	-9	nvarchar	10	20	None	None	0	None	None	-9	None	20	4	NO	39
NewsincQA	dbo	Logs	MainLogID	4	int	10	4	0	10	1	None	None	4	None	None	5	YES	38
NewsincQA	dbo	Logs	LastLogID	-7	bit	1	1	None	None	1	None	((1))	-7	None	None	6	YES	50
NewsincQA	dbo	Logs	EventLocation	-9	nvarchar	50	100	None	None	0	None	('')	-9	None	100	7	NO	39
NewsincQA	dbo	Logs	EventName	-9	nvarchar	50	100	None	None	0	None	('')	-9	None	100	8	NO	39

index_name	index_description	index_keys
PK__Logs__5E5499A803B16C81	clustered, unique, primary key located on PRIMARY	LogID
