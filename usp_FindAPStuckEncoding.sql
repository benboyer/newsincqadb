CREATE procedure [dbo].[usp_FindAPStuckEncoding]
as
BEGIN
	select	distinct c.PartnerID, ca.ContentID, c.Name, c.createddate, c.UpdatedDate, cis.Name ContentStatus, ca.Content_AssetID, ca.AssetTypeID,
			ca.TargetPlatformID, cais.Name ContentAssetStatus, ca.Active CAactive, ais.Name AssetStatus, a.Active AActive, a.EncodingID, -- ca.AssetID, mt.MediaType, cvis.Name ContentVideoStatus, c.Active	Cactive, 
		 a.FilePath + '/' + a.Filename as AssetURL, case when ca.AssetTypeID = 1 then cv.FileName when ca.AssetTypeID = 2 then cv.StillframeFileName when ca.AssetTypeID = 3 then cv.ThumbnailFileName else null end as LegacyFileURL 
	-- update ca set ca.importstatusid = 5, ca.active = 1
	from	Content c join ImportStatus cis on c.ContentImportStatus = cis.ImportStatusID
	left join	ContentVideo cv on c.ContentID = cv.ContentID left join ContentVideoImportStatus cvis on cv.ContentVideoImportStatusID = cis.ImportStatusID
	join	Content_Asset ca 
	on		c.ContentID = ca.ContentID join ImportStatus cais on ca.ImportStatusID = cais.ImportStatusID
	join	Asset a 
	on		ca.AssetID = a.AssetID join ImportStatus ais on a.ImportStatusID = ais.ImportStatusID
	join	MimeType mt
	on		a.MimeTypeID = mt.MimeTypeID
	where	c.PartnerID = 499
	and		c.CreatedDate <= DATEADD(N, -30, GETDATE())
	and		a.ImportStatusID = 4
	and		a.MimeTypeID = 1
	order by ca.ContentID
END
