TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Templates	TemplatesID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	Templates	Name	12	varchar	20	20	None	None	1	None	None	12	None	20	2	YES	39
NewsincQA	dbo	Templates	Description	12	varchar	200	200	None	None	1	None	None	12	None	200	3	YES	39
NewsincQA	dbo	Templates	URLLine	12	varchar	800	800	None	None	1	None	None	12	None	800	4	YES	39
NewsincQA	dbo	Templates	MimeType	12	varchar	600	600	None	None	1	None	None	12	None	600	5	YES	39
NewsincQA	dbo	Templates	SortOrder	4	int	10	4	0	10	1	None	None	4	None	None	6	YES	38

index_name	index_description	index_keys
PK_Templates	clustered, unique, primary key located on PRIMARY	TemplatesID
