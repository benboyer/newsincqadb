TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Partner_PlaylistContentSource_Map	Partner_PlaylistContentSource_MapID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	Partner_PlaylistContentSource_Map	PartnerID	4	int	10	4	0	10	1	None	None	4	None	None	2	YES	38
NewsincQA	dbo	Partner_PlaylistContentSource_Map	PlaylistID	4	int	10	4	0	10	1	None	None	4	None	None	3	YES	38
NewsincQA	dbo	Partner_PlaylistContentSource_Map	LastContentID	-5	bigint	19	8	0	10	1	None	None	-5	None	None	4	YES	108

index_name	index_description	index_keys
PK_Partner_PlaylistContentSource_Map	clustered, unique, primary key located on PRIMARY	Partner_PlaylistContentSource_MapID
