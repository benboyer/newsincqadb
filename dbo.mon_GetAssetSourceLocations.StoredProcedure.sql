USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[mon_GetAssetSourceLocations]
	--declare 
	@ContentID bigint
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetAssetSourceLocations'

	--select @ContentID = 23604101
	--declare @ContentID bigint = 23608460
	select	@ContentID ContentID, fca.FeedContentID, fca.FeedContentAssetID, fca.Width, fca.Bitrate, fca.Duration FCADuration, fca.FilePath FeedSourceFilePath, tp.Name SelectedForPlatform, fca.MimeType, isnull(at.Name, '') SelectedForAssetType, isnull(convert(varchar(2), ca.AssetTypeID), '') AssetTypeID, 
		--case when a.EncodingID is null then 'no' else 'yes' end as TranscodeReq,
		case when a.EncodingID is not null then a.EncodingID else '' end as EncodingID,
		case when a.AssetID is null then '' else convert(varchar(10), a.assetid) end as AssetID,
		case when a.FileName is null then '' else a.FilePath + '/' + a.Filename end as ProductionFilePath, 
		case when ic.Name is null then '' else ic.Name end as ContentImportStatus,
		case when ia.Name is null then '' else ia.Name end as AssetImportStatus, 
		case when a.Active is not null then convert(char(1), a.Active) else '' end as AssetActive,
		case	when at.AssetTypeID = 1 and cv.FileName is not null then cv.FileName
				when at.AssetTypeID = 2 and cv.StillframeFileName is not null then cv.StillframeFileName
				when at.AssetTypeID = 3 and cv.ThumbnailFileName is not null  then cv.ThumbnailFileName
				else ''
				end as LegacyProductionFilePath
	-- declare @ContentID bigint = 23608460 select *
	from	(select * from FeedContentAsset (nolock) where FeedContentID in (select FeedContentID from FeedContent (nolock) where ContentID = @ContentID)) fca
	left join Asset a (nolock)
	on		fca.FeedContentAssetID = a.FeedContentAssetID
	left join (select * from Content_Asset (nolock) where ContentID = @ContentID)  ca
	on		a.AssetID = ca.AssetID
	left join	ImportStatus ic (nolock)
	on		ca.ImportStatusID = ic.ImportStatusID
	left join	AssetType at (nolock)
	on		ca.AssetTypeID = at.AssetTypeID
	left join TargetPlatform_AssetType tpat
	on		at.AssetTypeID = tpat.AssetTypeID
	left join TargetPlatform tp
	on		tpat.TargetPlatformID = tp.TargetPlatformID
	left join	ImportStatus ia (nolock)
	on		a.importstatusid = ia.ImportStatusID
	left join ContentVideo cv (nolock)
	on		ca.ContentID = cv.ContentID
	order by fca.FeedContentID, tpat.TargetPlatformID, at.AssetTypeID-- fca.FeedContentAssetID, a.AssetID 

	/*
	select	ca.ContentID, ca.AssetTypeID, at.Name AssetType, fca.FilePath FeedSourceFilePath, 
		--case when a.EncodingID is null then 'no' else 'yes' end as TranscodeReq,
		case when a.EncodingID is not null then a.EncodingID else '' end as EncodingID,
		a.FilePath + '/' + a.Filename as ProductionFilePath, 
		ic.Name ContentImportStatus,
		ia.Name AssetImportStatus, a.Active AssetActive,
		case	when at.AssetTypeID = 1 then cv.FileName
				when at.AssetTypeID = 2 then cv.StillframeFileName
				when at.AssetTypeID = 3 then cv.ThumbnailFileName
				else null
				end as LegacyProductionFilePath
	from	(select * from Content_Asset where ContentID = @ContentID)  ca
	join	ImportStatus ic
	on		ca.ImportStatusID = ic.ImportStatusID
	join	Asset a
	on		ca.AssetID = a.AssetID
	join	AssetType at
	on		ca.AssetTypeID = at.AssetTypeID
	join	FeedContentAsset fca
	on		a.FeedContentAssetID = fca.FeedContentAssetID
	join	ImportStatus ia
	on		a.importstatusid = ia.ImportStatusID
	left join ContentVideo cv
	on		ca.ContentID = cv.ContentID
	*/

	set nocount off
END
GO
