CREATE PROCEDURE [dbo].[GetZoneIDByTrackingGroup]
	-- declare
	@trackingGroup int = null
AS
BEGIN
	SET FMTONLY OFF
	set nocount on
	declare @zone int,
			@name varchar(max)

		exec SprocTrackingUpdate 'GetZoneIDByTrackingGroup'

	select @zone = Zoneid,
		@name = name
	from (SELECT TOP 1
			p.ZoneID, t.Name
		from (select Name, partnerid from mTrackingGroup (nolock) where TrackingGroup = @trackingGroup) t
		join [Partner] p (nolock)
		on	t.partnerid = p.partnerid) a

	if @zone = 0 set @zone = null

	select isnull(@zone, 50974) as ZoneID, isnull(@name, 'UNKNOWN DISTRIBUTOR') as Name
	/*
	ALTER PROCEDURE [dbo].[GetZoneIDByTrackingGroup]
		@trackingGroup int
	AS
	--	exec SprocTrackingUpdate 'GetZoneIDByTrackingGroup'

		SELECT TOP 1
			p.ZoneID
		FROM [Partner] p
		WHERE p.TrackingGroup = @trackingGroup
	*/
	set nocount off
END
