USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[adm_TrueUpFromLegacyTables]
as
BEGIN
	set nocount on
	set fmtonly off
	exec SprocTrackingUpdate 'adm_TrueUpFromLegacyTables'

	update p set p.isprovider = 1
	from	NDNUsersLEGACY nul (nolock)
	join	Partner p (nolock)
	on		nul.UserID = p.ContactID
	where	(p.isprovider = 0 and nul.usertypeid = 3 )

	update p set p.isdistributor = 0
	from	Partner p
	left join (select * from NDNUsersLEGACY (nolock) where UserTypeID > 1) nul
	on		p.ContactID = nul.UserID
	where	p.isDistributor = 1
	and		nul.EmailID is null

	update p set p.ismediasource = 1
	from	OrganizationsLEGACY (nolock) ol
	join	Partner p
	on		ol.PartnerID = p.PartnerID
	where	ol.ismediasource = 1
	and		p.ismediasource = 0
/*
-- COMMENTED OUT - only needed for initial load.  CR procs handle launcher table upon create/update
	update l set l.DisableEmailButton = wpl.DisableSocialNetworking
	from WidgetPlayerLegacy (nolock) wpl
	join Launcher l
	on		wpl.LauncherID = l.LauncherID
	where	wpl.DisableSocialNetworking <> l.DisableEmailButton or l.DisableEmailButton is null

	update l set l.DisableAdsOnPlayer = wpl.DisableAds
	-- select top 100 *
	from WidgetPlayerLegacy (nolock) wpl
	join Launcher l
	on		wpl.LauncherID = l.LauncherID
	where	wpl.DisableAds <> l.DisableAdsOnPlayer or l.DisableAdsOnPlayer is null

	update l set l.DisableAutoPlay = wpl.DisableVideoPlayOnLoad
	-- select top 100 *
	from WidgetPlayerLegacy (nolock) wpl
	join Launcher l
	on		wpl.LauncherID = l.LauncherID
	where	wpl.DisableVideoPlayOnLoad <> l.DisableAutoPlay or l.DisableAutoPlay is null

	update l set l.DisableShareButton = wpl.DisableSingleEmbed
	-- select top 100 *
	from WidgetPlayerLegacy (nolock) wpl
	join Launcher l
	on		wpl.LauncherID = l.LauncherID
	where	wpl.DisableSingleEmbed <> l.DisableShareButton or l.DisableShareButton is null

	update l set l.ContinuousPlay = wpl.ContinuousPlay
	-- select top 100 *
	from WidgetPlayerLegacy (nolock) wpl
	join Launcher l
	on		wpl.LauncherID = l.LauncherID
	where	wpl.ContinuousPlay <> l.ContinuousPlay or l.ContinuousPlay is null
*/
	set nocount off
END
GO
