TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	DMAInclude	DMAIncludeID	-5	bigint identity	19	8	0	10	0	None	None	-5	None	None	1	NO	63
NewsincQA	dbo	DMAInclude	ContentID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	DMAInclude	DMACode	4	int	10	4	0	10	1	None	None	4	None	None	3	YES	38
NewsincQA	dbo	DMAInclude	StartDTM	11	smalldatetime	16	16	0	None	1	None	None	9	3	None	4	YES	111
NewsincQA	dbo	DMAInclude	EndDTM	11	smalldatetime	16	16	0	None	1	None	None	9	3	None	5	YES	111

index_name	index_description	index_keys
IX_DMAIncludeCIDdmacd	nonclustered located on PRIMARY	ContentID, DMACode
IX_DMAIncludeStdtEnDt	nonclustered located on PRIMARY	StartDTM, EndDTM
PK_DMAInclude	clustered, unique, primary key located on PRIMARY	DMAIncludeID
