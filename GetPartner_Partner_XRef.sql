CREATE procedure [dbo].[GetPartner_Partner_XRef]
	@PartnerID int = 499,
	@PartnerSourceID varchar(20)
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetPartner_Partner_XRef'
	
	if exists (select 1 from Partner_Partner_xRef where MasterPartnerID = @PartnerID and PartnerSourceID = @PartnerSourceID)
	begin
		select PartnerID, convert(varchar(20), PartnerIdentifier) as PartnerIdentifier  -- select * -- select top 100 *
		from	Partner_Partner_xRef
		where	MasterPartnerID = @PartnerID
		and		PartnerSourceID = @PartnerSourceID
	end
	else
	begin
		select PartnerID, convert(varchar(20), TrackingGroup) as PartnerIdentifier
		from	Partner where PartnerID = @PartnerID
	end

	set nocount off
END
