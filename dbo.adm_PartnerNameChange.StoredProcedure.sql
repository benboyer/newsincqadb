USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[adm_PartnerNameChange]
(
	--declare
	@partnerid int = null,
	@trackinggroup int = null,
	@oldname varchar(200),
	@newname varchar(200)
)
as
BEGIN
	set FMTONLY OFF
	set NOCOUNT ON
	exec sproctrackingupdate 'adm_PartnerNameChange'

	if	@partnerid is null
		and
		@trackinggroup is null
	begin
		select 'Gimme a PartnerID or Trackinggroup' as MessageBack
		return
	end

	set @oldname = LTRIM(rtrim(@oldname))
	set @newname = LTRIM(rtrim(@newname))
	
	if @partnerid is null set @partnerid = (select partnerid from partner (nolock) where trackinggroup = @trackinggroup)
	if @trackinggroup is null set @trackinggroup = (select trackinggroup from partner (nolock) where partnerid = @partnerid)

	if	(select COUNT(*) from partner (nolock) where partnerid = @partnerid and Name = @oldname) = 1
		and
		(select COUNT(*) from PartnerLookup (nolock) where PartnerID = CONVERT(varchar(20), @trackinggroup) and Name = @oldname) = 1
	begin
		--select * from Partner where PartnerID = @partnerid and Name = @oldname
		update Partner set Name = @newname, UpdateUserID = 7534, UpdatedDate = GETDATE(), ShortName = lower(dbo.GetNOspecialChars(@newname)) where PartnerID = @partnerid and Name = @oldname
		
		-- select * from PartnerLookup where PartnerID = CONVERT(varchar(20), @trackinggroup) and Name = @oldname
		update PartnerLookup set Name = @newname where PartnerID = CONVERT(varchar(20), @trackinggroup) and Name = @oldname

		select 'Name Updated: from ' + @oldname + ' to ' + @newname
		from partner p  (nolock)
		join partnerlookup pl (nolock)
		on p.trackinggroup = pl.partnerid and p.name = pl.name
		where p.partnerid = @partnerid
	end
	else
	begin
		select 'Something is fubar.  Fixit' as MessageBack
	end
END
GO
