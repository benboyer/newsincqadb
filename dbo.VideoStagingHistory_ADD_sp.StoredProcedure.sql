USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VideoStagingHistory_ADD_sp]
           @VideoStagingId int,
           @Status int,
           @CodecUsed nvarchar,
           @ProcessDate smalldatetime,
           @Remarks nvarchar

AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'VideoStagingHistory_ADD_sp'

	INSERT INTO dbo.ContentVideoImportLog
           (ContentID,
           ContentVideoImportStatus,
           CreatedDate,
           [Message])
     VALUES
           (@VideoStagingId,
           @Status,
           @ProcessDate,
           @Remarks)
END
GO
