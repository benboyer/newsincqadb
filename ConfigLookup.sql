TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	ConfigLookup	ConfigId	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	ConfigLookup	Key	-9	nvarchar	50	100	None	None	0	None	None	-9	None	100	2	NO	39
NewsincQA	dbo	ConfigLookup	Value	-9	nvarchar	1000	2000	None	None	0	None	None	-9	None	2000	3	NO	39
NewsincQA	dbo	ConfigLookup	Category	-9	nvarchar	250	500	None	None	0	None	None	-9	None	500	4	NO	39

index_name	index_description	index_keys
IX_ConfigLookupCat	nonclustered located on PRIMARY	Category
PK_ConfigLookUp	clustered, unique, primary key located on PRIMARY	ConfigId
UK_ConfigLookUp	nonclustered, unique, unique key located on PRIMARY	Key
