USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VideoStaging_UPDATE_sp]
	@VideoStagingID int,
	@UserID int,
	@ClipURL nvarchar(200),
	@Title nvarchar(100),
	@Description nvarchar(1000),
	@EventDate datetime,
	@Height decimal(10,2),
	@Width decimal(10,2),
	@Duration decimal(10,2)=null,
	@CreatedOn datetime,
	@ThumbnailURL nvarchar(200),
	@PublishDate datetime,
	@Keywords nvarchar(500),
	@Guid nvarchar(500),
	@ReadyForEncoding bit=null,
	@OriginalName nvarchar(200),
	@EncodingStatus smallint,
	@EncodingPercentCompleted int=null,
	@EncodingStarted datetime=null,
	@EncodingCompleted datetime=null,
	@ExpiryDate datetime,
	@Status int=null,
	@Active bit,
	@IsEnabled bit,
	@IsDeleted bit,
	@IngestionSource nvarchar(50),
	@FileSize int=null,
	@StillFrameURL nvarchar(200)=null,
	@AudioTrackURL nvarchar(1000)=null,
	@EncodedURL nvarchar(1000),
	@VideoId int,
	@NexidiaStatus int=null,
	@Mp3Started datetime=null,
	@Mp3Completed datetime=null,
	@NiaStatus int,
	@CodecUsed nchar(100)=null,
	@FileExtension nvarchar(5)=null,
	@Category nvarchar(50)



AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'VideoStaging_UPDATE_sp'

	Declare @NewID int;
	Declare @ErrorCode int;


	Declare @PartnerID int;

	Select @PartnerID = PartnerID
	From [User]
	Where UserID = @UserID;

    -- Insert statements for procedure here
	--    BEGIN TRANSACTION
	-- select top 100 * from VideosLEGACY order by contentid desc

	UPDATE VideosLEGACY SET NumberOfReviews=null, ClipPopularity=null, CandidateOfInterest=null, NumberOfPlays=null,
	SentToFreeWheel = 0, FileExtension=@FileExtension, IngestionStatusMessage= null, IsDeleted=@IsDeleted
	WHERE ContentID = @VideoStagingID

	UPDATE Content
	SET ContentTypeID = 1,
	PartnerID = @PartnerID,
	ContentSourceID = 1,
	Name = @Title,
	[Description] = @Description,
	Category = @Category,
	Keyword = @Keywords,
	EffectiveDate = @PublishDate,
	ExpirationDate = @ExpiryDate,
	Active = @Active,
	isDeleted = @IsDeleted,
	CreatedDate = @CreatedOn,
	CreatedUserID = @UserID,
	UpdatedDate = GETDATE(),
	UpdatedUserID = @UserID
	WHERE
	ContentID = @VideoStagingID

	if @@ERROR = 0
	BEGIN

	 select @NewID = SCOPE_IDENTITY()

	 Update ContentVideo
	 Set [GUID] = @Guid,
	 ContentVideoImportStatusID = @EncodingStatus,
	 [FileName] = null,
	 ImportFilePath = @ClipURL,
	 Height = @Height,
	 Width = @Width,
	 Duration = @Duration,
	 ThumbnailImportFilePath = @ThumbnailURL,
	 ThumbnailFileName = null,
	 StillframeImportFilePath = @StillFrameURL,
	 StillframeFileName = null
	 Where ContentID = @VideoStagingID

-- select top 100 * from VideoStagingLEGACY where filesize is not null order by contentid desc
	END
	if @@ERROR = 0
	BEGIN
		Update VideoStagingLEGACY
		SET FileExtension = @FileExtension,
		NexidiaStatus = 0,
		NiaStatus = 0,
		IsDeleted = @IsDeleted
		Where ContentID = @VideoStagingID
	END
--	if @@ERROR = 0
--		COMMIT TRANSACTION
--	ELSE
--		ROLLBACK TRANSACTION

	SELECT @NewID as ID

END
GO
