USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EncodingDotComStatus](
	[ContentID] [int] NOT NULL,
	[MediaID] [nvarchar](100) NOT NULL,
	[AssetType] [nvarchar](50) NOT NULL,
	[Processed] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_EncodingDotComStatus_1] PRIMARY KEY CLUSTERED 
(
	[ContentID] ASC,
	[MediaID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EncodingDotComStatus] ADD  CONSTRAINT [DF_EncodingDotComStatus_CreatedDate]  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
