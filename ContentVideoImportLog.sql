TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	ContentVideoImportLog	ContentVideoImportLogID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	ContentVideoImportLog	ContentID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	ContentVideoImportLog	ContentVideoImportStatus	4	int	10	4	0	10	0	None	None	4	None	None	3	NO	56
NewsincQA	dbo	ContentVideoImportLog	Message	-9	nvarchar	255	510	None	None	1	None	None	-9	None	510	4	YES	39
NewsincQA	dbo	ContentVideoImportLog	CreatedDate	11	datetime	23	16	3	None	0	None	(getutcdate())	9	3	None	5	NO	61

index_name	index_description	index_keys
IX_ContentVideoImportLogCID	nonclustered located on PRIMARY	ContentID
PK_ContentVideoImportLog	clustered, unique, primary key located on PRIMARY	ContentVideoImportLogID
