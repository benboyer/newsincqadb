CREATE PROCEDURE [dbo].[SiteSection_ADD_sp]
	@Name nvarchar(50),
	@CreatedDT smalldatetime,
	@IsEnabled bit,
	@IsDefault bit,
	@UserID int = null
AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'SiteSection_ADD_sp'

	Declare @PartnerID int;

	Select @PartnerID = PartnerID from [User] where UserID = @UserID

    Insert into Section(PartnerID, Name, [Default], Active, CreatedUserID)
    values(@PartnerID, @Name, @IsDefault, @IsEnabled, @UserID)

	Select SCOPE_IDENTITY() as ID
	SET NOCOUNT OFF;

END
