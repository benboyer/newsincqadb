CREATE procedure [dbo].[usp_Playlist_Content_DynMap]  
 @PartnerID bigint = null,  
 @contentid bigint = null  
AS  
begin  
 exec SprocTrackingUpdate 'usp_Playlist_Content_DynMap'  
  
 -- declare @ContentID bigint, @PartnerID bigint set @PartnerID = 499 set @ContentID = 23395629  
 declare @plid bigint  
 select distinct c.ContentID, a.Categories, pcpm.playlistid, pcpm.MaxVideoCount  
 into #mapped -- select * from #mapped  
 from dbo.fn_Categories_fmContentCategories(@ContentID) a  
 join Content c  
 on  @contentid = c.contentid  
 join Playlist_content_partnermap pcpm  
 on  a.Categories = pcpm.sourcecategory  
 left join Playlist_Content pc  
 on  c.ContentID = pc.ContentID  
 and  pcpm.playlistid = pc.PlaylistID  
 where c.partnerid = pcpm.partnerid  
 and  pc.ContentID is null  
 and not exists  
  (select 1  
  from Playlist_Content pc2  
  --join Content c2       -- this line removed 20120626  
  --on  pc2.ContentID = c2.ContentID -- this line removed 20120626  
  where pc2.PlaylistID = pcpm.PlaylistID  
  and  pc2.ContentID = @contentid)   -- this line added 20120626  
--  and  c2.Name <> 'AP Top Stories'   -- this line removed 20120626  
--  and  c2.name = c.name)     -- this line removed 20120626  
  
 if (select COUNT(*) from #mapped) > 0  
 begin  
  while (select COUNT(*) from #mapped) > 0  
  begin  
   set @plid = (select MIN(playlistid) from #mapped)  
   if (select COUNT(*) from playlist_content where playlistid = @plid) > 0  
    update Playlist_Content  
    set [Order] = [Order] + 1  
    where PlaylistID = @plid  
     insert into Playlist_Content(ContentID, PlaylistID, [Order])  
   select distinct ContentID, PlaylistID, 0  
   from #mapped  
   where playlistid = @plid  
   and  not exists  
     (select 1  
     from Playlist_Content pc  
     where pc.ContentID = #mapped.ContentID  
     and  pc.PlaylistID = #mapped.playlistid)  
  
    insert into PlaylistVideosLEGACY(PlaylistID, ContentID, Sharing)  
   select distinct PlaylistID, ContentID, 1  
   from #mapped  
   where playlistid = @plid  
   and  not exists  
     (select 1  
     from PlaylistVideosLEGACY pc  
     where pc.ContentID = #mapped.ContentID  
     and  pc.PlaylistID = #mapped.playlistid)  
   if @@ERROR = 0  
   begin  
  
    update pcm  
    set  pcm.LastContentID = m.ContentID -- select top 100 *  
    from #mapped m  
    join Playlist_content_partnermap pcm  
    on  m.playlistid = pcm.playlistid  
    and  @PartnerID = pcm.PartnerID  
    where m.PlaylistID = @plid  
      
-- Delete any videos that exceed the count of maxvideosfortheplaylist, first from the legacy table  
    delete pvl  
    from (select playlistid, contentid  
      from Playlist_Content  
      where PlaylistID = @plid  
      and  [Order] > (select MaxVideoCount -1 from #mapped where PlaylistID = @plid)) a  
    join PlaylistVideosLEGACY pvl  
    on  a.PlaylistID = pvl.PlaylistID  
    and  a.ContentID = pvl.ContentID  
-- Delete any videos that exceed the count of maxvideosfortheplaylist, second, form the real table  
    delete  
    from Playlist_Content  
    where PlaylistID = @plid  
    and  [Order] > (select MaxVideoCount -1 from #mapped where PlaylistID = @plid)  
-- Update the Playlist table to reflect that content has changed (for player service cache updating)  
    update Playlist  
    set  ContentUpdatedDate = GETDATE()  
    where PlaylistID = @plid  
-- Delete the variable for this record from our temp table  
-- declare @plid bigint set @plid = (select MIN(playlistid) from #mapped) select @plid  
    delete  
    from #mapped  
    where playlistid = @plid  
    and  ContentID = @contentid  
   end -- end of "no error" condition  
  end  -- end of "Counting rows in the temp table"  
 end   -- end of "we have data in the temp table" condition  
 drop table #mapped  
 select @@error  
END  
  
