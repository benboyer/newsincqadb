create procedure dbo.dmev_Partner_TargetPlatformProfile
	@PartnerID int = null,
	@TargetPlatformID int = null
as
select * 
from dme_Partner_TargetPlatformProfile (nolock) ptpp
where	ptpp.PartnerID = ISNULL(@PartnerID, ptpp.PartnerID)
and		ptpp.TargetPlatformID = ISNULL(@TargetPlatformID, ptpp.TargetPlatformID)
