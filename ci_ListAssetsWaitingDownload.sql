CREATE procedure [dbo].[ci_ListAssetsWaitingDownload]
	-- declare
	@systemid int,
	@priority int = 10000,
	@sourceid int = 0
as
BEGIN
	set FMTONLY OFF
	set NOCOUNT ON
	exec sproctrackingupdate 'ci_ListAssetsWaitingDownload'
	select cc.assetid
	from (select ca.*
		from	(select * from Asset (nolock) where ImportStatusID in (1,2)) a
		join	content_asset (nolock) ca
		on		a.assetid = ca.assetid
		where	ca.importstatusid <> 6) cc
	join	Content (nolock) c
	on		cc.ContentID = c.contentid
	join	Feed (nolock) f
	on		c.FeedID = f.FeedId
	where	f.contentimportsystemid = @systemid and c.ContentSourceID = @sourceid
	and		(@priority = 0 or f.FeedPriorityID = @priority)
	set NOCOUNT OFF
END
