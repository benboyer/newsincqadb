CREATE PROCEDURE [dbo].[GetDetailTextSearchResult]
(	-- declare
	@SearchString VARCHAR(max) = null,
	@DescriptionString varchar(max) = null,
	@TitleString varchar(max) = null,
	@KeywordString varchar(max) = null,
	@WidgetId INT = NULL,
	@UserId INT = 0,
	@OrgId INT =0,
	@Count INT = 100,
	@OrderClause NVARCHAR(MAX) = null,
	@StatusType INT = 0,
	@IsAdmin BIT = 0,
	@WhereCondition NVARCHAR(MAX) = ''
	, @StartDTM datetime = null
	, @EndDTM datetime = null
	, @devicetype int = 1
	, @VideoID bigint = null
	, @MinVideoID bigint = null -- max value of the current external record set: used for pagination
	, @MaxVideoID bigint = null -- min value of the current external record set: used for pagination
)

AS
BEGIN
	SET FMTONLY OFF
	set NOCOUNT ON
	exec SprocTrackingUpdate 'GetDetailTextSearchResult'

	-- NEW for more recent search results
	-- declare @iStartDTM datetime = '2011-04-01', @iEndDTM datetime = '2011-04-30 23:59:59'
	DECLARE @iStartDTM nvarchar(12),
			@iEndDTM nvarchar(12),
			@queryString NVARCHAR(MAX),
			@WhereClause NVARCHAR(MAX),
			@OrganizationId INT,
			@range varchar(max),
			@SearchStringPredicate varchar(max) = null,
			@distpartner int,
			@now datetime = getdate(),
			@userTG varchar(10)

			select @distpartner = partnerid from [User] where UserID = @UserId
			SELECT @OrganizationId = PartnerId FROM [User] WHERE UserID = @UserId
			set @userTG = (select convert(varchar(10), trackinggroup) from Partner where PartnerID = @OrganizationId)
			set @IsAdmin = (select isadmin from NDNUsersLEGACY where UserID = @UserId)

			if @devicetype is null set @devicetype = 1

		declare @count_external int set @count_external = isnull(@count, 100)

		if @VideoID is null
		begin
				if @DescriptionString is not null or @TitleString is not null or @KeywordString is not null or @SearchString is not null
				begin

					set @SearchStringPredicate = '(select distinct * from ('

					if @DescriptionString is not null
							set @SearchStringPredicate = @SearchStringPredicate + ' select * from freetexttable(videos, [description], ''' + @DescriptionString + ''')'

					if @TitleString is not null
					begin
						if @DescriptionString is not null set @SearchStringPredicate = @SearchStringPredicate + '
						union all
						'
						set @SearchStringPredicate = @SearchStringPredicate + ' select * from freetexttable(videos, [Title], ''' + @TitleString + ''')'
					end

					if @KeywordString is not null
					begin
						if @DescriptionString is not null or @TitleString is not null set @SearchStringPredicate = @SearchStringPredicate + '
						union all
						'
						set @SearchStringPredicate = @SearchStringPredicate + ' select * from freetexttable(videos, [keywords], ''' + @KeywordString + ''')'
					end

					if (@DescriptionString is not null or @TitleString is not null or @KeywordString is not null) and @SearchString is not null
						set @SearchStringPredicate = @SearchStringPredicate + '
						union all
						'
					if @SearchString is not null
					begin
						--if @DescriptionString is not null or @TitleString is not null or @KeywordString is not null
						set @SearchStringPredicate = @SearchStringPredicate + ' select * from FREETEXTTABLE(Videos, ([Description], Title, Keywords), ''' + isnull(@SearchString, '') + ''')'
					end
					set @SearchStringPredicate = @SearchStringPredicate + ') a ) as KT '
				end
		--select @searchStringPredicate

		-- INNER JOIN FREETEXTTABLE(Videos, ([Description], Title, Keywords), ''' + @SearchString + ''')
		--			set @SearchString = @SearchString  + ''')
		set @iStartDTM = convert(varchar(12), @StartDTM, 111)
		set @iEndDTM = convert(varchar(12), @EndDTM, 111)
		if @iStartDTM is not null
			set @range = ' AND vs.Publishdate between ' + '''' + @iStartDTM + '''' + ' and ' + '''' + @iEndDTM + ''''
		--select @range ranger

--		declare @count_external int set @count_external = isnull(@count, 100)
		declare @OrderClause_external varchar(max) set @OrderClause_external = @OrderClause

		set @count = @count * 4
		set @OrderClause = ' vs.PublishDate DESC '


		SET @WidgetId =  ISNULL(@WidgetId,0)
		SET @UserId =  ISNULL(@UserId,0)
		SET @OrgId =  ISNULL(@OrgId,0)
		SET @Count =  ISNULL(@Count,100)
		SET @OrderClause =  ISNULL(@OrderClause,'')
		SET	@WhereClause =  ISNULL(@WhereClause,'')


		SET @queryString=
			' select vs.[VideoID]
			  ,vs.[UserID]
			  ,vs.[UserType]
			  ,vs.[ClipURL]
			  ,vs.[Title]
			  ,vs.[Description]
			  ,ISNULL(vs.[EventDate], vs.[PublishDate]) AS EventDate
			  ,vs.[NumberofReviews]
			  ,vs.[Height]
			  ,vs.[Width]
			  ,vs.[Duration]
			  ,vs.[ClipPopularity]
			  ,vs.[IsAdvertisement]
			  ,vs.[CreatedOn]
			  ,vs.[ThumbnailURL]
			  ,vs.[PublishDate]
			  ,ISNULL(vs.[Keywords], '''') AS Keywords
			  ,vs.[NumberOfPlays]
			  ,vs.[OriginalName]
			  ,vs.[ExpiryDate]
			  ,vs.[IsEnabled]
			  ,vs.[IsDeleted]
			  ,isnull(us.FirstName,'''') + '' '' + isnull(us.LastName,'''') AS UserName
			  ,us.OrganizationID' +
				case when @searchStringPredicate is not null then ',KT.[Rank]
				'
				else ',0 as rank
				'
				end +
			  ',isnull(org.Name,'''') as OrganizationName
			  ,isnull(org.IsMediaSource, 0) as IsMediaSource
			  ,us.[isAdmin]
			  FROM Videos (nolock) vs join content (nolock) c on vs.videoid = c.contentid '
				+ case when @SearchStringPredicate is not null then '
						INNER JOIN ' + @SearchStringPredicate + '
				ON vs.VideoID = KT.[KEY]
				join content_asset (nolock) ca on vs.videoid = ca.contentid and ' + convert(varchar(10), @devicetype) + ' = ca.targetplatformid and 1 = ca.assettypeid join assettype at on ca.assettypeid = at.assettypeid join mimetype mt on at.mimetypeid = mt.mimetypeid and ' + '''' + 'video' + '''' + ' = mt.mediatype
				'
				else '
				join content_asset (nolock) ca on vs.videoid = ca.contentid and ' + convert(varchar(10), @devicetype) + ' = ca.targetplatformid and 1 = ca.assettypeid join assettype at on ca.assettypeid = at.assettypeid join mimetype mt on at.mimetypeid = mt.mimetypeid and ' + '''' + 'video' + '''' + ' = mt.mediatype
				'
				end + '
				INNER JOIN NDNUsers (nolock) us ON vs.UserId = us.UserId
				INNER JOIN Organizations (nolock) AS org ON org.OrganizationID = us.OrganizationID
				WHERE 1 = 1 '

		IF(@StatusType IS NOT NULL AND @StatusType != -1  )
		BEGIN
			SELECT @WhereClause = ' AND  '
			SELECT @WhereClause = @WhereClause + (CASE CONVERT(NVARCHAR(1),@StatusType)
										WHEN '0' THEN '  c.active = 1 AND c.IsDeleted = 0 AND (c.expirationdate IS NULL OR c.expirationdate > GETDATE()) '
										WHEN '1' THEN '  ( (c.active = 0 OR c.expirationdate < getdate()) AND c.IsDeleted = 0) '
										WHEN '2' THEN '  c.IsDeleted = 1 '
										END)
		END

--		IF (@IsAdmin = 0)
--			SELECT @WhereClause = @WhereClause + ' AND vs.UserId = ' + CONVERT(NVARCHAR(20),@UserID) ;

		IF (@WhereCondition IS NOT NULL AND @WhereCondition != '')
			SELECT @WhereClause = @WhereClause + ' AND '+ @WhereCondition ;

		IF(@OrgId IS NULL OR @OrgId >0)
			SELECT @WhereClause = @Whereclause + ' AND org.OrganizationId = ' + CONVERT(NVARCHAR(20),@OrgId)


		SET @queryString = @queryString + ISNULL(@WhereClause,'')

		if (@StartDTM) is not null and (@EndDTM) is not null
			SELECT @queryString = @queryString + @range
		-- select @range
		-- select @WhereClause
		-- select @queryString


		-- IF(@WhereClause IS NOT NULL AND @OrderClause IS NOT NULL AND @OrderClause != '')
			SET @queryString = @queryString + ' Order By ' + @OrderClause
		--	select @queryString
	end
	-- select @VideoID vId, @MinVideoID MinVID, @MaxVideoID MAxVID, @UserId UsrID, @userTG UTG, @distpartner DistPID, @organizationid OrgID, @IsAdmin isadmin


	create table #ftsr
			(VideoID int, UserID int, UserType int, ClipURL varchar(max), title varchar(max), Description varchar(max),
			EventDate datetime, NumberOfReviews int, height decimal(10,4), width decimal(10,4), duration decimal(10,4),
			ClipPopularity float, IsAdvertisement bit, createdon datetime, ThumbnailURL varchar(max), PublishDate datetime,
			Keywords varchar(max), NumberOfPlays int, originalname varchar(max), Expirydate datetime, isEnabled bit,
			IsDeleted bit, UserName varchar(max), organizationid int, [rank] int, OrganizationName varchar(max), isMediaSource bit, isAdmin bit)

	if @VideoID is null and @MinVideoID is null and @MaxVideoID is null
	begin
		insert into #ftsr(VideoID, UserID, UserType, ClipURL, title, Description,
				EventDate, NumberOfReviews, height, width, duration,
				ClipPopularity, IsAdvertisement, createdon, ThumbnailURL, PublishDate,
				Keywords, NumberOfPlays, originalname, Expirydate, isEnabled,
				IsDeleted, UserName,  organizationid, [rank], OrganizationName, isMediaSource, isAdmin)

		EXEC ( @queryString );
	end

	if isnull(@UserId, 0) = 0
		set @distpartner = -1
-------------------------------

-- declare @videoid int = 24297397, @distpartner int = 197, @now datetime = getdate(), @userid int = 7534
	if @VideoID is not null
	begin
		insert into #ftsr(VideoID, UserID, UserType, ClipURL, title, Description,
				EventDate, NumberOfReviews, height, width, duration,
				ClipPopularity, IsAdvertisement, createdon, ThumbnailURL, PublishDate,
				Keywords, NumberOfPlays, originalname, Expirydate, isEnabled,
				IsDeleted, UserName,  organizationid, [rank], OrganizationName, isMediaSource, isAdmin)
		-- declare @videoid int = 24297397, @distpartner int = 170, @now datetime = getdate(), @userid int = 7534, @userTG int = 10557, @organizationid int = 170, @widgetid int = 0, @IsAdmin bit = 1
		select VideoID, u.UserID, UserType, ClipURL, Title, v.Description, EventDate, NumberOfReviews, Height, Width, v.Duration,
				ClipPopularity, IsAdvertisement, CreatedOn, ThumbnailURL, PublishDate,
				Keywords, NumberOfPlays, OriginalName, ExpiryDate, IsEnabled,
				v.IsDeleted, username, organizationid, 100 [rank], p.name organizationname, ol.IsMediaSource, u.isAdmin
		from	(select * from Videos (nolock) where VideoID = @VideoID) v
		join	Content c (nolock)
		on		v.VideoID = c.ContentID
		join	Partner p (nolock)
		on		c.PartnerID = p.PartnerID
		join	NDNUsers u  (nolock)
		on		c.CreatedUserID = u.UserID
		join	OrganizationsLEGACY ol (nolock)
		on		p.PartnerID = ol.PartnerID
		where	(dbo.AllowedContentID(c.contentid, @userTG, 1) = 1 or @organizationid = c.partnerid or @IsAdmin = 1)
		and		(dbo.AllowedLauncherContent(@widgetid, @userTG) = 1 or @widgetid = 0)
	end

	if (@MinVideoID is not null and @MaxVideoID is null)
	begin
		insert into #ftsr(VideoID, UserID, UserType, ClipURL, title, Description,
				EventDate, NumberOfReviews, height, width, duration,
				ClipPopularity, IsAdvertisement, createdon, ThumbnailURL, PublishDate,
				Keywords, NumberOfPlays, originalname, Expirydate, isEnabled,
				IsDeleted, UserName,  organizationid, [rank], OrganizationName, isMediaSource, isAdmin)
		select VideoID, u.UserID, UserType, ClipURL, Title, v.Description, EventDate, NumberOfReviews, Height, Width, v.Duration,
				ClipPopularity, IsAdvertisement, CreatedOn, ThumbnailURL, PublishDate,
				Keywords, NumberOfPlays, OriginalName, ExpiryDate, IsEnabled,
				v.IsDeleted, username, organizationid, 100 [rank], p.name organizationname, ol.IsMediaSource, u.isAdmin
		from	(select * from Videos (nolock) where VideoID > @MinVideoID) v
		join	Content c (nolock)
		on		v.VideoID = c.ContentID
		join	Partner p (nolock)
		on		c.PartnerID = p.PartnerID
		join	NDNUsers u  (nolock)
		on		c.CreatedUserID = u.UserID
		join	OrganizationsLEGACY ol (nolock)
		on		p.PartnerID = ol.PartnerID
		where	(dbo.AllowedContentID(c.contentid, @userTG, 1) = 1 or @organizationid = c.partnerid or @IsAdmin = 1)
		and		(dbo.AllowedLauncherContent(@widgetid, @userTG) = 1 or @widgetid = 0)
	end


	if (@MinVideoID is not null and @MaxVideoID is not null)
	begin
		insert into #ftsr(VideoID, UserID, UserType, ClipURL, title, Description,
				EventDate, NumberOfReviews, height, width, duration,
				ClipPopularity, IsAdvertisement, createdon, ThumbnailURL, PublishDate,
				Keywords, NumberOfPlays, originalname, Expirydate, isEnabled,
				IsDeleted, UserName,  organizationid, [rank], OrganizationName, isMediaSource, isAdmin)
		select VideoID, u.UserID, UserType, ClipURL, Title, v.Description, EventDate, NumberOfReviews, Height, Width, v.Duration,
				ClipPopularity, IsAdvertisement, CreatedOn, ThumbnailURL, PublishDate,
				Keywords, NumberOfPlays, OriginalName, ExpiryDate, IsEnabled,
				v.IsDeleted, username, organizationid, 100 [rank], p.name organizationname, ol.IsMediaSource, u.isAdmin
		from	(select * from Videos (nolock) where VideoID >= @MinVideoID and VideoID <= @MaxVideoID) v
		join	Content c (nolock)
		on		v.VideoID = c.ContentID
		join	Partner p (nolock)
		on		c.PartnerID = p.PartnerID
		join	NDNUsers u  (nolock)
		on		c.CreatedUserID = u.UserID
		join	OrganizationsLEGACY ol (nolock)
		on		p.PartnerID = ol.PartnerID
		where	(dbo.AllowedContentID(c.contentid, @userTG, 1) = 1 or @organizationid = c.partnerid or @IsAdmin = 1)
		and		(dbo.AllowedLauncherContent(@widgetid, @userTG) = 1 or @widgetid = 0)
	end
	-- select top 100 * from videos

	-- select @count_external
	-- declare
	set @queryString = ' select top ' + convert(varchar(20), @count_external) + '
			a.VideoID, a.UserID, a.UserType, a.ClipURL, a.title, left(a.Description, 500) Description,
			a.EventDate, a.NumberOfReviews, a.Height, a.Width, a.Duration,
			isnull(a.ClipPopularity, 0) ClipPopularity, a.IsAdvertisement, a.createdon, a.ThumbnailURL, a.PublishDate,
			left(a.Keywords, 100) Keywords, a.NumberOfPlays, a.originalname, a.Expirydate, a.isEnabled,
			a.IsDeleted, xyz.OnlyOwnerView, a.UserName,  a.organizationid, a.[rank], a.OrganizationName, a.isMediaSource, a.isAdmin
			from #ftsr a join content xyz on a.VideoID = xyz.ContentID
			where	(dbo.AllowedContentID(a.videoid, ' + convert(varchar(10), @userTG) + ', 1) = 1 or ' + convert(varchar(10), @organizationid) + ' = a.organizationid or ' + convert(char(1), @isAdmin) + ' = 1)
				' + case when @widgetid is null then '' else ' and dbo.AllowedLauncherContent(' + convert(varchar(10), @widgetid) + ',' + CONVERT(varchar(10),  @userTG) + ') = 1'  end + '
			order by ' + isnull(@OrderClause_external, 'a.PublishDate DESC')
	-- select @queryString
	EXEC ( @queryString );

	drop table #ftsr


	SET NOCOUNT OFF
END
