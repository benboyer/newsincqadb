USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Playlist_Content_PartnerMapDefaultCategory2](
	[Playlist_Content_PartnerMapDefaultCategory2ID] [int] IDENTITY(1,1) NOT NULL,
	[PartnerID] [int] NULL,
	[Category] [varchar](60) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
