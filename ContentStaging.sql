TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	ContentStaging	ContentID	-5	bigint identity	19	8	0	10	0	None	None	-5	None	None	1	NO	63
NewsincQA	dbo	ContentStaging	ContentTypeID	5	smallint	5	2	0	10	0	None	None	5	None	None	2	NO	52
NewsincQA	dbo	ContentStaging	PartnerID	4	int	10	4	0	10	0	None	None	4	None	None	3	NO	56
NewsincQA	dbo	ContentStaging	ContentSourceID	5	smallint	5	2	0	10	0	None	None	5	None	None	4	NO	52
NewsincQA	dbo	ContentStaging	Name	12	varchar	500	500	None	None	0	None	None	12	None	500	5	NO	39
NewsincQA	dbo	ContentStaging	Description	12	varchar	1000	1000	None	None	0	None	None	12	None	1000	6	NO	39
NewsincQA	dbo	ContentStaging	Category	12	varchar	250	250	None	None	1	None	None	12	None	250	7	YES	39
NewsincQA	dbo	ContentStaging	Keyword	12	varchar	1000	1000	None	None	1	None	None	12	None	1000	8	YES	39
NewsincQA	dbo	ContentStaging	Duration	3	decimal	10	12	2	10	1	None	None	3	None	None	9	YES	106
NewsincQA	dbo	ContentStaging	EffectiveDate	11	smalldatetime	16	16	0	None	0	None	None	9	3	None	10	NO	58
NewsincQA	dbo	ContentStaging	ExpirationDate	11	smalldatetime	16	16	0	None	1	None	None	9	3	None	11	YES	111
NewsincQA	dbo	ContentStaging	CreatedDate	11	smalldatetime	16	16	0	None	0	None	(getdate())	9	3	None	12	NO	58
NewsincQA	dbo	ContentStaging	CreatedUserID	4	int	10	4	0	10	0	None	None	4	None	None	13	NO	56
NewsincQA	dbo	ContentStaging	UpdatedDate	11	smalldatetime	16	16	0	None	1	None	(getdate())	9	3	None	14	YES	111
NewsincQA	dbo	ContentStaging	UpdatedUserID	4	int	10	4	0	10	1	None	None	4	None	None	15	YES	38
NewsincQA	dbo	ContentStaging	Active	-7	bit	1	1	None	None	0	None	((1))	-7	None	None	16	NO	50
NewsincQA	dbo	ContentStaging	IsArchived	-7	bit	1	1	None	None	1	None	((0))	-7	None	None	17	YES	50
NewsincQA	dbo	ContentStaging	isDeleted	-7	bit	1	1	None	None	1	None	((0))	-7	None	None	18	YES	50
NewsincQA	dbo	ContentStaging	FeedID	4	int	10	4	0	10	1	None	None	4	None	None	19	YES	38

index_name	index_description	index_keys
PK_ContentStaging_1	clustered, unique, primary key located on PRIMARY	ContentID
