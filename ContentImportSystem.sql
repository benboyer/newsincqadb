TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	ContentImportSystem	ContentImportSystemID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	ContentImportSystem	IPaddress	12	varchar	60	60	None	None	1	None	None	12	None	60	2	YES	39
NewsincQA	dbo	ContentImportSystem	DatabaseName	12	varchar	60	60	None	None	1	None	None	12	None	60	3	YES	39
NewsincQA	dbo	ContentImportSystem	Description	12	varchar	60	60	None	None	1	None	None	12	None	60	4	YES	39
NewsincQA	dbo	ContentImportSystem	BoxName	12	varchar	50	50	None	None	1	None	None	12	None	50	5	YES	39
NewsincQA	dbo	ContentImportSystem	Zencoder	-7	bit	1	1	None	None	0	None	((0))	-7	None	None	6	NO	50

index_name	index_description	index_keys
PK_ContentImportSystem	clustered, unique, primary key located on PRIMARY	ContentImportSystemID
