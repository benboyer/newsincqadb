USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[fd_flatContent]
	--declare   
	@StartIdx INT = NULL,      
    @StartDate DATETIME,      
    @EndDate DATETIME,        
    @WindowSize INT = NULL      
	-- select @StartIdx = 25406511, @StartDate = '2013-12-01 01:59:59', @EndDate = '2013-12-01 00:00:00', @WindowSize = 2000
	-- select @StartIdx = null, @StartDate = '2013-12-17 02:00:00', @EndDate = '2013-12-16 23:00:00', @WindowSize = 2000
as
BEGIN
	/*
		THIS PROCEDURE WAS A TEST FOR AN ALTERNATE APPROACH TO feeds_Search_GetContentWindow.
		The test failed, with feeds_Search_GetContentWindow consistently returning results more quickly, in 1/2 the time in most cases.
		Under some circumstances this procedure could be more efficient (such frequent execution that memory pressure becomses an issue).
		It could also be tested on a more robust server than this dinky little QA server with different results.
	*/
	set fmtonly off
	set nocount on

	exec SprocTrackingUpdate 'fd_flatContent'
	DECLARE @ResolveTotalResults BIT      
	IF @StartIdx IS NULL AND @WindowSize IS NOT NULL      
		SET @ResolveTotalResults = 1      
	ELSE      
		SET @ResolveTotalResults = 0      

	DECLARE @MyStartIdx INT      
	IF @StartIdx IS not NULL      
	BEGIN      
		SET @MyStartIdx = @StartIdx      
	END      
	      
	DECLARE @MyStartDate DATETIME      
	IF @StartDate IS NULL      
	BEGIN      
		SET @MyStartDate = GETDATE()      
	END      
	ELSE      
	BEGIN      
		SET @MyStartDate = @StartDate      
	END      
	      
	DECLARE @MyEndDate DATETIME      
	IF @EndDate IS NULL      
	BEGIN      
		SET @MyEndDate = (SELECT TOP 1 UpdatedDate FROM Content WHERE UpdatedDate IS NOT NULL ORDER BY UpdatedDate ASC)      
	END      
	ELSE      
	BEGIN      
		SET @MyEndDate = @EndDate      
	END   

	SELECT top (@windowsize)
	C.ContentID, C.UUID, C.PartnerID, C.Name PartnerName, C.Name Title, C.[Description],
	C.Category, C.Keyword, C.CreatedDate, C.EffectiveDate, C.ExpirationDate,
	C.UpdatedDate, C.Active,
	V.AssetID, V.AssetTypeID, V.MimeType, V.MediaType, dbo.GetPathToServeAsset(V.AssetID) AS ContentURL, V.Height AS VideoHeight, V.Width AS VideoWidth, V.Duration,
	T.AssetID, T.AssetTypeID, T.MimeType, T.MediaType, dbo.GetPathToServeAsset(T.AssetID) AS ThumbnailURL, T.Height AS ThumbHeight, T.Width AS ThumbWidth
	FROM (	select this.*
			from Content (nolock) this
			where updateddate between @myenddate and @mystartdate
			and (@StartIdx is null or contentid not in (SELECT ContentID FROM Content C WHERE C.UpdatedDate = @MyStartDate and C.ContentId >= @MyStartIdx))
		) c
		JOIN 
		(
			SELECT CA.ContentID, A.AssetID, A.AssetTypeID, MT.MimeType, MT.MediaType, A.Height, A.Width, A.Duration 
			FROM Asset A
			INNER JOIN Content_Asset (nolock) CA ON CA.AssetID = A.AssetID
			INNER JOIN MimeType (nolock) MT ON MT.MimeTypeID = A.MimeTypeID
			WHERE CA.AssetTypeID = 1 AND CA.Active = 1
		) V on V.ContentID = C.ContentID
		JOIN
		(
			SELECT CA.ContentID, A.AssetID, A.AssetTypeID, MT.MimeType, MT.MediaType, A.Height, A.Width 
			FROM Asset A
			INNER JOIN Content_Asset (nolock) CA ON CA.AssetID = A.AssetID
			INNER JOIN MimeType (nolock) MT ON MT.MimeTypeID = A.MimeTypeID
			WHERE CA.AssetTypeID = 3 AND CA.Active = 1
		) T on T.ContentID = C.ContentID

		ORDER BY C.UpdatedDate DESC, c.contentid desc

	IF 1 = @ResolveTotalResults      
	BEGIN
		SELECT COUNT(*) FROM Content C
			INNER JOIN [Partner] P ON P.PartnerId = C.PartnerID
			INNER JOIN [Content_Asset] CA ON CA.ContentID = C.ContentID
		WHERE
			CA.Active = 1 AND
			CA.AssetTypeID = 1 AND
			C.UpdatedDate BETWEEN @MyEndDate AND @MyStartDate
	--        (C.ContentId NOT IN (SELECT ContentID FROM @ExcludedContent))
			and (@StartIdx is null or c.contentid not in (SELECT ContentID FROM Content WHERE UpdatedDate = @MyStartDate and ContentId >= @MyStartIdx))

	END
END
GO
