USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usrmaint_UpdateUserPermissions]
	@Partnerid int,
	@userlogin varchar(40)
as
BEGIN
	set fmtonly OFF
	set NOCOUNT ON
	exec	SprocTrackingUpdate 'usrmaint_UpdateUserPermissions'

		-- declare 	@PartnerID int =  381, @userlogin varchar(40) = 'beaumontbc' 
		
		if exists (select 1 from Partner (nolock) p
					join	[User] (nolock) u
					on		p.PartnerID = u.PartnerID
					where	p.PartnerID = @Partnerid
					and		u.Login = @userlogin
					and		p.StatusID = 2
					and		u.StatusID < 4
					and		not exists 
							(select 1 from User_Permission up (nolock)
							where up.UserID = u.UserID))
		begin
			insert into User_Permission(UserID, PermissionID)
			-- declare 	@PartnerID int =  381, @userlogin varchar(40) = 'beaumontbc'
			select	u.UserID, 1 -- select count(*) -- select *
			from	Partner (nolock) p
			join	[User] (nolock) u
			on		p.PartnerID = u.PartnerID
			where	p.PartnerID = @Partnerid
			and		u.Login = @userlogin
			and		p.StatusID = 2
			and		u.StatusID < 4
			--and		u.Login not like 'ndn_%'
			and		not exists 
					(select 1 from User_Permission up (nolock)
					where up.UserID = u.UserID)
			if @@ERROR = 0
			begin
				select 'User login ' + @userlogin + ' now has permissions, is assigned to PartnerID ' + (select convert(varchar(20), partnerid) + ', named ' +  name from Partner where PartnerID = (select PartnerID from [User] where login = @userlogin))
				-- return
			end	
		end
		else
		begin
			if exists (select	u.UserID, 1 -- select count(*) -- select *
						from	Partner (nolock) p
						join	[User] (nolock) u
						on		p.PartnerID = u.PartnerID
						where	p.PartnerID = @Partnerid
						and		u.Login = @userlogin
						and		p.StatusID = 2
						and		u.StatusID < 4
						--and		u.Login not like 'ndn_%'
						and		exists 
								(select 1 from User_Permission up (nolock)
								where up.UserID = u.UserID))
			begin
				-- declare @userlogin varchar(20) = 'plainviewbc'
				-- select 'User already has permissions, is assigned to PartnerID ' + (select convert(varchar(20), partnerid) + ', named ' +  name from Partner where PartnerID = (select PartnerID from [User] where login = @userlogin))
				select 'User login ' + @userlogin + ' already has permissions, is assigned to PartnerID ' + (select convert(varchar(20), partnerid) + ', named ' +  name from Partner where PartnerID = (select PartnerID from [User] where login = @userlogin))
			end

			if (select partnerid from [User] where LOGIN = @userlogin)
				<>
				@Partnerid
			begin
				-- declare @partnerid int = 989765, @userlogin varchar(20) = 'plainviewbc'
				select 'login ' + @userlogin + ' belongs to PartnerID ' + CONVERT(varchar(10), partnerid) + ' (' + name + ') with DPID ' + CONVERT(varchar(10), TrackingGroup) + ' and the input PartnerID is ' + CONVERT(varchar(10), @partnerid) + ' ' + isnull((select Name from Partner where PartnerID = @partnerid), '[ Partner does not exists]')
				from Partner where PartnerID = (select PartnerID from [user] where login = @userlogin)
			end
		end		
		
		
/*		
		insert into User_Permission(UserID, PermissionID)
		-- declare 	@PartnerID int =  381, @userlogin varchar(40) = 'plainviewbc'
		select	u.UserID, 1 -- select count(*) -- select *
		from	Partner (nolock) p
		join	[User] (nolock) u
		on		p.PartnerID = u.PartnerID
		where	p.PartnerID = @Partnerid
		and		u.Login = @userlogin
		and		p.StatusID = 2
		and		u.StatusID < 4
		--and		u.Login not like 'ndn_%'
		and		not exists 
				(select 1 from User_Permission up (nolock)
				where up.UserID = u.UserID)


select * from [User] where login = 'beaumontbc'
select * from [user] where PartnerID = 381
select * from [user] where PartnerID = 381
select * from Partner where PartnerID = 381
select * from Partner where PartnerID = 381
select * from Partner where Name like '%san an%'
*/

	--	PartnerStatement Access
	--		Add new
/*
		insert into User_Permission(UserID, PermissionID)
		select	u.UserID, 1 -- select count(*) -- select *
		from	Partner (nolock) p
		join	[User] (nolock) u
		on		p.PartnerID = u.PartnerID
		--join	NDNUsersLEGACY (nolock) nul
		--on		u.UserID = nul.UserID
		where	p.PartnerID <> -1
		and		p.StatusID = 2
		and		u.StatusID < 4
		--and		u.Login not like 'ndn_%'
		and		not exists 
				(select 1 from User_Permission up (nolock)
				where up.UserID = u.UserID)

	--		delete when dropped/changed status
		delete up	-- select count(*)  -- select *
		from	User_Permission up
		left join	(select	u.UserID
				from	Partner (nolock) p
				join	[User] (nolock) u
				on		p.PartnerID = u.PartnerID
				where	p.PartnerID <> -1
				and		p.StatusID = 2
				and		u.StatusID < 4
				--and		u.Login not like 'ndn_%'
				) a
		on		up.UserID = a.UserID
		where	a.UserID is null
	--	END PartnerStatement Access
*/
END
GO
