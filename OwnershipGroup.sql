TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	OwnershipGroup	OwnershipGroupID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	OwnershipGroup	OwnershipGroup	12	varchar	100	100	None	None	1	None	None	12	None	100	2	YES	39
NewsincQA	dbo	OwnershipGroup	AdUnit	12	varchar	200	200	None	None	1	None	None	12	None	200	3	YES	39
NewsincQA	dbo	OwnershipGroup	UpdatedDate	11	datetime	23	16	3	None	0	None	(getdate())	9	3	None	4	NO	61
NewsincQA	dbo	OwnershipGroup	UpdatedBy	12	varchar	20	20	None	None	0	None	None	12	None	20	5	NO	39

index_name	index_description	index_keys
PK__Ownershi__C6A0DEEF0BCDA75D	clustered, unique, primary key located on PRIMARY	OwnershipGroupID
