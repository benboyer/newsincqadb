CREATE procedure [dbo].[GetLauncherTemplates]
as
begin
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetLauncherTemplates'

	select	lt.LauncherTemplateID,
			t.Name LauncherType,
			lt.name DisplayName,
			lt.Description,
			lt.Height,
			lt.Width,
			lt.ImageLocation,
			lt.Active,
			t.LauncherCompatible, -- New field 20121120
			t.PlayerCompatible -- New field 20121120
	from	launchertemplate lt
	join	Launchertypes t
	on		lt.launchertypesid = t.LauncherTypesID
	order by lt.sortorder

	set nocount off
end
