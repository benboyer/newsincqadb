CREATE VIEW dbo.Organizations
AS
SELECT     dbo.Partner.PartnerID AS OrganizationId, dbo.OrganizationsLEGACY.ContentItemId, dbo.Partner.Name, dbo.OrganizationsLEGACY.Acronym, 
                      dbo.OrganizationsLEGACY.CandidateOfficePlaceholder, ISNULL(dbo.OrganizationsLEGACY.AllowUploadingVideo, 0) AS AllowUploadingVideo, 
                      dbo.Partner.PartnerID AS UserID, dbo.OrganizationsLEGACY.LogoUrl, ISNULL(dbo.OrganizationsLEGACY.IsMediaSource, 0) AS IsMediaSource, 
                      ISNULL(dbo.OrganizationsLEGACY.IsContentPrivate, 0) AS IsContentPrivate, ISNULL(dbo.OrganizationsLEGACY.OrganizationType, 0) AS OrganizationType, 
                      dbo.OrganizationsLEGACY.Phone
FROM         dbo.Partner LEFT OUTER JOIN
                      dbo.OrganizationsLEGACY ON dbo.Partner.PartnerID = dbo.OrganizationsLEGACY.PartnerID
