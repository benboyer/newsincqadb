CREATE procedure [dbo].[usp_SetContentToError_all]
	@contentid bigint
AS
BEGIN
	set FMTONLY OFF
	set	NOCOUNT ON
	exec SprocTrackingUpdate 'usp_SetContentToError_all'

		update	a 
		set		a.importstatusid = 9 
		from	(select assetid from Content_Asset (nolock) where ContentID = @contentid) ca
		join	Asset a
		on		ca.AssetID = a.AssetID

		update	ca 
		set		ca.importstatusid = 9 -- declare @ContentID bigint = 23571665 select *
		from	Content_Asset ca
		where	ca.ContentID = @contentid 

		update	cv 
		set		cv.ContentVideoimportstatusid = 5 -- declare @ContentID bigint = 23571665 select *
		from	ContentVideo cv
		where	cv.ContentID = @contentid 
		
		update	c 
		set		c.contentimportstatus = 9 -- declare @ContentID bigint = 23571665 select *
		from	Content c
		where	c.ContentID = @contentid

		if @@ERROR = 0
			select  convert(varchar(10), @contentid) +  ' set to Error Status' as MessageBack
	SET NOCOUNT OFF
END