USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPlayerShow]
	@widgetID int,
	@trackingGroup int = null
AS
BEGIN
-- declare @widgetid int = 8223, @trackinggroup int = 90081
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetPlayerShow'
	declare @iwidgetID int,
			@itrackinggroup int,
			@iplayertypesid int
	select @iwidgetID = @widgetID, @itrackinggroup = @trackingGroup-- , @iplayertypesid = @PlayerTypesID

	SELECT   top 1 l.LauncherID AS WidgetId,
			--	l.DisableEmailButton DisableSocialNetworking, -- wpl.DisableSocialNetworking,
			--	l.DisableAdsOnPlayer DisableAds, -- wpl.DisableAds,
			--	l.DisableAutoPlay DisableVideoPlayOnDemand, -- wpl.DisableVideoPlayOnLoad,
			--	l.DisableShareButton DisableSingleEmbed, -- wpl.DisableSingleEmbed,
			--	l.ContinuousPlay, --  wpl.ContinuousPlay,
			--	l.Width, -- pt.Width,
			--	l.Height, --
			case when blk.launcherid is null then wpl.DisableSocialNetworking else 1 end as DisableSocialNetworking, wpl.DisableAds, wpl.DisableVideoPlayOnLoad, wpl.DisableSingleEmbed, wpl.ContinuousPlay, pt.Width, pt.Height,
			p.ZoneID
--				 , isnull(p.LandingURL, 'http://landing.newsinc.com/shared/video.html') LandingURL, isnull(palo.logourl, ' http://assets.newsinc.com/newsinconebyone.png') LogoLocation, p.Name DistributorName, isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'News Distribution Network, Inc.') ProducerCategory
--				 , isnull(p.LandingURL, 'http://landing.newsinc.com/shared/video.html') LandingURL, isnull(palo43.logourl, ' http://assets.newsinc.com/newsinconebyone.png') LogoURL43, isnull(palo169.logourl, ' http://assets.newsinc.com/newsinconebyone.png') LogoURL169, ISNULL(paloD.LogoURL, 'no logo') LogoURLDisplay, p.Name DistributorName, isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'News Distribution Network, Inc.') ProducerCategory
				 , isnull(lu.LandingURL, isnull(p.LandingURL, 'http://landing.newsinc.com/shared/video.html')) LandingURL, isnull(palo43.logourl, 'http://assets.newsinc.com/partnerlogo.jpg') LogoURL43, isnull(palo169.logourl, 'http://assets.newsinc.com/partnerlogo169.jpg') LogoURL169, ISNULL(paloD.LogoURL, 'no logo') LogoURLDisplay, p.Name DistributorName, isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'News Distribution Network, Inc.') ProducerCategory

	FROM	(select * from Launcher (nolock) where launcherid = @iwidgetid) l
			left join	(select pl.partnerid, l.LogoURL
						from	partner_logo pl (nolock)
						join	Logo l (nolock)
						on		pl.logoid = l.LogoID
						join	LogoType lt (nolock)
						on		l.logotypeid = lt.LogoTypeID
						--and		@iPlayerTypesID = l.playertypesid
						where	pl.launcherid = @iwidgetID) palo
			on			l.PartnerID = palo.partnerid
			left join	(select pl.partnerid, pl.launcherid, l.LogoURL
						from	partner_logo pl (nolock)
						join	Logo l (nolock)
						on		pl.logoid = l.LogoID
						join	LogoType lt (nolock)
						on		l.logotypeid = lt.LogoTypeID
						and		1 = l.LogoTypeID) palo43
			on			l.PartnerID = palo43.partnerid
			and			l.LauncherID = palo43.LauncherID
			left join	(select pl.partnerid, pl.LauncherID, l.LogoURL
						from	partner_logo pl (nolock)
						join	Logo l (nolock)
						on		pl.logoid = l.LogoID
						join	LogoType lt (nolock)
						on		l.logotypeid = lt.LogoTypeID
						and		2 = l.LogoTypeID) palo169
			on			l.PartnerID = palo169.partnerid
			and			l.LauncherID = palo169.LauncherID
			left join	(select pl.partnerid, pl.LauncherID, l.LogoURL
						from	partner_logo pl (nolock)
						join	Logo l (nolock)
						on		pl.logoid = l.LogoID
						join	LogoType lt (nolock)
						on		l.logotypeid = lt.LogoTypeID
						and		6 = l.LogoTypeID) paloD
			on			l.PartnerID = paloD.partnerid
			and			l.LauncherID = paloD.LauncherID

			INNER JOIN	WidgetPlayerLegacy wpl (nolock)
			ON			l.LauncherID = wpl.LauncherID
			join		PlayerType pt (nolock)
			on			l.PlayerTypeID = pt.PlayerTypeID
					left join LauncherTemplate ltem (nolock)
					on		l.LauncherTemplateID = ltem.LauncherTemplateID
						left join LauncherTypes lt (nolock)
						on		ltem.Launchertypesid = lt.LauncherTypesID
						left join partner_landingurl plu (nolock)
						on		l.PartnerID = plu.partnerid
						left join landingurl lu (nolock)
						on		plu.landingurlid = lu.landingurlid
						and		1 = lu.Active
						and		1 = lu.LandingURLTypeID
-- above line added 2012-06-22 to fix hulu landing page showing up for the 1 partner (so far) with a hulu landing page

			left join	mTrackingGroup p2  (nolock)-- Partner p
			on			@itrackinggroup = p2.TrackingGroup
			left join	Partner p  (nolock)-- select * from mTrackingGroup
			on			p2.partnerid = p.PartnerID
				left join	Playlist_Content_PartnerMap pm   (nolock)-- select * from playlist_content_partnermap where feedid is not null
				on			l.PartnerID = pm.PartnerID
				left join	NDNCategory nc (nolock)
				on			pm.NDNCategoryID = nc.NDNCategoryID
		left join (select lp.LauncherID
					from	Launcher_Playlist (nolock) lp
					join	Playlist_Content (nolock) pc
					on		lp.PlaylistID = pc.PlaylistID
					join	Content (nolock) c
					on		pc.ContentID = c.ContentID
					where	c.PartnerID in (select partnerid
											from vw_partnerdefaultsettings
											where entity = 'BlockShareForProviderID' and value = 'true')) blk
		on		l.LauncherID = blk.launcherid

			where	l.launcherid  = @iwidgetID
		--	and		(pm.PartnerID is null or nc.NDNCategory is not null)
		--	and dbo.AllowedLauncherContent(@iwidgetid, @itrackinggroup) = 1
			set nocount off
END
GO
