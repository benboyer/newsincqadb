USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[WidgetPlaylist_DELETE_sp]
	@WidgetId int,
	@PlaylistId int

AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'WidgetPlaylist_DELETE_sp'

	DELETE Launcher_Playlist WHERE LauncherID=@WidgetId AND PlaylistID=@PlaylistId

END
GO
