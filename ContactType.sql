TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	ContactType	ContactTypeID	5	smallint identity	5	2	0	10	0	None	None	5	None	None	1	NO	52
NewsincQA	dbo	ContactType	Name	-9	nvarchar	100	200	None	None	0	None	None	-9	None	200	2	NO	39
NewsincQA	dbo	ContactType	Description	-9	nvarchar	1000	2000	None	None	1	None	None	-9	None	2000	3	YES	39
NewsincQA	dbo	ContactType	Order	5	smallint	5	2	0	10	0	None	None	5	None	None	4	NO	52
NewsincQA	dbo	ContactType	Expression	-9	nvarchar	255	510	None	None	1	None	None	-9	None	510	5	YES	39

index_name	index_description	index_keys
PK_ContactType	clustered, unique, primary key located on PRIMARY	ContactTypeID
