TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	FeedCategory	FeedCategoryID	-5	bigint identity	19	8	0	10	0	None	None	-5	None	None	1	NO	63
NewsincQA	dbo	FeedCategory	FeedContentID	-5	bigint	19	8	0	10	0	None	None	-5	None	None	2	NO	63
NewsincQA	dbo	FeedCategory	Category	12	varchar	250	250	None	None	0	None	None	12	None	250	3	NO	39

index_name	index_description	index_keys
NDX_FeedCatFCID	nonclustered located on PRIMARY	FeedContentID
PK_FeedCategory_1	clustered, unique, primary key located on PRIMARY	FeedCategoryID
