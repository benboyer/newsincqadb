CREATE VIEW dbo.dme_User
AS
SELECT     u.PartnerID, u.UserID, u.FirstName, u.LastName, u.Login, u.Active, a.Address1, a.Address2, a.City, s.Abbreviation AS StateCd, a.Zip, c.Name AS Country, u.StatusID, 
                      u.CreatedDate, u.UpdatedDate
FROM         dbo.[User] AS u WITH (nolock) LEFT OUTER JOIN
                      dbo.Address_User AS au ON u.UserID = au.UserID LEFT OUTER JOIN
                      dbo.Address AS a WITH (nolock) ON au.AddressID = a.AddressID LEFT OUTER JOIN
                      dbo.State AS s WITH (nolock) ON a.StateID = s.StateID LEFT OUTER JOIN
                      dbo.Country AS c WITH (nolock) ON a.CountryID = c.CountryID
