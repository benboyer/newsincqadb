USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BuildLauncherPlaylistVideos]
--	declare
	@widgetID int = null,
	@playlistID int = null,
	@trackingGroup int = null,
	@contentid bigint = null,
	@devicetype int = 1
AS

exec SprocTrackingUpdate 'BuildLauncherPlaylistVideos'
-----------------------------------------------
/*
	declare
	@widgetID int = 3231,
	@playlistID int = 507,
	@trackingGroup int = 90121,
	@contentid bigint = null,
	@devicetype int = 1
*/
-- Updated 9/17/2013 (switched to allowedlaunchercontentP)
--------------------------------------
	Declare	@now datetime,
		@iwidgetID int,
		@iplaylistID int,
		@itrackinggroup int,
		@idevicetype int
	set		@now = GETDATE()
	select @iwidgetID = isnull(@widgetID, 1), @iplaylistID = isnull(@playlistID, (select value from vw_sysDefaultSettings (nolock) where entity = 'Playlist' and Setting = 'Number')), @itrackinggroup = @trackingGroup, @idevicetype = isnull(@devicetype, 1)

	if	@PlaylistID = 9999999
	begin
		exec GetSingleContent @contentid, @iTrackingGroup, @deviceType
		return
	end
	select  distinct -- a.iscontentprivate, a.partnerid OwningPartnerid, pd.partnerid DistribPartnerid,
				a.PlaylistID,
				a.ContentID, a.Name, a.Description, a.TrackingGroup ContentPartnerID, a.VideoGroupName, a.Duration, convert(date, a.PubDate) PubDate, a.Keyword, convert(varchar(120), null) Timeline,
				case	when a.AssetTypeName like 'Video%' then 'src'
						else a.AssetTypeName
				end as AssetType,
			a.DeliveryPath as AssetLocation,
				a.ProducerName, a.ProducerNameAlt, pd.Name DistributorName, pd.shortname DistributorNameAlt, a.logourl ProducerLogo, a.[Order]+1 ContentOrder
	-- into drop table #mainset
	from	(select	pt.partnerid, pt.TrackingGroup, pt.isContentPrivate, p.playlistid, pc.ContentID, c.Name,
				c.Description, c.EffectiveDate PubDate, pc.[Order], aa.FilePath, aa.Filename, aa.Duration, c.Keyword,
				VG.VideoGroupName, at.Name AssetTypeName, mt.FileExtension, pt.logourl, pt.Name ProducerName, pt.shortname ProducerNameAlt,
				dbo.GetAssetDeliveryPath(mt.FileExtension, c.partnerid, ca.contentid, ca.assetid) DeliveryPath
		from	Playlist p (nolock)
		join	Playlist_Content pc (nolock)
		on		p.PlaylistID = pc.PlaylistID
		join	Content c
		on		pc.ContentID = c.ContentID
		join	Partner_TargetPlatformProfile ptp
		on		c.PartnerID = ptp.PartnerID
		and		@idevicetype = ptp.TargetPlatformID
		join	TargetPlatform_AssetType tpat
		on		ptp.TargetPlatformID = tpat.TargetPlatformID

		join	Content_Asset ca
		on		c.ContentID = ca.ContentID
		join	AssetType at
		on		tpat.AssetTypeID = at.AssetTypeID
		and		ca.AssetTypeID = at.AssetTypeID
		join	Asset aa
		on		ca.AssetID = aa.AssetID

		join	MimeType mt
		on		at.MimeTypeid = mt.MimeTypeID
		JOIN	partner pt (nolock)
		ON		c.PartnerID = pt.PartnerID
		left join	VideoGroups VG
		on		p.PlaylistID = vg.VideoGroupId
		where	p.PlaylistID = @iplaylistID
		and		isnull(aa.isDeleted, 0) = 0
		and		dbo.AllowedLauncherContentP(@iwidgetID, c.PartnerID) = 1
		) a
		left join partner pd
		on	@itrackinggroup = pd.TrackingGroup
		where		dbo.AllowedContentID(a.contentid, @itrackinggroup, @devicetype) = 1
		--and			dbo.AllowedLauncherContent(@iwidgetID, @itrackinggroup) = 1
	ORDER BY a.[Order]+1, a.ContentID
GO
