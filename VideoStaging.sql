CREATE view VideoStaging as
SELECT     Content.ContentID AS VideoStagingID, Content.CreatedUserID AS UserID, CAST(1 AS int) AS UserType, ISNULL(ContentVideo.ImportFilePath, v.VideoFilePath) 
                      AS ClipURL, Content.Name AS Title, Content.Description, Content.EffectiveDate AS EventDate, ISNULL(ContentVideo.Height, v.Height) AS Height, 
                      ISNULL(ContentVideo.Width, v.Width) AS Width, ISNULL(ContentVideo.Duration, v.Duration) AS Duration, Content.CreatedDate AS CreatedOn, 
                      ISNULL(ContentVideo.ThumbnailImportFilePath, t.ThumbnailFilePath) AS ThumbnailURL, Content.EffectiveDate AS PublishDate, Content.Keyword AS Keywords, 
                      ContentVideo.GUID, ISNULL(ContentVideo.ImportFilePath, v.VideoFilePath) AS OriginalName, CONVERT(smallint, 
                      CASE WHEN content.ContentImportStatus = 5 THEN 4 WHEN contentvideo.ContentVideoImportStatusID = 5 THEN 4 WHEN ContentVideo.ContentVideoImportStatusID = 7
                       THEN 2 WHEN v.ImportStatusID = 5 THEN 4 WHEN v.ImportStatusID <> 5 THEN 2 WHEN ContentVideo.ContentVideoImportStatusID IS NOT NULL 
                      THEN ContentVideo.ContentVideoImportStatusID ELSE NULL END) AS EncodingStatus, Content.ExpirationDate AS ExpiryDate, ISNULL(Content.Active, v.Active) 
                      AS Active, ISNULL(Content.Active, v.Active) AS IsEnabled, CAST(ISNULL(Content.isDeleted, 0) AS bit) AS IsDeleted, ContentSource.Name AS IngestionSource, 
                      ISNULL(ContentVideo.StillframeImportFilePath, s.StillFrameFilePath) AS StillFrameURL, Content.ContentID AS VideoId, Content.Category, 
                      VideoStagingLEGACY.ReadyForEncoding, VideoStagingLEGACY.EncodingPercentComplete AS EncodingPercentCompleted, 
                      VideoStagingLEGACY.EncodingStarted, VideoStagingLEGACY.EncodingCompleted, VideoStagingLEGACY.Status, VideoStagingLEGACY.FileSize, 
                      VideoStagingLEGACY.AudioTrackURL, VideoStagingLEGACY.EncodedURL, VideoStagingLEGACY.NexidiaStatus, VideoStagingLEGACY.Mp3Started, 
                      VideoStagingLEGACY.Mp3Completed, VideoStagingLEGACY.CodecUsed, VideoStagingLEGACY.FileExtension, VideoStagingLEGACY.NiaStatus
FROM         (SELECT     ContentID, ContentTypeID, PartnerID, Version, ContentSourceID, Name, Description, Category, Keyword, Duration, EffectiveDate, ExpirationDate, 
                                              CreatedDate, CreatedUserID, UpdatedDate, UpdatedUserID, Active, IsFileArchived, isDeleted, FeedID, SourceGUID, ContentImportStatus
                       FROM          Content WITH (nolock)
                       WHERE      (ContentImportStatus <> 9)) AS Content INNER JOIN
                          (SELECT     ContentID, AssetTypeID, MAX(AssetID) AS assetid
                            FROM          Content_Asset
                            WHERE      (TargetPlatformID = 1)
                            GROUP BY ContentID, AssetTypeID) AS mca ON Content.ContentID = mca.ContentID INNER JOIN
                      ContentSource WITH (nolock) ON Content.ContentSourceID = ContentSource.ContentSourceID LEFT OUTER JOIN
                      ContentVideo WITH (nolock) ON Content.ContentID = ContentVideo.ContentID LEFT OUTER JOIN
                      VideoStagingLEGACY WITH (nolock) ON ContentVideo.ContentID = VideoStagingLEGACY.ContentID INNER JOIN
                          (SELECT     ca.ContentID, av.Height, av.Width, ISNULL(av.FilePath, '') + '/' + ISNULL(av.Filename, '') AS VideoFilePath, av.Duration, av.ImportStatusID, av.Active, 
                                                   av.AssetID
                            FROM          Content_Asset AS ca WITH (nolock) LEFT OUTER JOIN
                                                   Asset AS av WITH (nolock) ON ca.AssetID = av.AssetID LEFT OUTER JOIN
                                                   AssetType AS atv WITH (nolock) ON ca.AssetTypeID = atv.AssetTypeID
                            WHERE      (ca.TargetPlatformID = 1) AND (atv.Name = 'VideoPC')) AS v ON Content.ContentID = v.ContentID AND mca.assetid = v.AssetID LEFT OUTER JOIN
                          (SELECT     ca.ContentID, asf.FilePath AS StillFrameFilePath, asf.AssetID
                            FROM          Content_Asset AS ca WITH (nolock) INNER JOIN
                                                   Asset AS asf WITH (nolock) ON ca.AssetID = asf.AssetID INNER JOIN
                                                   AssetType AS atsf WITH (nolock) ON ca.AssetTypeID = atsf.AssetTypeID
                            WHERE      (ca.TargetPlatformID = 1) AND (atsf.Name = 'StillFramePC')) AS s ON Content.ContentID = s.ContentID AND mca.assetid = s.AssetID LEFT OUTER JOIN
                          (SELECT     ca.ContentID, ath.FilePath AS ThumbnailFilePath, ath.AssetID
                            FROM          Content_Asset AS ca WITH (nolock) INNER JOIN
                                                   Asset AS ath WITH (nolock) ON ca.AssetID = ath.AssetID INNER JOIN
                                                   AssetType AS atth WITH (nolock) ON ca.AssetTypeID = atth.AssetTypeID
                            WHERE      (ca.TargetPlatformID = 1) AND (atth.Name = 'ThumbnailPC')) AS t ON Content.ContentID = t.ContentID AND mca.assetid = t.AssetID
