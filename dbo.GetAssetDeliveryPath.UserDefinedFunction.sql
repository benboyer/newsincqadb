USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetAssetDeliveryPath]
(
	@extension varchar(10), @partnerid int, @contentid bigint, @assetid bigint
)

RETURNS varchar(400)
AS
BEGIN
	DECLARE @DeilveryPath varchar(400)

	declare @contentidv varchar(20), @assetidv varchar(20), @partneridv varchar(20)
	set @assetidv = convert(varchar(20), @assetid)
	set @contentidv = CONVERT(varchar(20), @contentid)
	set @partneridv = CONVERT(varchar(20), @partnerid)

	SELECT @DeilveryPath = case
			when deliverypath IS not null then
				replace(replace(replace(DeliveryPath, '{partnerid}', @partneridv), '{contentid}', @Contentidv), '{assetid}', @assetidv)
				-- + '/' + DeliveryLevel1
				+ case when DeliveryLevel1 IS not null then '/' + replace(replace(replace(DeliveryLevel1, '{partnerid}', @partneridv), '{contentid}', @Contentidv), '{assetid}', @assetidv) else '' end
				+ case when DeliveryLevel2 IS not null then '/' + replace(replace(replace(DeliveryLevel2, '{partnerid}', @partneridv), '{contentid}', @Contentidv), '{assetid}', @assetidv) else '' end
				+ case when DeliveryLevel3 IS not null then '/' + replace(replace(replace(DeliveryLevel3, '{partnerid}', @partneridv), '{contentid}', @Contentidv), '{assetid}', @assetidv)  else '' end
				+ case when DeliveryLevel4 IS not null then '/' + replace(replace(replace(DeliveryLevel4, '{partnerid}', @partneridv), '{contentid}', @Contentidv), '{assetid}', @assetidv)  else '' end
				+ case when IncludeDeliveryExtension  = 1 then '.' + @extension else '' end
			else
				replace(replace(replace(StoragePath, '{partnerid}', @partneridv), '{contentid}', @Contentidv), '{assetid}', @assetidv)
				-- + '/' + StorageLevel1
				+ case when StorageLevel1 IS not null then '/' + replace(replace(replace(StorageLevel1, '{partnerid}', @partneridv), '{contentid}', @Contentidv), '{assetid}', @assetidv) else '' end
				+ case when StorageLevel2 IS not null then '/' + replace(replace(replace(StorageLevel2, '{partnerid}', @partneridv), '{contentid}', @Contentidv), '{assetid}', @assetidv) else '' end
				+ case when StorageLevel3 IS not null then '/' + replace(replace(replace(StorageLevel3, '{partnerid}', @partneridv), '{contentid}', @Contentidv), '{assetid}', @assetidv)  else '' end
				+ case when StorageLevel4 IS not null then '/' + replace(replace(replace(StorageLevel4, '{partnerid}', @partneridv), '{contentid}', @Contentidv), '{assetid}', @assetidv)  else '' end
				+ case when IncludeStorageExtension  = 1 then '.' + @extension else '' end
			end
	from DistributionFilePath
	where MimeFileExtension = @extension

	RETURN @DeilveryPath

END
GO
