CREATE procedure getPartnerOwnershipFromTrackinggroup @tg int as

select p.PartnerID, p.TrackingGroup, o.OwnershipGroupID, p.Name, p.isContentPrivate, p.CreatedDate from Partner p
left outer join OwnershipGroup_Partner op
 on p.PartnerID = op.PartnerID
left outer join ownershipgroup o
 on op.OwnershipGroupID = o.OwnershipGroupID
where trackinggroup = @tg

