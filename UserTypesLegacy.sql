TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	UserTypesLegacy	UserTypeID	-6	tinyint identity	3	1	0	10	0	None	None	-6	None	None	1	NO	48
NewsincQA	dbo	UserTypesLegacy	Name	-9	nvarchar	50	100	None	None	0	None	None	-9	None	100	2	NO	39

index_name	index_description	index_keys
PK_UserTypesLegacy	clustered, unique, primary key located on PRIMARY	UserTypeID
