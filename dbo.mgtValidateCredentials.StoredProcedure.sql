USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[mgtValidateCredentials]
	@Login nvarchar(150)
	, @Password nvarchar(128)
As

	Declare @PasswordSalt varbinary(256)

	Set @PasswordSalt = ( Select PasswordSalt From [User] Where Login = @Login )
BEGIN
	set FMTONLY OFF
	set NOCOUNT ON
	exec SprocTrackingUpdate 'mgtValidateCredentials'
	If @PasswordSalt Is Null
	Declare @Hash varbinary(max)
	Set @Hash = HASHBYTES('SHA1', @PasswordSalt + Cast(@Password As varbinary(128)))
	PRINT @PasswordSalt
	PRINT @Hash

	If EXISTS( Select 1
						 From [User]
						 Where Login = @Login
						 And Password = @Hash)
						 BEGIN
	PRINT 'User exists';
	return 1;
	END
	ELSE
	PRINT 'No matches';
	return 0;
	set NOCOUNT OFF
END
GO
