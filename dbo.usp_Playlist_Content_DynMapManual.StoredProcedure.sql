USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Playlist_Content_DynMapManual]
	@DoThisPartnerID bigint = null
as
	set nocount on
	exec SprocTrackingUpdate 'usp_Playlist_Content_DynMapManual'

	-- declare @DoThisPartnerID bigint set @DoThisPartnerID = 1325
	if	@DoThisPartnerID is null
		set		@DoThisPartnerID = -1

	-- declare @DoThisPartnerID bigint set @DoThisPartnerID = 499
	if @DoThisPartnerID = 1325
	begin
		update	Content
		set		Category = REPLACE(category, '|', ',')
		where	partnerid = 1325
		and		ContentID > (select MIN(lastcontentid) from Playlist_Content_PartnerMap where PartnerID = 1325)
		and		category like '%|%'
	end
	-- select * from playlist_content where contentid = 23566810
	-- declare @DoThisPartnerID bigint set @DoThisPartnerID = 499
	select	distinct pcpm.PartnerID, c.ContentID
		--, pcpm.SourceCategory, pcpm.PlaylistID, pcpm.LastContentID
	into	#updateme -- select * from #updateme -- drop table #updateme
	from	dbo.Playlist_Content_PartnerMap pcpm
	join	Content c
	on		pcpm.PartnerID = c.PartnerID
	and		@DoThisPartnerID = c.PartnerID
	where	pcpm.SourceCategory in (select categories from dbo.fn_Categories_fmContentCategories(c.ContentID) where c.ContentID > pcpm.LastContentID ) -- isnull(pcpm.LastContentID, 0)
-- added back 2013-10-02
	and		c.ContentID > (select Max(LastContentID) ContentID from Playlist_Content_PartnerMap where PartnerID = @DoThisPartnerID and PlaylistID = pcpm.PlaylistID)
	--and		c.category is not null
	--and		c.EffectiveDate > DATEADD(dd, -29, getutcdate())
	and		c.ContentImportStatus <> 9
	and		case when @DoThisPartnerID = 1325 then c.CreatedDate
			else c.EffectiveDate end
			> DATEADD(dd, -29, getutcdate())
	and	exists (select *
				from dbo.fn_Categories_fmContentCategories(c.ContentID)
				where	Categories = pcpm.SourceCategory)
	and	not exists	(select *
				from Playlist_Content pc
				where	pc.ContentID = c.ContentID
				and		pc.PlaylistID = pcpm.PlaylistID)

	declare @contentid bigint, @partnerid bigint
	while (select COUNT(*) from #updateme) > 0
	begin
		set @contentid = (select min(contentid) from #updateme)
		set @partnerid = (select partnerid from #updateme where ContentID = @contentid)
	-- select @contentid, @partnerid
		exec dbo.usp_Playlist_Content_DynMap @partnerid, @contentid
		delete from #updateme where PartnerID = @partnerid and ContentID = @contentid
	end
	drop table #updateme
	set nocount off

select @@ERROR
GO
