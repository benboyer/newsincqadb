TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	AllowContent	AllowContentID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	AllowContent	ContentProviderPartnerID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	AllowContent	DistributorPartnerID	4	int	10	4	0	10	0	None	None	4	None	None	3	NO	56
NewsincQA	dbo	AllowContent	CreatedDate	11	datetime	23	16	3	None	0	None	(getutcdate())	9	3	None	4	NO	61

index_name	index_description	index_keys
IX_ACprovIDdistID	nonclustered located on PRIMARY	ContentProviderPartnerID, DistributorPartnerID
IX_AllowContent_Dist	nonclustered located on PRIMARY	DistributorPartnerID
IX_AllowContent_Prod	nonclustered located on PRIMARY	ContentProviderPartnerID
PK_AllowContent	clustered, unique, primary key located on PRIMARY	AllowContentID
