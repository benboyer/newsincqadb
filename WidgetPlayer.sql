CREATE VIEW dbo.WidgetPlayer
AS
SELECT     dbo.Launcher.LauncherID AS WidgetId, dbo.WidgetPlayerLegacy.DisableSocialNetworking, dbo.WidgetPlayerLegacy.DisableAds, 
                      dbo.WidgetPlayerLegacy.DisableVideoPlayOnLoad, dbo.WidgetPlayerLegacy.DisableSingleEmbed, dbo.WidgetPlayerLegacy.PartnerLogo, 
                      dbo.WidgetPlayerLegacy.StyleSheet, dbo.WidgetPlayerLegacy.ContinuousPlay, dbo.Launcher.PlayerTypeID AS PlayerID, dbo.Launcher.PlayerTypeID AS Layout, 
                      dbo.WidgetPlayerLegacy.DisableKeywords
FROM         dbo.Launcher INNER JOIN
                      dbo.WidgetPlayerLegacy ON dbo.Launcher.LauncherID = dbo.WidgetPlayerLegacy.LauncherID