TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	admExceptionalProcessing	admExceptionalProcessingID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	admExceptionalProcessing	PartnerID	4	int	10	4	0	10	1	None	None	4	None	None	2	YES	38
NewsincQA	dbo	admExceptionalProcessing	ProcedureName	12	varchar	200	200	None	None	1	None	None	12	None	200	3	YES	39
NewsincQA	dbo	admExceptionalProcessing	LastContentID	-5	bigint	19	8	0	10	1	None	None	-5	None	None	4	YES	108
NewsincQA	dbo	admExceptionalProcessing	LastRunDTM	11	datetime	23	16	3	None	1	None	None	9	3	None	5	YES	111
NewsincQA	dbo	admExceptionalProcessing	CreatedDTM	11	datetime	23	16	3	None	1	None	None	9	3	None	6	YES	111
NewsincQA	dbo	admExceptionalProcessing	UpdatedDTM	11	datetime	23	16	3	None	1	None	None	9	3	None	7	YES	111
NewsincQA	dbo	admExceptionalProcessing	UpdatedUser	12	varchar	200	200	None	None	1	None	None	12	None	200	8	YES	39
NewsincQA	dbo	admExceptionalProcessing	RunIntervalMinutes	4	int	10	4	0	10	1	None	None	4	None	None	9	YES	38

index_name	index_description	index_keys
PK__admExcep__898A62073CCE24EC	clustered, unique, primary key located on PRIMARY	admExceptionalProcessingID
