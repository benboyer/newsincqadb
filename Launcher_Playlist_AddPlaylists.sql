CREATE PROCEDURE [dbo].[Launcher_Playlist_AddPlaylists]
	-- declare
	@LauncherID int,
	@PlaylistId int,
	@Order int = null
AS
BEGIN
	--	select @LauncherID = 8898, @PlaylistId = 304, @Order = 0
	--	select @LauncherID, @PlaylistId	, @Order

	set fmtonly off
	SET NOCOUNT ON
	exec SprocTrackingUpdate 'Launcher_Playlist_AddPlaylists'

	IF @LauncherID = 0 or @PlaylistId = 0
	BEGIN
		select 'Bad Parameters' as MessageBack
		return
	END
	IF @Order is null set @Order = 0

	if	(select COUNT(*)
		from	Launcher_Playlist (nolock)
		where	LauncherID = @LauncherID
		and		PlaylistID = @PlaylistId) > 0
	BEGIN
		update Launcher_Playlist
		set [Order] = [Order] + 1
		where LauncherID = @LauncherID and PlaylistID = @PlaylistId and [Order] >= @Order

	END
		INSERT into Launcher_Playlist(LauncherID, PlaylistID, [Order])
		select @LauncherID, @PlaylistId, @Order
		where not exists (select 1
				from Launcher_Playlist
				where	LauncherID = @LauncherID
				and		PlaylistID = @PlaylistId)

		update	Launcher_Playlist
		set		[Order] = @Order
		where	LauncherID = @LauncherID
		and		PlaylistID = @PlaylistId

	if @@ERROR = 0
		select 'Success' as MessageBack

	SET NOCOUNT OFF
END
