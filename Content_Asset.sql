TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Content_Asset	Content_AssetID	-5	bigint identity	19	8	0	10	0	None	None	-5	None	None	1	NO	63
NewsincQA	dbo	Content_Asset	ContentID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	Content_Asset	AssetID	-5	bigint	19	8	0	10	0	None	None	-5	None	None	3	NO	63
NewsincQA	dbo	Content_Asset	AssetTypeID	4	int	10	4	0	10	0	None	None	4	None	None	4	NO	56
NewsincQA	dbo	Content_Asset	TargetPlatformID	5	smallint	5	2	0	10	0	None	None	5	None	None	5	NO	52
NewsincQA	dbo	Content_Asset	ImportStatusID	4	int	10	4	0	10	1	None	None	4	None	None	6	YES	38
NewsincQA	dbo	Content_Asset	Active	-7	bit	1	1	None	None	0	None	((0))	-7	None	None	7	NO	50

index_name	index_description	index_keys
ContentAsset_CID	nonclustered located on PRIMARY	ContentID
IX_ConAsset_CIDisid	nonclustered located on PRIMARY	ContentID, ImportStatusID
IX_Content_Asset_Asset	nonclustered located on PRIMARY	AssetID
IX_Content_Asset_AssetType	nonclustered located on PRIMARY	AssetTypeID
IX_Content_Asset_Content	nonclustered located on PRIMARY	ContentID
NDX_CA_CIDat	nonclustered located on PRIMARY	ContentID
NDX_CA_impstatus	nonclustered located on PRIMARY	ImportStatusID
NDX_CA_tpidCIDaidATID	nonclustered located on PRIMARY	TargetPlatformID
NDX_ConAs_atidTPIDcidAID	nonclustered located on PRIMARY	AssetTypeID, TargetPlatformID
NDX_ConAsset_IMPSTcidAIDatidPltfrm	nonclustered located on PRIMARY	ImportStatusID
PK_Content_AssetType_1	clustered, unique, primary key located on PRIMARY	Content_AssetID
