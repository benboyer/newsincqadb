USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Partner_PlaylistContentSource_Map](
	[Partner_PlaylistContentSource_MapID] [int] IDENTITY(1,1) NOT NULL,
	[PartnerID] [int] NULL,
	[PlaylistID] [int] NULL,
	[LastContentID] [bigint] NULL,
 CONSTRAINT [PK_Partner_PlaylistContentSource_Map] PRIMARY KEY CLUSTERED 
(
	[Partner_PlaylistContentSource_MapID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
