CREATE procedure [dbo].[usp_LogoURL_Update]
		@PartnerID int = null,
		--@TrackingGroup int = null,
		@LogoURL varchar(200),
		@FullLogoURL varchar(200)
as
	if (@PartnerID is null /*and @trackinggroup is null*/) or @LogoURL is null
	begin
		select 'Give me input' as MessageBack
		Return
	end

	if		(select COUNT(*)
			from	Partner p (nolock)
			join	organizationslegacy ol (nolock)
			on		p.PartnerID = ol.PartnerID
			where	p.PartnerID = ISNULL(@Partnerid, p.PartnerID)
			--and		p.TrackingGroup = ISNULL(@TrackingGroup, p.TrackingGroup)
			and		ISNULL(p.LogoUrl, '-') <> @FullLogoURL
			and		ISNULL(ol.LogoUrl, '-') <> @LogoURL)
			= 1
	begin

	--	select @LogoURL as 'This is it'
			update P
			set		p.LogoURL = @FullLogoURL
			from	Partner p
			join	organizationslegacy ol (nolock)
			on		p.PartnerID = ol.PartnerID
			where	p.PartnerID = ISNULL(@Partnerid, p.PartnerID)
			--and		p.TrackingGroup = ISNULL(@TrackingGroup, p.TrackingGroup)
			and		p.PartnerID is not null
			and		ISNULL(p.LogoUrl, '-') <> @FullLogoURL

			if @@ERROR = 0
			begin
				update ol
				set		ol.LogoURL = @LogoURL
				from	Partner p
				join	organizationslegacy ol (nolock)
				on		p.PartnerID = ol.PartnerID
				where	p.PartnerID = ISNULL(@Partnerid, p.PartnerID)
				--and		p.TrackingGroup = ISNULL(@TrackingGroup, p.TrackingGroup)
				and		p.PartnerID is not null
				and		ISNULL(ol.LogoUrl, '-') <> @LogoURL
			end
			if @@ERROR = 0
			select 'Updated' as MessageBack

	end
	else
	begin
		--select 'Something is wrong' as MessageBack
		select 'Something is wrong.  There are ' + convert(varchar(20), count(*)) + 'PossibleMatches, ' + isnull(p.LogoURL, 'no logourl') + ' exists in the partner table, ' + isnull(ol.LogoURL, 'no url') + ' exists in the OrganizatonsLegacytable' as MessageBack -- p.Name, p.StatusID, p.TrackingGroup, u.login, ol.LogoUrl, u.*
		from	organizationslegacy ol (nolock)
		join	partner p
		on		ol.PartnerID = p.PartnerID
		where	p.PartnerID = ISNULL(@PartnerID, p.PartnerID)
		--and		p.TrackingGroup = ISNULL(@TrackingGroup, p.TrackingGroup)
		group by p.LogoURL, ol.LogoUrl
/*
		select	COUNT(*) PartnerTrackingGroupMatches
		from	partner p
		where	p.TrackingGroup = ISNULL(@TrackingGroup, p.TrackingGroup)

		select count(*) as PartnerID_OrganizationsLMatches
		from	Partner u (nolock)
		join	organizationslegacy ol (nolock)
		on		u.PartnerID = ol.PartnerID
		where	u.PartnerID = ISNULL(@PartnerID, u.PartnerID)
*/
end
