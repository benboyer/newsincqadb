USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetSiteSectionPartner]
	@PartnerID int = null,
	@SectionPartnerID int = null
as
begin
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetSiteSectionPartner'

	select SectionPartnerID, value, IsDefault
	from SectionPartner
	where PartnerID = ISNULL(@PartnerID, SectionPartnerID)
	and	SectionPartnerID = ISNULL(@SectionPartnerID, SectionPartnerID)
	order by Value
	set nocount off
end
GO
