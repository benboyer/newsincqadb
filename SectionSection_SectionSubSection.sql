TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	SectionSection_SectionSubSection	SectionSection_SectionSubSectionID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	SectionSection_SectionSubSection	SectionSectionID	4	int	10	4	0	10	1	None	None	4	None	None	2	YES	38
NewsincQA	dbo	SectionSection_SectionSubSection	SectionSubSectionID	4	int	10	4	0	10	1	None	None	4	None	None	3	YES	38
NewsincQA	dbo	SectionSection_SectionSubSection	AllowPageTypes	4	int	10	4	0	10	1	None	((0))	4	None	None	4	YES	38

index_name	index_description	index_keys
ndx_ss_sss_section	nonclustered located on PRIMARY	SectionSectionID
ndx_ss_sss_subsection	nonclustered located on PRIMARY	SectionSubSectionID
PK_SectionSectionSubSection	clustered, unique, primary key located on PRIMARY	SectionSection_SectionSubSectionID
