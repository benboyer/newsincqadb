TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	PartnerContentImportFolder	PartnerFolderID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	PartnerContentImportFolder	PartnerID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	PartnerContentImportFolder	FolderPath	12	varchar	500	500	None	None	0	None	None	12	None	500	3	NO	39
NewsincQA	dbo	PartnerContentImportFolder	XsltPath	12	varchar	500	500	None	None	0	None	None	12	None	500	4	NO	39
NewsincQA	dbo	PartnerContentImportFolder	Enabled	-7	bit	1	1	None	None	0	None	((0))	-7	None	None	5	NO	50
NewsincQA	dbo	PartnerContentImportFolder	ContentSourceID	4	int	10	4	0	10	0	None	((0))	4	None	None	6	NO	56

index_name	index_description	index_keys
PCIF_PID	nonclustered located on PRIMARY	PartnerID
PK_PartnerContentImportFolder	clustered, unique, primary key located on PRIMARY	PartnerFolderID
