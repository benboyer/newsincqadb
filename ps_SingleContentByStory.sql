CREATE PROCEDURE [dbo].[ps_SingleContentByStory]  
@PartnerId int,  
@StoryId varchar(64)  
  
AS            
BEGIN            
 set FMTONLY OFF            
 set NOCOUNT ON            
 exec SprocTrackingUpdate 'ps_SingleContentByStory'            
      
 select      
    -- @playlistid as PlaylistID,      
    c.ContentID, c.Name, c.Description, pt.TrackingGroup ContentPartnerID, c.PartnerID AS CPID,           
    null as VideoGroupName, fa.Duration, convert(date, c.EffectiveDate) PubDate, c.Keyword, c.antikw AS AKW, c.UpdatedDate, convert(varchar(120), null) Timeline,            
    case when fa.Name like 'Video%' then 'src'            
      else fa.Name      
    end as AssetType,      
 fa.AssetLocation,      
 fa.AssetMimeType,      
 fa.AssetSortOrder,      
 fa.AssetLocation2,      
 fa.AssetMimeType2,      
 fa.AssetSortOrder2,      
 dbo.GetPathToServeAsset(fa.assetid) AssetLocation3,      
    pt.name as ProducerName, pt.shortname ProducerNameAlt, isnull(pt.logourl,'http://assets.newsinc.com/newsinconebyone.png') ProducerLogo, convert(int, 0) as ContentOrder,            
    isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'news') ProducerCategory,            
    REPLACE(c.Name, ' ', '-') + + '-' +CONVERT(varchar(20), c.ContentID) as SEOFriendlyTitle,            
    case when dsm.partnerid is null then 1            
      when dsm.partnerid is not null then 0            
    end as embedCodeAccessible            
/*    ,case when dsm.partnerid is null then 1      
      when dsm.partnerid is not null then 0      
    end as shareEmailAccessible,      
    case when dss.partnerid is null then 1      
      when dss.partnerid is not null then 0      
    end as shareSocialAccessible      
*/              
  from Content (nolock) c      
    
  join partner (nolock) pt      
 on pt.PartnerID = c.PartnerID          
    
  left join Playlist_Content_PartnerMap pm (nolock)      
  on pt.PartnerID = pm.PartnerID and c.FeedID = pm.FeedID            
  left join NDNCategory nc (nolock)            
  on   pm.NDNCategoryID = nc.NDNCategoryID            
  left join vw_partnerDefaultSettings (nolock) dsm            
  on pt.PartnerID = dsm.PartnerID and Entity = 'BlockShareForProviderID'          
    
--  left join (select partnerid from vw_partnerDefaultSettings (nolock) where Entity = 'BlockShareForProviderID' and Setting = 'shareEmail' and Value = 'true') dsm      
--  on core.PartnerID = dsm.PartnerID      
--  left join (select partnerid from vw_partnerDefaultSettings (nolock) where Entity = 'BlockShareForProviderID' and Setting = 'shareSocial' and Value = 'true') dss      
--  on core.PartnerID = dss.PartnerID      
  
 cross apply dbo.ps_fn_getAssetDataMultiAsset(c.PartnerID, c.contentid) fa          
  where c.PartnerID = @PartnerId AND c.StoryID = @StoryId AND C.Active = 1  
  order by replace(fa.Name, 'Video', 'src'), fa.Assetsortorder, fa.AssetMimeType        
 set NOCOUNT OFF      
      
end 