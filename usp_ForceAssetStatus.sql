CREATE procedure [dbo].[usp_ForceAssetStatus]
	@assetid bigint,
	@Status int
as
BEGIN
	update Asset
	set ImportStatusID = @Status
	where AssetID = @assetid

	select AssetID, ImportStatusID, Active
	from Asset
	where AssetID = @assetid

END
