TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Player_Setting	PlayerID	4	int	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	Player_Setting	SettingID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	Player_Setting	Value	-9	nvarchar	50	100	None	None	0	None	None	-9	None	100	3	NO	39
NewsincQA	dbo	Player_Setting	CreatedDate	11	smalldatetime	16	16	0	None	0	None	(getutcdate())	9	3	None	4	NO	58
NewsincQA	dbo	Player_Setting	UpdatedDate	11	smalldatetime	16	16	0	None	1	None	None	9	3	None	5	YES	111

index_name	index_description	index_keys
PK_PlayerSettings	clustered, unique, primary key located on PRIMARY	PlayerID, SettingID
