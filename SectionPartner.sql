TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	SectionPartner	SectionPartnerID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	SectionPartner	PartnerID	4	int	10	4	0	10	1	None	None	4	None	None	2	YES	38
NewsincQA	dbo	SectionPartner	Value	12	varchar	26	26	None	None	1	None	None	12	None	26	3	YES	39
NewsincQA	dbo	SectionPartner	Description	12	varchar	26	26	None	None	1	None	None	12	None	26	4	YES	39
NewsincQA	dbo	SectionPartner	IsDefault	-7	bit	1	1	None	None	1	None	((0))	-7	None	None	5	YES	50

index_name	index_description	index_keys
IX_SectionPartner_PartnerValue	nonclustered, unique located on PRIMARY	PartnerID, Value
PK_SectionPartner	clustered, unique, primary key located on PRIMARY	SectionPartnerID
