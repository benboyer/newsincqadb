CREATE PROCEDURE [dbo].[PlaylistVideos_ADD_sp]
   @VideoID int,  
      @SortOrder int,  
      @Sharing int,  
      @PlaylistID int,  
      @ContentItemID int  
AS  
BEGIN  
 SET NOCOUNT ON;  
 exec SprocTrackingUpdate 'PlaylistVideos_ADD_sp'  
  
 BEGIN TRANSACTION  
 INSERT Playlist_Content(PlaylistID, ContentID, [Order])  
 VALUES (@PlaylistID, @VideoID, @SortOrder)  
 IF @@ERROR = 0  
 BEGIN  
  INSERT PlaylistVideosLEGACY (PlaylistID,ContentID, Sharing, ContentItemID)  
  VALUES(@PlaylistID, @VideoID, @Sharing, @ContentItemID)  
    
  UPDATE Playlist  
  SET ContentUpdatedDate = GETDATE()  
  where PlaylistID = @PlaylistID  
 END  
  
 IF @@ERROR = 0  
  COMMIT TRANSACTION  
 ELSE  
  ROLLBACK TRANSACTION  
  
 RETURN @@ERROR  
  
  
END  
  
