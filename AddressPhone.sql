CREATE VIEW dbo.AddressPhone
AS
SELECT     TOP (100) PERCENT dbo.Address_User.AddressID, dbo.Address_User.UserID, dbo.ContactDetail.Value AS Phone
FROM         dbo.ContactType INNER JOIN
                      dbo.ContactDetail ON dbo.ContactType.ContactTypeID = dbo.ContactDetail.ContactTypeID RIGHT OUTER JOIN
                      dbo.ContactDetail_User ON dbo.ContactDetail.ContactDetailID = dbo.ContactDetail_User.ContactDetailID RIGHT OUTER JOIN
                      dbo.Address_User ON dbo.ContactDetail_User.UserID = dbo.Address_User.UserID
WHERE     (dbo.ContactType.Name = N'Phone')
ORDER BY dbo.Address_User.AddressID