TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	DeliverySetting_TrackingGroup	DeliverySettings_TrackingGroupID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	DeliverySetting_TrackingGroup	DeliverySettingsID	4	int	10	4	0	10	1	None	None	4	None	None	2	YES	38
NewsincQA	dbo	DeliverySetting_TrackingGroup	TrackingGroup	4	int	10	4	0	10	1	None	None	4	None	None	3	YES	38
NewsincQA	dbo	DeliverySetting_TrackingGroup	Value	12	varchar	20	20	None	None	1	None	None	12	None	20	4	YES	39

index_name	index_description	index_keys
PK_DeliverySetting_TrackingGroup	clustered, unique, primary key located on PRIMARY	DeliverySettings_TrackingGroupID
