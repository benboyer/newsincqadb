USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Partner_TargetPlatformProfile](
	[Partner_TargetPlatformProfileID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PartnerID] [int] NOT NULL,
	[ProfileDescriptionID] [int] NULL,
	[TargetPlatformID] [smallint] NOT NULL,
	[isImportEnabled] [bit] NULL,
	[isDistributionEnabled] [bit] NULL,
	[ActivateOnImport] [bit] NOT NULL,
 CONSTRAINT [PK_Partner_TargetDeviceProfile] PRIMARY KEY CLUSTERED 
(
	[Partner_TargetPlatformProfileID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Partner_TargetDeviceProfile_PID_DID] ON [dbo].[Partner_TargetPlatformProfile] 
(
	[PartnerID] ASC,
	[TargetPlatformID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Partner_TargetPlatformProfile]  WITH CHECK ADD  CONSTRAINT [FK_Partner_TargetPlatformProfile_Partner] FOREIGN KEY([PartnerID])
REFERENCES [dbo].[Partner] ([PartnerID])
GO
ALTER TABLE [dbo].[Partner_TargetPlatformProfile] CHECK CONSTRAINT [FK_Partner_TargetPlatformProfile_Partner]
GO
ALTER TABLE [dbo].[Partner_TargetPlatformProfile]  WITH CHECK ADD  CONSTRAINT [FK_Partner_TargetPlatformProfile_TargetPlatform] FOREIGN KEY([TargetPlatformID])
REFERENCES [dbo].[TargetPlatform] ([TargetPlatformID])
GO
ALTER TABLE [dbo].[Partner_TargetPlatformProfile] CHECK CONSTRAINT [FK_Partner_TargetPlatformProfile_TargetPlatform]
GO
ALTER TABLE [dbo].[Partner_TargetPlatformProfile] ADD  CONSTRAINT [DF_Partner_TargetPlatformProfile_ActivateOnImport]  DEFAULT ((1)) FOR [ActivateOnImport]
GO
