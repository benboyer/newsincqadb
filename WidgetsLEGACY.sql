TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	WidgetsLEGACY	LauncherID	4	int	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	WidgetsLEGACY	ContentItemID	4	int	10	4	0	10	1	None	None	4	None	None	2	YES	38
NewsincQA	dbo	WidgetsLEGACY	DCId	4	int	10	4	0	10	1	None	None	4	None	None	3	YES	38
NewsincQA	dbo	WidgetsLEGACY	VanityName	12	varchar	32	32	None	None	1	None	None	12	None	32	4	YES	39
NewsincQA	dbo	WidgetsLEGACY	WidgetIdentificationName	-9	nvarchar	100	200	None	None	1	None	None	-9	None	200	5	YES	39
NewsincQA	dbo	WidgetsLEGACY	IsDefault	-7	bit	1	1	None	None	1	None	None	-7	None	None	6	YES	50
NewsincQA	dbo	WidgetsLEGACY	DefaultTab	4	int	10	4	0	10	1	None	None	4	None	None	7	YES	38
NewsincQA	dbo	WidgetsLEGACY	GenerateNow	-7	bit	1	1	None	None	1	None	None	-7	None	None	8	YES	50
NewsincQA	dbo	WidgetsLEGACY	TemplateZip	-4	image	2147483647	2147483647	None	None	1	None	None	-4	None	2147483647	9	YES	37
NewsincQA	dbo	WidgetsLEGACY	Height	4	int	10	4	0	10	1	None	None	4	None	None	10	YES	38
NewsincQA	dbo	WidgetsLEGACY	Width	4	int	10	4	0	10	1	None	None	4	None	None	11	YES	38

index_name	index_description	index_keys
PK_WidgetsExt	clustered, unique, primary key located on PRIMARY	LauncherID
