USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_GetPartnerInfoByUserID]
	@userid int
as
BEGIN
	SET FMTONLY OFF
	set nocount on
	exec SprocTrackingUpdate 'usp_GetPartnerInfoByUserID'

	select	p.PartnerID, p.Name, p.ZoneID, p.LandingURL, p.FeedGUID, p.isFeedGUIDActive, p.TrackingGroup, u.UserID
	from	[User] u
	join	partner p
	on		u.PartnerID = p.PartnerID
	where userid = @userid

	set nocount off
END
GO
