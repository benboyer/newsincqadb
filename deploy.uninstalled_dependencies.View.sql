USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [deploy].[uninstalled_dependencies] as
select a.feature_order, a.feature_type, a.feature_name, b.feature_order requires_order, b.feature_type requires_type, b.feature_name requires_name
from (select * 
	from deploy.schema_feature sf 
	where depends_on is not null) a
join deploy.schema_feature b
on	a.depends_on = b.feature_order
where b.completed_on is null
GO
