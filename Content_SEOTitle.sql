TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Content_SEOTitle	Content_SEOTitleID	-5	bigint identity	19	8	0	10	0	None	None	-5	None	None	1	NO	63
NewsincQA	dbo	Content_SEOTitle	ContentID	-5	bigint	19	8	0	10	0	None	None	-5	None	None	2	NO	63
NewsincQA	dbo	Content_SEOTitle	SEOtitle	12	varchar	500	500	None	None	0	None	None	12	None	500	3	NO	39
NewsincQA	dbo	Content_SEOTitle	Version	4	int	10	4	0	10	0	None	((0))	4	None	None	4	NO	56

index_name	index_description	index_keys
IX_Content_SEOTitle_name	nonclustered, unique located on PRIMARY	SEOtitle, Version
PK_Content_SEOTitle	clustered, unique, primary key located on PRIMARY	Content_SEOTitleID
