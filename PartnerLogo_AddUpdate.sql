CREATE PROCEDURE [dbo].[PartnerLogo_AddUpdate]
		@logoID int,
		@LogoURL varchar(max),
		@PartnerID int,
		@LauncherID int,
		@LogoTypeID int,
		@PlayerTypesID int = null,
		@Active bit = 1,
		@default bit = 0
AS
BEGIN

	SET FMTONLY OFF
	SET NOCOUNT ON
	exec SprocTrackingUpdate 'PartnerLogo_AddUpdate'
	-- declare	@logoID int = 9757, @LogoURL varchar(max) = 'http://widget.newsinc.com/partnerlogo/7060.partnerlogo600.jpg', @PartnerID int = 1077, @LauncherID int = 7060, @LogoTypeID varchar(10) = 2, @PlayerTypesID int, @Active bit = 1, @default bit = 0

	IF @PartnerID is null and @LauncherID is null
	BEGIN
		select 'PartnerID and LauncherID can not both be null' as messageback
		return
	END

	IF (@LogoTypeID = 1 and @playertypesid <> 1)
		or
		(@logotypeid = 2 and @playertypesid <> 2)
	BEGIN
		select 'LogoType and PlayerTypesID are incompatible.  Fix ~em and try again.' as messageback
		return
	END

	if @PlayerTypesID is null
	begin
		set @PlayerTypesID = (select case 
									when @LogoTypeID = 1 then 1
									when @LogoTypeID = 2 then 2
									else null end) 
	end
	if (@LogoTypeID > 2 and @PlayerTypesID < 4)
	BEGIN
		select 'LogoTypes 1 and 2 are reserved for header logos for PlayerTypes 1, and 2 & 3 respectively.  Fix ~em and try again.' as messageback
		return
	END

	if	isnull(@PartnerID, 0) = 0 set @PartnerID = (select partnerid from Launcher where LauncherID = @LauncherID)

	if @logoID is null
	BEGIN
	-- declare	@logoID int = null, @LogoURL varchar(max) = 'nothersroctestlogo', @PartnerID int = null, @LauncherID int = 7000, @LogoTypeID varchar(10) = 1, @PlayerTypesID int = 3, @Active bit = 1, @default bit = 0
		insert into Logo(LogoTypeID, LogoURL, PlayerTypesID, Active, partnerDefault)
		select	@LogoTypeID, @LogoURL, @PlayerTypesID, @active, @default
		where not exists
			(select 1 from Logo where LogoTypeID = @LogoTypeID and LogoURL = @LogoURL and isnull(PlayerTypesID, '') = isnull(@playertypesid, ''))

		insert into Partner_Logo(PartnerID, LauncherID, LogoID)
		select	@PartnerID, @LauncherID, l.LogoID
		from	Logo l
		where	LogoURL = @LogoURL
		and		LogoTypeID = @LogoTypeID
		and		isnull(PlayerTypesID, '') = isnull(@PlayerTypesID, '')
		and		not exists
				(select 1
				from	Partner_Logo pl
				where	pl.PartnerID = @PartnerID
				and		pl.LogoID = l.LogoID
				and		pl.LauncherID = @LauncherID)

		select	l.LogoID
		from	Logo l
		where	LogoURL = @LogoURL
		and		LogoTypeID = @LogoTypeID
		and		isnull(PlayerTypesID, '') = isnull(@PlayerTypesID, '')

	END
	ELSE
	BEGIN
		update Logo
		set	LogoTypeID = @LogoTypeID,
			LogoURL = @LogoURL,
			PlayerTypesID = @PlayerTypesID,
			Active = @Active,
			PartnerDefault = @default
		where	LogoID = @logoID

		-- eliminate competeting defaults
		update Logo
		set		PartnerDefault = 0
		where	LogoTypeID = @LogoTypeID
		and		LogoURL = @LogoURL
		and		PlayerTypesID = @PlayerTypesID
		and		Active = @Active
		and		LogoID <> @logoID

		update Partner_Logo
		set	LauncherID = @LauncherID
		where	LogoID = @logoID
		and		LauncherID <> @LauncherID

	END

	set nocount off
END
