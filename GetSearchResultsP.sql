Create PROCEDURE [dbo].[GetSearchResultsP]
	(	--  declare
	@SearchString NVARCHAR(4000),
	@WidgetId INT = NULL,
	@UserId INT = 0,
	@OrgId INT =0,
	@Count INT = 100,
	@OrderClause NVARCHAR(MAX) = null,
	@StatusType INT = 0,
	@IsAdmin BIT = 0,
	@WhereCondition NVARCHAR(MAX) = null
	, @StartDTM datetime = null
	, @EndDTM datetime = null,
	@MinVideoID BIGINT = null, -- max value of the current external record set: used for pagination
	@MaxVideoID BIGINT = null, -- min value of the current external record set: used for pagination
	@MatchType int = 1, -- 1= exact match, 0= near (all words), 2= gimme something (any words), 9 = VideoID match
    @SearchType varchar(16) = 'all', -- 'title', -- 'all', 'title', 'keywords', 'description' may be used
	@updates bit = 0,
	@ReturnSet int = 1	-- 1 = all (as per FullTextSearch), 2 = ContentID and Rank only
)
AS
BEGIN
-- select @searchstring = 'obama', @userid = 8284, @OrgId = 0, @count = 120
	-- bugfix 2013-05-23
	set FMTONLY OFF
	set NOCOUNT ON
	exec SprocTrackingUpdate 'GetSearchResults'

	-- insert into sprocparamtracker(sproc, param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12, param13, param14, param15, param16, param17)
	-- select 'GetSearchResults', LEFT(@SearchString, 200),@WidgetId,@UserId,@OrgId,@Count,@OrderClause,@StatusType,@IsAdmin,@WhereCondition,@StartDTM,@EndDTM,@MinVideoID,@MaxVideoID,@MatchType,@SearchType,@updates,@ReturnSet

---------------------------------------------

	if isnull(@StatusType, -1) < 0 set @StatusType = 0
	if @MaxVideoID = 0 set @MaxVideoID = null
	if @MinVideoID = 0 set @MinVideoID = null
	if @WhereCondition = '' set @WhereCondition = null
	if @OrgId < 1 set @OrgId = null
	if @Count is null set @Count = 100
	if @updates is null set @updates = 0
	declare @userisAdmin bit set @userisAdmin = (select convert(bit,isadmin) from NDNUsersLEGACY where UserID = @UserId)
	if @userisAdmin = 0 and @isAdmin = 1 set @userisAdmin = 1

	declare @TG int = (select trackinggroup from partner where partnerid = (select partnerid from [User] where UserID = @UserId)),
			@SearchStringNear varchar(4000),
			@DescriptionStringNear varchar(4000),
			@NameStringNear varchar(4000),
			@KeywordStringNear varchar(4000),
			@SearchStringExact varchar(4000),
			@DescriptionStringExact varchar(4000),
			@NameStringExact varchar(4000),
			@KeywordStringExact varchar(4000),
			@countmore int
			if @SearchType is null	set @SearchType =  'all'
			if @OrderClause is null set @OrderClause  = 'createddate desc'

		if @OrgId = 0 set @OrgId = null

		if @WhereCondition like '%partnerid%' and isnull(@OrgId, 0) = 0
		begin
			set @OrgId = (select convert(int, right(@wherecondition, charindex('=',reverse(@WhereCondition))-1)))
		end

		set @countmore = @count * 2

	-- set @SearchStringNear  = @SearchString
	set @SearchStringNear  = '"' + replace(replace(@SearchString, '"',''), ' ', '" near "') + '"'
	set @SearchStringExact = '"' + replace(@SearchString, '"','') + '"'
	-- select @userisAdmin admin, @TG Tg

	-- select @SearchString SearchString, @SearchStringNear SearchStringNear, @SearchStringExact SearchStringExact
	--select @SearchString searchstring, @WidgetId WidgetID, @UserId UserID, @OrgId OrgID, @Count Count, @OrderClause OrderClause, @StatusType StatusType, @userisAdmin UisAdmin, @IsAdmin IsAdmin, @WhereCondition WhereCondition, @StartDTM StartDTM, @EndDTM EndDTM, @MinVideoID MinVideoID, @MaxVideoID MaxVideoID,@MatchType MatchType, @SearchType SearchType,	@updates Updates, @ReturnSet ReturnSet, @UserisAdmin UserisAdmin, @TG Tg
	create table #tmpFoundList( flid bigint not null identity(1,1) primary key, contentid bigint, [rank] int)

	if @MatchType = 9
	begin
		declare @vid bigint
		set @vid = (select MAX(myvid.vid)
			from	(select isnull(@MinVideoID, 0) vid
					union
					select ISNULL(@MaxVideoID, 0) vid ) myvid)

		insert into #tmpFoundList(contentid, [rank])
		select  ContentID, 1 [rank]
		from	Content (nolock)
		where	contentid = @vid
		and		(dbo.AllowedContentID(@vid, @tg, 1) = 1 or @userisAdmin = 1)
	end
	if @MatchType < 9 and isnull(@SearchString, '') = '' -- and (@StartDTM is not null or @EndDTM is not null)
	begin
		-- set @countmore = @countmore * 2
		set @MatchType = 10
		--if (select @OrderClause) like '%Post%' or (select @OrderClause) like '%created%'
		--begin
		insert into #tmpFoundList(contentid)
		-- declare @countmore int = 26, @orgid int = 499, @tg int = 99999, @startdtm datetime, @enddtm datetime
		select top (@countmore) contentid
		from Content (nolock)
		where	 (PartnerID = ISNULL(@orgid, partnerid)	and		dbo.AllowedContentID(contentid, @tg, 1) = 1)
		and		(@startdtm is null or CreatedDate >= @StartDTM)
		and		(@EndDTM is null or CreatedDate <= @EndDTM)
		order by ContentID desc
		--end
		--else if (select @OrderClause) like '%Event%' or (select @OrderClause) like '%publish%'
		--begin
		--	insert into #tmpFoundList(contentid)
		--	select top (@countmore) contentid
		--	from Content (nolock)
		--	where	 (PartnerID = ISNULL(@orgid, partnerid)	and		dbo.AllowedContentID(contentid, @tg, 1) = 1)
		--	and		(@startdtm is null or CreatedDate >= @StartDTM)
		--	and		(@EndDTM is null or CreatedDate <= @EndDTM)
		--	order by EffectiveDate desc
		--end
		--else
		--begin
		--	insert into #tmpFoundList(contentid)
		--	select top (@countmore) contentid
		--	from Content (nolock)
		--	where	 (PartnerID = ISNULL(@orgid, partnerid)	and		dbo.AllowedContentID(contentid, @tg, 1) = 1)
		--	and		(@startdtm is null or CreatedDate >= @StartDTM)
		--	and		(@EndDTM is null or CreatedDate <= @EndDTM)
		--	order by ContentID desc
		--end

	--	order by case when @OrderClause like '%Post%' or @OrderClause like '%created%' then CreatedDate end desc,
	--			case when @OrderClause like '%Event%' or @OrderClause like '%publish%' then EffectiveDate end desc,
	--			case when @OrderClause like '%Update%' then UpdatedDate end desc--,
	--ContentID desc
	--			 CreatedDate desc
	------------------------------
	end
	if @MatchType = 0 -- All words, must be "near" each other
	begin
		-- declare @searchtype varchar(20) = 'all', @searchstringnear varchar(200) = 'obama'
		insert into #tmpFoundList(contentid, [rank])
		select  a.ContentID, a.[rank]
		from	(select c.contentid, ft.[rank], c.CreatedDate
				from	(select	[key], [RANK]
						from	CONTAINStable(content, (name, description, keyword), @SearchStringNear)
						where	@SearchStringNear is not null
						and		@SearchType ='all') ft
				join	content c
				on		ft.[key] = c.contentid
				union
				select c.contentid, ft.[rank], c.CreatedDate
				from	(select	[key], [RANK]
						from	CONTAINStable(content, (description), @SearchStringNear)
						where	@SearchType = 'description') ft
				join	content c
				on		ft.[key] = c.contentid
				union
				select c.contentid, ft.[rank], c.CreatedDate
				from	(select	[key], [RANK]
						from	CONTAINStable(content, (keyword), @SearchStringNear)
						where	@SearchType = 'keywords') ft
				join	content c
				on		ft.[key] = c.contentid
				union
				select c.contentid, ft.[rank], c.CreatedDate
				from	(select	[key], [RANK]
						from	CONTAINStable(content, (name), @Searchstringnear)
						where	@SearchType = 'title') ft
				join	content c
				on		ft.[key] = c.contentid
				) a
		order by a.[rank] desc, a.createddate desc
	end

	if @MatchType = 1 -- exact match
	begin
	-- declare @searchtype varchar(20) = 'all', @searchstringexact varchar(200) = 'obama'
		insert into #tmpFoundList(contentid)
		select	c.contentid
		from	Content c
		WHERE	(@SearchType in ('all', 'title') and CONTAINS(c.name, @SearchStringExact))
		or		(@SearchType in ('all', 'description') and CONTAINS(c.Description, @SearchStringExact))
		or		(@SearchType in ('all', 'keywords') and CONTAINS(c.Keyword, @SearchStringExact))
		order by
				case when @OrderClause like '%Post%' or @OrderClause like '%created%' then CreatedDate end desc,
				case when @OrderClause like '%Event%' or @OrderClause like '%publish%' then EffectiveDate end desc,
				case when @OrderClause like '%Update%' then UpdatedDate end desc,
				c.CreatedDate desc
	end

	if @MatchType = 2 -- any of the words, by rank
	begin
		insert into #tmpFoundList(contentid, [rank])
		-- declare @searchtype varchar(20) = 'all', @searchstring varchar(200) = 'obama'
		select  a.ContentID, a.[RANK]
		from	(/*
				select c.contentid, ft.[rank], c.CreatedDate
				from	(select	[key], RANK
						from	FREETEXTTABLE(content, (name, description, keyword), @SearchString)
						where	@SearchType  = 'all') ft
				join	content c
				on		ft.[key] = c.contentid
				union all
				*/-- declare @searchtype varchar(20) = 'all', @searchstring varchar(200) = 'obama'
				select c.contentid, ft.[rank], c.CreatedDate
				from	(select	[key], [RANK]
						from	FREETEXTTABLE(content, (name), @SearchString)
						where	@SearchType in ('all','title')) ft
				join	content c
				on		ft.[key] = c.contentid
				union all
				-- declare @searchtype varchar(20) = 'all', @searchstring varchar(200) = 'obama'
				select c.contentid, ft.[rank], c.CreatedDate
				from	(select	[key], [RANK]
						from	FREETEXTTABLE(content, (description), @SearchString)
						where	@SearchType in ('all', 'description')) ft
				join	content c
				on		ft.[key] = c.contentid
				union all
				select c.contentid, ft.[rank], c.CreatedDate
				from	(select	[key], [RANK]
						from	FREETEXTTABLE(content, (keyword), @SearchString)
						where	@SearchType in ('all', 'keywords')) ft
				join	content c
				on		ft.[key] = c.contentid
				) a
		order by a.[rank] desc, a.createddate desc
	end
--------------------------------------------------------------------------------------------------------------------------------------
-- select count(*) InterimRecords from #tmpFoundList

	if (select @ReturnSet) = 1
	begin
		-- declare @countmore int = 84, @OrderClause varchar(200) = 'publish date', @updates bit = 0, @startdtm date, @enddtm date, @orgid int, @minvideoid int, @maxvideoid int,@count int = 42, @userid int = 7534, @userisadmin bit = 1, @statustype int = 0, @tg int = 10557
		select top (@Count)
				v.ContentID [VideoID]
			  ,ul.[UserID]
			  ,ul.UserTypeID [UserType]
			  , convert(varchar(200), isnull(a.filepath + '/' + a.filename, 'http://video.newsinc.com/' + convert(varchar(20), v.contentid) + '.flv')) [ClipURL]
			  ,convert(varchar(200),v.name) [Title]
			  ,v.[Description]
			  ,v.Effectivedate EventDate
			  ,0 [NumberofReviews]
			  ,isnull(a.[Height], cv.Height) Height
			  ,isnull(a.[Width], cv.Width) Width
			  ,isnull(a.[Duration], cv.Duration) Duration
			  ,0 [ClipPopularity]
			  ,0 [IsAdvertisement]
			  ,v.createddate [createdon]
			  ,convert(varchar(200), ISNULL(aa.filepath + '/' +aa.filename, cv.thumbnailfilename)) [ThumbnailURL]
			  ,v.effectivedate [PublishDate]
			  ,convert(varchar(200), ISNULL(v.[Keyword], '')) AS Keywords
			  ,null [NumberOfPlays]
			  ,'http://Rawvideo.newsinc.com/' + convert(varchar(20), v.contentid) + '.flv' [originalname]
			  ,v.expirationdate  [ExpiryDate]
			  ,v.active [IsEnabled]
			  ,v.[IsDeleted]
			  ,v.OnlyOwnerView
			  ,isnull(us.FirstName,'') + ' ' + isnull(us.LastName,'') AS UserName
			  ,p.PartnerID OrganizationID
			  ,v.[Rank]
			  ,p.Name as OrganizationName
			  ,isnull(org.IsMediaSource, 0) as IsMediaSource
			  ,@userisAdmin isadmin
	-- declare @startdtm datetime, @enddtm datetime, @updates bit = 0, @minvideoid int, @maxvideoid int, @orgid int, @tg int = 10557, @userisadmin int = 0, @countmore int = 408, @orderclause varchar(20) = 'posteddate', @statustype int = 0, @userid int = 9294 select top 1000 *
		from	(select top (@count) c.*, r.[rank]
				from	(select contentid, max([rank]) [rank] from #tmpFoundList (nolock) group by contentid) r
				join	Content (nolock) c
				on		r.contentid = c.ContentID
				where	c.ContentImportStatus = 5
				and		(@startdtm is null or c.CreatedDate > @startdtm)
				and		(@enddtm is null or c.createddate < @enddtm)
				and		(@MinVideoID is null or c.ContentID <= @MinVideoID)
				and		(@MaxVideoID is null or c.ContentID >= @MaxVideoID)
				and		c.PartnerID = ISNULL(@orgid, c.partnerid)
				ORDER BY
					case when @OrderClause like '%Post%' or @OrderClause like '%created%' then c.CreatedDate end desc,
					case when @OrderClause like '%Event%' or @OrderClause like '%publish%' then c.EffectiveDate end desc,
					case when @OrderClause like '%Update%' then c.UpdatedDate end desc,
				c.CreatedDate desc) v
		--join	Content (nolock) v
		--on		r.contentid = v.ContentID
		join	content_asset (nolock) ca
		on		v.contentid = ca.contentid
		and		1 = ca.targetplatformid
		and		1 = ca.assettypeid
		join	asset (nolock) a
		on		ca.assetid = a.assetid

		join	content_asset (nolock) ca2
		on		v.contentid = ca2.contentid
		and		1 = ca.targetplatformid
		and		3 = ca2.assettypeid
		join	asset (nolock) aa
		on		ca2.assetid = aa.assetid
		left join ContentVideo (nolock) cv on v.ContentID = cv.ContentID
		join	Partner (nolock) p
		on		v.PartnerID = p.PartnerID
		join	OrganizationsLEGACY (nolock) org
		on		p.PartnerID = org.PartnerID
		join	ndnuserslegacy (nolock) ul
		on		v.createduserid = ul.userid
		join	[user] (nolock) us
		on		v.createduserid = us.userid
		left join (select * from vw_partnerdefaultsettings where entity = 'BlockContentFromControlRoom' and value = 'true') bcr
		on		v.PartnerID = bcr.partnerid
		where	bcr.PartnerID is null
		and		((@StatusType = 0 and (dbo.AllowedContentID(v.ContentID, @tg, 1) = 1 /*or @userisAdmin = 1*/))
			or
				(@StatusType = 1 and v.Active = 0 and v.isDeleted = 0 and (v.CreatedUserID = @UserId or @userisAdmin = 1) )
			or
				(@StatusType = 2 and v.isDeleted = 1 and (v.CreatedUserID = @UserId or @userisAdmin = 1)))
			and		v.PartnerID = ISNULL(@orgid, v.partnerid)
			and		v.contentid <= isnull(@MinVideoID, v.contentid)
			and		v.contentid >= ISNULL(@MaxVideoID, v.contentid)
			and		(
						(@updates = 0
							and
								(@startdtm is null or (v.CreatedDate >= @StartDTM))
							and	(@enddtm is null or (v.CreatedDate <= @EndDTM)))
						or (@updates = 1
							and
								(@startdtm is null or (v.updatedDate >= @StartDTM))
							and	(@enddtm is null or (v.updatedDate <= @EndDTM)))
					)
		ORDER BY
				case when @OrderClause like '%Post%' or @OrderClause like '%created%' then v.CreatedDate end desc,
				case when @OrderClause like '%Event%' or @OrderClause like '%publish%' then v.EffectiveDate end desc,
				case when @OrderClause like '%Update%' then v.UpdatedDate end desc,
				v.CreatedDate desc
	end
	if (select @ReturnSet) = 2
	begin
		select top (@countmore)	t.contentid, max(t.[rank]) [rank] from #tmpFoundList (nolock) t
		join Content (nolock) c
		on		t.contentid = c.ContentID
		left join (select * from vw_partnerdefaultsettings where entity = 'BlockContentFromControlRoom' and value = 'true') bcr
		on		c.PartnerID = bcr.partnerid
		where	bcr.PartnerID is null
		and		dbo.AllowedContentID(c.contentid, @tg, 1) = 1
		and		dbo.AllowedLauncherContentP(@WidgetId, c.PartnerID) = 1
		group by t.contentid
		order by t.contentid desc
	end
	drop table #tmpFoundList
--------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------
/*
	set FMTONLY OFF
	set NOCOUNT ON
	exec SprocTrackingUpdate 'GetSearchResults'

---------------------------------------------
	-- Redefined search, for better matching to phrases (including exact)

	if @StatusType is null set @StatusType = 0
	if @MaxVideoID = 0 set @MaxVideoID = null
	if @MinV
ideoID = 0 set @MinVideoID = null
	if @WhereCondition = '' set @WhereCondition = null
	if @OrgId = 0 set @OrgId = null
	if @Count is null set @Count = 100
	if @updates is null set @updates = 0
	declare @userisAdmin bit set @userisAdmin = isnull(@isAdmin, (select convert(bit,isadmin) from NDNUsersLEGACY where UserID = @UserId))

	declare @TG int = (select trackinggroup from partner where partnerid = (select partnerid from [User] where UserID = @UserId)),
			@SearchStringNear varchar(4000),
			@DescriptionStringNear varchar(4000),
			@NameStringNear varchar(4000),
			@KeywordStringNear varchar(4000),
			@SearchStringExact varchar(4000),
			@DescriptionStringExact varchar(4000),
			@NameStringExact varchar(4000),
			@KeywordStringExact varchar(4000),
			@countmore int
			if @SearchType is null	set @SearchType =  'all'
			if @OrderClause is null set @OrderClause  = 'publishdate desc'
			if @StatusType is null set @StatusType = 0

		if @OrgId = 0 set @OrgId = null
		set @countmore = @count * 2

	-- set @SearchStringNear  = @SearchString
	set @SearchStringNear  = '"' + replace(replace(@SearchString, '"',''), ' ', '" near "') + '"'
	set @SearchStringExact = '"' + replace(@SearchString, '"','') + '"'
	-- select @userisAdmin admin, @TG Tg

	-- select @SearchString SearchString, @SearchStringNear SearchStringNear, @SearchStringExact SearchStringExact
	-- select @SearchString searchstring, @WidgetId WidgetID, @UserId UserID, @OrgId OrgID, @Count Count, @OrderClause OrderClause, @StatusType StatusType, @IsAdmin IsAdmin, @WhereCondition WhereCondition, @StartDTM StartDTM, @EndDTM EndDTM, @MinVideoID MinVideoID, @MaxVideoID MaxVideoID,@MatchType MatchType, @SearchType SearchType,	@updates Updates, @ReturnSet ReturnSet, @UserisAdmin UserisAdmin, @TG Tg
	create table #tmpFoundList( flid bigint not null identity(1,1) primary key, contentid bigint, [rank] int)

	if @MatchType = 9
	begin
		declare @vid bigint
		set @vid = (select MAX(myvid.vid)
			from	(select isnull(@MinVideoID, 0) vid
					union
					select ISNULL(@MaxVideoID, 0) vid ) myvid)

		insert into #tmpFoundList(contentid, [rank])
		select  ContentID, 1 [rank]
		from	Content (nolock)
		where	contentid = @vid
		and		(dbo.AllowedContentID(@vid, @tg, 1) = 1 or @userisAdmin = 1)
	end

	if @MatchType = 0 -- All words, must be "near" each other
	begin
		-- declare @searchtype varchar(20) = 'all', @searchstringnear varchar(200) = 'obama'
		insert into #tmpFoundList(contentid, [rank])
		select  a.ContentID, a.[rank]
		from	(select c.contentid, ft.[rank], c.CreatedDate
				from	(select	[key], [RANK]
						from	CONTAINStable(content, (name, description, keyword), @SearchStringNear)
						where	@SearchStringNear is not null
						and		@SearchType ='all') ft
				join	content c
				on		ft.[key] = c.contentid
				union
				select c.contentid, ft.[rank], c.CreatedDate
				from	(select	[key], [RANK]
						from	CONTAINStable(content, (description), @SearchStringNear)
						where	@SearchType = 'description') ft
				join	content c
				on		ft.[key] = c.contentid
				union
				select c.contentid, ft.[rank], c.CreatedDate
				from	(select	[key], [RANK]
						from	CONTAINStable(content, (keyword), @SearchStringNear)
						where	@SearchType = 'keywords') ft
				join	content c
				on		ft.[key] = c.contentid
				union
				select c.contentid, ft.[rank], c.CreatedDate
				from	(select	[key], [RANK]
						from	CONTAINStable(content, (name), @Searchstringnear)
						where	@SearchType = 'title') ft
				join	content c
				on		ft.[key] = c.contentid
				) a
		order by a.[rank] desc, a.createddate desc
	end

	if @MatchType = 1 -- exact match
	begin
	-- declare @searchtype varchar(20) = 'all', @searchstringexact varchar(200) = 'obama'
		insert into #tmpFoundList(contentid)
		select	c.contentid
		from	Content c
		WHERE	(@SearchType in ('all', 'title') and CONTAINS(c.name, @SearchStringExact))
		or		(@SearchType in ('all', 'description') and CONTAINS(c.Description, @SearchStringExact))
		or		(@SearchType in ('all', 'keywords') and CONTAINS(c.Keyword, @SearchStringExact))
		order by c.CreatedDate desc
	end

	if @MatchType = 2 -- any of the words, by rank
	begin
		insert into #tmpFoundList(contentid, [rank])
		-- declare @searchtype varchar(20) = 'all', @searchstring varchar(200) = 'obama'
		select  a.ContentID, a.[RANK]
		from	(/*
				select c.contentid, ft.[rank], c.CreatedDate
				from	(select	[key], RANK
						from	FREETEXTTABLE(content, (name, description, keyword), @SearchString)
						where	@SearchType  = 'all') ft
				join	content c
				on		ft.[key] = c.contentid
				union all
				*/-- declare @searchtype varchar(20) = 'all', @searchstring varchar(200) = 'obama'
				select c.contentid, ft.[rank], c.CreatedDate
				from	(select	[key], [RANK]
						from	FREETEXTTABLE(content, (name), @SearchString)
						where	@SearchType in ('all','title')) ft
				join	content c
				on		ft.[key] = c.contentid
				union all
				-- declare @searchtype varchar(20) = 'all', @searchstring varchar(200) = 'obama'
				select c.contentid, ft.[rank], c.CreatedDate
				from	(select	[key], [RANK]
						from	FREETEXTTABLE(content, (description), @SearchString)
						where	@SearchType in ('all', 'description')) ft
				join	content c
				on		ft.[key] = c.contentid
				union all
				select c.contentid, ft.[rank], c.CreatedDate
				from	(select	[key], [RANK]
						from	FREETEXTTABLE(content, (keyword), @SearchString)
						where	@SearchType in ('all', 'keywords')) ft
				join	content c
				on		ft.[key] = c.contentid
				) a
		order by a.[rank] desc, a.createddate desc
	end
--------------------------------------------------------------------------------------------------------------------------------------
	-- select count(*) InterimRecords from #tmpFoundList

	if (select @ReturnSet) = 1
	begin
		-- declare @countmore int = 84, @OrderClause varchar(200) = 'publish date', @updates bit = 0, @startdtm date, @enddtm date, @orgid int, @minvideoid int, @maxvideoid int,@count int = 42, @userid int = 7534, @userisadmin bit = 1, @statustype int = 0, @tg int = 10557
		select top (@Count)
				v.ContentID [VideoID]
			  ,ul.[UserID]
			  ,ul.UserTypeID [UserType]
			  , convert(varchar(200), isnull(a.filepath + '/' + a.filename, 'http://video.newsinc.com/' + convert(varchar(20), v.contentid) + '.flv')) [ClipURL]
			  ,convert(varchar(200),v.name) [Title]
			  ,v.[Description]
			  ,v.Effectivedate EventDate
			  ,0 [NumberofReviews]
			  ,isnull(a.[Height], cv.Height) Height
			  ,isnull(a.[Width], cv.Width) Width
			  ,isnull(a.[Duration], cv.Duration) Duration
			  ,0 [ClipPopularity]
			  ,0 [IsAdvertisement]
			  ,v.createddate [createdon]
			  ,convert(varchar(200), ISNULL(aa.filepath + '/' +aa.filename, cv.thumbnailfilename)) [ThumbnailURL]
			  ,v.effectivedate [PublishDate]
			  ,convert(varchar(200), ISNULL(v.[Keyword], '')) AS Keywords
			  ,null [NumberOfPlays]
			  ,'http://Rawvideo.newsinc.com/' + convert(varchar(20), v.contentid) + '.flv' [originalname]
			  ,v.expirationdate  [ExpiryDate]
			  ,v.active [IsEnabled]
			  ,v.[IsDeleted]
			  ,v.OnlyOwnerView
			  ,isnull(us.FirstName,'') + ' ' + isnull(us.LastName,'') AS UserName
			  ,p.PartnerID OrganizationID
			  ,v.[Rank]
			  ,p.Name as OrganizationName
			  ,isnull(org.IsMediaSource, 0) as IsMediaSource
			  ,@userisAdmin isadmin
	-- declare @countmore int = 48, @orderclause varchar(20) = 'posteddate' select top 1000 *
		from	(select top (@countmore) c.*, r.[rank]
				from	(select contentid, max([rank]) [rank] from #tmpFoundList (nolock) group by contentid) r
				join	Content (nolock) c
				on		r.contentid = c.ContentID
				ORDER BY case
					when @OrderClause like '%Post%' then c.CreatedDate
					when @OrderClause like '%Event%' or @OrderClause like '%publish%' then c.EffectiveDate
					when @OrderClause like '%Update%' then c.UpdatedDate
					else 1
					end		DESC ,
				c.CreatedDate desc) v
		--join	Content (nolock) v
		--on		r.contentid = v.ContentID
		join	content_asset (nolock) ca
		on		v.contentid = ca.contentid
		and		1 = ca.targetplatformid
		and		1 = ca.assettypeid
		join	asset (nolock) a
		on		ca.assetid = a.assetid

		join	content_asset (nolock) ca2
		on		v.contentid = ca2.contentid
		and		1 = ca.targetplatformid
		and		3 = ca2.assettypeid
		join	asset (nolock) aa
		on		ca2.assetid = aa.assetid
		left join ContentVideo (nolock) cv on v.ContentID = cv.ContentID
		join	Partner (nolock) p
		on		v.PartnerID = p.PartnerID
		join	OrganizationsLEGACY (nolock) org
		on		p.PartnerID = org.PartnerID
		join	ndnuserslegacy (nolock) ul
		on		v.createduserid = ul.userid
		join	[user] (nolock) us
		on		v.createduserid = us.userid
		left join (select * from vw_partnerdefaultsettings where entity = 'BlockContentFromControlRoom' and value = 'true') bcr
		on		v.PartnerID = bcr.partnerid
		where	bcr.PartnerID is null
		and		((@StatusType = 0 and (dbo.AllowedContentID(v.ContentID, @tg, 1) = 1 or @userisAdmin = 1))
			or
				(@StatusType = 1 and v.Active = 0 and v.isDeleted = 0 and (v.CreatedUserID = @UserId or @userisAdmin = 1) )
			or
				(@StatusType = 2 and v.isDeleted = 1 and (v.CreatedUserID = @UserId or @userisAdmin = 1)))
			and		v.PartnerID = ISNULL(@orgid, v.partnerid)
			and		v.contentid <= isnull(@MinVideoID, v.contentid)
			and		v.contentid >= ISNULL(@MaxVideoID, v.contentid)
			and		(
						(@updates = 0
							and
								(@startdtm is null or (v.CreatedDate >= @StartDTM))
							and	(@enddtm is null or (v.CreatedDate <= @EndDTM)))
						or (@updates = 1
							and
								(@startdtm is null or (v.updatedDate >= @StartDTM))
							and	(@enddtm is null or (v.updatedDate <= @EndDTM)))
					)
		ORDER BY case
					when @OrderClause like '%Post%' then v.CreatedDate
					when @OrderClause like '%Event%' or @OrderClause like '%publish%' then v.EffectiveDate
					when @OrderClause like '%Update%' then v.UpdatedDate
					else 1
				end		DESC ,
				v.CreatedDate desc
	end
	if (select @ReturnSet) = 2
	begin
		select	t.contentid, max(t.[rank]) [rank] from #tmpFoundList (nolock) t
		join Content (nolock) c
		on		t.contentid = c.ContentID
		left join (select * from vw_partnerdefaultsettings where entity = 'BlockContentFromControlRoom' and value = 'true') bcr
		on		c.PartnerID = bcr.partnerid
		where	bcr.PartnerID is null
		and		dbo.AllowedContentID(c.contentid, @tg, 1) = 1
		group by t.contentid
	end
	--------------------------------------------------------------------------------------------------------------------------------------
	drop table #tmpFoundList
*/
END
