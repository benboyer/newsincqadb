TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Playlist_Keyword	PlaylistID	4	int	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	Playlist_Keyword	KeywordID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56

index_name	index_description	index_keys
PK_Playlist_Keyword	clustered, unique, primary key located on PRIMARY	PlaylistID, KeywordID
