TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	FeedContentAsset	FeedContentAssetID	-5	bigint identity	19	8	0	10	0	None	None	-5	None	None	1	NO	63
NewsincQA	dbo	FeedContentAsset	FeedContentID	-5	bigint	19	8	0	10	0	None	None	-5	None	None	2	NO	63
NewsincQA	dbo	FeedContentAsset	MimeTypeID	4	int	10	4	0	10	1	None	None	4	None	None	3	YES	38
NewsincQA	dbo	FeedContentAsset	MimeType	12	varchar	20	20	None	None	1	None	None	12	None	20	4	YES	39
NewsincQA	dbo	FeedContentAsset	Height	4	int	10	4	0	10	1	None	None	4	None	None	5	YES	38
NewsincQA	dbo	FeedContentAsset	FilePath	12	varchar	1000	1000	None	None	1	None	None	12	None	1000	6	YES	39
NewsincQA	dbo	FeedContentAsset	Width	4	int	10	4	0	10	1	None	None	4	None	None	7	YES	38
NewsincQA	dbo	FeedContentAsset	Bitrate	4	int	10	4	0	10	1	None	None	4	None	None	8	YES	38
NewsincQA	dbo	FeedContentAsset	Duration	3	decimal	20	22	2	10	1	None	None	3	None	None	9	YES	106
NewsincQA	dbo	FeedContentAsset	Final	-7	bit	1	1	None	None	1	None	None	-7	None	None	10	YES	50

index_name	index_description	index_keys
NDX_FCA_fcid	nonclustered located on PRIMARY	FeedContentID
PK_FeedContentAssets	clustered, unique, primary key located on PRIMARY	FeedContentAssetID
