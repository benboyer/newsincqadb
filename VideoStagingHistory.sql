CREATE VIEW dbo.VideoStagingHistory
AS
SELECT     ContentVideoImportLogID AS VideoStagingHistoryID, ContentID AS VideoStagingId, ContentVideoImportStatus AS Status, CAST(NULL AS nvarchar(100)) AS CodecUsed, 
                      CreatedDate AS ProcessDate, Message AS Remarks
FROM         dbo.ContentVideoImportLog