TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	ci_DecodeHTML	ci_DecodeHTMLid	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	ci_DecodeHTML	badvalue	12	varchar	30	30	None	None	1	None	None	12	None	30	2	YES	39
NewsincQA	dbo	ci_DecodeHTML	goodvalue	12	varchar	30	30	None	None	1	None	None	12	None	30	3	YES	39
NewsincQA	dbo	ci_DecodeHTML	fixlogic	12	varchar	120	120	None	None	1	None	None	12	None	120	4	YES	39

index_name	index_description	index_keys
PK_ci_DecodeHTML	clustered, unique, primary key located on PRIMARY	ci_DecodeHTMLid
