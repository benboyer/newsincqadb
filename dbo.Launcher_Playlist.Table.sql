USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Launcher_Playlist](
	[LauncherID] [int] NOT NULL,
	[PlaylistID] [int] NOT NULL,
	[Order] [int] NULL,
 CONSTRAINT [PK_Launcher_Playlist_1] PRIMARY KEY CLUSTERED 
(
	[LauncherID] ASC,
	[PlaylistID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_LauPlst_pid] ON [dbo].[Launcher_Playlist] 
(
	[PlaylistID] ASC,
	[Order] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_LP_order] ON [dbo].[Launcher_Playlist] 
(
	[Order] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Launcher_Playlist]  WITH CHECK ADD  CONSTRAINT [FK_Launcher_Playlist_Playlist] FOREIGN KEY([PlaylistID])
REFERENCES [dbo].[Playlist] ([PlaylistID])
GO
ALTER TABLE [dbo].[Launcher_Playlist] CHECK CONSTRAINT [FK_Launcher_Playlist_Playlist]
GO
ALTER TABLE [dbo].[Launcher_Playlist]  WITH CHECK ADD  CONSTRAINT [Launcher_Launcher_Playlist] FOREIGN KEY([LauncherID])
REFERENCES [dbo].[Launcher] ([LauncherID])
GO
ALTER TABLE [dbo].[Launcher_Playlist] CHECK CONSTRAINT [Launcher_Launcher_Playlist]
GO
