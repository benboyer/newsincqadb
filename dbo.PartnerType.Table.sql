USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PartnerType](
	[PartnerTypeID] [tinyint] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](1000) NULL,
	[CreatedDate] [smalldatetime] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_PartnerType] PRIMARY KEY CLUSTERED 
(
	[PartnerTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PartnerType]  WITH CHECK ADD  CONSTRAINT [FK_PartnerType_PartnerType] FOREIGN KEY([PartnerTypeID])
REFERENCES [dbo].[PartnerType] ([PartnerTypeID])
GO
ALTER TABLE [dbo].[PartnerType] CHECK CONSTRAINT [FK_PartnerType_PartnerType]
GO
ALTER TABLE [dbo].[PartnerType] ADD  CONSTRAINT [DF_PartnerType_CreatedDate]  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
