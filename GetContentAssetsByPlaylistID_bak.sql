create PROCEDURE [dbo].[GetContentAssetsByPlaylistID_bak]
--  declare
	@widgetID int = null,
	@playlistID int = null,
	@trackingGroup int = null,
	@contentid bigint = null,
	@devicetype int = 1
AS
 BEGIN
		set FMTONLY OFF
		set nocount on
		exec SprocTrackingUpdate 'GetContentAssetsByPlaylistID'
	-- declare 	@widgetID int = 3233, @playlistID int = 507, @trackingGroup int = 90121, @contentid bigint = null, @devicetype int = 3
	if @devicetype = 3
	begin
		exec ps_ContentAssetsByPlaylistID @widgetID, @playlistID, @trackingGroup, @contentid, @devicetype
		return;
	end


	declare @PlaylistCap int = 50
		Declare	--@now datetime,
				@iwidgetID int,
				@iplaylistID int,
				@itrackinggroup int,
				@idevicetype int
		-- set		@now = GETDATE()
		select @iwidgetID = isnull(@widgetID, 1), @iplaylistID = isnull(@playlistID, (select value from vw_sysDefaultSettings (nolock) where entity = 'Playlist' and Setting = 'Number')), @itrackinggroup = isnull(@trackingGroup, 10557), @idevicetype = isnull(@devicetype, 1)

		if	@PlaylistID = 9999999
		begin
			exec GetSingleContent @contentid, @iTrackingGroup, @deviceType
			return
		end
		select	 distinct a.PlaylistID, a.ContentID, a.Name, a.Description, a.TrackingGroup ContentPartnerID, vg.VideoGroupName, a.Duration,
				a.effectivedate PubDate, a.Keyword, convert(varchar(120), null) Timeline,
				case when at.Name like 'video%' then 'src'
					else at.Name  -- 'unk'
					end as AssetType,
					case when at.Name like 'External%' then a.Filename
					else
						a.DeliveryPath end as AssetLocation, convert(varchar(20), null) as AssetMimeType, convert(int, null) as AssetSortOrder,
					convert(varchar(500), null) as AssetLocation2, convert(varchar(20),null) as AssetMimeType2, convert(int,null) as AssetSortOrder2,
					a.ProducerName, a.ProducerNameAlt, a.logourl ProducerLogo, a.[Order]+1 ContentOrder, isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'news') ProducerCategory,
					REPLACE(a.Name, ' ', '-') + + '-' +CONVERT(varchar(20), a.ContentID) as SEOFriendlyTitle,
					case when dsm.partnerid is null then 1
						when dsm.partnerid is not null then 0
					end as embedCodeAccessible
			from	(select	pc.PlaylistID, c.ContentID, c.Name, c.Description, p.TrackingGroup, c.effectivedate, c.Keyword, convert(varchar(120), null) Timeline,
						ca.AssetTypeID, a.duration, a.FilePath, a.filename, p.Name ProducerName, p.ShortName ProducerNameAlt, isnull(p.LogoURL, 'http://assets.newsinc.com/newsinconebyone.png') LogoURL, pc.[Order], a.MimeTypeID, p.PartnerID, p.isContentPrivate, c.FeedID,
						dbo.GetAssetDeliveryPath(mt.FileExtension, c.partnerid, ca.contentid, ca.assetid) DeliveryPath
				from	(select top (@PlaylistCap) * from playlist_content (nolock) where playlistid = @iplaylistid and dbo.AllowedContentID(contentid, @itrackinggroup, @devicetype) = 1 order by [ORDER] ASC) pc
				join	Content c (nolock)
				on		pc.ContentID = c.ContentID
				join	partner p (nolock)
				on		c.PartnerID = p.PartnerID
				join	Content_Asset ca (nolock)
				on		c.ContentID = ca.ContentID
				join	Asset a (nolock)
				on		ca.AssetID = a.AssetID
				join	MimeType (nolock) mt
				on		a.MimeTypeID = mt.MimeTypeID
				where	pc.PlaylistID = @iplaylistid
				and		ca.TargetPlatformID = @idevicetype
				-- and		c.Active = 1
				and		ca.Active = 1
				and		a.Active = 1) a
		join	AssetType at (nolock)
		on		a.AssetTypeID = at.AssetTypeID
		join	mtrackinggroup pd  (nolock)
		on		@itrackingGroup = pd.TrackingGroup
		left join	VideoGroups VG (nolock)
		on		a.PlaylistID = vg.VideoGroupId
		left join	Playlist_Content_PartnerMap pm   (nolock)-- select * from playlist_content_partnermap where feedid is not null
		on			a.PartnerID = pm.PartnerID
		and			a.FeedID = pm.FeedID
		left join	NDNCategory nc (nolock)
		on			pm.NDNCategoryID = nc.NDNCategoryID
		left join (select partnerid from vw_partnerDefaultSettings (nolock) where Entity = 'BlockShareForProviderID') dsm
		on	a.PartnerID = dsm.PartnerID
		where		dbo.AllowedLauncherContent(@iwidgetID, @itrackinggroup) = 1
		order by a.[Order]+1, a.ContentID

		set nocount off

END
