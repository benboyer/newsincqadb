TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Partner	PartnerID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	Partner	ParentPartnerID	4	int	10	4	0	10	1	None	None	4	None	None	2	YES	38
NewsincQA	dbo	Partner	Name	-9	nvarchar	50	100	None	None	0	None	None	-9	None	100	3	NO	39
NewsincQA	dbo	Partner	ContactID	4	int	10	4	0	10	1	None	None	4	None	None	4	YES	38
NewsincQA	dbo	Partner	Website	-9	nvarchar	200	400	None	None	1	None	None	-9	None	400	5	YES	39
NewsincQA	dbo	Partner	StatusID	5	smallint	5	2	0	10	0	None	((1))	5	None	None	6	NO	52
NewsincQA	dbo	Partner	CreatedDate	11	smalldatetime	16	16	0	None	0	None	(getutcdate())	9	3	None	7	NO	58
NewsincQA	dbo	Partner	CreatedUserID	4	int	10	4	0	10	0	None	None	4	None	None	8	NO	56
NewsincQA	dbo	Partner	UpdatedDate	11	datetime	23	16	3	None	1	None	None	9	3	None	9	YES	111
NewsincQA	dbo	Partner	UpdateUserID	4	int	10	4	0	10	1	None	None	4	None	None	10	YES	38
NewsincQA	dbo	Partner	TrackingGroup	4	int	10	4	0	10	1	None	None	4	None	None	11	YES	38
NewsincQA	dbo	Partner	ShortName	12	varchar	60	60	None	None	1	None	None	12	None	60	12	YES	39
NewsincQA	dbo	Partner	LandingURL	12	varchar	120	120	None	None	1	None	None	12	None	120	13	YES	39
NewsincQA	dbo	Partner	LogoURL	12	varchar	400	400	None	None	1	None	None	12	None	400	14	YES	39
NewsincQA	dbo	Partner	isContentPrivate	-7	bit	1	1	None	None	1	None	None	-7	None	None	15	YES	50
NewsincQA	dbo	Partner	ZoneID	4	int	10	4	0	10	1	None	None	4	None	None	16	YES	38
NewsincQA	dbo	Partner	FeedGUID	-11	uniqueidentifier	36	16	None	None	0	None	(newid())	-11	None	None	17	NO	37
NewsincQA	dbo	Partner	isFeedGUIDActive	-7	bit	1	1	None	None	1	None	None	-7	None	None	18	YES	50
NewsincQA	dbo	Partner	DefaultEmbedWidth	4	int	10	4	0	10	1	None	((425))	4	None	None	19	YES	38
NewsincQA	dbo	Partner	DefaultEmbedRatio	12	varchar	10	10	None	None	1	None	('custom')	12	None	10	20	YES	39
NewsincQA	dbo	Partner	DefaultEmbedHeight	4	int	10	4	0	10	1	None	((320))	4	None	None	21	YES	38
NewsincQA	dbo	Partner	isProvider	-7	bit	1	1	None	None	0	None	((0))	-7	None	None	22	NO	50
NewsincQA	dbo	Partner	isDistributor	-7	bit	1	1	None	None	0	None	((1))	-7	None	None	23	NO	50
NewsincQA	dbo	Partner	isMediaSource	-7	bit	1	1	None	None	0	None	((0))	-7	None	None	24	NO	50
NewsincQA	dbo	Partner	isProgressive	-7	bit	1	1	None	None	0	None	((0))	-7	None	None	25	NO	50

index_name	index_description	index_keys
IX_Partner_private	nonclustered located on PRIMARY	isContentPrivate
IX_Partner_TrkGrp	nonclustered located on PRIMARY	TrackingGroup, ZoneID
NDEX_fedguid	nonclustered located on PRIMARY	FeedGUID
PK_Partner	clustered, unique, primary key located on PRIMARY	PartnerID
