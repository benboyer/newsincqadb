USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetFullTextSearchResult_DB01]
(
	@SearchString NVARCHAR(4000),
	@WidgetId INT = NULL,
	@UserId INT = 0,
	@OrgId INT =0,
	@Count INT = 100,
	@OrderClause NVARCHAR(MAX) = null,
	@StatusType INT = 0,	
	@IsAdmin BIT = 0,
	@WhereCondition NVARCHAR(MAX) = ''
	, @StartDTM datetime = null
	, @EndDTM datetime = null
)

AS
BEGIN
	---------------------------------------------
	-- NEW for more recent search results
	-- declare @iStartDTM datetime = '2011-04-01', @iEndDTM datetime = '2011-04-30 23:59:59'
	DECLARE @iStartDTM nvarchar(12),
			@iEndDTM nvarchar(12),
			@queryString NVARCHAR(MAX),
			@WhereClause NVARCHAR(MAX), 
			@OrganizationId INT,
			@range varchar(max)
	set @iStartDTM = convert(varchar(12), @StartDTM, 111)
	set @iEndDTM = convert(varchar(12), @EndDTM, 111)
	if @iStartDTM is not null
		set @range = ' AND vs.Publishdate between ' + '''' + @iStartDTM + '''' + ' and ' + '''' + @iEndDTM + ''''
	--select @range ranger

	SELECT @OrganizationId = OrganizationId FROM NDNUsers WHERE UserID = @UserId

	declare @count_external int set @count_external = isnull(@count, 100)
	declare @OrderClause_external varchar(max) set @OrderClause_external = @OrderClause

	set @count = 25000
	set @OrderClause = 'PublishDate DESC' 


	SET @WidgetId =  ISNULL(@WidgetId,0)
	SET @UserId =  ISNULL(@UserId,0)
	SET @OrgId =  ISNULL(@OrgId,0)
	SET @Count =  ISNULL(@Count,100)
	SET @OrderClause =  ISNULL(@OrderClause,'')
	SET	@WhereClause =  ISNULL(@WhereClause,'')


	SET @queryString= 
		' SELECT  TOP ' + CONVERT(NVARCHAR(20),@Count) + ' vs.[VideoID]
		  ,vs.[UserID]
		  ,vs.[UserType]
		  ,vs.[ClipURL]
		  ,vs.[Title]
		  ,vs.[Description]
		  ,ISNULL(vs.[EventDate], vs.[PublishDate]) AS EventDate
		  ,vs.[NumberofReviews]
		  ,vs.[Height]
		  ,vs.[Width]
		  ,vs.[Duration]
		  ,vs.[ClipPopularity]
		  ,vs.[IsAdvertisement]
		  ,vs.[CreatedOn]
		  ,vs.[ThumbnailURL]
		  ,vs.[PublishDate]
		  ,ISNULL(vs.[Keywords], '''') AS Keywords
		  ,vs.[NumberOfPlays]
		  ,vs.[OriginalName]	
		  ,vs.[ExpiryDate]	
		  ,vs.[IsEnabled]
		  ,vs.[IsDeleted]
		  ,isnull(us.FirstName,'''') + '' '' + isnull(us.LastName,'''') AS UserName
		  ,us.OrganizationID
		  ,KT.[Rank]
		  ,isnull(org.Name,'''') as OrganizationName
		  ,isnull(org.IsMediaSource, 0) as IsMediaSource 
			FROM Videos vs 
			INNER JOIN FREETEXTTABLE(Videos, ([Description], Title, Keywords), ''' + @SearchString + ''') AS 
			KT ON vs.VideoID = KT.[KEY]
			INNER JOIN NDNUsers us ON vs.UserId = us.UserId
			INNER JOIN Organizations AS org ON org.OrganizationID = us.OrganizationID
			WHERE   org.OrganizationId not in (select OrganizationId from WidgetExcludedOrgs where widgetId = isnull(' + CONVERT(NVARCHAR(20),@WidgetId) + ',0))	
			AND  (((CASE 
			WHEN ((SELECT IsAdmin FROM NDNUsers WHERE UserID = ' + CONVERT(NVARCHAR(20),@UserId) + ' ) = 1 )THEN 1
			WHEN [vs].[UserID] = ' + CONVERT(NVARCHAR(20),@UserId) + ' THEN 1 
			WHEN (((SELECT UserTypeID FROM NDNUsers WHERE UserID = ' + CONVERT(NVARCHAR(20),@UserId) + ') = 1) and org.[IsContentPrivate] = 1) THEN CONVERT(Int,
				(CASE 
					WHEN EXISTS(
						SELECT NULL AS [EMPTY]
						FROM [dbo].[AllowVideoView] AS [AllowVideoView], [dbo].[NDNUsers] AS [NDNUsers], [dbo].[Organizations] AS [Organizations]
						WHERE ([NDNUsers].[UserID] = [vs].[UserID]) AND (([AllowVideoView].[ContentProviderOrgID]) = [NDNUsers].[OrganizationID]) 
						AND (([Organizations].[OrganizationId]) = [NDNUsers].[OrganizationID]) AND ([NDNUsers].[UserID] = [vs].[UserID]) 
							AND ([AllowVideoView].[DistributorOrgID] = 0)
						) THEN 1
					ELSE 0
				 END))
			WHEN org.[IsContentPrivate] = 1 THEN CONVERT(Int,
				(CASE 
					WHEN EXISTS(
						SELECT NULL AS [EMPTY]
						FROM [dbo].[AllowVideoView] AS [AllowVideoView], [dbo].[NDNUsers] AS [NDNUsers], [dbo].[Organizations] AS [Organizations]
						WHERE ([NDNUsers].[UserID] = [vs].[UserID]) AND (([AllowVideoView].[ContentProviderOrgID]) = [NDNUsers].[OrganizationID]) 
						AND (([Organizations].[OrganizationId]) = [NDNUsers].[OrganizationID]) 
							AND (([AllowVideoView].[DistributorOrgID]) = ' + +CONVERT(VARCHAR(20),ISNULL(@OrganizationId,0))+')
						) THEN 1
					ELSE 0
				 END))
			ELSE 1
		 END)) = 1) '



	IF(@StatusType IS NOT NULL AND @StatusType != -1  )
	BEGIN
		SELECT @WhereClause = ' AND  '
		SELECT @WhereClause = @WhereClause + (CASE CONVERT(NVARCHAR(1),@StatusType)
									WHEN '0' THEN '  IsEnabled = 1 AND IsDeleted = 0 AND (ExpiryDate IS NULL OR ExpiryDate > GETDATE()) '
									WHEN '1' THEN '  ( (IsEnabled = 0 OR ExpiryDate < getdate()) AND IsDeleted = 0) '							
									WHEN '2' THEN '  IsDeleted = 1 '												
									END)
	END

	IF (@IsAdmin = 0)
		SELECT @WhereClause = @WhereClause + ' AND vs.UserId = ' + CONVERT(NVARCHAR(20),@UserID) ;

	IF (@WhereCondition IS NOT NULL AND @WhereCondition != '')
		SELECT @WhereClause = @WhereClause + ' AND '+ @WhereCondition ;

	IF(@OrgId IS NULL OR @OrgId >0)
		SELECT @WhereClause = @Whereclause + ' AND org.OrganizationId = ' + CONVERT(NVARCHAR(20),@OrgId)


	SET @queryString = @queryString + ISNULL(@WhereClause,'')  

	if (@StartDTM) is not null and (@EndDTM) is not null
		SELECT @queryString = @queryString + @range
	-- select @range 
	-- select @WhereClause
	-- select @queryString


	-- IF(@WhereClause IS NOT NULL AND @OrderClause IS NOT NULL AND @OrderClause != '')
		SET @queryString = @queryString + ' Order By ' + @OrderClause

	create table #ftsr
			(VideoID int, UserID int, UserType int, ClipURL varchar(max), title varchar(max), Description varchar(max), 
			EventDate datetime, NumberOfReviews int, height decimal(10,4), width decimal(10,4), duration decimal(10,4), 
			ClipPopularity float, IsAdvertisement bit, createdon datetime, ThumbnailURL varchar(max), PublishDate datetime, 
			Keywords varchar(max), NumberOfPlays int, originalname varchar(max), Expirydate datetime, isEnabled bit, 
			IsDeleted bit, UserName varchar(max), organizationid int, [rank] int, OrganizationName varchar(max), isMediaSource bit) 
	insert into #ftsr(VideoID, UserID, UserType, ClipURL, title, Description, 
			EventDate, NumberOfReviews, height, width, duration, 
			ClipPopularity, IsAdvertisement, createdon, ThumbnailURL, PublishDate, 
			Keywords, NumberOfPlays, originalname, Expirydate, isEnabled, 
			IsDeleted, UserName,  organizationid, [rank], OrganizationName, isMediaSource)

	EXEC ( @queryString );
	-- select @count_external
	set @queryString = ' select top ' + convert(varchar(20), @count_external) + ' 
			VideoID, UserID, UserType, ClipURL, title, left(Description, 500) Description, 
			EventDate, NumberOfReviews, Height, Width, Duration,
			isnull(ClipPopularity, 0) ClipPopularity, IsAdvertisement, createdon, ThumbnailURL, PublishDate, 
			left(Keywords, 100) Keywords, NumberOfPlays, originalname, Expirydate, isEnabled, 
			IsDeleted, UserName,  organizationid, [rank], OrganizationName, isMediaSource
			from #ftsr order by PublishDate DESC' --  + isnull(@OrderClause_external, 'PublishDate DESC')
	-- select @queryString
	EXEC ( @queryString );

	drop table #ftsr



END
GO
