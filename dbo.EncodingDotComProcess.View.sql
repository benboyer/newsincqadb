USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[EncodingDotComProcess]

as
SELECT     dbo.EncodingDotComStatus.ContentID, COUNT(dbo.EncodingDotComStatus.ContentID) AS ProcessCount
FROM         dbo.EncodingDotComStatus INNER JOIN
                      dbo.[Content] ON dbo.EncodingDotComStatus.ContentID = dbo.[Content].ContentID
WHERE     (dbo.EncodingDotComStatus.Processed = 1) AND (dbo.[Content].EffectiveDate <= GetUTCDate())
GROUP BY dbo.EncodingDotComStatus.ContentID
GO
