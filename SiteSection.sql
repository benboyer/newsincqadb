CREATE VIEW dbo.SiteSection
AS
SELECT     SectionID AS ID, CreatedUserID AS UserID, PartnerID, Name, CreatedDate AS CreatedDT, Active AS IsEnabled, [Default] AS IsDefault
FROM         dbo.Section
