CREATE procedure [dbo].[Launcher_Delete]
		@LauncherID int = null
AS
BEGIN
	UPDATE Launcher SET
		Active = 0
	WHERE
		LauncherID = @LauncherID
END
