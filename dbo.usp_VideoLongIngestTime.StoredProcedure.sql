USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_VideoLongIngestTime]
AS
BEGIN
	exec SprocTrackingUpdate 'usp_VideoLongIngestTime'

	declare @count int
	set @count = (select COUNT(*) from	Content c
		join	ContentVideo cv
		on		c.ContentID = cv.ContentID
		join	ContentVideoImportStatus s
		on		cv.ContentVideoImportStatusID = s.ContentVideoImportStatusID
		join	Partner p
		on		c.PartnerID = p.PartnerID
		join	[User] u
		on		c.CreatedUserID = u.UserID
		join	ContentType ct
		on		c.ContentTypeID = ct.ContentTypeID
		left join	EncodingDotComStatus edcs
		on		c.ContentID = edcs.ContentID
		and		ct.name = edcs.AssetType
		where	c.CreatedDate < dateadd(n, -45, getdate())
		and		isnull(c.EffectiveDate, getdate()) <= GETDATE()
		and		cv.ContentVideoImportStatusID < 4)

	if @Count < 1

	RAISERROR('Because there was no data the report was not sent out.',16,1)

	else
		SET NOCOUNT ON;
	select  c.ContentID, c.Name VideoName, s.ContentVideoImportStatusID, s.name VideoImportStatus, cv.Duration, cv.ImportFilePath,
			cv.ThumbnailImportFilePath, cv.StillFrameImportFilePath, cv.[GUID], c.CreatedUserID, edcs.AssetType, edcs.Processed, u.login, c.PartnerID,
			p.name PartnerName, c.CreatedDate, datediff(n, c.CreatedDate, getdate()) MinutesStalled -- , u.* -- s.ContentVideoImportStatusID, s.Name,
	from	Content c
	join	ContentVideo cv
	on		c.ContentID = cv.ContentID
	join	ContentVideoImportStatus s
	on		cv.ContentVideoImportStatusID = s.ContentVideoImportStatusID
	join	Partner p
	on		c.PartnerID = p.PartnerID
	join	[User] u
	on		c.CreatedUserID = u.UserID
	join	ContentType ct
	on		c.ContentTypeID = ct.ContentTypeID
	left join	EncodingDotComStatus edcs
	on		c.ContentID = edcs.ContentID
	and		ct.name = edcs.AssetType
	where	c.CreatedDate < dateadd(n, -45, getdate())
	and		isnull(c.EffectiveDate, getdate()) <= GETDATE()
	and		cv.ContentVideoImportStatusID < 4
	order by c.createddate, c.contentid
	SET NOCOUNT OFF;

END
GO
