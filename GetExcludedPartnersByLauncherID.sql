CREATE procedure [dbo].[GetExcludedPartnersByLauncherID]
	--declare
	@LauncherID int
as
BEGIN
	exec SprocTrackingUpdate 'GetExcludedPartnersByLauncherID'

	select	p.PartnerID, p.name PartnerName
	from	Launcher_ContentExclusion lce
	join	partner p
	on		lce.PartnerID = p.PartnerID
	where	lce.LauncherID = @LauncherID
	order by p.Name
END
