CREATE VIEW [dbo].[WidgetPlaylist]
AS
SELECT     LauncherID AS WidgetId, PlaylistID AS PlaylistId, CAST([Order] AS int) AS SortOrder
FROM         dbo.Launcher_Playlist