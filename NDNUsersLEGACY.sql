TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	NDNUsersLEGACY	UserID	4	int	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	NDNUsersLEGACY	EmailID	-9	nvarchar	256	512	None	None	0	None	None	-9	None	512	2	NO	39
NewsincQA	dbo	NDNUsersLEGACY	ReceiveDailyIQEmail	-7	bit	1	1	None	None	1	None	None	-7	None	None	3	YES	50
NewsincQA	dbo	NDNUsersLEGACY	MI	-9	nvarchar	60	120	None	None	1	None	None	-9	None	120	4	YES	39
NewsincQA	dbo	NDNUsersLEGACY	Gender	1	char	1	1	None	None	1	None	None	1	None	1	5	YES	39
NewsincQA	dbo	NDNUsersLEGACY	DOB	11	datetime	23	16	3	None	1	None	None	9	3	None	6	YES	111
NewsincQA	dbo	NDNUsersLEGACY	AddressID	4	int	10	4	0	10	1	None	None	4	None	None	7	YES	38
NewsincQA	dbo	NDNUsersLEGACY	ContentItemID	4	int	10	4	0	10	1	None	None	4	None	None	8	YES	38
NewsincQA	dbo	NDNUsersLEGACY	PartnerId	-9	nvarchar	255	510	None	None	1	None	None	-9	None	510	9	YES	39
NewsincQA	dbo	NDNUsersLEGACY	FreeWheelType	-9	nvarchar	20	40	None	None	0	None	None	-9	None	40	10	NO	39
NewsincQA	dbo	NDNUsersLEGACY	IsAdmin	-7	bit	1	1	None	None	0	None	((0))	-7	None	None	11	NO	50
NewsincQA	dbo	NDNUsersLEGACY	UserTypeID	-6	tinyint	3	1	0	10	0	None	None	-6	None	None	12	NO	48
NewsincQA	dbo	NDNUsersLEGACY	IsFtpEnabled	-7	bit	1	1	None	None	1	None	None	-7	None	None	13	YES	50
NewsincQA	dbo	NDNUsersLEGACY	XSLT	-1	text	2147483647	2147483647	None	None	1	None	None	-1	None	2147483647	14	YES	35
NewsincQA	dbo	NDNUsersLEGACY	AccountStatus	4	int	10	4	0	10	0	None	None	4	None	None	15	NO	56
NewsincQA	dbo	NDNUsersLEGACY	AllowVideoUpload	-7	bit	1	1	None	None	0	None	None	-7	None	None	16	NO	50
NewsincQA	dbo	NDNUsersLEGACY	ProposedOrganization	-9	nvarchar	500	1000	None	None	1	None	None	-9	None	1000	17	YES	39
NewsincQA	dbo	NDNUsersLEGACY	EnableRssFeed	-7	bit	1	1	None	None	0	None	None	-7	None	None	18	NO	50
NewsincQA	dbo	NDNUsersLEGACY	LandingURL	-9	nvarchar	2083	4166	None	None	1	None	None	-9	None	4166	19	YES	39
NewsincQA	dbo	NDNUsersLEGACY	CustomRssFeedXSLT	-9	nvarchar	255	510	None	None	1	None	None	-9	None	510	20	YES	39

index_name	index_description	index_keys
PK_UsersLEGACY	clustered, unique, primary key located on PRIMARY	UserID
