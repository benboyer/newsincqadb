TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	launchersPlayersCombined	LauncherTypesID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	launchersPlayersCombined	Name	-9	nvarchar	20	40	None	None	1	None	None	-9	None	40	2	YES	39
NewsincQA	dbo	launchersPlayersCombined	Description	-9	nvarchar	60	120	None	None	1	None	None	-9	None	120	3	YES	39
NewsincQA	dbo	launchersPlayersCombined	LauncherCompatible	-7	bit	1	1	None	None	0	None	((1))	-7	None	None	4	NO	50
NewsincQA	dbo	launchersPlayersCombined	PlayerCompatible	-7	bit	1	1	None	None	0	None	((1))	-7	None	None	5	NO	50
NewsincQA	dbo	launchersPlayersCombined	LegacyPlayerTypesName	12	varchar	50	50	None	None	1	None	None	12	None	50	6	YES	39
NewsincQA	dbo	launchersPlayersCombined	LegacyPlayerTypesID	4	int	10	4	0	10	1	None	None	4	None	None	7	YES	38

index_name	index_description	index_keys
PK_launcherSPlayersCombined	clustered, unique, primary key located on PRIMARY	LauncherTypesID
