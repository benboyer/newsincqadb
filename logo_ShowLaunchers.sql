CREATE procedure [dbo].[logo_ShowLaunchers]
	@PartnerID int,
	@LogoTypeID int = null,
	@LogoID int = null,
	@AntiLogoID int = null
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'logo_ShowLaunchers'

--if @LogoTypeID is null and @LogoID is not null
--	set @LogoTypeID = (select logotypeid from Logo where LogoID = @LogoID)

/*
	if	@LogoTypeID is null
	and
		@AntiLogoID is not null
	begin
		set @LogoTypeID = (select logotypeid from Logo where LogoID = @AntiLogoID)
	end
*/
	select	p.PartnerID, p.Name, p.TrackingGroup, l.LauncherID, l.Name LauncherName, lo.LogoID, lt.Name LogoType, lt.Description LogoDescription, lo.PartnerDefault, lo.LogoURL
	from	partner p
	join	Launcher l
	on		p.PartnerID = l.PartnerID
	left join	Partner_Logo pl
	on		p.PartnerID = pl.PartnerID
	and		l.LauncherID = pl.LauncherID
	left join	Logo lo
	on		pl.LogoID = lo.LogoID
	left join	LogoType lt
	on		lo.LogoTypeID = lt.LogoTypeID
	where	p.PartnerID = @PartnerID
--	and	not exists(select 1 from Partner_Logo plz join Logo lz on plz.LogoID = lz.LogoID where plz.PartnerID = p.PartnerID and lz.LogoTypeID = lt.LogoTypeID and @LogoTypeID is null)
	and		(lt.LogoTypeID = @LogotypeID or @LogoTypeID is null)
	and		(lo.LogoID = @LogoID or @LogoID is null)
	and		(lo.LogoID <> @AntiLogoID or @AntiLogoID is null)
	order by p.PartnerID, lt.LogoTypeID desc, lo.PartnerDefault desc, lo.LogoURL
	set nocount off
END
