CREATE VIEW dbo.audit_ContentFeedContentAssets
AS
SELECT     dbo.[Content].ContentID, dbo.[Content].Name, dbo.[Content].PartnerID, dbo.FeedContentAsset.FeedContentAssetID, dbo.FeedContentAsset.FeedContentID, 
                      dbo.MimeType.MimeType, dbo.MimeType.FileExtension, dbo.MimeType.MediaType, dbo.FeedContentAsset.FilePath, dbo.FeedContentAsset.Height, 
                      dbo.FeedContentAsset.Width, dbo.FeedContentAsset.Bitrate, dbo.FeedContentAsset.Duration
FROM         dbo.Asset INNER JOIN
                      dbo.Content_Asset ON dbo.Asset.AssetID = dbo.Content_Asset.AssetID INNER JOIN
                      dbo.[Content] ON dbo.Content_Asset.ContentID = dbo.[Content].ContentID INNER JOIN
                      dbo.FeedContentAsset ON dbo.Asset.FeedContentAssetID = dbo.FeedContentAsset.FeedContentAssetID INNER JOIN
                      dbo.MimeType ON dbo.FeedContentAsset.MimeTypeID = dbo.MimeType.MimeTypeID
