TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	PartnerLookup	PartnerID	-9	nvarchar	50	100	None	None	0	None	None	-9	None	100	1	NO	39
NewsincQA	dbo	PartnerLookup	Name	-9	nvarchar	100	200	None	None	0	None	None	-9	None	200	2	NO	39
NewsincQA	dbo	PartnerLookup	Constant	-9	nvarchar	100	200	None	None	1	None	None	-9	None	200	3	YES	39
NewsincQA	dbo	PartnerLookup	Description	-9	nvarchar	1000	2000	None	None	1	None	None	-9	None	2000	4	YES	39
NewsincQA	dbo	PartnerLookup	DisplayConstant	-9	nvarchar	100	200	None	None	1	None	None	-9	None	200	5	YES	39

index_name	index_description	index_keys
PK_PartnerLookup	clustered, unique, primary key located on PRIMARY	PartnerID
