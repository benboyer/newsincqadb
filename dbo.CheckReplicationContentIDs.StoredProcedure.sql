USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CheckReplicationContentIDs]
as
begin
	set FMTONLY OFF
	set	NOCOUNT ON
	exec SprocTrackingUpdate 'CheckReplicationContentIDs'
	
	declare @lastContentID bigint,
			@lastDB02cid bigint,
			@lastPSDB05cid bigint,
			@lastPSDB06cid bigint,
			@lastPSDB07cid bigint,
			@lastPSDB08cid bigint,
			@MostRecentCreatedDt smalldatetime
			
	set @lastcontentid = (select top 1 contentid from content (nolock) order by contentid desc)
	set @MostRecentCreatedDt = (select top 1 createddate from content (nolock) order by CreatedDate desc)
	select @lastDB02cid = (select top 1 contentid from db02.newsincrpt.dbo.content order by contentid desc)
	select @lastPSDB05cid = (select top 1 contentid from PSDB05.newsinc.dbo.content order by contentid desc)
	select @lastPSDB06cid = (select top 1 contentid from PSDB06.newsinc.dbo.content order by contentid desc)
	select @lastPSDB07cid = (select top 1 contentid from PSDB07.newsinc.dbo.content order by contentid desc)
	select @lastPSDB08cid = (select top 1 contentid from PSDB08.newsinc.dbo.content order by contentid desc)

--	if (@lastDB02cid <> @lastContentID)
--	begin
--		waitfor delay '00:00:01'
--		set @lastDB02cid = (select top 1 contentid from db02.newsincrpt.dbo.content order by contentid desc)
--	end

--	if (@lastPSDB05cid <> @lastContentID)
--	begin
--		waitfor delay '00:00:01'
--		set @lastPSDB05cid = (select top 1 contentid from [ec2-50-16-49-17.compute-1.amazonaws.com].newsinc.dbo.content order by contentid desc)
--	end

--	if (@lastPSDB06cid <> @lastContentID)
--	begin
--		waitfor delay '00:00:01'
--		set @lastPSDB06cid = (select top 1 contentid from [ec2-50-19-34-175.compute-1.amazonaws.com].newsinc.dbo.content order by contentid desc)
--	end

	select	@MostRecentCreatedDt LastContentCreatedDate,
			@lastContentID DB01ContentID, 
			@lastDB02cid DB02ContentID, 
			@lastPSDB05cid PSD05ContentID,
			@lastPSDB06cid PSD06ContentID,
			@lastPSDB07cid PSDB07ContentID,
			@lastPSDB08cid PSDB08ContentID, 
			case when @lastContentID = @lastDB02cid and @lastContentID = @lastPSDB05cid and @lastContentID = @lastPSDB06cid and @lastContentID = @lastPSDB07cid and @lastContentID = @lastPSDB08cid then 'Okay' else 'Uh-Oh, does not match. Check at least twice before worrying' end as CIDStatus
	set	NOCOUNT OFF
end
GO
