TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	EmailAlertConfig	EmailAlertConfigID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	EmailAlertConfig	EmailAlertName	12	varchar	42	42	None	None	1	None	None	12	None	42	2	YES	39
NewsincQA	dbo	EmailAlertConfig	CheckFrequency_minutes	4	int	10	4	0	10	1	None	None	4	None	None	3	YES	38
NewsincQA	dbo	EmailAlertConfig	SendAlertFrequency_minutes	4	int	10	4	0	10	1	None	None	4	None	None	4	YES	38
NewsincQA	dbo	EmailAlertConfig	AddressList	12	varchar	500	500	None	None	1	None	None	12	None	500	5	YES	39
NewsincQA	dbo	EmailAlertConfig	LastCheckedDTM	11	datetime	23	16	3	None	1	None	None	9	3	None	6	YES	111
NewsincQA	dbo	EmailAlertConfig	LastSentDTM	11	datetime	23	16	3	None	1	None	None	9	3	None	7	YES	111
NewsincQA	dbo	EmailAlertConfig	ProcedureToRun	12	varchar	200	200	None	None	1	None	None	12	None	200	8	YES	39
NewsincQA	dbo	EmailAlertConfig	EmailMessage	12	varchar	500	500	None	None	1	None	None	12	None	500	9	YES	39
NewsincQA	dbo	EmailAlertConfig	EmailProfile	12	varchar	200	200	None	None	1	None	None	12	None	200	10	YES	39
NewsincQA	dbo	EmailAlertConfig	Active	-7	bit	1	1	None	None	0	None	((1))	-7	None	None	11	NO	50

index_name	index_description	index_keys
PK_EmailAlertConfig	clustered, unique, primary key located on PRIMARY	EmailAlertConfigID
