TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Section	SectionID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	Section	PartnerID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	Section	Name	-9	nvarchar	50	100	None	None	0	None	None	-9	None	100	3	NO	39
NewsincQA	dbo	Section	OldID	-11	uniqueidentifier	36	16	None	None	1	None	None	-11	None	None	4	YES	37
NewsincQA	dbo	Section	Default	-7	bit	1	1	None	None	0	None	None	-7	None	None	5	NO	50
NewsincQA	dbo	Section	Active	-7	bit	1	1	None	None	0	None	None	-7	None	None	6	NO	50
NewsincQA	dbo	Section	CreatedDate	11	smalldatetime	16	16	0	None	0	None	(getdate())	9	3	None	7	NO	58
NewsincQA	dbo	Section	CreatedUserID	4	int	10	4	0	10	0	None	None	4	None	None	8	NO	56
NewsincQA	dbo	Section	UpdatedDate	11	smalldatetime	16	16	0	None	1	None	None	9	3	None	9	YES	111
NewsincQA	dbo	Section	UpdatedUserID	4	int	10	4	0	10	1	None	None	4	None	None	10	YES	38
NewsincQA	dbo	Section	SectionPartnerID	4	int	10	4	0	10	1	None	None	4	None	None	11	YES	38
NewsincQA	dbo	Section	SectionSectionID	4	int	10	4	0	10	1	None	None	4	None	None	12	YES	38
NewsincQA	dbo	Section	SectionSubSectionID	4	int	10	4	0	10	1	None	None	4	None	None	13	YES	38
NewsincQA	dbo	Section	SectionPageTypeID	4	int	10	4	0	10	1	None	None	4	None	None	14	YES	38

index_name	index_description	index_keys
IX_Section_name	nonclustered located on PRIMARY	Name
NDX_Section_CUID	nonclustered located on PRIMARY	CreatedUserID
NDX_Section_PIDdef	nonclustered located on PRIMARY	PartnerID, Default
PK_Section	clustered, unique, primary key located on PRIMARY	SectionID
