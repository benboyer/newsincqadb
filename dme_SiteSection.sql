create view dme_SiteSection
as
select s.PartnerID, s.SectionID, s.CreatedUserID, s.Name FullSiteSection, sp.SectionPartnerID, sp.Value SectionPartnerUnique, sp.IsDefault DefaultUnique, ss.Value Section, sss.Value SubSection, spt.Value PageType 
from	Section s (nolock)
left join SectionPartner sp (nolock)
on		s.SectionPartnerID = sp.SectionPartnerID
left join SectionSection ss (nolock)
on		s.SectionSectionID = ss.SectionSectionID
left join SectionSubSection sss (nolock)
on		s.SectionSubSectionID = sss.SectionSubSectionID
left join	SectionPageType spt (nolock)
on			s.sectionpagetypeid = spt.SectionPageTypeID
