
CREATE PROCEDURE [dbo].[BlockedSites_listInternal_id]
	@includeInactive bit = 0,
	@id int = null
AS
BEGIN
	set nocount on
	set fmtonly off
	exec SprocTrackingUpdate 'BlockedSites_List'

	SELECT	BlockedSitesID, Domain, Active, isnull(DateUpdatedGMT, DateCreatedGMT) LastUpdatedDate, Comment
	  FROM	BlockedSites
	  WHERE ([Active] = 1 or @includeInactive = 1)
	  and	 (@id is null or BlockedSitesID = @id)
	set nocount off
END

