CREATE VIEW dbo.VideoStagingTest
AS
SELECT     dbo.[Content].ContentID AS VideoStagingID, dbo.[Content].CreatedUserID AS UserID, CAST(1 AS int) AS UserType, dbo.ContentVideo.ImportFilePath AS ClipURL, 
                      dbo.[Content].Name AS Title, dbo.[Content].Description, dbo.[Content].EffectiveDate AS EventDate, dbo.ContentVideo.Height, dbo.ContentVideo.Width, 
                      dbo.ContentVideo.Duration, dbo.[Content].CreatedDate AS CreatedOn, dbo.ContentVideo.ThumbnailImportFilePath AS ThumbnailURL, 
                      dbo.[Content].EffectiveDate AS PublishDate, dbo.[Content].Keyword AS Keywords, dbo.ContentVideo.GUID, dbo.ContentVideo.ImportFilePath AS OriginalName, 
                      dbo.ContentVideo.ContentVideoImportStatusID AS EncodingStatus, dbo.[Content].ExpirationDate AS ExpiryDate, dbo.[Content].Active, dbo.[Content].Active AS IsEnabled, 
                      CAST((CASE WHEN Active = 1 THEN 0 ELSE 1 END) AS bit) AS IsDeleted, dbo.ContentSource.Name AS IngestionSource, 
                      dbo.ContentVideo.StillframeImportFilePath AS StillFrameURL, dbo.[Content].ContentID AS VideoId, dbo.[Content].Category, 
                      dbo.VideoStagingLEGACY.ReadyForEncoding, dbo.VideoStagingLEGACY.EncodingPercentComplete AS EncodingPercentCompleted, 
                      dbo.VideoStagingLEGACY.EncodingStarted, dbo.VideoStagingLEGACY.EncodingCompleted, dbo.VideoStagingLEGACY.Status, dbo.VideoStagingLEGACY.FileSize, 
                      dbo.VideoStagingLEGACY.AudioTrackURL, dbo.VideoStagingLEGACY.EncodedURL, dbo.VideoStagingLEGACY.NexidiaStatus, dbo.VideoStagingLEGACY.Mp3Started, 
                      dbo.VideoStagingLEGACY.Mp3Completed, dbo.VideoStagingLEGACY.CodecUsed, ISNULL(dbo.VideoStagingLEGACY.FileExtension, 
                      RIGHT(dbo.ContentVideo.ImportFilePath, CHARINDEX('.', REVERSE(dbo.ContentVideo.ImportFilePath)))) AS FileExtension, dbo.VideoStagingLEGACY.NiaStatus
FROM         dbo.[Content] INNER JOIN
                      dbo.ContentVideo ON dbo.[Content].ContentID = dbo.ContentVideo.ContentID INNER JOIN
                      dbo.ContentSource ON dbo.[Content].ContentSourceID = dbo.ContentSource.ContentSourceID INNER JOIN
                      dbo.VideoStagingLEGACY ON dbo.ContentVideo.ContentID = dbo.VideoStagingLEGACY.ContentID
