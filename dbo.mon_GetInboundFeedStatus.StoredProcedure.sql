USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[mon_GetInboundFeedStatus]
AS
BEGIN
		set fmtonly off
		set nocount on
		exec SprocTrackingUpdate 'mon_GetInboundFeedStatus'
		
		select  p.PartnerID, p.Name, f.FeedId, f.ContentImportSystemID, cs.Name FeedSource, f.FeedPriorityID Priority, f.Active
		from	Feed f
		join	Partner p
		on		f.PartnerID = p.PartnerID
		join	ContentSource cs
		on		f.sourceid = cs.ContentSourceID
		order by p.Name

		set nocount off
END
GO
