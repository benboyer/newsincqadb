CREATE PROCEDURE [dbo].[GetSingleContent]
--  declare
	@ContentID bigint,
	@trackingGroup int = null,
	@devicetype int = 1,
	@playlistid int = 9999999
AS
	set FMTONLY OFF
	set NOCOUNT ON
	exec SprocTrackingUpdate 'GetSingleContent'
/*
	if @devicetype = 3
	begin
		exec ps_SingleContent @ContentID, @trackingGroup, @devicetype
		return
	end
*/
	Declare	@now datetime,
			@iContentID bigint,
			@itrackinggroup int,
			@idevicetype int
			,	@iplaylistid int
	set		@now = GETDATE()
	select	@iContentID = @ContentID, @itrackinggroup = @trackingGroup, @idevicetype = @devicetype , @iplaylistid = isnull(@playlistid, 9999999)

	select  distinct
				--@iplaylistid as PlaylistID, --
				convert(int, 9999999) as PlaylistID,
				a.ContentID, a.Name, a.Description, a.TrackingGroup ContentPartnerID, a.VideoGroupName, a.Duration, convert(date, a.PubDate) PubDate, a.Keyword, convert(varchar(120), null) Timeline,
				case	when a.AssetTypeName like 'Video%' then 'src'
						else a.AssetTypeName -- 'unk'
				end as AssetType,
					case when a.AssetTypeName like 'External%' then a.Filename
				else
						a.DeliveryPath end as AssetLocation, convert(varchar(20), null) as AssetMimeType, convert(int, null) as AssetSortOrder,
					convert(varchar(500), null) as AssetLocation2, convert(varchar(20),null) as AssetMimeType2, convert(int,null) as AssetSortOrder2,
				a.ProducerName, a.ProducerNameAlt, isnull(a.logourl,'http://assets.newsinc.com/newsinconebyone.png') ProducerLogo, convert(int, 0) as ContentOrder,
				isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'news') ProducerCategory,
				REPLACE(a.Name, ' ', '-') + + '-' +CONVERT(varchar(20), a.ContentID) as SEOFriendlyTitle,
				case when dsm.partnerid is null then 1
						when dsm.partnerid is not null then 0
				end as embedCodeAccessible
	from (select	pt.partnerid, pt.TrackingGroup, pt.isContentPrivate, c.ContentID, c.Name,
				c.Description, c.EffectiveDate PubDate, null as [order], aa.FilePath, aa.Filename, aa.Duration, c.Keyword,
				null as VideoGroupName, at.Name AssetTypeName, mt.FileExtension, pt.logourl, pt.Name ProducerName, pt.ShortName ProducerNameAlt,
				dbo.GetAssetDeliveryPath(mt.FileExtension, c.partnerid, ca.contentid, ca.assetid) DeliveryPath, c.FeedID
		from	(select * from Content (nolock) where contentid = @iContentID) c
		join	Partner_TargetPlatformProfile (nolock) ptp
		on		c.PartnerID = ptp.PartnerID
		and		@idevicetype = ptp.TargetPlatformID
		join	TargetPlatform_AssetType (nolock) tpat
		on		ptp.TargetPlatformID = tpat.TargetPlatformID

		join	Content_Asset (nolock) ca
		on		c.ContentID = ca.ContentID
		join	AssetType (nolock) at
		on		tpat.AssetTypeID = at.AssetTypeID
		and		ca.AssetTypeID = at.AssetTypeID
		join	Asset (nolock) aa
		on		ca.AssetID = aa.AssetID

		join	MimeType (nolock) mt
		on		at.MimeTypeid = mt.MimeTypeID
		JOIN	partner (nolock) pt
		ON		c.PartnerID = pt.PartnerID) a
		left join	Playlist_Content_PartnerMap pm   (nolock)-- select * from playlist_content_partnermap where feedid is not null
		on			a.PartnerID = pm.PartnerID
		and			a.FeedID = pm.FeedID
		left join	NDNCategory nc (nolock)
		on			pm.NDNCategoryID = nc.NDNCategoryID
		left join (select partnerid from vw_partnerDefaultSettings (nolock) where Entity = 'BlockShareForProviderID') dsm
		on	a.PartnerID = dsm.PartnerID
		-- where		dbo.AllowedContentID(a.contentid, @itrackinggroup, @idevicetype) = 1
		-- Exclusion check not needed.  Assumption is that prior calls (launcher/playlist or playlist/content) has done exclusion checks.
		order by case	when a.AssetTypeName like 'Video%' then 'src'
						else a.AssetTypeName
					end
	SET NOCOUNT OFF
