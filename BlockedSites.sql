TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	BlockedSites	BlockedSitesID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	BlockedSites	Domain	12	varchar	1000	1000	None	None	0	None	None	12	None	1000	2	NO	39
NewsincQA	dbo	BlockedSites	Active	-7	bit	1	1	None	None	0	None	((1))	-7	None	None	3	NO	50
NewsincQA	dbo	BlockedSites	DateCreatedGMT	11	datetime	23	16	3	None	0	None	(getutcdate())	9	3	None	4	NO	61
NewsincQA	dbo	BlockedSites	DateUpdatedGMT	11	datetime	23	16	3	None	1	None	None	9	3	None	5	YES	111
NewsincQA	dbo	BlockedSites	CreatedBy	12	varchar	60	60	None	None	1	None	None	12	None	60	6	YES	39
NewsincQA	dbo	BlockedSites	UpdatedBy	12	varchar	60	60	None	None	1	None	None	12	None	60	7	YES	39
NewsincQA	dbo	BlockedSites	Comment	12	varchar	200	200	None	None	1	None	None	12	None	200	8	YES	39

index_name	index_description	index_keys
IX_BlockedSites_active	nonclustered located on PRIMARY	Active
NDX_BlkSit_dom	nonclustered located on PRIMARY	Domain
PK_BlockedSites	clustered, unique, primary key located on PRIMARY	BlockedSitesID
