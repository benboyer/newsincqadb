TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	KeywordType	KeywordTypeID	-6	tinyint	3	1	0	10	0	None	None	-6	None	None	1	NO	48
NewsincQA	dbo	KeywordType	Name	12	varchar	30	30	None	None	0	None	None	12	None	30	2	NO	39
NewsincQA	dbo	KeywordType	CreatedUTCDate	11	smalldatetime	16	16	0	None	0	None	(getutcdate())	9	3	None	3	NO	58

index_name	index_description	index_keys
PK_KeywordType	clustered, unique, primary key located on PRIMARY	KeywordTypeID
