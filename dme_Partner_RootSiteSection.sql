CREATE view dbo.dme_Partner_RootSiteSection
as
select partnerid, isnull(isDefault, 0) isDefault, value
from sectionpartner
