TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	WidgetPlayerLegacy	LauncherID	4	int	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	WidgetPlayerLegacy	DisableSocialNetworking	-7	bit	1	1	None	None	1	None	None	-7	None	None	2	YES	50
NewsincQA	dbo	WidgetPlayerLegacy	DisableAds	-7	bit	1	1	None	None	1	None	None	-7	None	None	3	YES	50
NewsincQA	dbo	WidgetPlayerLegacy	DisableVideoPlayOnLoad	-7	bit	1	1	None	None	1	None	None	-7	None	None	4	YES	50
NewsincQA	dbo	WidgetPlayerLegacy	DisableSingleEmbed	-7	bit	1	1	None	None	1	None	None	-7	None	None	5	YES	50
NewsincQA	dbo	WidgetPlayerLegacy	PartnerLogo	-4	image	2147483647	2147483647	None	None	1	None	None	-4	None	2147483647	6	YES	37
NewsincQA	dbo	WidgetPlayerLegacy	StyleSheet	-4	image	2147483647	2147483647	None	None	1	None	None	-4	None	2147483647	7	YES	37
NewsincQA	dbo	WidgetPlayerLegacy	DisableKeywords	-7	bit	1	1	None	None	1	None	None	-7	None	None	8	YES	50
NewsincQA	dbo	WidgetPlayerLegacy	ContinuousPlay	4	int	10	4	0	10	1	None	None	4	None	None	9	YES	38

index_name	index_description	index_keys
PK_WidgetPlayerLegacy	clustered, unique, primary key located on PRIMARY	LauncherID
