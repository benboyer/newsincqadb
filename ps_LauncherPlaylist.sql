CREATE PROCEDURE [dbo].[ps_LauncherPlaylist]                            
 @widgetID int = null,                
 @trackingGroup int = 10557,                
 @playlistid bigint = null,                
 @DeviceType int = 1                            
AS                            
BEGIN                            
    
-- Launcher exists but playlist doesn't -- Return all Playlists for launcher ordered by playlist order    
-- Launcher exists Playlist exists -- Return all Playlists for launcher ordered by playlist order    
-- Launcher exists Playlist exists but not related -- Return only the playlist    
-- Launcher doesnt exists but playlist does -- return playlist    
    
-- EXEC [ps_LauncherPlaylist] 1529, 10557  -- launcher exists with access    
-- EXEC [ps_LauncherPlaylist] 1528, 10557, 992  -- launcher exists, playlist exists but launcher doesn't have playlist    
-- EXEC [ps_LauncherPlaylist] 88888, 10557, 992 -- launcher doesn't exist, but playlist does    
-- EXEC [ps_LauncherPlaylist] 88888, 10557, 88888 -- neither exist    
                 
 set nocount on                            
 set fmtonly off                            
 declare @done bit = 0                          
                           
 declare @list table (PlaylistID int, Title nvarchar(100), PlaylistOrder int, ContentUpdatedDate datetime)                          
 if ( @TrackingGroup = 0 ) set @trackingGroup = 10557        
 declare @partnerid int = ( select top 1 partnerid from mtrackinggroup with(nolock) where TrackingGroup = @TrackingGroup)    
                 
 if @playlistid is not null                
 insert into @list                          
 select lp.PlaylistID, p.name as Title, lp.[Order] as PlaylistOrder, p.ContentUpdatedDate as ContentUpdatedDate from    
 ( select distinct playlistid from dbo.allowedcontentbylauncherID(@widgetID, @partnerid) ) l    
 inner join Playlist p on p.PlaylistID = l.PlaylistID     
 inner join Launcher_Playlist lp on lp.PlaylistID = p.PlaylistID and lp.PlaylistID = l.playlistID and lp.LauncherID = @widgetID    
 where exists ( select top 1 launcherid from Launcher_Playlist where LauncherID = @widgetID and PlaylistID = @playlistid )    
 if @@ROWCOUNT > 0                           
 begin                          
  print 1    
  set nocount off                          
  select * from @list order by PlaylistOrder                     
  return --set @done = 1                          
 end                          
    
 if ( @done = 0 )                           
    insert into @list                          
    select p.PlaylistID, p.Name as Title, 0, p.ContentUpdatedDate from Playlist p    
    where p.PlaylistID = @playlistID    
    and exists ( select top 1 * from dbo.AllowedContentbyPlaylistID(@PlaylistID, @PartnerID) )    
                         
 if @@ROWCOUNT > 0                           
 begin       
  print 2    
  set nocount off                          
  select * from @list order by PlaylistOrder                           
  return --set @done = 1                          
 end        
                 
 if @done = 0 and @playlistid is null                
 begin        
 insert into @list                          
 select lp.PlaylistID, p.name as Title, lp.[Order] as PlaylistOrder, p.ContentUpdatedDate as ContentUpdatedDate from    
 ( select distinct playlistid from dbo.allowedcontentbylauncherID(@widgetID, @partnerid) ) l    
 inner join Playlist p on p.PlaylistID = l.PlaylistID     
 inner join Launcher_Playlist lp on lp.PlaylistID = p.PlaylistID and lp.PlaylistID = l.playlistID and lp.LauncherID = @widgetID    
 where exists ( select top 1 launcherid from Launcher_Playlist where LauncherID = @widgetID )  
 order by lp.[order]             
end         
                         
 if @@ROWCOUNT > 0                           
 begin                          
  print 3                          
  set nocount off                          
  select * from @list order by PlaylistOrder                              
  return -- set @done = 1               
 end                          
                            
 if ( @done = 0 )                           
begin        
 print 4   
 set @playlistid = (select convert(int,value) from vw_sysdefaultsettings where entity = 'Playlist' and setting = 'Number')    
    select p.PlaylistID, p.Name as Title, 0 as PlaylistOrder, p.ContentUpdatedDate as ContentUpdatedDate from Playlist p    
    where p.PlaylistID = @playlistID    
    and exists ( select top 1 * from dbo.AllowedContentbyPlaylistID(@PlaylistID, @PartnerID) )    
                        
end                          
 --set nocount off                          
 --if ( @done = 0 )                            
 --select * from @list order by PlaylistOrder                              
                          
end   
  
return  
/*  
select * from  
  
select * from sysobjects where name like '%launcher%P'  
  
sp_helptext AllowedLauncherContentP  
*/  
  
