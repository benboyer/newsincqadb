CREATE view dbo.LandingPageFeed_Hulu
as
SELECT     TOP (100) PERCENT c.CreatedDate, c.ContentID, c.Name AS Title, c.Description, c.EffectiveDate AS PublishDate, a.AssetID, at.Name AS AssetType, 
                      CASE WHEN at.Name LIKE 'External%' THEN a.Filename ELSE CASE WHEN LEFT(a.FileName, 4) = 'http' THEN a.FileName WHEN LEFT(a.filepath, 4) 
                      = 'http' THEN CASE WHEN a.Filename IS NOT NULL THEN CASE WHEN RIGHT(a.FilePath, 1) = '/' OR
                      LEFT(a.filename, 1) = '/' THEN a.FilePath + a.Filename ELSE a.FilePath + '/' + a.Filename END END WHEN a.FilePath IS NOT NULL AND a.FilePath IS NOT NULL 
                      THEN 'http://' + a.FilePath + '/' + a.Filename WHEN a.FilePath IS NOT NULL AND a.Filename IS NULL THEN 'http://' + a.FilePath WHEN a.FilePath IS NULL AND 
                      a.Filename IS NOT NULL THEN 'http://' + a.Filename ELSE isnull(a.filepath, '') + '/' + a.Filename END END AS FilePath, a.Height, a.Width, pc.PlaylistID
FROM         dbo.[Content] AS c WITH (nolock) INNER JOIN
                      dbo.Content_Asset AS ca WITH (nolock) ON c.ContentID = ca.ContentID INNER JOIN
                      dbo.AssetType AS at ON ca.AssetTypeID = at.AssetTypeID INNER JOIN
                      dbo.Asset AS a WITH (nolock) ON ca.AssetID = a.AssetID INNER JOIN
                      dbo.Playlist_Content AS pc WITH (nolock) ON c.ContentID = pc.ContentID
WHERE     (c.PartnerID = 1325) AND (c.Active = 1) AND (ca.Active = 1) AND (a.Active = 1)
ORDER BY c.CreatedDate DESC, c.ContentID, AssetType