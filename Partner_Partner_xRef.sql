TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Partner_Partner_xRef	Partner_Partner_xrefID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	Partner_Partner_xRef	MasterPartnerID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	Partner_Partner_xRef	PartnerSourceID	12	varchar	20	20	None	None	1	None	None	12	None	20	3	YES	39
NewsincQA	dbo	Partner_Partner_xRef	PartnerIdentifier	12	varchar	10	10	None	None	1	None	None	12	None	10	4	YES	39
NewsincQA	dbo	Partner_Partner_xRef	PartnerID	4	int	10	4	0	10	1	None	None	4	None	None	5	YES	38

index_name	index_description	index_keys
PK_Partner_Partner_xRef	clustered, unique, primary key located on PRIMARY	Partner_Partner_xrefID
