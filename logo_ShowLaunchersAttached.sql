create procedure [dbo].[logo_ShowLaunchersAttached]
	@PartnerID int,
	@LogoTypeID int = null,
	@LogoID int = null
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'logo_ShowLaunchers'

--if @LogoTypeID is null and @LogoID is not null
--	set @LogoTypeID = (select logotypeid from Logo where LogoID = @LogoID)
--	 declare @partnerid int, @logoid int set @PartnerID = 487 set @logoid = 3542
	select	p.PartnerID, p.Name, p.TrackingGroup, l.LauncherID, l.Name LauncherName, lo.LogoID, lt.Name LogoType, lt.Description LogoDescription, lo.PartnerDefault, lo.LogoURL
	from	partner p
	join	Launcher l
	on		p.PartnerID = l.PartnerID
	join	Partner_Logo pl
	on		p.PartnerID = pl.PartnerID
	and		l.LauncherID = pl.LauncherID
	join	Logo lo
	on		pl.LogoID = lo.LogoID
	join	LogoType lt
	on		lo.LogoTypeID = lt.LogoTypeID
	where	p.PartnerID = @PartnerID
	and		pl.LogoID = isnull(@LogoID, pl.logoid)

--	and		(lt.LogoTypeID = isnull(@LogotypeID, lt.LogoTypeID) or lt.LogoTypeID is null)
--	and		(lo.LogoID = ISNULL(@LogoID, lo.LogoID) or lo.LogoID is null)
	order by p.PartnerID, lt.LogoTypeID desc, lo.PartnerDefault desc, lo.LogoURL
	set nocount off
END
