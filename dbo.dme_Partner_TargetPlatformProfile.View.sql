USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[dme_Partner_TargetPlatformProfile]
as
select Partner_TargetPlatformProfileID, PartnerID, ProfileDescriptionID, TargetPlatformID, isImportEnabled, isDistributionEnabled
from Partner_TargetPlatformProfile (nolock)
GO
