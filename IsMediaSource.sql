-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[IsMediaSource]
(
	-- Add the parameters for the function here
	@PartnerID int
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @retVal bit

	-- Add the T-SQL statements to compute the return value here
	SELECT @retVal = 1

	-- Return the result of the function
	RETURN @retVal

END