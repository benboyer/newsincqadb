CREATE procedure [dbo].[usp_CheckPrivateConflict]
	--declare
	@ProviderID int,
	@DistributorID int
as
BEGIN
	-- declare @ProviderID int = 453, @DistributorID int = 457

	declare	@isPrivate bit,
			@isAllowed bit,
			@Response varchar(200)

--	set @ProviderID = (select partnerid from mTrackingGroup where TrackingGroup = @ProviderDPID)
--	set @DistributorID = (select partnerid from mTrackingGroup where TrackingGroup = @DistributorDPID)

	set @isprivate =  (select iscontentprivate from Partner where PartnerID = @ProviderID)
	-- select @isPrivate
	-- declare @ProviderID int = 453, @DistributorID int = 457, @response varchar(200)
	if (select COUNT(*) from Partner where PartnerID = @ProviderID) = 0
		or
		(select COUNT(*) from Partner where PartnerID = @DistributorID) = 0
	begin
			set @Response = 'At least one of those PartnerIDs do not exist. Double check!'
	select @Response as PrivateConflictCheck
	return
	end

	if @isPrivate = 1
	begin
		if		-- declare @ProviderID int = 453, @DistributorID int = 457, @response varchar(200)
			(select	COUNT(*)
			from	AllowContent ac
			where	ContentProviderPartnerID = @ProviderID
			and		DistributorPartnerID = @DistributorID) > 0
		begin
			set @Response = (select 'Provider ' + convert(varchar(10), a.PartnerID) + ' (' + a.name + ') is Private, Distributor ' + convert(varchar(10), b.partnerid) + ' (' + b.name + ') is allowed'
			from Partner a, Partner b
			where	a.PartnerID = @ProviderID
			and		b.PartnerID = @DistributorID)
		end
		else
		begin
			-- declare @ProviderID int = 453, @DistributorID int = 457, @response varchar(200)

			set @Response = (select 'Provider ' + convert(varchar(10), a.PartnerID) + ' (' + a.name + ') is Private, Distributor ' + convert(varchar(10), b.partnerid) + ' (' + b.name + ') is NOT allowed'
			from Partner a, Partner b
			where	a.PartnerID = @ProviderID
			and		b.PartnerID = @DistributorID)
			--select @Response
		end
	end
	else
	begin
			set @Response = (select 'Provider ' + convert(varchar(10), a.PartnerID) + ' is NOT Private'
			from Partner a
			where PartnerID = @ProviderID)
	end
	select @Response as PrivateConflictCheck
END
