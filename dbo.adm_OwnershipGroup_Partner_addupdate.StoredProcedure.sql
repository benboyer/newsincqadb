USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[adm_OwnershipGroup_Partner_addupdate]
	--declare 
	@GroupID int,
	@PartnerID int,
	@TGAdunitName varchar(100),
	@Delete bit = 0
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'adm_OwnershipGroup_Partner_addupdate'

	if  @Delete = 0 and 
		@GroupID is not null
		and	@PartnerID is not null
		and exists (select 1 from ownershipgroup (nolock) where ownershipgroupid = @GroupID)
		and exists (select 1 from Partner (nolock) where PartnerID = @PartnerID)
	BEGIN
		insert into ownershipgroup_partner(ownershipgroupid, OwnershipAdUnitName, partnerid, TrackingGroupAdUnitName)
		select @groupid, AdUnit, @partnerid, @TGAdunitName
		from OwnershipGroup 
		where OwnershipGroupID = @GroupID
		and not exists 
			(select 1 
			from	ownershipgroup_partner
			where	partnerid = @partnerid)
	END

	if		@Delete = 1
		--and		@GroupID is null
		and		@PartnerID is not null
	BEGIN
		delete
		from	ownershipgroup_partner
		where	Partnerid = @PartnerID
	END
	set nocount off
END
GO
