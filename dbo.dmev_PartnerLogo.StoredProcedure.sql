USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[dmev_PartnerLogo]
	@PartnerID int = null,
	@LauncherID int = null,
	@LogoURL varchar(120) = null,
	@LogoDescription varchar(20) = null
as
	select * 
	from dme_PartnerLogo (nolock) pl
	where	pl.partnerid = ISNULL(@PartnerID, pl.partnerid)
	and		pl.logourl  like '%' + isnull(@LogoURL, pl.logourl) + '%'
	-- and		'%' + pl.name + '%' like isnull(@Name, pl.name)
	and		pl.LauncherID = ISNULL(@LauncherID, pl.LauncherID)
	and		pl.LogoDescription = ISNULL(@LogoDescription, pl.LogoDescription)
GO
