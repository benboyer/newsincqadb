USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[RunEmailAlerts]
as
BEGIN
	set fmtonly off
	set nocount on

	declare @alertid int, @sql varchar(5000), @to varchar(1000), @sub varchar(1000), @msg varchar(5000), @prof varchar(200)
	declare @alertcount table(num int)

	create	table #ealert(EmailAlertConfigID int)
	insert into	#ealert(EmailAlertConfigID)
	select	EmailAlertConfigID
	from	emailalertconfig
	where	(lastcheckeddtm > dateadd(n, -CheckFrequency_minutes, getdate()) or lastcheckeddtm is null)
	and		(lastsentdtm  > dateadd(n, -SendAlertFrequency_minutes, getdate()) or lastsentdtm is null)

		--select * from #ealert

		--select * from EmailAlertConfig
	set @alertid = (select min(EmailAlertConfigID) from #ealert)
	set @sql = (select ProcedureToRun from EmailAlertConfig where EmailAlertConfigID = @alertid)
		--select count(*) from @alertcount

	insert into @alertcount(num)
	exec @sql
		--select convert(varchar(20), num) from @alertcount
		update EmailAlertConfig set LastCheckedDTM = getdate() where EmailAlertConfigID = @alertid
		if (select count(*) from @alertcount) > 0
		begin
			set @to = (select AddressList from EmailAlertConfig where EmailAlertConfigid = @alertid)
			set @sub = (select EmailAlertName from EmailAlertConfig where EmailAlertConfigid = @alertid)
			set @msg = (select convert(varchar(20), num) from @alertcount) + ' ' + (select EmailMessage from EmailAlertConfig where EmailAlertConfigid = @alertid)
			set @prof = (select emailprofile from EmailAlertConfig  where EmailAlertConfigID = @alertid)
			EXEC msdb.dbo.sp_send_dbmail @profile_name = @prof,
			@recipients = @to,
			@subject = @sub,
			@body = @msg
			update EmailAlertConfig set LastSentDTM = getdate() where EmailAlertConfigID = @alertid
			delete from #ealert where EmailAlertConfigID = @alertid
		end
		select @msg messageback
	drop table #ealert

	set nocount on
END
GO
