
CREATE procedure [dbo].[usp_ListPartnerLogos_pretty]
	@PartnerID int = null,
	@DPID int = null
as
BEGIN
	-- declare @PartnerID int = null, @DPID int = 99999
	-- declare @PartnerID int = null, @DPID int = 90081
	-- declare @PartnerID int = 898, @DPID int =90479
	if @partnerid is null
		set @partnerid = (select partnerid from mTrackingGroup (nolock) where trackinggroup = @dpid)

	select	p.partnerid, p.name PartnerName, '' as Partner_LogoID, '' as LauncherID, '' as LogoID,  lt.LogoTypeID, lt.name LogoType, 'producer' Description, logourl, 1 Active, 'partner' as SourceTable
		from	partner p (nolock),
				logotype lt (nolock)
		where	p.PartnerID = @partnerid
		and		p.LogoURL is not null
		and		lt.logotypeid = 5
	union	
		select p.partnerid, p.name, '' as Partner_LogoID, '' as LauncherID, '' as LogoID, lt.logotypeid, lt.name LogoType, 'producer' Description, ol.logourl, 1 Active, 'OrganizationsLegacy' as SourceTable
		from	OrganizationsLEGACY ol (nolock)
		join	Partner p
		on		ol.PartnerID = p.PartnerID,
				logotype lt (nolock)
		where	ol.PartnerID = @partnerid
		and		ol.LogoUrl is not null
		and		lt.logotypeid = 5
	union
		-- declare @PartnerID int = 898, @DPID int =90479
		select	p.partnerid, p.name, pl.Partner_LogoID, isnull(la.LauncherID, 0) LauncherID, l.LogoID, lt.logotypeid, lt.name LogoType, lt.Description, l.logourl, l.Active, 'Logo' as SourceTable
		from	(select * from partner where PartnerID = @PartnerID) p
		join	Launcher la
		on		p.PartnerID = la.PartnerID
		left join partner_logo pl (nolock)
		on		la.LauncherID = pl.LauncherID
		left join	logo l (nolock)
		on		pl.logoid = l.logoid
		left join	logotype lt
		on		l.logotypeid = lt.logotypeid
		where	p.partnerid = @partnerid
	order by  LauncherID, lt.LogoTypeID
END


