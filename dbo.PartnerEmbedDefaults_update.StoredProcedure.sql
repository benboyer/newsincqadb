USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[PartnerEmbedDefaults_update]
	-- declare
	@PartnerID int,
	@Width int,
	@Height int,
	@Ratio varchar(10) = null
as
BEGIN
	set fmtonly off
	SET NOCOUNT ON
	exec SprocTrackingUpdate 'PartnerEmbedDefaults_update'
	-- declare @PartnerID int = 102, @Width int = 426, @height int = 321, @Ratio varchar(10) = '4x3'
	if @Width = 0 set @Width = null
	if @Height= 0 set @Height = null
	if @Ratio = '0' set @Ratio = null
	if exists (select 1 from partner where PartnerID = @PartnerID)
	begin -- select @PartnerID, @Width, @Ratio end
		update partner
		set	DefaultEmbedWidth = @Width,
			DefaultEmbedHeight = @Height,
			DefaultEmbedRatio = @Ratio
		where	PartnerID = @PartnerID

		select PartnerID,Name, DefaultEmbedWidth, DefaultEmbedHeight, DefaultEmbedRatio
		from Partner 
		where PartnerID = @PartnerID
	end
	else	-- if not exists (select 1 from partner where PartnerID = @PartnerID)
	begin
		select 'Check the PartnerID and try again.' as MessageBack
	end

	set nocount off
END
GO
