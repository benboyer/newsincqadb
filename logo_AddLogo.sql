

CREATE procedure [dbo].[logo_AddLogo]
	@PartnerID int,
	@LogoTypeID int,
	@LogoURL varchar(200),
	@Default bit = 1,
	@Active bit = 1
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'logo_AddLogo'

	declare @LogoID int
	insert into Logo(LogoTypeID, LogoURL, PartnerDefault, Active)
	select @LogoTypeID, @LogoURL, @Default, @Active
	where not exists (select 1
			from  Logo
			where	LogoTypeID = @LogoTypeID
			and		LogoURL = @LogoURL)

		set @LogoID = (Select LogoID from Logo where LogoURL = @LogoURL)

		insert into partner_logo(PartnerID, LogoID)
		select @PartnerID, @LogoID
		where not exists(
			select 1
			from Partner_Logo
			where PartnerID = @partnerid
			and	 LogoID = @LogoID)

	-- handle legacy tables
	if @Active = 1 and  @LogoTypeID = 5
	begin
		--update Partner and other tables
		update Partner set LogoURL = @LogoURL where PartnerID = @PartnerID
		update organizationslegacy set LogoURL = right(@LogoURL, charindex('/', reverse(@LogoURL))-1) where PartnerID = @Partnerid
	end

	exec logo_ListLogos @partnerID, @LogoTypeID
	set nocount off
END
