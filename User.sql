TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	User	UserID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	User	PartnerID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	User	FirstName	-9	nvarchar	50	100	None	None	0	None	None	-9	None	100	3	NO	39
NewsincQA	dbo	User	LastName	-9	nvarchar	50	100	None	None	0	None	None	-9	None	100	4	NO	39
NewsincQA	dbo	User	Title	-9	nvarchar	100	200	None	None	1	None	None	-9	None	200	5	YES	39
NewsincQA	dbo	User	Login	-9	nvarchar	150	300	None	None	0	None	None	-9	None	300	6	NO	39
NewsincQA	dbo	User	Password	-3	varbinary	128	128	None	None	0	None	None	-3	None	128	7	NO	37
NewsincQA	dbo	User	Active	-7	bit	1	1	None	None	0	None	None	-7	None	None	8	NO	50
NewsincQA	dbo	User	CreatedUserID	4	int	10	4	0	10	0	None	None	4	None	None	9	NO	56
NewsincQA	dbo	User	CreatedDate	11	smalldatetime	16	16	0	None	0	None	(getutcdate())	9	3	None	10	NO	58
NewsincQA	dbo	User	UpdatedDate	11	smalldatetime	16	16	0	None	1	None	None	9	3	None	11	YES	111
NewsincQA	dbo	User	UpdatedUserID	4	int	10	4	0	10	1	None	None	4	None	None	12	YES	38
NewsincQA	dbo	User	PasswordFormat	4	int	10	4	0	10	0	None	None	4	None	None	13	NO	56
NewsincQA	dbo	User	PasswordSalt	-3	varbinary	128	128	None	None	1	None	None	-3	None	128	14	YES	37
NewsincQA	dbo	User	PasswordToken	-9	nvarchar	255	510	None	None	1	None	None	-9	None	510	15	YES	39
NewsincQA	dbo	User	StatusID	5	smallint	5	2	0	10	0	None	None	5	None	None	16	NO	52
NewsincQA	dbo	User	IsAdmin_Conv	-7	bit	1	1	None	None	1	None	((0))	-7	None	None	17	YES	50

index_name	index_description	index_keys
NDX_User_StatUIDpidLogin	nonclustered located on PRIMARY	StatusID
PK_User	clustered, unique, primary key located on PRIMARY	UserID
