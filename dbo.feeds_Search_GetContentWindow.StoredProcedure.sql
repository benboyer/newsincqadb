USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[feeds_Search_GetContentWindow]         
 -- Add the parameters for the stored procedure here        
    @StartIdx INT = NULL,      
    @StartDate DATETIME,      
    @EndDate DATETIME,        
    @WindowSize INT = NULL,
    @ReturnTotal BIT = 0  
AS        
BEGIN        
      
SET NOCOUNT ON;      
      
DECLARE @ResolveTotalResults BIT      
IF (@StartIdx IS NULL AND @WindowSize IS NOT NULL) OR @ReturnTotal = 1
    SET @ResolveTotalResults = 1      
ELSE      
    SET @ResolveTotalResults = 0      
      
DECLARE @MyStartIdx INT      
IF @StartIdx IS NULL      
BEGIN      
    SET @MyStartIdx = (SELECT TOP 1 ContentID FROM Content ORDER BY ContentId DESC) + 1      
END      
ELSE      
BEGIN      
    SET @MyStartIdx = @StartIdx      
END      
      
DECLARE @MyStartDate DATETIME      
IF @StartDate IS NULL      
BEGIN      
    SET @MyStartDate = GETDATE()      
END      
ELSE      
BEGIN      
    SET @MyStartDate = @StartDate      
END      
      
DECLARE @MyEndDate DATETIME      
IF @EndDate IS NULL      
BEGIN      
    SET @MyEndDate = (SELECT TOP 1 UpdatedDate FROM Content WHERE UpdatedDate IS NOT NULL ORDER BY UpdatedDate ASC)      
END      
ELSE      
BEGIN      
    SET @MyEndDate = @EndDate      
END   

DECLARE @ExcludedContent TABLE      
(      
    ContentId INT      
)    /*  
INSERT INTO @ExcludedContent      
-- TODO: NOT SURE IF THIS WORKS COMPLETELY- should be ContentID == MyStartIdx?
SELECT ContentID FROM Content C WHERE C.ContentId >= @MyStartIdx AND C.UpdatedDate = @MyStartDate
*/

CREATE TABLE #TempContent(
    ContentID INT PRIMARY KEY,        
    UUID UNIQUEIDENTIFIER,   
    PartnerID INT,      
    PartnerName NVARCHAR(50),      
    Title VARCHAR(500),        
    [Description] VARCHAR(1000),
    Category VARCHAR(250) NULL,
    Keyword VARCHAR(1000) NULL,
    CreatedDate DATETIME,  
    EffectiveDate DATETIME,  
    ExpirationDate DATETIME NULL,  
    UpdatedDate DATETIME NULL,
    Active BIT NULL        
)

IF @WindowSize IS NOT NULL
BEGIN
INSERT INTO #TempContent
SELECT TOP(@WindowSize)
    C.ContentID, C.UUID, P.PartnerID, p.Name, C.Name, C.[Description], C.Category, C.Keyword,
    C.CreatedDate, C.EffectiveDate, C.ExpirationDate, C.UpdatedDate, C.Active
FROM Content C
    INNER JOIN [Partner] P ON P.PartnerId = C.PartnerID
    INNER JOIN [Content_Asset] CA ON CA.ContentID = C.ContentID
WHERE
    CA.Active = 1 AND
    CA.AssetTypeID = 1 AND
    C.UpdatedDate BETWEEN @MyEndDate AND @MyStartDate AND
    (C.ContentId NOT IN (SELECT ContentID FROM @ExcludedContent))
ORDER BY C.UpdatedDate DESC, C.ContentID DESC
END
ELSE BEGIN
INSERT INTO #TempContent
SELECT
    C.ContentID, C.UUID, P.PartnerID, p.Name, C.Name, C.[Description], C.Category, C.Keyword,
    C.CreatedDate, C.EffectiveDate, C.ExpirationDate, C.UpdatedDate, C.Active
FROM Content C
    INNER JOIN [Partner] P ON P.PartnerId = C.PartnerID
    INNER JOIN [Content_Asset] CA ON CA.ContentID = C.ContentID
WHERE
    CA.Active = 1 AND
    CA.AssetTypeID = 1 AND
    C.UpdatedDate BETWEEN @MyEndDate AND @MyStartDate AND
    (C.ContentId NOT IN (SELECT ContentID FROM @ExcludedContent))
ORDER BY C.UpdatedDate DESC
END



SELECT
C.ContentID, C.UUID, C.PartnerID, C.PartnerName, C.Title, C.[Description],
C.Category, C.Keyword, C.CreatedDate, C.EffectiveDate, C.ExpirationDate,
C.UpdatedDate, C.Active,
V.AssetID, V.AssetTypeID, V.MimeType, V.MediaType, dbo.GetPathToServeAsset(V.AssetID) AS ContentURL, V.Height AS VideoHeight, V.Width AS VideoWidth, V.Duration,
T.AssetID, T.AssetTypeID, T.MimeType, T.MediaType, dbo.GetPathToServeAsset(T.AssetID) AS ThumbnailURL, T.Height AS ThumbHeight, T.Width AS ThumbWidth
FROM #TempContent C
    LEFT OUTER JOIN 
    (
        SELECT CA.ContentID, A.AssetID, A.AssetTypeID, MT.MimeType, MT.MediaType, A.Height, A.Width, A.Duration FROM Asset A
            INNER JOIN Content_Asset CA ON CA.AssetID = A.AssetID
            INNER JOIN MimeType MT ON MT.MimeTypeID = A.MimeTypeID
        WHERE CA.AssetTypeID = 1 AND CA.Active = 1 AND CA.ContentID IN (SELECT ContentID FROM #TempContent)
    ) V on V.ContentID = C.ContentID
    LEFT OUTER JOIN
    (
        SELECT CA.ContentID, A.AssetID, A.AssetTypeID, MT.MimeType, MT.MediaType, A.Height, A.Width FROM Asset A
            INNER JOIN Content_Asset CA ON CA.AssetID = A.AssetID
            INNER JOIN MimeType MT ON MT.MimeTypeID = A.MimeTypeID
        WHERE CA.AssetTypeID = 3 AND CA.Active = 1 AND CA.ContentID IN (SELECT ContentID FROM #TempContent)
    ) T on T.ContentID = C.ContentID
    ORDER BY C.UpdatedDate DESC, C.ContentID DESC

-- Resolve Total Results count, if required      
IF 1 = @ResolveTotalResults      
BEGIN
    SELECT COUNT(*) FROM Content C
        INNER JOIN [Partner] P ON P.PartnerId = C.PartnerID
        INNER JOIN [Content_Asset] CA ON CA.ContentID = C.ContentID
    WHERE
        CA.Active = 1 AND
        CA.AssetTypeID = 1 AND
        C.UpdatedDate BETWEEN @MyEndDate AND @MyStartDate AND
        (C.ContentId NOT IN (SELECT ContentID FROM @ExcludedContent))
END
    
DROP TABLE #TempContent

END
GO
