USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DistributionFilePath](
	[DistributionFilePathID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[MimeFileExtension] [varchar](20) NOT NULL,
	[StoragePath] [varchar](200) NULL,
	[DeliveryPath] [varchar](200) NULL,
	[StorageLevel1] [varchar](100) NULL,
	[DeliveryLevel1] [varchar](100) NULL,
	[StorageLevel2] [varchar](100) NULL,
	[DeliveryLevel2] [varchar](100) NULL,
	[StorageLevel3] [varchar](100) NULL,
	[DeliveryLevel3] [varchar](100) NULL,
	[StorageLevel4] [varchar](100) NULL,
	[DeliveryLevel4] [varchar](100) NULL,
	[IncludeStorageExtension] [bit] NOT NULL,
	[IncludeDeliveryExtension] [bit] NOT NULL,
	[active] [bit] NOT NULL,
	[StorageLevel5] [varchar](100) NULL,
	[DeliveryLevel5] [varchar](100) NULL,
	[StorageLevel6] [varchar](100) NULL,
	[DeliveryLevel6] [varchar](100) NULL,
 CONSTRAINT [PK_DistributionFilePath] PRIMARY KEY CLUSTERED 
(
	[DistributionFilePathID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_DistributionFilePath_ext] ON [dbo].[DistributionFilePath] 
(
	[MimeFileExtension] ASC,
	[StoragePath] ASC,
	[DeliveryPath] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DistributionFilePath] ADD  CONSTRAINT [DF_DistributionFilePath_IncludeStorageExtension]  DEFAULT ((1)) FOR [IncludeStorageExtension]
GO
ALTER TABLE [dbo].[DistributionFilePath] ADD  CONSTRAINT [DF_DistributionFilePath_IncludeDeliveryExtension]  DEFAULT ((1)) FOR [IncludeDeliveryExtension]
GO
ALTER TABLE [dbo].[DistributionFilePath] ADD  CONSTRAINT [DF_DistributionFilePath_active]  DEFAULT ((1)) FOR [active]
GO
