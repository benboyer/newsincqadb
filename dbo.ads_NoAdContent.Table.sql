USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ads_NoAdContent](
	[ads_NoAdContentID] [int] IDENTITY(1,1) NOT NULL,
	[videoid] [bigint] NOT NULL,
	[filteraddeddate] [datetime] NULL,
	[filteractive] [bit] NOT NULL,
	[updateddate] [datetime] NULL,
	[KeywordAddPhrase] [varchar](200) NULL,
 CONSTRAINT [PK__ads_NoAdContentID] PRIMARY KEY CLUSTERED 
(
	[ads_NoAdContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ads_NoAdContent] ADD  CONSTRAINT [DF_ads_NoAdContentID_Act]  DEFAULT ((1)) FOR [filteractive]
GO
