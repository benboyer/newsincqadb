USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[BlockedSites_AddUpdate]
	--declare
	@ID int = null,
	@Host varchar(1000),
	@user varchar(20),
	@active bit,
	@Comment varchar(200) = null,
	@Rules varchar(1000) = null
AS
	--select @ID = 13, @user = 'me', @active = 1, @Host = 'abetterfourthsite'
	-- select @user = 'A wonderful guy', @ID = 4, @active = 1, @Host = 'myFourthsite'
BEGIN
	set fmtonly OFF
	set nocount ON
	exec SprocTrackingUpdate 'BlockedSites_AddUpdate'

	if @Host = ''
	begin
		return
	end

	select @Host = ltrim(RTRIM(@Host))

	if @Rules is null
	begin
		set @Rules = @Host + '/*'
	end
	
	select @Rules = LTRIM(rtrim(@Rules))

	if	@user is null
	begin
		select 'Nice Try.  Identify yourself!' as MessageBack
		return
	End

	if	ISNULL(@Host, '') = '' or len(@Host) < 5 or @active is null
	begin
		select 'Nice Try.  Use real values' as MessageBack
		return
	End

	if exists (select 1 from BlockedSite (nolock) where BlockedSiteID = @ID)
	begin

		insert into BlockedSitesLog(BlockedSiteID, LastAction, LastActionDateGMT, ActionTakenBy, FieldName, FieldValue)
		select	BlockedSiteID, 'Updated', GETUTCDATE(), @user, 'Host', Host
		from	BlockedSite (nolock)
		where	BlockedSiteID = @ID
		and		Host <> @Host

		insert into BlockedSitesLog(BlockedSiteID, LastAction, LastActionDateGMT, ActionTakenBy, FieldName, FieldValue)
		select	BlockedSiteID, 'Updated', GETUTCDATE(), @user, 'Active', active
		from	BlockedSite (nolock)
		where	BlockedSiteID = @ID
		and		Active <> @active

		insert into BlockedSitesLog(BlockedSiteID, LastAction, LastActionDateGMT, ActionTakenBy, FieldName, FieldValue)
		--declare @user varchar(10) = 'bob', @id int = 13, @Comment varchar(10) = 'harpy'
		select	BlockedSiteID, 'Updated', GETUTCDATE(), @user, 'Comment', Comment
		from	BlockedSite (nolock)
		where	BlockedSiteID = @ID
		and		isnull(Comment, '') <> isnull(@Comment, '')

		insert into BlockedSitesLog(BlockedSiteID, LastAction, LastActionDateGMT, ActionTakenBy, FieldName, FieldValue)
		select	BlockedSiteID, 'Updated', GETUTCDATE(), @user, 'Rules', Rules
		from	BlockedSite (nolock)
		where	BlockedSiteID = @ID
		and		isnull(Rules, '') <> isnull(@Rules, '')

		if exists	(select 1
					from	BlockedSite (nolock)
					where	BlockedSiteID = @ID
					and		(Host <> @Host
							or
							Active <> @active)
							or
							isnull(Comment, '') <> isnull(@Comment, ''))
		begin
			update BlockedSite set Active = @active, DateUpdatedGMT = GETUTCDATE(), UpdatedBy = @user where BlockedSiteID = @ID and Active <> @active
			update BlockedSite set Host = @Host, DateUpdatedGMT = GETUTCDATE(), UpdatedBy = @user  where BlockedSiteID = @ID and isnull(Host, '') <> @Host
		--declare @user varchar(10) = 'bob', @id int = 13, @Comment varchar(10) = 'harpy'
			update BlockedSite set Comment = @Comment, DateUpdatedGMT = GETUTCDATE(), UpdatedBy = @user  where BlockedSiteID = @ID and isnull(Comment, '') <> isnull(@Comment, '')
			update BlockedSite set Rules= @Rules, DateUpdatedGMT = GETUTCDATE(), UpdatedBy = @user  where BlockedSiteID = @ID and isnull(Rules, '') <> @Rules
		end

	end
	else
	begin
		if not exists (select 1
							from BlockedSite bs
							where	bs.Host = @Host)
		begin
			insert into BlockedSite(Host, Rules, Active, CreatedBy, DateCreatedGMT, Comment)
			select @Host, @Rules, @active, @user, GETUTCDATE(), @Comment
			where not exists (select 1
							from BlockedSite bs
							where	bs.Host = @Host)

			insert into BlockedSitesLog(BlockedSiteID, LastAction, LastActionDateGMT, ActionTakenBy)
			select BlockedSiteID, 'Created', GETUTCDATE(), @user
			from	BlockedSite
			where	Host = @Host
			and		CreatedBy = @user
			and		Active = @active
		end
		else
		begin
			insert into BlockedSitesLog(BlockedSiteID, LastAction, LastActionDateGMT, ActionTakenBy)
			select BlockedSiteID, 'PreviouslyCreated', GETUTCDATE(), @user
			from	BlockedSite
			where	Host = @Host
		end
	end
	set nocount OFF
END
GO
