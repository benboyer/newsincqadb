USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetDetailTextSearchResult_3]
(	-- declare
	@SearchString VARCHAR(5000) = null,
	@DescriptionString varchar(5000) = null,
	@TitleString varchar(5000) = null,
	@KeywordString varchar(5000) = null,
	@WidgetId INT = NULL,
	@UserId INT = 0,
	@OrgId INT =0,
	@Count INT = 100,
	@OrderClause NVARCHAR(MAX) = null,
	@StatusType INT = 0,
	@IsAdmin BIT = 0,
	@WhereCondition NVARCHAR(MAX) = ''
	, @StartDTM datetime = null
	, @EndDTM datetime = null
	, @devicetype int = 1
	, @VideoID bigint = null
	, @MinVideoID bigint = null -- max value of the current external record set: used for pagination
	, @MaxVideoID bigint = null -- min value of the current external record set: used for pagination
)

AS
BEGIN

	SET FMTONLY OFF
	set NOCOUNT ON
	exec SprocTrackingUpdate 'GetDetailTextSearchResult_3'

	if isnull(@StatusType, -1) < 0 set @StatusType = 0
	declare @matchtype int = 1,
			@searchtype varchar(26)

	if	(@SearchString is null or @SearchString = '') and (@DescriptionString is null or @DescriptionString = '') and (@TitleString is null or @TitleString = '') and (@KeywordString is null or @KeywordString = '')
		and
		@VideoID is not null
	begin
		set @matchtype = 9
		set @MinVideoID = @VideoID
		set	@MaxVideoID = @VideoID
	end
	set @searchtype =
			case
				when @SearchString Is not null then 'all'
				when @TitleString is not null then 'title'
				when @DescriptionString IS not null then 'description'
				when @KeywordString is not null then 'keywords'
				else 'all'
				end

	if (select @Count) > 100 set @Count = 100

		if @WhereCondition like '%partnerid%' and isnull(@OrgId, 0) = 0
		begin
			set @OrgId = (select convert(int, right(@wherecondition, charindex('=',reverse(@WhereCondition))-1)))
		end

	set @SearchString = coalesce(@SearchString, @DescriptionString, @TitleString, @KeywordString)
	-- select @SearchType, @SearchString, @TitleString, @DescriptionString, @KeywordString, @Count, @StartDTM, @EndDTM, @MinVideoID, @MaxVideoID
	-- insert into sprocparamtracker(sproc, param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12, param13, param14, param15, param16, param17, param18)
	-- select	'GetDetailTextSearchResult_3',left(@SearchString, 50),left(@DescriptionString, 50),left(@TitleString, 50),LEFT(@KeywordString, 50),@WidgetId,@UserId,@OrgId,@Count,@OrderClause,@StatusType,@IsAdmin,@WhereCondition,@StartDTM,@EndDTM,@devicetype,@VideoID,@MinVideoID,@MaxVideoID


	create table #tmpsrch(VideoID bigint, UserID int, UserType int, ClipURL varchar(200), Title varchar(500), Description varchar(5000), EventDate datetime, NumberofReviews int, Height int, Width int, Duration decimal(8,2), ClipPopularity int, IsAdvertisement bit, CreatedOn datetime, ThumbnailURL varchar(200), PublishDate datetime, Keywords varchar(500), NumberOfPlays int, OriginalName varchar(200), ExpiryDate datetime, IsEnabled bit, IsDeleted bit, OnlyOwnerView bit, UserName varchar(200), OrganizationID int, Rank int, OrganizationName  varchar(200), IsMediaSource bit, IsAdmin bit)

	insert into #tmpsrch(VideoID, UserID, UserType, ClipURL, Title, Description, EventDate, NumberofReviews, Height, Width, Duration, ClipPopularity, IsAdvertisement, CreatedOn, ThumbnailURL, PublishDate, Keywords, NumberOfPlays, OriginalName, ExpiryDate, IsEnabled, IsDeleted, OnlyOwnerView, UserName, OrganizationID, Rank, OrganizationName, IsMediaSource,IsAdmin)
	exec GetSearchResults @searchString, @widgetid, @userid, @orgid, @count, @orderclause, @statustype, @isadmin, @wherecondition, @startdtm, @enddtm,@minvideoid, @maxvideoid,
	@matchtype, @searchtype, 0, 1
---------------------------------------------------------------------
	-- added 20130520
	if @matchtype <> 9 and (select COUNT(*) from #tmpsrch (nolock)) = 0
	begin
		if @matchtype = 1
			set @matchtype = 0
		insert into #tmpsrch(VideoID, UserID, UserType, ClipURL, Title, Description, EventDate, NumberofReviews, Height, Width, Duration, ClipPopularity, IsAdvertisement, CreatedOn, ThumbnailURL, PublishDate, Keywords, NumberOfPlays, OriginalName, ExpiryDate, IsEnabled, IsDeleted, OnlyOwnerView, UserName, OrganizationID, Rank, OrganizationName, IsMediaSource,IsAdmin)
		exec GetSearchResults @searchString, @widgetid, @userid, @orgid, @count, @orderclause, @statustype, @isadmin, @wherecondition, @startdtm, @enddtm,@minvideoid, @maxvideoid,
		@matchtype, @searchtype, 0, 1
	end
	if @matchtype in (0,1) and (select COUNT(*) from #tmpsrch (nolock)) = 0
	begin
		set @matchtype = 2
		insert into #tmpsrch(VideoID, UserID, UserType, ClipURL, Title, Description, EventDate, NumberofReviews, Height, Width, Duration, ClipPopularity, IsAdvertisement, CreatedOn, ThumbnailURL, PublishDate, Keywords, NumberOfPlays, OriginalName, ExpiryDate, IsEnabled, IsDeleted, OnlyOwnerView, UserName, OrganizationID, Rank, OrganizationName, IsMediaSource,IsAdmin)
		exec GetSearchResults @searchString, @widgetid, @userid, @orgid, @count, @orderclause, @statustype, @isadmin, @wherecondition, @startdtm, @enddtm,@minvideoid, @maxvideoid,
		@matchtype, @searchtype, 0, 1
	end
-------------------------------------------------------------------

	select * from #tmpsrch t where VideoID = ISNULL(@videoid, t.videoid)
	order by case when (@OrderClause like '%Post%' OR @OrderClause like '%created%') and @OrderClause like '%desc%' then t.CreatedOn end desc,
			case when (@OrderClause like '%Post%' OR @OrderClause like '%created%') and @OrderClause not like '%desc%' then t.CreatedOn end,
			case when (@OrderClause like '%Event%' or @OrderClause like '%publish%') and @OrderClause like '%desc%' then t.EventDate end desc,
			case when (@OrderClause like '%Event%' or @OrderClause like '%publish%') and @OrderClause not like '%desc%' then t.EventDate end,
			case when @OrderClause like '%Update%' then t.CreatedOn end desc,
			CreatedOn desc

END
GO
