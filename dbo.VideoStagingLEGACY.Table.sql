USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideoStagingLEGACY](
	[ContentID] [int] NOT NULL,
	[Duration] [float] NULL,
	[ReadyForEncoding] [bit] NULL,
	[EncodingPercentComplete] [int] NULL,
	[EncodingStarted] [datetime] NULL,
	[EncodingCompleted] [date] NULL,
	[Status] [int] NULL,
	[FileSize] [int] NULL,
	[AudioTrackURL] [nvarchar](1000) NULL,
	[EncodedURL] [nvarchar](1000) NULL,
	[NexidiaStatus] [int] NOT NULL,
	[Mp3Started] [datetime] NULL,
	[Mp3Completed] [datetime] NULL,
	[NiaStatus] [int] NOT NULL,
	[CodecUsed] [nchar](100) NULL,
	[FileExtension] [nvarchar](5) NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_VideoStagingLEGACY] PRIMARY KEY CLUSTERED 
(
	[ContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[VideoStagingLEGACY] ADD  CONSTRAINT [DF_VideoStagingLEGACY_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
