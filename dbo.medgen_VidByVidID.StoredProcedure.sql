USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[medgen_VidByVidID]
(
	@VideoID bigint
)
as
BEGIN
	set fmtonly off
	set nocount on

	exec SprocTrackingUpdate 'medgen_VidByVidID'

	select *
	from Videos
	where VideoID = @VideoID
	set nocount off
END
GO
