CREATE PROCEDURE [dbo].[ci_EncodingSlow]
	 @SystemID int = 1000
as
BEGIN
	set fmtonly off
	SET NOCOUNT ON
	exec SprocTrackingUpdate 'ci_EncodingSlow'
	-- declare @systemid int = 1000
	declare @lag int = 35
	declare @now datetime = dateadd(n, -@lag, GETDATE()), @nowpenalty datetime = dateadd(n, -60, GETDATE())

	select distinct c.ContentID, a.AssetID, a.EncodingID, fca.filepath --, f.ContentImportSystemID
	-- declare @now datetime = dateadd(n, -5, GETDATE()) select distinct c.contentid, c.PartnerID, c.CreatedDate, c.EffectiveDate, c.Name -- COUNT(distinct c.contentid)
	from	Content (nolock) c
	join	Partner (nolock) p
	on		c.PartnerID = p.PartnerID
	join	Content_Asset (nolock) ca
	on		c.ContentID = ca.ContentID
	join	Asset (nolock) a
	on		ca.AssetID = a.AssetID
	join	feedcontentasset fca
	on		a.feedcontentassetid = fca.feedcontentassetid
	join	TargetPlatform (nolock) tp
	on		ca.TargetPlatformID = tp.TargetPlatformID
	left join	Feed (nolock) f
	on		c.FeedID = f.FeedId
	left join ContentImportSystem cis --- select top 1000 * from ContentImportSystem
	on		f.ContentImportSystemID = cis.ContentImportSystemID
	where	-- ca.ImportStatusID <> 6
			not exists (select 1 from Content_Asset cca where cca.ContentID = c.contentid and cca.ImportStatusID = 6)

	and		(@SystemID = 1000 or f.ContentImportSystemID = @SystemID)
	and		(isnull(cis.zencoder, 0) = 0 and left(a.encodingid,1) <> 'z' and left(a.encodingid,1) <> 'n')
	and		c.ContentImportStatus not in (5, 9) --<> 5 -- (5, 9)
	and		isnull(a.ImportStatusID, 0) = 4 -- not in (5, 9)-- <> 5 --(5, 9)
	and		c.EffectiveDate <= GETDATE()
	and		c.ContentImportStatus <> 11  -- select * from  newsincrpt.dbo.importstatus
	and		((c.CreatedDate <= @now and not (f.ContentImportSystemID = 3 and f.FeedPriorityID = 2))
			or
			(c.CreatedDate <= @nowpenalty and f.ContentImportSystemID = 3 and f.FeedPriorityID = 2))
	and (a.RetryStart is null or a.RetryStart < DATEADD(n, -@lag, getdate()))

	set nocount off
END
