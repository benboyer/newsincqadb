TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Playlist_Content_PartnerMap	Playlist_Content_PartnerMapID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	Playlist_Content_PartnerMap	PartnerID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	Playlist_Content_PartnerMap	FeedID	4	int	10	4	0	10	1	None	None	4	None	None	3	YES	38
NewsincQA	dbo	Playlist_Content_PartnerMap	PlaylistID	4	int	10	4	0	10	1	None	None	4	None	None	4	YES	38
NewsincQA	dbo	Playlist_Content_PartnerMap	SourceCategory	12	varchar	50	50	None	None	1	None	None	12	None	50	5	YES	39
NewsincQA	dbo	Playlist_Content_PartnerMap	DestinationCategory	12	varchar	50	50	None	None	1	None	None	12	None	50	6	YES	39
NewsincQA	dbo	Playlist_Content_PartnerMap	NDNCategoryID	4	int	10	4	0	10	1	None	None	4	None	None	7	YES	38
NewsincQA	dbo	Playlist_Content_PartnerMap	isDefault	-7	bit	1	1	None	None	0	None	((0))	-7	None	None	8	NO	50
NewsincQA	dbo	Playlist_Content_PartnerMap	MaxVideoCount	4	int	10	4	0	10	1	None	((25))	4	None	None	9	YES	38
NewsincQA	dbo	Playlist_Content_PartnerMap	LastContentID	-5	bigint	19	8	0	10	1	None	None	-5	None	None	10	YES	108

index_name	index_description	index_keys
IX_C_PL_PartnerMap	nonclustered located on PRIMARY	PartnerID, SourceCategory
PK_Playlist_Content_PartnerMap_1	clustered, unique, primary key located on PRIMARY	Playlist_Content_PartnerMapID
