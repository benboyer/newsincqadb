CREATE view UserTypeToPartnerType
as 
SELECT dbo.[User].UserID, MAX(dbo.Partner_PartnerType.PartnerTypeID) AS PartnerTypeID
FROM         dbo.Partner_PartnerType INNER JOIN
                      dbo.[User] ON dbo.Partner_PartnerType.PartnerID = dbo.[User].PartnerID
GROUP BY dbo.[User].UserID