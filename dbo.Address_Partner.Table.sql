USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address_Partner](
	[PartnerID] [int] NOT NULL,
	[AddressID] [int] NOT NULL,
 CONSTRAINT [PK_AddressPartner] PRIMARY KEY CLUSTERED 
(
	[PartnerID] ASC,
	[AddressID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Address_Partner]  WITH CHECK ADD  CONSTRAINT [FK_AddressPartner_Address] FOREIGN KEY([AddressID])
REFERENCES [dbo].[Address] ([AddressID])
GO
ALTER TABLE [dbo].[Address_Partner] CHECK CONSTRAINT [FK_AddressPartner_Address]
GO
ALTER TABLE [dbo].[Address_Partner]  WITH CHECK ADD  CONSTRAINT [FK_AddressPartner_Partner] FOREIGN KEY([PartnerID])
REFERENCES [dbo].[Partner] ([PartnerID])
GO
ALTER TABLE [dbo].[Address_Partner] CHECK CONSTRAINT [FK_AddressPartner_Partner]
GO
