TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	PlaylistsLEGACY	PlaylistID	4	int	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	PlaylistsLEGACY	ContentItemID	4	int	10	4	0	10	1	None	None	4	None	None	2	YES	38
NewsincQA	dbo	PlaylistsLEGACY	SharingOption	4	int	10	4	0	10	1	None	None	4	None	None	3	YES	38
NewsincQA	dbo	PlaylistsLEGACY	SortOrder	4	int	10	4	0	10	0	None	None	4	None	None	4	NO	56
NewsincQA	dbo	PlaylistsLEGACY	AverageRate	6	float	15	8	None	10	0	None	None	6	None	None	5	NO	62
NewsincQA	dbo	PlaylistsLEGACY	IsDefault	-7	bit	1	1	None	None	0	None	None	-7	None	None	6	NO	50
NewsincQA	dbo	PlaylistsLEGACY	RSSFilename	-9	nvarchar	255	510	None	None	1	None	None	-9	None	510	7	YES	39
NewsincQA	dbo	PlaylistsLEGACY	RSSEnabled	-7	bit	1	1	None	None	1	None	None	-7	None	None	8	YES	50

index_name	index_description	index_keys
PK_PlaylistsLEGACY	clustered, unique, primary key located on PRIMARY	PlaylistID
