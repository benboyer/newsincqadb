CREATE PROCEDURE [dbo].[NDNAddresses_ADD_sp]
	@Address1 nvarchar(100) = null,
	@Address2 nvarchar(100) = null,
	@City nvarchar(100) = null,
	@Zip nvarchar(10) = null,
	@CountryID int,
	@State nvarchar(2) = null,
	@Phone nvarchar(50) = null

AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'NDNAddresses_ADD_sp'

    -- Insert statements for procedure here
    Insert into Address (CountryID, Address1, Address2, City, Zip, AddressTypeID, IsVerified, IsPrimary, Deleted, CreatedUserID)
    Values(@CountryID, @Address1, @Address2, @City, @Zip, 1, 1, 1, 0, -1)

    Declare @NewID int

    Select @NewID = SCOPE_IDENTITY();

    Insert into NDNAddressesLegacy(AddressID, Phone, [State])
    Values (@NewID, @Phone, @State)

    Select @NewID as ID

END
