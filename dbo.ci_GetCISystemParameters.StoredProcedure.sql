USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ci_GetCISystemParameters]
	@ContentImportSystemID int
as
BEGIN
	set		fmtonly off
	set		nocount on
	exec	SprocTrackingUpdate 'ci_GetCISystemParameters'
	select	IPAddress, DatabaseName, Description
	from	contentimportsystem
	where	ContentImportSystemID = @ContentImportSystemID
	set		nocount off
END
GO
