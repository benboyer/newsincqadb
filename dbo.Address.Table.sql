USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address](
	[AddressID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[AddressTypeID] [tinyint] NOT NULL,
	[CountryID] [int] NOT NULL,
	[Address1] [nvarchar](100) NULL,
	[Address2] [nvarchar](100) NULL,
	[City] [nvarchar](100) NULL,
	[StateID] [smallint] NULL,
	[ProvinceCounty] [nvarchar](100) NULL,
	[Zip] [nvarchar](10) NULL,
	[PlusFour] [nvarchar](10) NULL,
	[IsVerified] [bit] NOT NULL,
	[IsPrimary] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[CreatedDate] [smalldatetime] NULL,
	[CreatedUserID] [int] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUserID] [int] NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[AddressID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_Country] FOREIGN KEY([CountryID])
REFERENCES [dbo].[Country] ([CountryID])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Address_Country]
GO
ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_State] FOREIGN KEY([StateID])
REFERENCES [dbo].[State] ([StateID])
GO
ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Address_State]
GO
ALTER TABLE [dbo].[Address] ADD  CONSTRAINT [DF__Address__Created__3D89085B]  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
