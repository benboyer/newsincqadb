USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[cr_GetFeaturedPlaylists]
	-- declare
	@userid int,
	@playlistid int = null
as
BEGIN
	set FMTONLY OFF
	set	nocount on

	exec SprocTrackingUpdate 'cr_GetFeaturedPlaylists'
	-- declare 	@userid int = 7534, @playlistid int = null

	declare @TG int = (select trackinggroup from mTrackingGroup (nolock) where partnerid = (select partnerid from [User](nolock) where UserID = @userid))
	select  top 100 Name, p.PlaylistId, p.IsSponsored
	from	Playlist(nolock) p
	where	p.PlaylistID is not null
	and		p.IsFeatured = 1
	and		PlaylistTypeID = 1
	and		Active = 1
	--and     p.shared = 2
	and		(ExpirationDate is null or ExpirationDate > GETDATE())
	and		ContentGroupID is not null
	and		(exists (select 1
					from	Playlist_Content(nolock) pc
					join	Content c on pc.ContentID = c.ContentID
					left join  (select * from vw_partnerdefaultsettings where entity = 'BlockContentFromControlRoom' and value = 'true') bcr
					on	c.partnerid = bcr.partnerid
					where	bcr.PartnerID is null
					and		pc.PlaylistID = p.PlaylistID and dbo.AllowedContentID(pc.contentid, @TG, 1) = 1
			)
			or
			(select isadmin from NDNUsersLEGACY where UserID = @userid) = 1)

	order by Name
	set	nocount on
END
GO
