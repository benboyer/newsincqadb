TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Keyword	KeywordID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	Keyword	Name	-9	nvarchar	100	200	None	None	0	None	None	-9	None	200	2	NO	39
NewsincQA	dbo	Keyword	KeywordTypeID	-6	tinyint	3	1	0	10	0	None	None	-6	None	None	3	NO	48
NewsincQA	dbo	Keyword	IsActive	-7	bit	1	1	None	None	0	None	((1))	-7	None	None	4	NO	50
NewsincQA	dbo	Keyword	IncludeKeywords	-9	nvarchar	1000	2000	None	None	1	None	None	-9	None	2000	5	YES	39
NewsincQA	dbo	Keyword	ExcludeKeywords	-9	nvarchar	1000	2000	None	None	1	None	None	-9	None	2000	6	YES	39
NewsincQA	dbo	Keyword	ExpirationUTCDate	11	smalldatetime	16	16	0	None	1	None	None	9	3	None	7	YES	111
NewsincQA	dbo	Keyword	CreatedUTCDate	11	smalldatetime	16	16	0	None	0	None	(getutcdate())	9	3	None	8	NO	58

index_name	index_description	index_keys
IE_Keyword_ExpirationUTCDate	nonclustered located on PRIMARY	ExpirationUTCDate
PK_Keyword	clustered, unique, primary key located on PRIMARY	KeywordID
