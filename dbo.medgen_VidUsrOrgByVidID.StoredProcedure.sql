USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[medgen_VidUsrOrgByVidID]
	--declare
	@ContentID bigint
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'medgen_VidUsrOrgByVidID'
	select *
	from (select * from Videos (nolock) where VideoID = @ContentID) V
	join NDNUsers nu (nolock)
	on	v.UserID = nu.UserID
	join Organizations o (nolock)
	on	nu.OrganizationID = o.OrganizationId
	where v.VideoID = @ContentID
	set nocount off

END
GO
