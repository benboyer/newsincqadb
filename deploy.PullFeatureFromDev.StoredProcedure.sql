USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [deploy].[PullFeatureFromDev]
	-- declare
	@type varchar(20),		-- must be 'procedure', 'function', 'view', or 'table'
	@name varchar(60),		-- entity name
	@schema varchar(20),	-- schema in which the entity is to be created/updated
	-- @installation_code varchar(max) = null,	-- updated from database definition, not to be entered manually (use direct sql if manual update needed)
	@dependsOn int = null,	-- previously added feature (entity name) upon which this feature depends
	@definer varchar(60) = null,
	@tickets varchar(120) = null

AS
	--select @type = 'procedure', @name = 'adm_RetirePartner', @schema = 'dbo', @definer = 'bboyer'
BEGIN
	insert into deploy.schema_feature(feature_type, feature_name, for_schema, depends_on, tickets_applicable)
	select	@type, @name, @schema, @DependsOn, @tickets
	where not exists (select 1 from deploy.schema_feature 
					where feature_type = @type 
					and feature_name = @name 
					and for_schema = @schema 
					and completed_on is null)

	update	sf set sf.Installation_code = m.definition, defined_by = isnull(@definer, SUSER_NAME()), depends_on = @DependsOn, tickets_applicable = ISNULL(@tickets, sf.tickets_applicable) -- select *
	-- select *
	from	NewsincDEV.sys.schemas s 
	join	NewsincDEV.sys.objects p on s.schema_id = p.schema_id 
	join	NewsincDEV.sys.sql_modules m on p.object_id = m.object_id 
	join	deploy.schema_feature sf on s.name = sf.for_schema and p.name = sf.feature_name
	join	(select MAX(feature_order) feature_order from deploy.schema_feature where feature_type = @type and feature_name = @name) ref on sf.feature_order = ref.feature_order
	where	sf.feature_type = @type
	and		sf.feature_name = @name
	and		(sf.installation_code is null or sf.installation_code <> m.definition)
END
GO
