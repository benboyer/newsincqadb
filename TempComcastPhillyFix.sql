CREATE VIEW dbo.TempComcastPhillyFix
AS
SELECT     ContentID, Name,
                          (SELECT     COUNT(Name) AS Expr1
                            FROM          dbo.[Content] AS Content2
                            WHERE      (Name = dbo.[Content].Name) AND (PartnerID = 692)) AS CountOfName
FROM         dbo.[Content]
WHERE     (PartnerID = 693)
