TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	PartnerDefaultSetting	PartnerDefaultSettingID	-5	bigint identity	19	8	0	10	0	None	None	-5	None	None	1	NO	63
NewsincQA	dbo	PartnerDefaultSetting	PartnerID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	PartnerDefaultSetting	DefaultsettingTypeID	4	int	10	4	0	10	1	None	None	4	None	None	3	YES	38
NewsincQA	dbo	PartnerDefaultSetting	SettingID	4	int	10	4	0	10	1	None	None	4	None	None	4	YES	38
NewsincQA	dbo	PartnerDefaultSetting	Value	12	varchar	120	120	None	None	1	None	None	12	None	120	5	YES	39
NewsincQA	dbo	PartnerDefaultSetting	Comment	12	varchar	120	120	None	None	1	None	None	12	None	120	6	YES	39

index_name	index_description	index_keys
NDX_PDS_IDs_val	nonclustered located on PRIMARY	PartnerID, DefaultsettingTypeID, SettingID, Value
PK_PartnerDefaultSetting	clustered, unique, primary key located on PRIMARY	PartnerDefaultSettingID
