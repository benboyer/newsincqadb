USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Feed_NDNCategory]
AS
SELECT     c.PartnerID, c.ContentID, pcpm.FeedID, nc.NDNCategoryID, nc.NDNCategory
FROM         dbo.[Content] AS c INNER JOIN
                      dbo.Playlist_Content_PartnerMap AS pcpm ON c.PartnerID = pcpm.PartnerID INNER JOIN
                      dbo.NDNCategory AS nc ON pcpm.NDNCategoryID = nc.NDNCategoryID
WHERE     (pcpm.FeedID IS NOT NULL)
GO
