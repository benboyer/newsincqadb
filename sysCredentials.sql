TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	sysCredentials	sysCredentialsID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	sysCredentials	Name	12	varchar	40	40	None	None	1	None	None	12	None	40	2	YES	39
NewsincQA	dbo	sysCredentials	Description	12	varchar	120	120	None	None	1	None	None	12	None	120	3	YES	39
NewsincQA	dbo	sysCredentials	Value	12	varchar	128	128	None	None	1	None	None	12	None	128	4	YES	39

