CREATE PROCEDURE [dbo].[BlockedSites_listInternal]
	@includeInactive bit = 0,
	@domain varchar(1000) = null
AS
BEGIN
	set nocount on
	set fmtonly off
	exec SprocTrackingUpdate 'BlockedSites_List'

	SELECT	BlockedSitesID, Domain, Active, isnull(DateUpdatedGMT, DateCreatedGMT) LastUpdatedDate, Comment
	  FROM	BlockedSites
	  WHERE ([Active] = 1 or @includeInactive = 1 or Domain = @domain)
	  and	 Domain = ISNULL(@domain, Domain)
	set nocount off
END

