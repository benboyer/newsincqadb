USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Launcher_ContentExclusion_Delete]
	@LaucherID int
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'Launcher_ContentExclusion_Delete'

	delete from Launcher_ContentExclusion
	where	launcherid = @LaucherID

	set nocount off
END
GO
