USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[adm_SetInboundFeedStatus]
	@Feedid int,
	@Status bit
AS
BEGIN
		set fmtonly off
		set nocount on
		exec SprocTrackingUpdate 'adm_SetInboundFeedStatus'
		if exists (select 1 from Feed where FeedId = @Feedid)
		begin
			update	Feed 
			set		Active = @Status
			where	FeedId = @Feedid
			
			select  p.PartnerID, p.Name, f.FeedId, cs.Name  FeedSource, f.ContentImportSystemID Box, f.FeedPriorityID Priority, f.Active
			from	Feed f
			join	Partner p
			on		f.PartnerID = p.PartnerID
			join	ContentSource cs
			on		f.sourceid = cs.ContentSourceID
			where	f.FeedId = @Feedid
			order by p.Name
		end
		else
		begin
			select 'PartnerID ' + convert(varchar(10), @Feedid) + ' not found in Feed Table.  Please Check it.' as MessageFromServier
		end
		set nocount off
END
GO
