USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ps_PlaylistUpdated]    
-- declare     
 @playlistid int     
AS    
BEGIN    
 set fmtonly off    
 set nocount on    
 exec SprocTrackingUpdate 'ps_PlaylistUpdated'    
 -- select PlaylistID, convert(smalldatetime, ContentUpdatedDate) ContentUpdatedDate    
 select PlaylistID, ContentUpdatedDate     
 from Playlist (nolock)    
 where PlaylistID = @playlistid    
 set nocount off    
END
GO
