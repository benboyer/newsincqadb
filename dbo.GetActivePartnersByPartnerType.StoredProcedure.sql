USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetActivePartnersByPartnerType]
	@partnertypeid int = null,
	@partnertype varchar(max) = null
as
BEGIN
	set fmtonly off
	SET NOCOUNT ON
	exec SprocTrackingUpdate 'GetActivePartnersByPartnerType'

	if @partnertype is null
	and	@partnertypeid is null
	begin
		select 'You must provide one parameter' as MessageBack
		return
	end
	if @partnertypeid is null
		set @partnertypeid = (select partnertypeid from PartnerType where Name like '%' + @partnertype + '%' or Description like '%' + @partnertype + '%')
	select p.PartnerID, p.Name
	from	partner p
	join	partner_partnerType ppt
	on		p.PartnerID = ppt.PartnerID
	join	UserPartnerStatus ups
	on		p.StatusID = ups.StatusID
	where	p.StatusID = 2
	and		ppt.PartnerTypeID = @partnertypeid

	SET NOCOUNT OFF
END
GO
