USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[setKeywordbyContentID] @contentID int, @newkeyword  varchar(1000) as

declare @keyword varchar(1000)
set @keyword = ( select ISNULL(LTRIM(RTRIM(Keyword)),'') from Content where ContentID = @contentid )

if ( len(@keyword) = 0 )
	update Content set Keyword = @newkeyword where ContentID = @contentid
else if ( SUBSTRING(@keyword, len(@keyword)-1, 1) = ',' )
	update Content set Keyword = Keyword + @newkeyword where ContentID = @contentid
else
	update Content set Keyword = Keyword + ',' + @newkeyword where ContentID = @contentid
GO
