CREATE FUNCTION [dbo].[GetFeedAssetFmt]
(
	@String varchar(1500)
)
RETURNS varchar(20)
AS

BEGIN
	DECLARE @ResultVar varchar(20)
	DECLARE @wid varchar(1500)
	if CHARINDEX('/fmt=', @string) > 0
      set   @wid = RIGHT(@string, len(@string) - charindex('/fmt=', @string) - 4)
	if @wid is not null and charindex('/', @wid) > 0
      set @wid = LEFT(@wid, charindex('/', @wid) -1)
	set @wid = replace(replace(@wid, '%', ''), 'undefined', '0')

	--	if @wid is null and CHARINDEX('pid=', @string) > 0
	--      set   @wid = RIGHT(@string, len(@string) - charindex('pid=', @string) - 3)
	--	if @wid is not null and charindex('&', @wid) > 0
	--      set @wid = LEFT(@wid, charindex('&', @wid) -1)
	--	set @wid = replace(replace(@wid, '%', ''), 'undefined', '0')

	--	if @wid = 'wid' or @wid = 'pid' set @wid = null
	--	if ISNUMERIC(replace(@wid, '.', '')) = 1
	set @ResultVar = replace(@wid, '.', '')
	RETURN @ResultVar

END
