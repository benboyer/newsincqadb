CREATE PROCEDURE [dbo].[GetMostRecentVideos]            
( -- declare            
 @WidgetId INT = NULL,            
 @UserId INT = 0,            
 @OrgId INT = 0,            
 @Count INT = 400,            
 @OrderClause NVARCHAR(MAX) = '',            
 @StatusType INT = 0,            
 @SearchType NVARCHAR(20) = '',            
 @SearchText NVARCHAR(MAX) ='',            
 @IsAdmin BIT = 0,            
 @WhereCondition NVARCHAR(MAX) = '',            
 @MinVideoID BIGINT = 0, -- max value of the current external record set: used for pagination            
 @MaxVideoID BIGINT = 0 -- min value of the current external record set: used for pagination            
)            
            
AS            
 -- Updated 2013-09-20 - added "and @OrganizationId is null" @ line 81, to correct content from blocked providers being returned.        
 --select @userid = 10099, @count = 400, @orgid = 1635, @statustype = 0, @statustype = 0, @searchtype = 'filename', @searchtext='turk', @Wherecondition= 'c.partnerid = 448', @orderclause = 'eventdate desc'            
 --select @userid = 10099, @count = 400, @orgid = 1635, @statustype = 2, @searchtype = null, @searchtext=null, @Wherecondition=null, @orderclause = null            
 --select @userid = 8125, @count = 400, @orgid = 1635, @statustype = 2, @searchtype = null, @searchtext=null, @Wherecondition=null, @orderclause = null            
 -- select @userid = 8125, @count = 400, @orgid = 0, @statustype = 1, @searchtype = null, @searchtext=null, @Wherecondition=null, @orderclause = null            
 -- select @userid = 10099, @count = 400, @orgid = -1, @statustype = 2, @searchtype = null, @searchtext=null, @Wherecondition=null, @orderclause = null            
BEGIN            
 set fmtonly off            
 set nocount on            
 exec SprocTrackingUpdate 'GetMostRecentVideos'            
            
 -- insert into sprocparamtracker(sproc, param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, param11, param12)            
 -- select 'GetMostRecentVideos',@WidgetId,@UserId,@OrgId,@Count,@OrderClause,@StatusType,@SearchType,@SearchText,@IsAdmin,@WhereCondition,@MinVideoID,@MaxVideoID            
            
 -- select @userid = 7534, @orgid = -1, @statustype = 0, @searchtext = null, @searchtype = null            
            
 DECLARE @queryString NVARCHAR(MAX)            
 DECLARE @WhereClause NVARCHAR(MAX)            
            
 if @Count > 400 set @Count = 400            
            
 SET @WidgetId =  ISNULL(@WidgetId,0)            
 SET @UserId =  ISNULL(@UserId,0)            
 SET @OrgId =  ISNULL(@OrgId,0)            
 SET @Count =  ISNULL(@Count,100)            
 SET @OrderClause = case when @OrderClause IS NULL or @OrderClause like '%created%' or @OrderClause like '%post%' then ' c.Createddate DESC, c.contentid DESC'            
      when @OrderClause like '%event%' OR @OrderClause like '%Publish%' then ' c.EffectiveDate DESC'            
      when @OrderClause like '%update%' then ' c.UpdatedDate DESC'            
      else ' c.CreatedDate DESC, c.contentid DESC'            
      end --  replace(replace(replace(ISNULL(@OrderClause,''), 'EventDate', 'c.EffectiveDate'), 'CreatedOn', 'c.CreatedDate'), 'PublishDate', 'c.EffectiveDate')            
 SET @WhereClause =  ISNULL(@WhereClause,'')            
 set @StatusType = ISNULL(@statustype, 0)            
            
 if @OrgId = -1 set @OrgId = 0            
 if @WhereCondition like '%partnerid%' and isnull(@OrgId, 0) = 0            
 begin            
  set @OrgId = (select convert(int, right(@wherecondition, charindex('=',reverse(@WhereCondition))-1)))            
 end            
            
            
 DECLARE @OrganizationId INT,            
   @userTG varchar(20),            
   @isUserAdmin bit            
            
 SELECT @OrganizationId = partnerid from [User] where UserID = @UserId --  OrganizationId FROM NDNUsers WHERE UserID = @UserId            
 select @userTG = convert(varchar(20), TrackingGroup) from Partner where PartnerID = @OrganizationId            
 SELECT @isUserAdmin = IsAdmin FROM NDNUsersLEGACY WHERE UserID = @UserId        
         
 -- select @OrganizationId, @userTG        
            
 if @StatusType > 0 and @isUserAdmin = 0 and @OrgId < 1            
 begin            
  set @OrgId = @OrganizationId            
 end            
            
 if @isUserAdmin = 0 and @IsAdmin = 1            
  set @isUserAdmin = 1            
        
 if @isUserAdmin = 0 and @orgid = 0 and @OrganizationId =0        
 begin        
   set @userTG = 10557        
   set @OrganizationId = 478        
 end            
         
 SET @queryString=            
 'SELECT TOP  '+ CONVERT(VARCHAR(20),@Count)+'  c.contentid [VideoID]            
      ,c.CreatedUserID [UserID]            
      ,CAST(ul.UserTypeID as INT) [UserType]            
            
 ,case when a.FilePath is not null then a.FilePath + ' + '''' + '/' + + '''' + ' + a.Filename            
   when a.FilePath is null and a.Filename is not null then a.Filename            
   else ' + '''' + '''' + '            
   end [ClipURL]            
            
      ,c.name [Title]            
      ,c.[Description]            
      ,c.EffectiveDate [EventDate]            
      ,0 [NumberofReviews]            
      ,CAST(isnull(a.[Height], 0) as DECIMAL) [Height]            
      ,CAST(isnull(a.[Width], 0) as DECIMAL) [Width]            
      ,CAST(isnull(a.[Duration], 0) as DECIMAL) [Duration]            
      ,CAST(0 as FLOAT) [ClipPopularity]            
      ,CAST(0 as BIT) [IsAdvertisement]            
      ,c.createddate [CreatedOn]            
   ,case when thm.FilePath is not null then thm.FilePath + ' + '''' + '/' + + '''' + ' + thm.Filename            
   when thm.FilePath is null and thm.Filename is not null then thm.Filename            
   else ' + '''' + '''' + '            
   end as ThumbnailURL            
      ,c.EffectiveDate [PublishDate]            
      ,c.keyword [Keywords]            
      ,0 [NumberOfPlays]            
   ,' + '''' + 'http://Rawvideo.newsinc.com/' + '''' + ' + convert(varchar(20), c.contentid) + ' + '''' + '.flv' + '''' + ' [OriginalName]            
   ,c.ExpirationDate [ExpiryDate]            
   ,c.active [IsEnabled]            
   ,c.[IsDeleted]            
   ,c.OnlyOwnerView            
   ,u.FirstName + '' '' + u.LastName AS UserName            
   ,c.partnerid OrganizationID            
   ,org.Name AS OrganizationName            
   ,isnull(ol.IsMediaSource, 0) IsMediaSource            
   ,org.logourl AS LogoURL            
   ,a.EncodingErrorMessage [IngestionStatusMessage]            
   ,ul.isadmin            
 FROM content (nolock) c            
  join (select ca.Contentid, a.*            
   from content_asset (nolock) ca            
   join Asset (nolock) a            
   on ca.AssetID = a.AssetID            
   where ca.AssetTypeID = 1 and a.active = 1 and ca.active = 1) a            
 on c.ContentID = a.ContentID            
  left join (select ca.ContentID, a.*            
   from Content_Asset ca            
   join Asset a            
   on  ca.AssetID = a.AssetID            
   where ca.AssetTypeID = 3 and a.active = 1 and ca.active = 1) thm            
 on c.ContentID = thm.ContentID            
            
 JOIN Partner (nolock) AS org ON c.partnerid = org.partnerid            
 join NDNUsersLEGACY (nolock) ul on c.CreatedUserID = ul.UserID            
 join [User] (nolock) u on c.CreatedUserID = u.UserID            
 join OrganizationsLEGACY (nolock) ol on c.PartnerID = ol.PartnerID            
 left join dbo.AllowedContentByPartnerID('+CONVERT(varchar,@OrganizationId)+') ac        
 on ac.contentid = c.contentid        
 left join (select top 1 * from vw_partnerdefaultsettings where entity = ' + '''' + 'BlockContentFromControlRoom' + '''' + ' and value = ' + '''' + 'true' + '''' + ') bcr            
 on c.partnerid = bcr.partnerid            
 left join (select top 1 pl.PartnerID, l.logourl from Partner_Logo (nolock) pl  join Logo (nolock) l on pl.LogoID = l.LogoID where l.LogoTypeID = 5 and active = 1) pl       
 on c.partnerid = pl.partnerid '         
 -- +CHAR(13)+        
 -- case  when @isUserAdmin = 0 and @orgid = 0 then        
 -- 'inner join dbo.AllowedContentByPartnerID('+CONVERT(varchar,@OrganizationId)+') ac        
 -- on ac.contentid = c.contentid '+CHAR(13)        
 -- else ' '+CHAR(13) end +        
 + 'WHERE  c.contentimportstatus = 5            
 and  bcr.partnerid is null            
 '        
--  + case  when isnull(@isUserAdmin, 0) = 0  then ' and ac.contentid is not null '        
  --   when @isUserAdmin = 1 /*and @orgid = 0*/ then ''            
    -- end        
 + case   when @orgid > 0 then ' and ' + convert(varchar(10), @OrgId) + ' = c.partnerid '          
 else ''        
 end        
--     when @OrgId IS not null then ' and ' + convert(varchar(10), @OrgId) + ' = c.partnerid '            
--     when @OrgId > 0 and @isUserAdmin = 0 then ' and ' + convert(varchar(10), @OrgId) + ' = c.partnerid '            
--     end            
-- + 'and  (dbo.AllowedContentID(c.contentid, ' + convert(varchar(10), @usertg) + ', 1) = 1 or ' + convert(varchar(1), @isUserAdmin) + ' = 1 or ' + convert(varchar(10), @OrganizationId) + ' = c.partnerid)    '        
+ 'and  (ac.contentid is not null or ' + convert(varchar(1), @isUserAdmin) + ' = 1 or ' + convert(varchar(10), @OrganizationId) + ' = c.partnerid)    '        
        
  +  case            
  when @widgetid = 0 then ''            
  else ' and (dbo.AllowedLauncherContent(' + convert(varchar(20), @widgetid) + ', c.partnerid) = 1 or ' + convert(varchar(20), @widgetid) + ' = ' + '''' + '''' + ') '            
  end            
 IF(@StatusType <> -1  )            
 BEGIN            
  SELECT @WhereClause = ' AND  '            
  SELECT @WhereClause = @WhereClause + (CASE CONVERT(NVARCHAR(1),@StatusType)            
         WHEN '0' THEN ' ( c.active = 1 ) '            
   WHEN '1' THEN ' ( c.active = 0 AND c.IsDeleted = 0) '            
         WHEN '2' THEN '  ( c.IsDeleted = 1) '            
         END)            
 END            
            
 IF(@SearchType IS NOT NULL AND @SearchType != '' AND @SearchType != '7' AND  @SearchType != 'None' AND @SearchText IS NOT NULL AND @SearchText != '' )            
  SELECT @WhereClause = @WhereClause + (CASE -- CONVERT(NVARCHAR(20),@SearchType)            
         WHEN CONVERT(NVARCHAR(20),@SearchType) = 'Title' THEN ' AND c.name like ''%'+ @SearchText + '%'''            
         WHEN CONVERT(NVARCHAR(20),@SearchType) = 'FileName' THEN ' AND a.filename like ''%'+ @SearchText + '%'''            
         WHEN CONVERT(NVARCHAR(20),@SearchType) = 'DateAdded' THEN ' AND DateDiff(dd, c.createddate, '''+ @SearchText + ''') =  0 '            
         WHEN CONVERT(NVARCHAR(20),@SearchType) = 'EventDate' THEN ' AND DateDiff(dd, c.effectivedate, '''+ @SearchText + ''') =  0 '            
         WHEN CONVERT(NVARCHAR(20),@SearchType) = 'ExpirationDate' THEN ' AND DateDiff(dd, c.Expirationdate, '''+ @SearchText + ''') =  0 '            
         WHEN CONVERT(NVARCHAR(20),@SearchType) ='Keywords' THEN ' AND c.keyword like ''%'+ @SearchText + '%'''            
         WHEN CONVERT(NVARCHAR(20),@SearchType) ='all' THEN '  AND (c.name like ''%'+ @SearchText + '%''' + ' or c.description like ''%'+ @SearchText + '%''' + ' or c.keyword like ''%'+ @SearchText + '%'''+')'            
         END)            
            
 IF (@MinVideoID > 0 and @MaxVideoID is null)            
  SELECT @WhereClause = @WhereClause + ' AND c.contentid < ' + CONVERT(NVARCHAR(20),@MinVideoID)  ;            
            
 IF (@MaxVideoID > 0 and @MinVideoID > 0)            
  SELECT @WhereClause = @WhereClause + ' AND c.contentid >= ' + CONVERT(NVARCHAR(20),@MinVideoID)   + ' AND c.contentid <= ' + CONVERT(NVARCHAR(20),@MaxVideoID)  ;;            
            
            
 IF (@WhereCondition IS NOT NULL AND @WhereCondition != '')            
  SELECT @WhereClause = @WhereClause + ' AND '+ @WhereCondition ;            
            
 SET @queryString = @queryString + ISNULL(@WhereClause,'')            
           
-- IF(@WhereClause IS NOT NULL AND @OrderClause IS NOT NULL AND @OrderClause != '')            
 IF(@OrderClause IS NOT NULL AND @OrderClause != '')            
  SET @queryString = @queryString  + CHAR(13) + ' Order By ' + @OrderClause            
            
 -- print @queryString            
 exec ( @queryString );            
--      ,' + '''' + 'http://video.newsinc.com/' + '''' + ' + convert(varchar(20),c.contentid) + ' + '''' + '.flv' + '''' + ' [ClipURL]            
            
 set nocount off            
            
 END   
