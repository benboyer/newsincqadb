USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetContentAssetsByPlaylistID]
--  declare
	@widgetID int = null,
	@playlistID int = null,
	@trackingGroup int = null,
	@contentid bigint = null,
	@devicetype int = 1
AS
 BEGIN
 -- updated 9/17/2013 (switched to allowedlaunchercontentP)
		set FMTONLY OFF
		set nocount on
		exec SprocTrackingUpdate 'GetContentAssetsByPlaylistID'
	-- declare 	@widgetID int = 4, @playlistID int = 507, @trackingGroup int = 90121, @contentid bigint = 24952729, @devicetype int = 1
	-- declare 	@widgetID int = 4, @playlistID int = 507, @trackingGroup int = 90121, @contentid bigint = null, @devicetype int = 1

	-- declare 	@widgetID int = 1075, @playlistID int = 507, @trackingGroup int = 10557, @contentid bigint = 24952729, @devicetype int = 2
	-- declare 	@widgetID int = 1075, @playlistID int = 507, @trackingGroup int = 10557, @contentid bigint = null, @devicetype int = 2
	-- declare 	@widgetID int = 1075, @playlistID int = 9999999, @trackingGroup int = 10557, @contentid bigint = 24952729, @devicetype int = 2

	declare @PlaylistCap int = 50
		Declare	--@now datetime,
				@iwidgetID int,
				@iplaylistID int,
				@itrackinggroup int,
				@idevicetype int
		-- set		@now = GETDATE()
		select @iwidgetID = isnull(@widgetID, 1), @iplaylistID = isnull(@playlistID, (select value from vw_sysDefaultSettings (nolock) where entity = 'Playlist' and Setting = 'Number')), @itrackinggroup = isnull(@trackingGroup, 10557), @idevicetype = isnull(@devicetype, 1)

		if	@PlaylistID = 9999999
		begin
			exec GetSingleContentMetadata @contentid, @iTrackingGroup, @deviceType
			return
		end
		select	 a.PlaylistID, a.ContentID, a.Name, a.Description, a.TrackingGroup ContentPartnerID, vg.VideoGroupName, a.Duration,
				a.effectivedate PubDate, a.Keyword, convert(varchar(120), null) Timeline,
				case when at.Name like 'video%' then 'src'
					else at.Name  -- 'unk'
					end as AssetType,
					case when at.Name like 'External%' then a.Filename
					else
						a.DeliveryPath
					end as AssetLocation,
					a.ProducerName, a.ProducerNameAlt, a.logourl ProducerLogo, case when a.ContentID = @contentid then 0 else a.[Order]+1 end ContentOrder, isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'news') ProducerCategory,
					REPLACE(a.Name, ' ', '-') + + '-' +CONVERT(varchar(20), a.ContentID) as SEOFriendlyTitle
			from	(select	pc.PlaylistID, c.ContentID, c.Name, c.Description, p.TrackingGroup, c.effectivedate, c.Keyword, convert(varchar(120), null) Timeline,
						ca.AssetTypeID, a.duration, a.FilePath, a.filename, p.Name ProducerName, p.ShortName ProducerNameAlt, isnull(p.LogoURL, 'http://assets.newsinc.com/newsinconebyone.png') LogoURL, pc.[Order], a.MimeTypeID, p.PartnerID, p.isContentPrivate, c.FeedID,
						dbo.GetAssetDeliveryPath(mt.FileExtension, c.partnerid, ca.contentid, ca.assetid) DeliveryPath
				from	(select top (@PlaylistCap) * from playlist_content (nolock) where playlistid = @iplaylistid and dbo.AllowedContentID(contentid, @itrackinggroup, @devicetype) = 1 order by [ORDER] ASC) pc
				join	Content c (nolock)
				on		pc.ContentID = c.ContentID
				join	partner p (nolock)
				on		c.PartnerID = p.PartnerID
				join	Content_Asset ca (nolock)
				on		c.ContentID = ca.ContentID
				join	Asset a (nolock)
				on		ca.AssetID = a.AssetID
				join	MimeType (nolock) mt
				on		a.MimeTypeID = mt.MimeTypeID
				where	pc.PlaylistID = @iplaylistid
				and		ca.TargetPlatformID = @idevicetype
				and		dbo.AllowedLauncherContentP(@iwidgetID, c.PartnerID) = 1) a
		join	AssetType at (nolock)
		on		a.AssetTypeID = at.AssetTypeID
		join	mtrackinggroup pd  (nolock)
		on		@itrackingGroup = pd.TrackingGroup
		left join	VideoGroups VG (nolock)
		on		a.PlaylistID = vg.VideoGroupId
		left join	Playlist_Content_PartnerMap pm   (nolock)-- select * from playlist_content_partnermap where feedid is not null
		on			a.PartnerID = pm.PartnerID
		and			a.FeedID = pm.FeedID

	left join	NDNCategory nc (nolock)
		on			pm.NDNCategoryID = nc.NDNCategoryID
----------------------------
/*
	union all
		select	 @playlistID PlaylistID, a.ContentID, a.Name, a.Description, a.TrackingGroup ContentPartnerID, vg.VideoGroupName, a.Duration,
				a.effectivedate PubDate, a.Keyword, convert(varchar(120), null) Timeline,
				case when at.Name like 'video%' then 'src'
					else at.Name  -- 'unk'
					end as AssetType,
					case when at.Name like 'External%' then a.Filename
					else
						a.DeliveryPath
					end as AssetLocation,
					a.ProducerName, a.ProducerNameAlt, a.logourl ProducerLogo, 0  ContentOrder, isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'news') ProducerCategory,
					REPLACE(a.Name, ' ', '-') + + '-' +CONVERT(varchar(20), a.ContentID) as SEOFriendlyTitle
			from	(select	@playlistID PlaylistID, c.ContentID, c.Name, c.Description, p.TrackingGroup, c.effectivedate, c.Keyword, convert(varchar(120), null) Timeline,
						ca.AssetTypeID, a.duration, a.FilePath, a.filename, p.Name ProducerName, p.ShortName ProducerNameAlt, isnull(p.LogoURL, 'http://assets.newsinc.com/newsinconebyone.png') LogoURL, 0 [Order], a.MimeTypeID, p.PartnerID, p.isContentPrivate, c.FeedID,
						dbo.GetAssetDeliveryPath(mt.FileExtension, c.partnerid, ca.contentid, ca.assetid) DeliveryPath
				from	(select * from Content (nolock) where ContentID = @contentid and dbo.AllowedContentID(contentid, @itrackinggroup, @idevicetype) = 1 ) c
				-- on		pc.ContentID = c.ContentID
				join	partner p (nolock)
				on		c.PartnerID = p.PartnerID
				join	Content_Asset ca (nolock)
				on		c.ContentID = ca.ContentID
				join	Asset a (nolock)
				on		ca.AssetID = a.AssetID
				join	MimeType (nolock) mt
				on		a.MimeTypeID = mt.MimeTypeID
				where	ca.TargetPlatformID = @idevicetype
				and		dbo.AllowedLauncherContentP(@iwidgetID, c.PartnerID) = 1) a
		join	AssetType at (nolock)
		on		a.AssetTypeID = at.AssetTypeID
		join	mtrackinggroup pd  (nolock)
		on		@itrackingGroup = pd.TrackingGroup
		left join	VideoGroups VG (nolock)
		on		@playlistID = vg.VideoGroupId
		left join	Playlist_Content_PartnerMap pm   (nolock)-- select * from playlist_content_partnermap where feedid is not null
		on			a.PartnerID = pm.PartnerID
		and			a.FeedID = pm.FeedID
		left join	NDNCategory nc (nolock)
		on			pm.NDNCategoryID = nc.NDNCategoryID
-----------------------
*/
		order by ContentOrder, a.ContentID

		set nocount off

END
GO
