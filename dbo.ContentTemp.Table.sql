USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContentTemp](
	[ContentID] [int] NOT NULL,
	[ContentTypeID] [smallint] NOT NULL,
	[PartnerID] [int] NOT NULL,
	[Version] [int] NULL,
	[ContentSourceID] [smallint] NOT NULL,
	[Name] [varchar](500) NOT NULL,
	[Description] [varchar](1000) NOT NULL,
	[Category] [varchar](250) NULL,
	[Keyword] [varchar](1000) NULL,
	[Duration] [decimal](20, 2) NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[ExpirationDate] [smalldatetime] NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[CreatedUserID] [int] NOT NULL,
	[UpdatedDate] [smalldatetime] NULL,
	[UpdatedUserID] [int] NULL,
	[Active] [bit] NOT NULL,
	[IsFileArchived] [bit] NULL,
	[isDeleted] [bit] NULL,
	[FeedID] [int] NULL,
	[SourceGUID] [varchar](250) NULL,
	[ContentImportStatus] [int] NULL,
	[TranscriptPath] [varchar](500) NULL,
	[TimesUpdated] [int] NOT NULL,
	[OnlyOwnerView] [bit] NOT NULL,
	[UUID] [uniqueidentifier] NOT NULL,
	[StoryID] [varchar](64) NULL,
	[antikw] [varchar](200) NULL,
 CONSTRAINT [PK_ContentTemp] PRIMARY KEY CLUSTERED 
(
	[ContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_ContentTemp] ON [dbo].[ContentTemp] 
(
	[UpdatedDate] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ContentTemp_1] ON [dbo].[ContentTemp] 
(
	[ContentID] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
