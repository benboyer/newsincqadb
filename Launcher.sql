TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Launcher	LauncherID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	Launcher	PartnerID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	Launcher	Name	-9	nvarchar	50	100	None	None	0	None	None	-9	None	100	3	NO	39
NewsincQA	dbo	Launcher	LauncherTypeID	4	int	10	4	0	10	1	None	None	4	None	None	4	YES	38
NewsincQA	dbo	Launcher	PlayerTypeID	4	int	10	4	0	10	1	None	((1))	4	None	None	5	YES	38
NewsincQA	dbo	Launcher	SectionID	4	int	10	4	0	10	1	None	None	4	None	None	6	YES	38
NewsincQA	dbo	Launcher	Active	-7	bit	1	1	None	None	0	None	None	-7	None	None	7	NO	50
NewsincQA	dbo	Launcher	CreatedDate	11	smalldatetime	16	16	0	None	0	None	(getutcdate())	9	3	None	8	NO	58
NewsincQA	dbo	Launcher	CreatedUserID	4	int	10	4	0	10	1	None	None	4	None	None	9	YES	38
NewsincQA	dbo	Launcher	UpdatedDate	11	smalldatetime	16	16	0	None	0	None	None	9	3	None	10	NO	58
NewsincQA	dbo	Launcher	UpdatedUserID	4	int	10	4	0	10	1	None	None	4	None	None	11	YES	38
NewsincQA	dbo	Launcher	isDefault	-7	bit	1	1	None	None	1	None	((0))	-7	None	None	12	YES	50
NewsincQA	dbo	Launcher	GenerateNow	-7	bit	1	1	None	None	1	None	((0))	-7	None	None	13	YES	50
NewsincQA	dbo	Launcher	Height	4	int	10	4	0	10	1	None	None	4	None	None	14	YES	38
NewsincQA	dbo	Launcher	Width	4	int	10	4	0	10	1	None	None	4	None	None	15	YES	38
NewsincQA	dbo	Launcher	ContinuousPlay	4	int	10	4	0	10	1	None	None	4	None	None	16	YES	38
NewsincQA	dbo	Launcher	DisableEmailButton	-7	bit	1	1	None	None	1	None	((0))	-7	None	None	17	YES	50
NewsincQA	dbo	Launcher	DisableAdsOnPlayer	-7	bit	1	1	None	None	1	None	((0))	-7	None	None	18	YES	50
NewsincQA	dbo	Launcher	DisableShareButton	-7	bit	1	1	None	None	1	None	((0))	-7	None	None	19	YES	50
NewsincQA	dbo	Launcher	DisableAutoPlay	-7	bit	1	1	None	None	1	None	((0))	-7	None	None	20	YES	50
NewsincQA	dbo	Launcher	DisableFullScreen	-7	bit	1	1	None	None	1	None	((0))	-7	None	None	21	YES	50
NewsincQA	dbo	Launcher	LauncherTemplateID	4	int	10	4	0	10	1	None	None	4	None	None	22	YES	38
NewsincQA	dbo	Launcher	LaunchInLandingPage	-7	bit	1	1	None	None	0	None	((0))	-7	None	None	23	NO	50
NewsincQA	dbo	Launcher	TransitionSeconds	4	int	10	4	0	10	0	None	((3))	4	None	None	24	NO	56

index_name	index_description	index_keys
NDX_Lau_actLIDltid	nonclustered located on PRIMARY	Active, LauncherID, LauncherTypeID
PK_Launcher	clustered, unique, primary key located on PRIMARY	LauncherID
