create PROCEDURE [dbo].[ValidateContentAssets_bak]
--  declare
	@trackingGroup int = null,
	@widgetID int = null,
	@playlistid int = 8888888,
	@contentids varchar(max) = null,
	@devicetype int = 1
AS
BEGIN

	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'ValidateContentAssets'

	if @devicetype = 3
		set @devicetype = 1
	if @playlistid is null set @playlistid = 8888888
	-- select @widgetid = 3233, @trackinggroup = 10557, @contentids = ('23537945,23537944,23537943,23537942,23537941,23537940,23537939,23537938,23537937,23537936,23537935,23537934,23537933,23537932,23537931,23537930,23537929,23537928,23537927,23537926'), @devicetype = 1

		Declare	@now datetime,
				@iwidgetID int,
				@iplaylistID int,
				@itrackinggroup int,
				@idevicetype int,
				@icontentids varchar(max)
		set		@now = GETDATE()
		select @iwidgetID = isnull(@widgetID, 1), @iplaylistID = @playlistid, @itrackinggroup = isnull(@trackingGroup, 10557), @idevicetype = isnull(@devicetype, 1), @icontentids = @contentids

		set nocount on
		exec SprocTrackingUpdate 'ValidateContentAssetsByTrackingGroup'

	select distinct a.PlaylistID, a.ContentID, a.Name, a.PubDate PublishDate, (a.[Order] + 1) as ContentOrder
	from	(select	pt.partnerid, pt.TrackingGroup, pt.isContentPrivate, @iplaylistID PlaylistID, p.ContentID, c.Name,
				c.Description, c.EffectiveDate PubDate, p.[Order],
				aa.FilePath, aa.Filename, aa.Duration, c.Keyword,
				null VideoGroupName, at.Name AssetTypeName, mt.FileExtension, pt.logourl, pt.Name ProducerName
		from	(select RowNumber as [order], Number as ContentID from fn_Numbers_fmCSnumbersList(@contentids)) p
		join	Content c (nolock)
		on		p.ContentID = c.ContentID
		join	Content_Asset ca
		on		c.ContentID = ca.ContentID
		and		@idevicetype = ca.TargetPlatformID
		join	Asset aa
		on		ca.AssetID = aa.AssetID
		join	AssetType at
		on		ca.AssetTypeID = at.AssetTypeID
		join	MimeType mt
		on		at.MimeTypeID = mt.MimeTypeID
		and		aa.MimeTypeID = mt.MimeTypeID
		join	Partner_TargetPlatformProfile ptp
		on		c.PartnerID = ptp.PartnerID
		and		@idevicetype = ptp.TargetPlatformID
		JOIN	partner pt (nolock)
		ON		c.PartnerID = pt.PartnerID
		where	dbo.AllowedContentID(c.contentid, @itrackinggroup, @idevicetype) = 1
		) a
	where	dbo.AllowedLauncherContent(@iwidgetid, @trackingGroup) = 1
	ORDER BY a.[Order]+1

	SET NOCOUNT OFF
END
