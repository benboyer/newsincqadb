USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BlockedSites_listInternal_id]
	@includeInactive bit = 0,
	@id int = null
AS
BEGIN
	set nocount on
	set fmtonly off
	exec SprocTrackingUpdate 'BlockedSites_List_id'

	SELECT	BlockedSiteID, Host, Active, isnull(DateUpdatedGMT, DateCreatedGMT) LastUpdatedDate, Comment
	  FROM	BlockedSite
	  WHERE ([Active] = 1 or @includeInactive = 1)
	  and	 (@id is null or BlockedSiteID = @id)
	set nocount off
END
GO
