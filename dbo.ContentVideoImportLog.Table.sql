USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContentVideoImportLog](
	[ContentVideoImportLogID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ContentID] [int] NOT NULL,
	[ContentVideoImportStatus] [int] NOT NULL,
	[Message] [nvarchar](255) NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ContentVideoImportLog] PRIMARY KEY CLUSTERED 
(
	[ContentVideoImportLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ContentVideoImportLogCID] ON [dbo].[ContentVideoImportLog] 
(
	[ContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContentVideoImportLog]  WITH CHECK ADD  CONSTRAINT [FK_ContentVideoImportLog_Content] FOREIGN KEY([ContentID])
REFERENCES [dbo].[Content] ([ContentID])
GO
ALTER TABLE [dbo].[ContentVideoImportLog] CHECK CONSTRAINT [FK_ContentVideoImportLog_Content]
GO
ALTER TABLE [dbo].[ContentVideoImportLog] ADD  CONSTRAINT [DF_ContentVideoImportLog_CreatedDate]  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
