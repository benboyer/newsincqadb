TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	UploadCategory	UploadCategoryID	-5	bigint identity	19	8	0	10	0	None	None	-5	None	None	1	NO	63
NewsincQA	dbo	UploadCategory	UploadContentID	-5	bigint	19	8	0	10	0	None	None	-5	None	None	2	NO	63
NewsincQA	dbo	UploadCategory	Category	12	varchar	40	40	None	None	0	None	None	12	None	40	3	NO	39

index_name	index_description	index_keys
PK_UploadCategory	clustered, unique, primary key located on PRIMARY	UploadCategoryID
