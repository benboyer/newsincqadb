TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	MimeType	MimeTypeID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	MimeType	MimeType	12	varchar	60	60	None	None	0	None	None	12	None	60	2	NO	39
NewsincQA	dbo	MimeType	FileExtension	12	varchar	20	20	None	None	0	None	None	12	None	20	3	NO	39
NewsincQA	dbo	MimeType	MediaType	12	varchar	20	20	None	None	1	None	None	12	None	20	4	YES	39

index_name	index_description	index_keys
IX_MimeType_FileExt	nonclustered located on PRIMARY	FileExtension
IX_MimeType_MimeType	nonclustered located on PRIMARY	MimeType
PK_MimeType	clustered, unique, primary key located on PRIMARY	MimeTypeID
