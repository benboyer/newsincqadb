USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VideoStagingTest]
AS
SELECT     dbo.[Content].ContentID AS VideoStagingID, dbo.[Content].CreatedUserID AS UserID, CAST(1 AS int) AS UserType, dbo.ContentVideo.ImportFilePath AS ClipURL, 
                      dbo.[Content].Name AS Title, dbo.[Content].Description, dbo.[Content].EffectiveDate AS EventDate, dbo.ContentVideo.Height, dbo.ContentVideo.Width, 
                      dbo.ContentVideo.Duration, dbo.[Content].CreatedDate AS CreatedOn, dbo.ContentVideo.ThumbnailImportFilePath AS ThumbnailURL, 
                      dbo.[Content].EffectiveDate AS PublishDate, dbo.[Content].Keyword AS Keywords, dbo.ContentVideo.GUID, dbo.ContentVideo.ImportFilePath AS OriginalName, 
                      dbo.ContentVideo.ContentVideoImportStatusID AS EncodingStatus, dbo.[Content].ExpirationDate AS ExpiryDate, dbo.[Content].Active, dbo.[Content].Active AS IsEnabled, 
                      CAST((CASE WHEN Active = 1 THEN 0 ELSE 1 END) AS bit) AS IsDeleted, dbo.ContentSource.Name AS IngestionSource, 
                      dbo.ContentVideo.StillframeImportFilePath AS StillFrameURL, dbo.[Content].ContentID AS VideoId, dbo.[Content].Category, 
                      dbo.VideoStagingLEGACY.ReadyForEncoding, dbo.VideoStagingLEGACY.EncodingPercentComplete AS EncodingPercentCompleted, 
                      dbo.VideoStagingLEGACY.EncodingStarted, dbo.VideoStagingLEGACY.EncodingCompleted, dbo.VideoStagingLEGACY.Status, dbo.VideoStagingLEGACY.FileSize, 
                      dbo.VideoStagingLEGACY.AudioTrackURL, dbo.VideoStagingLEGACY.EncodedURL, dbo.VideoStagingLEGACY.NexidiaStatus, dbo.VideoStagingLEGACY.Mp3Started, 
                      dbo.VideoStagingLEGACY.Mp3Completed, dbo.VideoStagingLEGACY.CodecUsed, ISNULL(dbo.VideoStagingLEGACY.FileExtension, 
                      RIGHT(dbo.ContentVideo.ImportFilePath, CHARINDEX('.', REVERSE(dbo.ContentVideo.ImportFilePath)))) AS FileExtension, dbo.VideoStagingLEGACY.NiaStatus
FROM         dbo.[Content] INNER JOIN
                      dbo.ContentVideo ON dbo.[Content].ContentID = dbo.ContentVideo.ContentID INNER JOIN
                      dbo.ContentSource ON dbo.[Content].ContentSourceID = dbo.ContentSource.ContentSourceID INNER JOIN
                      dbo.VideoStagingLEGACY ON dbo.ContentVideo.ContentID = dbo.VideoStagingLEGACY.ContentID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Content"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 201
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ContentVideo"
            Begin Extent = 
               Top = 6
               Left = 239
               Bottom = 114
               Right = 458
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ContentSource"
            Begin Extent = 
               Top = 185
               Left = 261
               Bottom = 263
               Right = 424
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "VideoStagingLEGACY"
            Begin Extent = 
               Top = 140
               Left = 603
               Bottom = 248
               Right = 808
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         O' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VideoStagingTest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'r = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VideoStagingTest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VideoStagingTest'
GO
