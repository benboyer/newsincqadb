USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[AllowedDistributorID]
(
	 @ProviderID int,
	 @DistributorID int
)
RETURNS bit
AS
BEGIN
	DECLARE @allowed bit = 0
		-- @AllowedContentID bigint,
		-- declare @ProviderID int = 499, @Distributorid int = 320, @allowed bit
	set		@Allowed =
		isnull((select 1
		from	(select * from Partner where PartnerID = @providerid) p
		left	join AllowContent ac
		on		p.PartnerID = ac.ContentProviderPartnerID
		and		@DistributorID = ac.DistributorPartnerID
		where	@providerid = @distributorid
		or		p.isContentPrivate = 0
		or	(	ac.AllowContentID is not null
				and exists
		(select 1
		from (select * from Partner where PartnerID = @distributorid) p2
		where	p2.PartnerID = ac.DistributorPartnerID))), 0)

		-- select @allowed
	RETURN @Allowed

END
GO
