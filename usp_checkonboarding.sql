CREATE procedure usp_checkonboarding @text varchar(100), @group int as

declare @partners table(partnerid int, trackinggroup int, name nvarchar(100), contactid int, statusid int)

insert into @partners
select PartnerID, TrackingGroup, name, contactid, statusid from [Partner] where Name like '%'+@text+'%' or TrackingGroup = @group

select * from @partners

select * from PartnerLookup where PartnerID in ( select trackinggroup from @partners ) or Name in ( select Name from @partners )

select UserID, PartnerID, FirstName, LastName, Login, statusID from [User] where UserID in ( select contactid from @partners ) or partnerid in ( select partnerid from @partners )

select UserID, PartnerId, EmailID, IsAdmin from NDNUsersLEGACY where Partnerid in ( select trackinggroup from @partners ) 
or UserID in ( select UserID from [User] where UserID in ( select contactid from @partners ) or partnerid in ( select partnerid from @partners ) )
