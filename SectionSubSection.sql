TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	SectionSubSection	SectionSubSectionID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	SectionSubSection	Value	12	varchar	10	10	None	None	0	None	None	12	None	10	2	NO	39
NewsincQA	dbo	SectionSubSection	Description	12	varchar	16	16	None	None	1	None	None	12	None	16	3	YES	39

index_name	index_description	index_keys
IX_SectionSubSection_Value	nonclustered, unique located on PRIMARY	SectionSubSectionID
PK_SectionSubSection	clustered, unique, primary key located on PRIMARY	Value
