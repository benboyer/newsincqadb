CREATE procedure [dbo].[cr_UserContentCounts]
	-- declare
	@userid int,
	@Requested varchar(20) -- 'recentlyadded', 'enabled, disabled, 'deleted'
as
BEGIN
	set nocount on
	set fmtonly off
-------------------
--  select @userid = 8125, @Requested = 'deleted'
-------------------
	declare @partnerid int = (select partnerid from [User] (nolock) where UserID = @userid)
	declare @isadmin bit = (select isadmin from NDNUsersLEGACY (nolock) where UserID = @userid)


	-- insert into sprocparamtracker(sproc, param1, param2, param3, param4)
	-- select 'cr_UserContentCounts',@UserId,@Requested,@partnerid, @isadmin

		/* Recently Added */
		if @Requested = 'recentlyadded'
		begin
			select	count(*) Records
			from Content c (nolock)
			left join UserInterface_setting uis -- select * from UserInterface_setting
			on		'RecentVideos_Hours' = uis.Setting
			left join User_UserInterface_Setting uuis (nolock)
			on		uis.UserInterface_SettingID = uuis.UserInterface_SettingID
			and		@userid = uuis.UserID
			where	(c.PartnerID = @partnerid or @isadmin = 1)
			AND		(c.CreatedDate > DATEADD(HOUR, -convert(int,isnull(uuis.Value, uis.defaultvalue)),GETDATE()))
			and		c.Active = 1
		end

		/* Deleted */
		if @Requested = 'deleted'
		begin
			select	count(*) Records
			from	Content c
			where	c.ContentImportStatus = 5
			and		(c.PartnerID = @partnerid or @isadmin = 1)
			AND 	c.isDeleted = 1
			and		c.Active = 0
		end

		/* Enabled Videos */
		if @Requested = 'enabled'
		begin
			select	count(*) Records
			from	Content c
			where	(c.PartnerID = @partnerid or @isadmin = 1)
			AND		c.isDeleted = 0 AND c.Active = 1
		end

		/* Disabled Videos */
		if @Requested = 'disabled'
		begin
			select	count(*) Records
			from	Content c
			where	c.ContentImportStatus = 5
			and		(c.PartnerID = @partnerid or @isadmin = 1)
			AND		c.isDeleted = 0
			AND		c.Active = 0
		end
	set nocount OFF
END
