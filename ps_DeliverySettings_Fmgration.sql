Create PROCEDURE [dbo].[ps_DeliverySettings_Fmgration] @TrackingGroup int = null
as
BEGIN
 -- Cloned from ps_DeliverySettings
 --	with:	eliminated @launcherID as imput variable	
 --			commented out join to DeliverySetting_LauncherTypes 
 set fmtonly off  
 set nocount on  
 exec SprocTrackingUpdate 'GetDeliverySettings'  
  
 -- declare @LauncherID int = 7010,  @TrackingGroup int = 90121--10557 -- select * from deliverysettings  
 select ds.PlayerVariable as Name, 
	case when ds.value = ds.defaultvalue then coalesce(/*dslt.Value*/ dstg.Value, ds.Value) else ds.value end Value,  
	convert(varchar(5), ds.DeliverySettingsID) + '=' + convert(varchar(5), 
		case when ISNUMERIC(case when ds.value = ds.defaultvalue then coalesce(/*dslt.Value*/ dstg.Value, ds.Value) else ds.value end) = 1  
			then case when ds.value = ds.defaultvalue 
				then coalesce(/*dslt.Value*/ dstg.Value, ds.Value) else ds.value end 
			else case when case when ds.value = ds.defaultvalue 
							then coalesce(/*dslt.Value,*/ dstg.Value, ds.Value) else ds.value end = 'true' then 1  
						when case when ds.value = ds.defaultvalue 
							then coalesce(/*dslt.Value,*/ dstg.Value, ds.Value) else ds.value end = 'false' then 0 
						end 
			end) as ShortVariable  
  
 from DeliverySettings ds  
 left join DeliverySetting_TrackingGroup dstg  
 on ds.deliverysettingsid = dstg.deliverysettingsid  
 and @TrackingGroup = dstg.trackinggroup  
-- left join DeliverySetting_LauncherTypes dslt  
 --on ds.deliverysettingsid = dslt.deliverysettingsid  
 --and @LauncherID = dslt.LauncherTypesID  
 where ds.Name = 'ForcePlayer2'
	set nocount off
END

