TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Partner_Logo	Partner_LogoID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	Partner_Logo	PartnerID	4	int	10	4	0	10	0	None	None	4	None	None	2	NO	56
NewsincQA	dbo	Partner_Logo	LogoID	4	int	10	4	0	10	0	None	None	4	None	None	3	NO	56
NewsincQA	dbo	Partner_Logo	LauncherID	4	int	10	4	0	10	1	None	None	4	None	None	4	YES	38

index_name	index_description	index_keys
IX_Partner_Logo_La	nonclustered located on PRIMARY	LauncherID
IX_Partner_Logo_Lo	nonclustered located on PRIMARY	LogoID
IX_Partner_Logo_P	nonclustered located on PRIMARY	PartnerID
PK_Partner_Logo	clustered, unique, primary key located on PRIMARY	Partner_LogoID
