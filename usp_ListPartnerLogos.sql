CREATE procedure [dbo].[usp_ListPartnerLogos]
	@PartnerID int
as
BEGIN
	declare @name varchar(max) = (select name from Partner (nolock) where PartnerID = @PartnerID)
	select	@name Partner, null as Partner_logoid, logourl, '' as LauncherID, null as LogoID, lt.LogoTypeID, lt.name LogoType, 'producer' Description, 'Partner' StoredInTable
		from	partner p,
				logotype lt
		where	partnerid = @partnerid
		and		lt.logotypeid = 5
	union	
		select @name Partner, null as partner_logiid, ol.logourl, '' as LauncherID, null as LogoID, lt.logotypeid, lt.name LogoType, 'producer' Description, 'OrganizationsLegacy' StoredInTable
		from	OrganizationsLEGACY ol,
				logotype lt
		where	partnerid = @partnerid
		and		lt.logotypeid = 5
	union
		select	@name Partner, pl.partner_logoid, l.logourl, pl.LauncherID, l.LogoID, lt.logotypeid, lt.name LogoType, lt.Description, 'Logo' StoredInTable
		from	partner_logo pl
		join	logo l
		on		pl.logoid = l.logoid
		join	logotype lt
		on		l.logotypeid = lt.logotypeid
		where	pl.partnerid = @partnerid 
	order by lt.LogoTypeID, LauncherID 
END
