USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[adm_DefaultSetting_Add_Update]
	@iSettingType varchar(36),
	@iSetting varchar(36),
	@value varchar(120),
	@deleteit bit = 0,
	@comment varchar(120) = null
as
BEGIN
	set FMTONLY OFF
	set NOCOUNT ON
	exec	SprocTrackingUpdate 'adm_DefaultSetting_Add_Update'

	declare @SettingTypeID int,
			@SettingID int

	set @SettingTypeID = (select DefaultSettingTypeID from DefaultSettingType where name = @iSettingType)
	set @SettingID = (select SettingID from Setting where Name = @iSetting )
	if @SettingTypeID is null or @SettingID is null
	begin
		select 'Can not find an input parameter(s).  Please check and try again' as MessageBack
		return
	end
	if @deleteit = 1
	and	exists
		(select 1 -- select *
		from	DefaultSetting
		where	DefaultSettingTypeID = @SettingTypeID
		and		SettingID = @SettingID
		and		Value = @value)
	begin
		delete
		from DefaultSetting
		where	DefaultSettingTypeID = @SettingTypeID
		and		SettingID = @SettingID
		and		Value = @value
	end
	else
	BEGIN
		if exists
			(select 1 -- select *
			from	DefaultSetting
			where	DefaultSettingTypeID = @SettingTypeID
			and		SettingID = @SettingID)
		begin
			update DefaultSetting
			set	Value = @value,
				Comment = @comment
			where	DefaultSettingTypeID = @SettingTypeID
			and		SettingID = @SettingID
		end
		else
		BEGIN
			insert into DefaultSetting(DefaultsettingTypeID, SettingID, Value, Comment)
			select @SettingTypeID, @SettingID, @value, @comment
		END
	END
		set NOCOUNT OFF
END
GO
