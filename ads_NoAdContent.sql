TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	ads_NoAdContent	ads_NoAdContentID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	ads_NoAdContent	videoid	-5	bigint	19	8	0	10	0	None	None	-5	None	None	2	NO	63
NewsincQA	dbo	ads_NoAdContent	filteraddeddate	11	datetime	23	16	3	None	1	None	None	9	3	None	3	YES	111
NewsincQA	dbo	ads_NoAdContent	filteractive	-7	bit	1	1	None	None	0	None	((1))	-7	None	None	4	NO	50
NewsincQA	dbo	ads_NoAdContent	updateddate	11	datetime	23	16	3	None	1	None	None	9	3	None	5	YES	111
NewsincQA	dbo	ads_NoAdContent	KeywordAddPhrase	12	varchar	200	200	None	None	1	None	None	12	None	200	6	YES	39

index_name	index_description	index_keys
PK__ads_NoAdContentID	clustered, unique, primary key located on PRIMARY	ads_NoAdContentID
