TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	FeedDMAInclude	FeedDMAIncludeID	-5	bigint identity	19	8	0	10	0	None	None	-5	None	None	1	NO	63
NewsincQA	dbo	FeedDMAInclude	FeedContentID	-5	bigint	19	8	0	10	1	None	None	-5	None	None	2	YES	108
NewsincQA	dbo	FeedDMAInclude	DMACode	12	varchar	20	20	None	None	1	None	None	12	None	20	3	YES	39

index_name	index_description	index_keys
PK__FeedDMAI__5BFAF6B17F9B5D0E	clustered, unique, primary key located on PRIMARY	FeedDMAIncludeID
