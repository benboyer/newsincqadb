create view dme_Partner_TargetPlatformProfile
as
select Partner_TargetPlatformProfileID, PartnerID, ProfileDescriptionID, TargetPlatformID, isImportEnabled, isDistributionEnabled
from Partner_TargetPlatformProfile (nolock)
