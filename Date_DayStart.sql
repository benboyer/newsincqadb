CREATE FUNCTION [dbo].[Date_DayStart]
(
	-- Add the parameters for the function here
	@datein datetime
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultVar datetime

	-- Add the T-SQL statements to compute the return value here -- declare @resultvar
	set	@ResultVar =  (select CAST(FLOOR(CAST(@datein AS FLOAT)) AS DATETIME))


	-- Return the result of the function
	RETURN @ResultVar

END
