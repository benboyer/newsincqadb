CREATE VIEW dbo.dme_PrivateProvider_AllowedDistributor
AS
SELECT DISTINCT p.PartnerID AS PrivateProviderPartnerID, pd.PartnerID AS AllowedDistributorPartnerID
FROM         dbo.AllowContent AS ac WITH (nolock) INNER JOIN
                          (SELECT     PartnerID, ParentPartnerID, Name, ContactID, Website, StatusID, CreatedDate, CreatedUserID, UpdatedDate, UpdateUserID, TrackingGroup, ShortName, 
                                                   LandingURL, LogoURL, isContentPrivate, ZoneID, FeedGUID, isFeedGUIDActive, DefaultEmbedWidth, DefaultEmbedRatio, DefaultEmbedHeight
                            FROM          dbo.Partner
                            WHERE      (isContentPrivate = 1)) AS p ON ac.ContentProviderPartnerID = p.PartnerID INNER JOIN
                      dbo.Partner AS pd ON ac.DistributorPartnerID = pd.PartnerID OR p.PartnerID = pd.PartnerID
