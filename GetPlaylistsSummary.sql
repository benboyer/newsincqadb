CREATE proc [dbo].[GetPlaylistsSummary]    
-- declare    
 @UserId int  = 0,    
 @ShowGlobalPlaylists bit = 1,    
 @ShowSponsoredPlaylists bit = 1,    
 @page int = 1,    
 @pagecnt int = 1000000    
AS    
BEGIN    
 --updated 2013-09-18    
 IF OBJECT_ID('tempdb..#plr1') IS NOT NULL    
  DROP TABLE #plr1    
    
    
 exec SprocTrackingUpdate 'GetPlaylistsSummary'    
 declare @tg int = (select trackinggroup from mTrackingGroup (nolock) where partnerid = (select partnerid from [User] (nolock) where UserID = @UserId))    
 declare @pid int = (select partnerid from mTrackingGroup where TrackingGroup = @tg)    
    -- declare @userid int = 9526, @tg int = 87754, @showglobalplaylists bit = 1, @showsponsoredplaylists bit = 1    
    
 -- select @pid, ( select COUNT(*) from dbo.AllowedContentByPartnerID(@pid))    
    
    select p.PlaylistID, p.Name Title, p.CreatedUserID, p.SharingOption, isnull(p.IsPartner, 0) IsPartner, p.RSSEnabled, sum(case when l.LauncherTypeID <> 1 then 1 else 0 end) as Widgets, sum(case when l.LauncherTypeID = 1 then 1 else 0 end) as Players,  
  
    -- 1 as Deleteable --    
    case ISNULL(COUNT(distinct lp.launcherid),0) when 0 then 1 else 0 end as Deleteable    
    
    into #plr1    
    -- declare @userid int = 9526, @tg int = 87754, @showglobalplaylists bit = 1, @showsponsoredplaylists bit = 1 select top 100 *    
    from (-- declare @userid int = 9526, @tg int = 87754, @showglobalplaylists bit = 0, @showsponsoredplaylists bit = 0    
      select p.*, pl.SharingOption, isnull(pl.RSSEnabled, 0) RSSEnabled    
      from PlaylistsLEGACY (nolock) pl    
      join Playlist (nolock)  p    
      on  pl.PlaylistID = p.PlaylistID    
      where (p.Active = 1    
        and (p.ExpirationDate is null or p.ExpirationDate > GETDATE()))    
        -- and  p.CreatedUserID = 9526    
      and  ((P.CreatedUserID = @UserId)    
        or  (@UserId = 0    
          or (p.CreatedUserID <> @UserId    
         AND pl.SharingOption = 2    
         AND (@ShowGlobalPlaylists = 1    
          OR (case @ShowSponsoredPlaylists when 1 then 1 end) = 1    
          AND IsSponsored = @ShowSponsoredPlaylists)    
         AND (IsSponsored = @ShowSponsoredPlaylists OR (case @ShowSponsoredPlaylists when 1 then 1 end) = 1))))) p    
    left join Launcher_Playlist (nolock) lp    
    on  p.PlaylistID = lp.PlaylistID    
    left join launcher (nolock)l    
    on  lp.LauncherID = l.LauncherID    
    where isnull(l.Active, 1) = 1    
    --and p.CreatedUserID = 9526    
    --and lp.playlistid = 14281    
    -- and dbo.AllowedLauncherContent(l.launcherid, @tg) = 1    
    group by p.PlaylistID, p.Name, p.CreatedUserID, p.SharingOption, p.RSSEnabled, p.IsPartner    
    
    
  -- declare @pid int = -1, @userid int = 7777, @tg int = 0, @showglobalplaylists bit = 1, @showsponsoredplaylists bit = 1    
   -- sp_help PlaylistsLEGACY    
    
  declare @plr2 Table(playlistID int, title varchar(100), createduserid int, sharingOption int, Ispartner bit, RSSEnabled bit, Widgets int, Players int, Videos int, Deleteable bit )    
    
  if ( (select isadmin from NDNUsersLEGACY where UserID = @userid) = 1 )    
  insert into @plr2    
  select *    
  from    
  (    
  select r1.PlaylistID, r1.Title, r1.CreatedUserID, r1.SharingOption, r1.IsPartner, r1.RSSEnabled, r1.Widgets, r1.Players, COUNT(distinct c.ContentID) Videos, r1.Deleteable    
  -- declare @pid int = 1351, @userid int = 8125, @tg int = 99999, @showglobalplaylists bit = 1, @showsponsoredplaylists bit = 1 select count(distinct c.contentid)    
  from #plr1 (nolock)r1    
  join Playlist pl    
  on  r1.PlaylistID = pl.PlaylistID    
  left join Playlist_Content (nolock) pc    
  on  r1.PlaylistID = pc.PlaylistID    
  --and  1 = dbo.AllowedContentID(pc.contentid, @tg, 1)    
  left join Content (nolock) c    
  on  pc.ContentID = c.ContentID    
  left join (select PartnerID from vw_partnerdefaultsettings where entity = 'BlockContentFromControlRoom' and value = 'true') bcr    
  on c.partnerid = bcr.partnerid    
  where bcr.PartnerID is null    
  and  ( c.Active = 1 or pc.ContentID is null )    
  group by r1.PlaylistID, r1.Title, r1.CreatedUserID, r1.SharingOption, r1.RSSEnabled, r1.IsPartner, r1.Widgets, r1.Players, r1.Deleteable    
  ) sub1    
  else    
  insert into @plr2    
  select *    
  from    
  (    
  select r1.PlaylistID, r1.Title, r1.CreatedUserID, r1.SharingOption, r1.IsPartner, r1.RSSEnabled, r1.Widgets, r1.Players, COUNT(distinct c.ContentID) Videos, r1.Deleteable    
  -- declare @pid int = 1351, @userid int = 8125, @tg int = 99999, @showglobalplaylists bit = 1, @showsponsoredplaylists bit = 1 select count(distinct c.contentid)    
  from #plr1 (nolock)r1    
  join Playlist pl    
  on  r1.PlaylistID = pl.PlaylistID    
  left join Playlist_Content (nolock) pc    
  on  r1.PlaylistID = pc.PlaylistID    
  --and  1 = dbo.AllowedContentID(pc.contentid, @tg, 1)    
  left join Content (nolock) c    
  on  pc.ContentID = c.ContentID    
  left join (select PartnerID from vw_partnerdefaultsettings where entity = 'BlockContentFromControlRoom' and value = 'true') bcr    
  on c.partnerid = bcr.partnerid    
  where bcr.PartnerID is null    
  and  ( pc.ContentID is null )    
  group by r1.PlaylistID, r1.Title, r1.CreatedUserID, r1.SharingOption, r1.RSSEnabled, r1.IsPartner, r1.Widgets, r1.Players, r1.Deleteable    
   UNION ALL    
  select r1.PlaylistID, r1.Title, r1.CreatedUserID, r1.SharingOption, r1.IsPartner, r1.RSSEnabled, r1.Widgets, r1.Players, COUNT(distinct c.ContentID) Videos, r1.Deleteable    
  -- declare @pid int = 1351, @userid int = 8125, @tg int = 99999, @showglobalplaylists bit = 1, @showsponsoredplaylists bit = 1 select count(distinct c.contentid)    
  from #plr1 (nolock)r1    
  join Playlist pl    
  on  r1.PlaylistID = pl.PlaylistID    
  left join Playlist_Content (nolock) pc    
  on  r1.PlaylistID = pc.PlaylistID    
  --and  1 = dbo.AllowedContentID(pc.contentid, @tg, 1)    
  left join Content (nolock) c    
  on  pc.ContentID = c.ContentID    
  left join (select PartnerID from vw_partnerdefaultsettings where entity = 'BlockContentFromControlRoom' and value = 'true') bcr    
  on c.partnerid = bcr.partnerid    
  -- left outer join [vw_AllowedContent] vac    
  -- on vac.contentid = c.contentid    
  inner join dbo.AllowedContentByPartnerID(@pid) acbp    
  on acbp.contentid = c.ContentID --and r1.PlaylistID = acbp.playlistid    
  where bcr.PartnerID is null    
  group by r1.PlaylistID, r1.Title, r1.CreatedUserID, r1.SharingOption, r1.RSSEnabled, r1.IsPartner, r1.Widgets, r1.Players, r1.Deleteable    
  ) sub2    
    
    
 -- declare @userid int = 7777, @tg int = 0, @showglobalplaylists bit = 1, @showsponsoredplaylists bit = 1    
 select PlaylistID, UserID, Title, SharingOption, IsPartner, Videos, Widgets, Players, EnableRSSFeed, RSSToken, Name, Deleteable from    
 (    
 select r2.PlaylistID, r2.CreatedUserID UserID, p2.Name Title, r2.SharingOption, r2.IsPartner,r2.Videos, r2.Widgets, r2.Players, r2.RSSEnabled EnableRssFeed, p2.RSSToken, u.FirstName + ' ' + u.LastName as Name,    
 RANK() over ( order by p2.Name asc, r2.PlaylistID asc ) as rnk, Deleteable    
 from @plr2 r2    
 join Playlist (nolock) p2    
 on  r2.PlaylistID = p2.PlaylistID    
 join [User] (nolock) u    
 on  p2.CreatedUserID = u.UserID    
 -- where r2.PlaylistID = 11399    
    
) sub2    
 where rnk between ((@page-1) * @pagecnt) + 1 and (@page * @pagecnt)    
 Order By Title    
    
 drop table #plr1    
 -- drop table #plr2    
    
END    
