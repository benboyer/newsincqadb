USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Logs](
	[LogID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[LogEntry] [datetime] NOT NULL,
	[EventDetails] [nvarchar](255) NOT NULL,
	[EventType] [nvarchar](10) NOT NULL,
	[MainLogID] [int] NULL,
	[LastLogID] [bit] NULL,
	[EventLocation] [nvarchar](50) NOT NULL,
	[EventName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK__Logs__5E5499A803B16C81] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Logs] ADD  CONSTRAINT [DF__Logs__LastLogID__0599B4F3]  DEFAULT ((1)) FOR [LastLogID]
GO
ALTER TABLE [dbo].[Logs] ADD  CONSTRAINT [DF__Logs__EventLocat__068DD92C]  DEFAULT ('') FOR [EventLocation]
GO
ALTER TABLE [dbo].[Logs] ADD  CONSTRAINT [DF__Logs__EventName__0781FD65]  DEFAULT ('') FOR [EventName]
GO
