USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[mon_GetContentDetails]
	@ContentID bigint
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'mon_GetContentDetails'
	
	select c.ContentID, c.PartnerID, c.CreatedDate, c.UpdatedDate, c.EffectiveDate, ca.TargetPlatformID, ca.AssetTypeID, c.ContentImportStatus Cimportstatus, c.Active Cactive, ca.ImportStatusID CAimportStatus, ca.Active CAactive, a.AssetID, a.ImportStatusID Aimportstatus, a.Active Aactive, a.EncodingID,
		case when a.filepath is not null then a.FilePath + '/' + a.Filename else '' end as AssetFile,
		fca.FeedContentID, fca.FeedContentAssetID, fca.MimeType, fca.Width, fca.Bitrate, fca.FilePath SourceFileFmFeed
	from Content c (nolock)
	join Content_Asset ca (nolock)
	on c.ContentID = ca.ContentID
	join Asset a  (nolock)
	on ca.AssetID = a.AssetID
	left join feedcontentasset fca (nolock)
	on	a.FeedContentAssetID = fca.feedcontentassetid
	where c.ContentID = @ContentID
	order by c.ContentID, c.CreatedDate, ca.TargetPlatformID, ca.AssetTypeID
	set nocount off
END
GO
