USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [deploy].[uninstalled_features] as
select *
from deploy.schema_feature
where completed_on is null
GO
