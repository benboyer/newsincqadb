create procedure adm_WatchFolderCreate
--	declare 
	@partnerid int,
	@SystemID int = 1,
	@Priority int = 1,
	@MaxUpdates int = 3,
	@MaxDownloads int = 3,
	@FolderXsltPath varchar(200) = 'http://xslt.newsinc.com.s3.amazonaws.com/Mobile/MRSSNewVTwo.xslt',
	@FolderPath varchar(200) = 'D:\NDN\FTP\localuser\',
	@FeedUrl varchar(200) = 'http://uploadfeedsvc.newsinc.com/Watchfolder/',
	@FeedXLT varchar(200) = 'http://xslt.newsinc.com/Mobile/UploadsNewTest2.xslt',
	@FolderActive bit = 1,
	@FeedActive bit = 1,
	@sourceid int = 3
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'adm_WatchFolderCreate'
	
	declare @sPartnerID varchar(10)
	declare @diditwork varchar(100) = ''
	set @sPartnerID = CONVERT(varchar(10), @partnerid)
	set @FolderPath = @FolderPath + @sPartnerID
	set @FeedUrl = @FeedUrl + @sPartnerID + '.xml'

	if exists (select 1 from PartnerContentImportFolder where PartnerID = @partnerid and ContentSourceID = 3)
	begin
		set @diditwork = @diditwork + 'Partner alread has watchfolder, check it.  '
	end
	else
	begin
		INSERT PartnerContentImportFolder ([PartnerID], [FolderPath], [XsltPath], [Enabled], ContentSourceID)
		select @partnerid, @FolderPath, @FolderXsltPath, @FolderActive, @sourceid
		if @@ERROR = 0
		begin
			set @diditwork = @diditwork + 'Created Watch folder for ' + @sPartnerID + '.  '
		end
		else
		begin
			set @diditwork = @diditwork + convert(varchar(10),@@ERROR) + 'Error creating Watch Folder.  '
		end
	end

	if exists (select 1 from Feed where PartnerID = @partnerid and SourceID = 3)
	begin
		set @diditwork = @diditwork + 'Partner already has watch folder feed. Check it.'
	end
	else
	begin
		insert feed (feedurl, partnerid, xslt, contentimportsystemid, feedpriorityid, MaxUpdates, MaxDownloads, sourceid, active) 
		select @FeedUrl FeedURL,
			c.partnerid PartnerID,
			@FeedXLT Xslt,
			@SystemID,
			@Priority,
			@MaxUpdates,
			@MaxDownloads,
			@sourceid,
			@FeedActive
		from partnercontentimportfolder c
		join partner p on c.partnerid = p.partnerid
		where not exists (select 1 from feed f where f.partnerid = c.partnerid and f.sourceid = 3)
		and c.partnerid = @partnerid
		if @@ERROR = 0
		begin
			set @diditwork = @diditwork + 'Created Watch Folder Feed for ' + @sPartnerID + '.'
		end
		else
		begin
			set @diditwork = @diditwork + convert(varchar(10),@@ERROR) + 'Error creating Watch Folder Feed.'
		end
	end
	select @diditwork MessageBack, a.PartnerFolderID, a.PartnerID fldPartnerid, a.FolderPath fldFolderPath, a.XsltPath fldXsltPath, a.Enabled fldEnabled, a.ContentSourceID fldContentSourceID,
	f.FeedId, f.FeedUrl, f.XSLT, f.ContentImportSystemID, f.FeedPriorityID, f.Active feedActive, f.MaxDownloads, f.MaxUpdates
	from PartnerContentImportFolder a join Feed f on a.PartnerID = f.PartnerID where a.PartnerID = @partnerid
	and f.SourceID = 3
	set nocount off
END

