CREATE VIEW dbo.Widgets
AS
SELECT     dbo.Launcher.LauncherID AS WidgetId, dbo.WidgetsLEGACY.ContentItemID, dbo.WidgetsLEGACY.DCId, dbo.WidgetsLEGACY.VanityName, dbo.Launcher.Name, 
                      CAST(dbo.Launcher.LauncherTypeID AS int) AS WidgetType, dbo.WidgetsLEGACY.WidgetIdentificationName, dbo.WidgetsLEGACY.IsDefault, 
                      dbo.WidgetsLEGACY.DefaultTab, CAST((CASE dbo.Launcher.Active WHEN 0 THEN 1 ELSE 0 END) AS bit) AS IsDeleted, CAST(dbo.Launcher.UpdatedDate AS datetime) 
                      AS LastUpdated, dbo.WidgetsLEGACY.GenerateNow, dbo.WidgetsLEGACY.TemplateZip, dbo.WidgetsLEGACY.Height, dbo.WidgetsLEGACY.Width, 
                      CAST(dbo.Launcher.SectionID AS int) AS SiteSectionId, dbo.Launcher.CreatedDate AS EnterDTM, CAST(dbo.Launcher.UpdatedDate AS datetime) AS LastChgDTM, 
                      dbo.Launcher.CreatedUserID AS UserID
FROM         dbo.Launcher INNER JOIN
                      dbo.WidgetsLEGACY ON dbo.Launcher.LauncherID = dbo.WidgetsLEGACY.LauncherID
