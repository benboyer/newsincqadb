USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContentImportSystem](
	[ContentImportSystemID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[IPaddress] [varchar](60) NULL,
	[DatabaseName] [varchar](60) NULL,
	[Description] [varchar](60) NULL,
	[BoxName] [varchar](50) NULL,
	[Zencoder] [bit] NOT NULL,
 CONSTRAINT [PK_ContentImportSystem] PRIMARY KEY CLUSTERED 
(
	[ContentImportSystemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ContentImportSystem] ADD  CONSTRAINT [DF_ContentImportSystem_Zencoder]  DEFAULT ((0)) FOR [Zencoder]
GO
