CREATE procedure [dbo].[GetPlayerTypes]
	@PlayerTypesID int = null
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetPlayerTypes'

	Select PlayerTypesID, Name, Description
	from PlayerTypes
	where	PlayerTypesID = ISNULL(@PlayerTypesID, PlayerTypesID)
	set nocount off
END
