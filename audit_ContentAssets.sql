CREATE VIEW dbo.audit_ContentAssets
AS
SELECT     dbo.[Content].ContentID, dbo.[Content].Name, dbo.[Content].PartnerID, dbo.[Content].ContentImportStatus, dbo.Content_Asset.ImportStatusID AS CAImportStatus, 
                      dbo.Asset.AssetID, dbo.Asset.AssetTypeID, dbo.Asset.MimeTypeID, dbo.Asset.Filename, dbo.Asset.FilePath, dbo.Asset.FileSize, dbo.Asset.ImportStatusID, 
                      dbo.Asset.Height, dbo.Asset.Width, dbo.Asset.Bitrate, dbo.Asset.Duration, dbo.[Content].Active AS CActive, dbo.Content_Asset.Active AS CAActive, dbo.Asset.Active, 
                      dbo.Asset.isArchived, dbo.Asset.isDeleted, dbo.Asset.FeedContentAssetID, dbo.Asset.EncodingID, dbo.MimeType.MimeType, dbo.MimeType.FileExtension, 
                      dbo.MimeType.MediaType
FROM         dbo.Asset INNER JOIN
                      dbo.Content_Asset ON dbo.Asset.AssetID = dbo.Content_Asset.AssetID INNER JOIN
                      dbo.[Content] ON dbo.Content_Asset.ContentID = dbo.[Content].ContentID INNER JOIN
                      dbo.MimeType ON dbo.Asset.MimeTypeID = dbo.MimeType.MimeTypeID
