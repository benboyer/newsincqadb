USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ps_fn_getAssetData](@partnerID int, @contentID int, @assetid bigint, @ext varchar(10))
RETURNS @assetinfo TABLE
(
	AssetLocation varchar (500) NULL,
	AssetMimeType varchar (500) NULL,
	AssetSortOrder int NULL,
	AssetLocation2 varchar (500) NULL,
	AssetMimeType2 varchar (500) NULL,
	AssetSortOrder2 int NULL
)
AS
BEGIN

	-- declare @partnerID int, @contentID int, @assetid bigint, @ext varchar(10)
	declare @returnvar varchar(500), @returnvar2 varchar(500), @templateidstream int, @templateidprogressive int
	-- select @contentID = 23752583, @partnerID = 1325, @assetid = 2430205, @ext = 'jpg'

	-- select fileextension from Asset a, MimeType mt
	-- where AssetID = 2430205 and a.MimeTypeID = mt.MimeTypeID

	if ( @ext in ('flv', 'mp4') )
	begin
		select top 1 @templateidstream = COALESCE(s1.Value, s2.Value) from
		( select Value from vw_partnerDefaultSettings (nolock) where Entity = 'PlayerServiceDeliveryTemplate' and Setting = @ext + 'stream' and PartnerID = @partnerid) s1
			FULL OUTER JOIN
		( select Value from vw_sysDefaultSettings (nolock) where Entity = 'PlayerServiceDeliveryTemplate' and Setting = @ext + 'stream' ) s2 on 1 = 1
	end

	if ( @ext in ('flv', 'mp4') )
	begin
		select top 1 @templateidprogressive = COALESCE(s1.Value, s2.Value) from
		( select Value from vw_partnerDefaultSettings (nolock) where Entity = 'PlayerServiceDeliveryTemplate' and Setting = @ext + 'progressive' and PartnerID = @partnerid) s1
			FULL OUTER JOIN
		( select Value from vw_sysDefaultSettings (nolock) where Entity = 'PlayerServiceDeliveryTemplate' and Setting = @ext + 'progressive' ) s2 on 1 = 1
	end

	if @ext not in ('txt') and @templateidstream is null
	begin
	  set @returnvar = (select FilePath + '/' + Filename from Asset where AssetID = @assetid)
	 if LEFT(@returnvar, 7) <> 'http://' and LEFT(@returnvar, 7) <> 'rtmp://'
	  set @returnvar = 'http://'+@returnvar
	end

	if @ext not in ('txt') and @templateidprogressive is null
	begin
	  set @returnvar2 = (select FilePath + '/' + Filename from Asset where AssetID = @assetid)
	 if LEFT(@returnvar2, 7) <> 'http://' and LEFT(@returnvar2, 7) <> 'rtmp://'
	  set @returnvar2 = 'http://'+@returnvar2
	end

	-- select ISNULL(@returnvar,

	insert into @assetinfo(AssetLocation, AssetLocation2) select @returnvar, @returnvar2

	-- insert into @assetinfo(AssetLocation) select top 1 urlline from templates (nolock) where TemplatesID = @templateidstream

	-- insert into @assetinfo(AssetLocation) select @templateidstream

	update @assetinfo set
	 AssetLocation = sub1.url,
	 AssetMimeType = sub1.mime,
	 AssetSortOrder = sub1.sort
	from
		( select top 1
			 REPLACE(replace(replace(urlline, '{assetid}', convert(varchar,@assetid)), '{partnerid}', convert(varchar,@partnerid)), '{contentid}', convert(varchar,@ContentID)) as url,
			 MimeType as mime, SortOrder as sort
		from templates (nolock)
		where TemplatesID = @templateidstream ) sub1

	update @assetinfo set
	 AssetLocation2 = sub2.url,
	 AssetMimeType2 = sub2.mime,
	 AssetSortOrder2 = sub2.sort
	from
		( select top 1
			 REPLACE(replace(replace(urlline, '{assetid}', convert(varchar,@assetid)), '{partnerid}', convert(varchar,@partnerid)), '{contentid}', convert(varchar,@ContentID)) as url,
			 MimeType as mime, SortOrder as sort
		from templates (nolock) where TemplatesID = @templateidprogressive ) sub2

	return

END
GO
