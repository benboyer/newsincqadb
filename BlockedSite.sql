TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	BlockedSite	BlockedSiteID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	BlockedSite	Host	12	varchar	1000	1000	None	None	0	None	None	12	None	1000	2	NO	39
NewsincQA	dbo	BlockedSite	Rules	12	varchar	2000	2000	None	None	0	None	None	12	None	2000	3	NO	39
NewsincQA	dbo	BlockedSite	Active	-7	bit	1	1	None	None	0	None	None	-7	None	None	4	NO	50
NewsincQA	dbo	BlockedSite	DateCreatedGMT	11	datetime	23	16	3	None	0	None	None	9	3	None	5	NO	61
NewsincQA	dbo	BlockedSite	DateUpdatedGMT	11	datetime	23	16	3	None	1	None	None	9	3	None	6	YES	111
NewsincQA	dbo	BlockedSite	CreatedBy	12	varchar	60	60	None	None	1	None	None	12	None	60	7	YES	39
NewsincQA	dbo	BlockedSite	UpdatedBy	12	varchar	60	60	None	None	1	None	None	12	None	60	8	YES	39
NewsincQA	dbo	BlockedSite	Comment	12	varchar	200	200	None	None	1	None	None	12	None	200	9	YES	39

index_name	index_description	index_keys
IX_BlockedSites_active	nonclustered located on PRIMARY	Active
NDX_BlkSit_dom	nonclustered, unique located on PRIMARY	Host
