CREATE PROCEDURE [dbo].[Organizations_Insert_sp]
	@ContentItemId int = null,
	@Name varchar(50),
	@Acronym nvarchar(50) = null,
	@CandidateOfficePlaceholder nvarchar(100) = null,
	@AllowUploadingVideo bit,
	@UserID int,
	@LogoURL nvarchar(1000) = null,
	@IsMediaSource bit,
	@IsContentPrivate bit,
	@OrganizationType int,
	@Phone nchar(10) = null

AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'Organizations_Insert_sp'

	Insert [Partner](Name, StatusID, CreatedDate,CreatedUserID, LogoURL, isContentPrivate)
	select	@Name,1,GETUTCDATE(),-1,
			case when @LogoURL like 'http://%' then @LogoURL
				else 'http://assets.newsinc.com/' + @LogoURL
			end,
			@IsContentPrivate

	Declare @NewID int;

	Select @NewID = SCOPE_IDENTITY();


	insert into Partner_PartnerType(PartnerID, PartnerTypeID)
	select @NewID, PartnerTypeID from PartnerType where PartnerTypeID = @OrganizationType

	if @IsMediaSource = 1 and (select COUNT(*) from Partner_PartnerType where PartnerID = @NewID and PartnerTypeID = 3) = 0
	begin
		insert into Partner_PartnerType(PartnerID, PartnerTypeID)
		select @NewID, 3
	end
		Insert OrganizationsLEGACY (PartnerID, ContentItemId, Phone, Acronym, CandidateOfficePlaceholder, LogoUrl, IsMediaSource, IsContentPrivate, OrganizationType, AllowUploadingVideo)
		Values (@NewID, @ContentItemId, @Phone, @Acronym, @CandidateOfficePlaceholder, @LogoURL, @IsMediaSource, @IsContentPrivate, @OrganizationType, @AllowUploadingVideo)

		Select @NewID as ID


END
