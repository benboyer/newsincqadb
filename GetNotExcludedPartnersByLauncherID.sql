CREATE procedure [dbo].[GetNotExcludedPartnersByLauncherID]
	-- declare
	@LauncherID int
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetNotExcludedPartnersByLauncherID'

	-- declare @LauncherID int = 7334
	select distinct p.PartnerID, p.name PartnerName
	from partner p
	join	OrganizationsLEGACY ol
	on		p.PartnerID = ol.PartnerID
	join	Partner_PartnerType ppt
	on		p.PartnerID = ppt.PartnerID
	left join Launcher_ContentExclusion lce
	on		@LauncherID = lce.LauncherID
	and		p.PartnerID = lce.PartnerID
	where	p.StatusID = 2
	and		lce.LauncherID is null
	and		ol.IsMediaSource = 1
	--and		ppt.PartnerTypeID = 3
	order by p.Name

	set nocount off
END
