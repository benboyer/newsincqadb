USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetContentFeedContent]      
 -- declare      
 @feedtoken uniqueidentifier = null,      
    @providerid int = 0,      
    @datefrom datetime = null,      
    @dateto datetime = null,      
    @numbertoreturn int = 100,      
    @Updates bit = 0      
as      
BEGIN      
      
 SET FMTONLY OFF      
    SET NOCOUNT ON;      
      
 declare @numbertodouble int = (select @numbertoreturn * 2)      
      
 if @datefrom is null and @dateto is not null      
  set @datefrom = DATEADD(dd, -30, @dateto)      
      
 -- select @datefrom, @dateto      
      
 exec SprocTrackingUpdate 'usp_GetContentFeedContent'      
 if @providerid = 0      
 begin      
  set @providerid = null      
 end      
 if @numbertoreturn = 0 or @numbertoreturn is null      
  set @numbertoreturn = 100      
 if @numbertoreturn > 500      
  set @numbertoreturn = 500      
 if @feedtoken is null      
     set @feedtoken = '00000000-0000-0000-0000-000000000000'      
    
 if @updates is null      
  set @updates = 0      
      
    DECLARE @accesspartnerid int, @landingurl varchar(120), @itrackinggroup int, @now datetime      
  select @accesspartnerid = PartnerID,      
    @landingurl = LandingURL,      
    @itrackinggroup = trackinggroup      
  from Partner where FeedGUID = @feedtoken      
 if @accesspartnerid is null      
  set @accesspartnerid = 0      
      
 -- select @itrackinggroup, @accesspartnerid, @updates      
      
 create table #tmpContent(contentid bigint not null)       
 if @Updates = 1      
 begin      
  insert into #tmpContent(contentid)      
  select top (@numbertodouble) contentid      
  from (select * from Content (nolock) where Active = 1 and PartnerID = isnull(@providerid, partnerid) and UpdatedDate between ISNULL(@datefrom, CreatedDate) and ISNULL(@dateto, CreatedDate)) c      
  order by UpdatedDate desc, ContentID desc      
 end      
 if @Updates = 0      
 begin      
  insert into #tmpContent(contentid)      
  select top (@numbertodouble) contentid      
  from (select * from Content (nolock) where Active = 1 and PartnerID = isnull(@providerid, partnerid) and CreatedDate between ISNULL(@datefrom, CreatedDate) and ISNULL(@dateto, CreatedDate)) c      
  order by CreatedDate desc, ContentID desc      
 end      
      
  select  top (@numbertoreturn) -- c.CreatedDate, c.UpdatedDate, *       
  c.ContentID, c.ContentTypeID, c.PartnerID, c.ContentSourceID, c.Name, c.Description, c.Category, c.Keyword, c.EffectiveDate, c.ExpirationDate, c.CreatedDate, c.CreatedUserID, c.UpdatedDate, c.UpdatedUserID, c.Active,      
    c.FeedID, partners.PartnerID, partners.ParentPartnerID, partners.Name, partners.ContactID, partners.Website, partners.StatusID, partners.CreatedDate, partners.CreatedDate, partners.CreatedUserID, c.UpdatedDate, /*Partners.UpdatedDate,*/ 
    partners.UpdateUserID,     
    partners.TrackingGroup, partners.ShortName, @landingurl LandingURL, partners.isContentPrivate, partners.ZoneID, partners.FeedGUID, partners.isFeedGUIDActive, c.IsFileArchived,      
    c.ContentID, c.ContentID, 0 NumberOfReviews, a.Duration, 0 as ClipPopularity, 0 as IsAdvertisement, null as CandidateOfInterest, 0 as NumberOfPlays, 1 as SentToFreeWheel, null as FileExtension, null as IngestionStatusMessage, a.IsDeleted,      
    c.PartnerID, null as ContentItemId, null as Phone, pt.PartnerTypeID as OrganizationType, Partners.ShortName, null as CandidateOfficePlaceholder, case when pt.PartnerTypeID IN (2,3) then 1 else 0 end as AllowUploadingVideo, Partners.LogoUrl, case when 
  
    
pt.PartnerTypeID IN (2,3) then 1 else 0 end as IsMediaSource, partners.isContentPrivate,      
    -- max(ac.AllowContentID),      
    0 as AllowContentID,      
    c.partnerid as  ContentProviderPartnerID,      
    pd.Partnerid as DistributorPartnerID, c.CreatedDate      
    , aa.FilePath + '/' + aa.Filename as TranscriptPath  
    , c.SourceGUID  
 from #tmpContent ta      
 join Content c       
 on ta.contentid = c.ContentID      
 JOIN Partner AS Partners (nolock)      
 ON  c.PartnerID = Partners.PartnerID      
 join Content_Asset ca (nolock)      
 on  c.ContentID = ca.contentid      
 and  1 = ca.TargetPlatformID      
 and  1 = ca.AssetTypeID -- (1 = ca.AssetTypeID or 17 = ca.assettypeid)      
 join Asset a (nolock)      
 on  ca.assetid = a.AssetID      
 left join Asset aa (nolock)      
 on  ca.AssetID = aa.AssetID      
 and  ca.AssetTypeID = 17      
 left join (select max(partnertypeid) PartnerTypeID, partnerid from Partner_PartnerType (nolock) group by partnerid) ppt      
 on  Partners.partnerid = ppt.PartnerID      
 left join PartnerType pt (nolock)      
 on  ppt.PartnerTypeID = pt.PartnerTypeID      
 left join mTrackingGroup pd -- select top 1 * from AllowContent      
 on  @itrackinggroup = pd.TrackingGroup      
 where dbo.AllowedContentID(c.ContentID, @itrackinggroup, 1) = 1      
 ORDER BY case when @Updates = 1 then c.UpdatedDate else c.CreatedDate end  DESC      
   , c.ContentID desc      
      
 drop table #tmpContent      
 set nocount off      
      
/*      
-- ROLLBACK CODE      
--------------------------------------      
 --set @feedtoken = 'cd301ef7-8fd0-4e10-b89a-8c7cf24161df'      
 -- set @feedtoken = '7B9EF57B-601E-4A98-B171-29E8773C1D90'      
 --set @numbertoreturn = 150      
 -- set @datefrom = '2013-03-15 09:00:00'      
 -- set @dateto = '2013-03-15 11:00:00'      
 -- set @updates = 1      
 -- set @providerid = 1882      
----------------------------------      
 SET FMTONLY OFF      
    SET NOCOUNT ON;      
      
 declare @numbertodouble int = (select @numbertoreturn * 2)      
      
 if @datefrom is null and @dateto is not null      
  set @datefrom = DATEADD(dd, -30, @dateto)      
      
 -- select @datefrom, @dateto      
      
 exec SprocTrackingUpdate 'usp_GetContentFeedContent'      
 if @providerid is null      
  set @providerid = 0      
 if @numbertoreturn = 0 or @numbertoreturn is null      
  set @numbertoreturn = 100      
 if @numbertoreturn > 500      
  set @numbertoreturn = 500      
 if @feedtoken is null      
     set @feedtoken = '00000000-0000-0000-0000-000000000000'      
      
    DECLARE @accesspartnerid int, @landingurl varchar(120), @itrackinggroup int, @now datetime      
  select @accesspartnerid = PartnerID,      
    @landingurl = LandingURL,      
    @itrackinggroup = trackinggroup      
  from Partner where FeedGUID = @feedtoken      
 if @accesspartnerid is null      
  set @accesspartnerid = 0      
      
 create table #tmpContent(contentid bigint not null)      
 if @Updates = 1      
 begin      
  insert into #tmpContent(contentid)      
  select top (@numbertodouble) contentid      
  from Content (nolock)      
  where UpdatedDate between isnull(@datefrom, updateddate) and isnull(@dateto, UpdatedDate)      
  and  (@providerid = 0 or partnerid = @providerid)      
  and  dbo.AllowedContentID(contentid, @itrackinggroup, 1) = 1      
  order by UpdatedDate desc, ContentID desc      
 end      
 if @Updates = 0      
 begin      
  insert into #tmpContent(contentid)      
  select top (@numbertodouble) contentid      
  from Content (nolock)      
  where CreatedDate between ISNULL(@datefrom, CreatedDate) and ISNULL(@dateto, CreatedDate)      
  and  (@providerid = 0 or partnerid = @providerid)      
  and  dbo.AllowedContentID(contentid, @itrackinggroup, 1) = 1      
  order by CreatedDate desc, ContentID desc      
 end      
      
  select top (@numbertoreturn) -- c.CreatedDate, c.UpdatedDate, *      
  c.ContentID, c.ContentTypeID, c.PartnerID, c.ContentSourceID, c.Name, c.Description, c.Category, c.Keyword, c.EffectiveDate, c.ExpirationDate, c.CreatedDate, c.CreatedUserID, c.UpdatedDate, c.UpdatedUserID, c.Active,      
    c.FeedID, partners.PartnerID, partners.ParentPartnerID, partners.Name, partners.ContactID, partners.Website, partners.StatusID, partners.CreatedDate, partners.CreatedDate, partners.CreatedUserID, Partners.UpdatedDate, partners.UpdateUserID, partners.T
  
    
rackingGroup, partners.ShortName, @landingurl LandingURL, partners.isContentPrivate, partners.ZoneID, partners.FeedGUID, partners.isFeedGUIDActive, c.IsFileArchived,      
    c.ContentID, c.ContentID, 0 NumberOfReviews, a.Duration, 0 as ClipPopularity, 0 as IsAdvertisement, null as CandidateOfInterest, 0 as NumberOfPlays, 1 as SentToFreeWheel, null as FileExtension, null as IngestionStatusMessage, a.IsDeleted,      
    c.PartnerID, null as ContentItemId, null as Phone, pt.PartnerTypeID as OrganizationType, Partners.ShortName, null as CandidateOfficePlaceholder, case when pt.PartnerTypeID IN (2,3) then 1 else 0 end as AllowUploadingVideo, Partners.LogoUrl, case when 
  
    
pt.PartnerTypeID IN (2,3) then 1 else 0 end as IsMediaSource, partners.isContentPrivate,      
    -- max(ac.AllowContentID),      
    0 as AllowContentID,      
    c.partnerid as  ContentProviderPartnerID,      
    pd.Partnerid as DistributorPartnerID, c.CreatedDate      
    , aa.FilePath + '/' + aa.Filename as TranscriptPath      
 from #tmpContent ta      
 join Content c      
 on ta.contentid = c.ContentID      
 JOIN Partner AS Partners (nolock)      
 ON  c.PartnerID = Partners.PartnerID      
 join Content_Asset ca (nolock)      
 on  c.ContentID = ca.contentid      
 and  1 = ca.TargetPlatformID      
 and  1 = ca.AssetTypeID -- (1 = ca.AssetTypeID or 17 = ca.assettypeid)      
 join Asset a (nolock)      
 on  ca.assetid = a.AssetID      
 left join Asset aa (nolock)      
 on  ca.AssetID = aa.AssetID      
 and  ca.AssetTypeID = 17      
 left join (select max(partnertypeid) PartnerTypeID, partnerid from Partner_PartnerType (nolock) group by partnerid) ppt      
 on  Partners.partnerid = ppt.PartnerID      
 left join PartnerType pt (nolock)      
 on  ppt.PartnerTypeID = pt.PartnerTypeID      
 left join mTrackingGroup pd -- select top 1 * from AllowContent      
 on  @itrackinggroup = pd.TrackingGroup      
 where  (@providerid = 0 or c.partnerid = @providerid)      
 ORDER BY case when @Updates = 1 then c.UpdatedDate else c.CreatedDate end  DESC      
   , c.ContentID desc      
      
 drop table #tmpContent      
 set nocount off      
      
*/      
END
GO
