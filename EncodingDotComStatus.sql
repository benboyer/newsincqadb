TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	EncodingDotComStatus	ContentID	4	int	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	EncodingDotComStatus	MediaID	-9	nvarchar	100	200	None	None	0	None	None	-9	None	200	2	NO	39
NewsincQA	dbo	EncodingDotComStatus	AssetType	-9	nvarchar	50	100	None	None	0	None	None	-9	None	100	3	NO	39
NewsincQA	dbo	EncodingDotComStatus	Processed	-7	bit	1	1	None	None	0	None	None	-7	None	None	4	NO	50
NewsincQA	dbo	EncodingDotComStatus	CreatedDate	11	datetime	23	16	3	None	0	None	(getutcdate())	9	3	None	5	NO	61

index_name	index_description	index_keys
PK_EncodingDotComStatus_1	clustered, unique, primary key located on PRIMARY	ContentID, MediaID
