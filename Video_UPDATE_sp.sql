CREATE PROCEDURE [dbo].[Video_UPDATE_sp]
           @VideoID int,
           @ContentItemID int = null,
           @UserID int,
           @ClipURL nvarchar(50) = null,
           @Title nvarchar(100),
           @Description nvarchar(1000),
           @EventDate datetime,
           @NumberOfReviews int = null,
           @Height decimal(10,2) = null,
           @Width decimal(10,2) = null,
           @Duration decimal(10,2) = null,
           @CreatedOn datetime,
           @ClipPopularity float = null,
           @CandidateOfInterest float = null,
           @ThumbnailURL nvarchar(50) = null,
           @PublishDate datetime,
           @Keywords nvarchar(500) = null,
           @NumberOfPlays int = null,
           @SentToFreeWheel bit,
           @IsEnabled bit,
           @IsDeleted bit,
           @ExpiryDate datetime = null,
           @OriginalName nvarchar(200),
           @StillFrameURL nvarchar(50) = null,
           @FileExtension nvarchar(5) = null,
           @Category nvarchar(50) = null,
           @EnterDTM datetime,
           @LastChgDTM datetime = null,
           @IngestionStatusMessage nvarchar(2000) = null,
           @IsAdvertisement bit



AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'Video_UPDATE_sp'

--	BEGIN TRANSACTION
-- select top 10 * from VideosLEGACY order by contentid desc
		UPDATE VideosLEGACY SET NumberOfReviews=@NumberOfReviews, ClipPopularity=@ClipPopularity, CandidateOfInterest=@CandidateOfInterest, NumberOfPlays=@NumberOfPlays,
		SentToFreeWheel =@SentToFreeWheel, FileExtension=@FileExtension, IngestionStatusMessage=@IngestionStatusMessage, IsDeleted=@IsDeleted
		WHERE ContentID = @VideoID

	if @@ERROR = 0
	BEGIN

		UPDATE Content SET Name=@Title, [Description]=@Description, Category=@Category, Keyword=@Keywords, EffectiveDate=@PublishDate,
		ExpirationDate=@ExpiryDate, Active=@IsEnabled, CreatedDate=@CreatedOn, UpdatedDate=GETDATE(), IsDeleted=@IsDeleted
		WHERE ContentID = @VideoID

	END
	if @@ERROR = 0
	BEGIN

		UPDATE ContentVideo SET [FileName]=@ClipURL, ImportFilePath=@ClipURL, Height=@Height, Width=@Width, Duration=@Duration, ThumbnailFileName=@ThumbnailURL, StillframeFileName=@StillFrameURL
		WHERE ContentID = @VideoID

	END

--	if @@ERROR =0
--		COMMIT TRANSACTION
--	ELSE
--		ROLLBACK TRANSACTION

END
