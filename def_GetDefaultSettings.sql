CREATE procedure [dbo].[def_GetDefaultSettings]
	--	declare 
	@partnerid int,
	@Entity varchar(60) = null,
	@Setting varchar(60) = null
as
BEGIN
	set nocount on
	set FMTONLY OFF
	exec SprocTrackingUpdate 'def_GetDefaultSettings'

	select ISNULL(convert(varchar(20),par.PartnerID), 'All') PartnerID, sys.Entity, sys.Setting, ISNULL(par.value, sys.value) Value
	from	(select	dst.Name Entity, s.Name Setting, ds.value Value
			-- select *
			from	Setting s
			join	DefaultSetting ds
			on		s.SettingID = ds.settingid 
			join	defaultsettingtype dst
			on		ds.DefaultsettingTypeID = dst.defaultsettingtypeid
			where	dst.name = isnull(@Entity, dst.name)
			and		dst.active = 1) sys
	left join (select	ds.PartnerID, dst.Name Entity, s.Name Setting, ds.value Value
			-- select *
			from	Setting s
			join	PartnerDefaultSetting ds
			on		s.SettingID = ds.settingid 
			join	defaultsettingtype dst
			on		ds.DefaultsettingTypeID = dst.defaultsettingtypeid
			where	dst.name = isnull(@Entity, dst.name)
			and		ds.partnerid = @partnerid
			and		dst.Active = 1) par
	on	sys.Entity = par.Entity
	and	sys.Setting = par.Setting
	where sys.Setting = ISNULL(@Setting, sys.Setting)
	order by sys.Entity, sys.Setting, par.PartnerID
	set nocount on
END


