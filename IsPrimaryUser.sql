-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[IsPrimaryUser]
(@UserId int,
@PartnerId int)
RETURNS bit
AS
BEGIN
	DECLARE @Return bit
	SET @Return = CAST((CASE WHEN(SELECT ContactID FROM dbo.Partner WHERE ContactID = @UserId and PartnerId = @PartnerId) IS NULL THEN 0 ELSE 1 END) AS bit)
RETURN @return
END

