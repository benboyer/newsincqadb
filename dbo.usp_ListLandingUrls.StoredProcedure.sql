USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_ListLandingUrls]
	--declare 
	@PartnerID int
as
BEGIN
	SET FMTONLY OFF
	set nocount on
	exec SprocTrackingUpdate 'usp_ListLandingUrls'

	select	p.Name as Partner, plu.Partner_LandingURLID, lu.Active, lu.LandingURL, lu.LandingURLID, lut.LandingURLTypeID, lut.Name LandingURLType
	from	Partner p
	join	Partner_LandingURL plu
	on		p.PartnerID = plu.PartnerID
	join	LandingURL lu
	on		plu.LandingURLID = lu.LandingURLID
	join	LandingURLType lut
	on		lu.LandingURLTypeID = lut.LandingURLTypeID
	where	p.PartnerID = @PartnerID

	set nocount off
END
GO
