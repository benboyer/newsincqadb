USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[cr_UserContentVideos]
	-- declare
	@userid int,
	@Requested varchar(20) -- 'recentlyadded', 'enabled, disabled, 'deleted'
as
BEGIN
	set nocount on
	set fmtonly off
	exec SprocTrackingUpdate 'cr_UserContentVideos'
-------------------
	-- select @userid = 8125, @ReqType = 1, @Requested = 'recentlyadded'
-------------------
	declare @partnerid int = (select partnerid from [User] (nolock) where UserID = @userid)
	declare @isadmin bit = (select isadmin from NDNUsersLEGACY (nolock) where UserID = @userid)

	-- insert into sprocparamtracker(sproc, param1, param2, param3, param4)
	-- select 'cr_UserContentVideos',@UserId,@Requested,@partnerid,@isadmin

	if @Requested = 'recentlyadded'
	begin
		select c.Name, c.[Description], c.Keyword, c.CreatedDate, EffectiveDate, ExpirationDate,
			case when a.filepath is not null and a.Filename is not null then a.FilePath + '/' + a.Filename
				when a.FilePath is null and a.Filename is not null then a.Filename
				else cv.ThumbnailFileName end as ThumbnailFileName
		from Content c
		join Content_Asset ca
		on		c.ContentID = ca.ContentID
		and		3 = ca.AssetTypeID
		join	Asset a
		on		ca.AssetID = a.AssetID
		join ContentVideo cv on
		cv.ContentID = c.ContentID
		left join UserInterface_setting uis -- select * from UserInterface_setting
		on		'RecentVideos_Hours' = uis.Setting
		left join User_UserInterface_Setting uuis
		on		uis.UserInterface_SettingID = uuis.UserInterface_SettingID
		and		@userid = uuis.UserID
		where (c.PartnerID = @partnerid or @isadmin = 1)
		AND		(c.CreatedDate > DATEADD(HOUR, -convert(int,isnull(uuis.Value, uis.defaultvalue)),GETDATE()))
		and		c.Active = 1
	end

	if @Requested = 'deleted'
	begin
		/* Deleted */
		select c.Name, c.[Description], c.Keyword, c.CreatedDate, EffectiveDate, ExpirationDate,
				case when a.filepath is not null and a.Filename is not null then a.FilePath + '/' + a.Filename
					when a.FilePath is null and a.Filename is not null then a.Filename
					else cv.ThumbnailFileName
				end as ThumbnailFileName
		from	Content c
		join	Content_Asset ca
		on		c.ContentID = ca.ContentID
		and		3 = ca.AssetTypeID
		join	Asset a
		on		ca.AssetID = a.AssetID
		join	ContentVideo cv
		on		cv.ContentID = c.ContentID
		where	c.ContentImportStatus = 5
		and		(c.PartnerID = @partnerid or @isadmin = 1)
		AND		c.isDeleted = 1
		and		c.Active = 0
	 end

	/* Enabled Videos */
	if @Requested = 'enabled'
	begin
		select c.Name, c.[Description], c.Keyword, c.CreatedDate, EffectiveDate, ExpirationDate,
			case when a.filepath is not null and a.Filename is not null then a.FilePath + '/' + a.Filename
				when a.FilePath is null and a.Filename is not null then a.Filename
				else cv.ThumbnailFileName end as ThumbnailFileName
		from Content c
		join Content_Asset ca
		on		c.ContentID = ca.ContentID
		and		3 = ca.AssetTypeID
		join	Asset a
		on		ca.AssetID = a.AssetID
		join ContentVideo cv on
		cv.ContentID = c.ContentID
		where	(c.PartnerID = @partnerid or @isadmin = 1)
		AND		c.isDeleted = 0 AND c.Active = 1
	end

	/* Disabled Videos */
	if @Requested = 'disabled'
	begin
		select c.Name, c.[Description], c.Keyword, c.CreatedDate, EffectiveDate, ExpirationDate,
			case when a.filepath is not null and a.Filename is not null then a.FilePath + '/' + a.Filename
				when a.FilePath is null and a.Filename is not null then a.Filename
				else cv.ThumbnailFileName end as ThumbnailFileName
		from Content c
		left join Content_Asset ca
		on		c.ContentID = ca.ContentID
		and		3 = ca.AssetTypeID
		left join	Asset a
		on		ca.AssetID = a.AssetID
		left join ContentVideo cv on
		cv.ContentID = c.ContentID
		where c.ContentImportStatus = 5
		and		(c.PartnerID = @partnerid or @isadmin = 1)
		AND		c.isDeleted = 0
		AND		c.Active = 0
	end
	set nocount OFF
END
GO
