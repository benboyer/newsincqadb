CREATE PROCEDURE [dbo].[cr_Video_UPDATE]
           @VideoID int,
           @UserID int,
           @Title varchar(100),
           @Description varchar(1000),
           @EventDate datetime,
           @CreatedOn datetime,
           @PublishDate datetime,
           @Keywords nvarchar(500) = null,
           @IsEnabled bit,
           @IsDeleted bit,
           @ExpiryDate datetime = null,
           @Category nvarchar(50) = null,
           @OnlyOwnerView bit = 0
AS
BEGIN

/*
           @Height decimal(10,2) = null,
           @Width decimal(10,2) = null,
           @Duration decimal(10,2) = null,
*/

	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'Video_UPDATE_sp'

		UPDATE VideosLEGACY SET IsDeleted=@IsDeleted
		WHERE ContentID = @VideoID

	if @@ERROR = 0
	BEGIN

		UPDATE Content SET Name=@Title, [Description]=@Description, Category=@Category, Keyword=@Keywords, EffectiveDate=@PublishDate,
		ExpirationDate=@ExpiryDate, Active=@IsEnabled, CreatedDate=@CreatedOn, UpdatedDate=GETDATE(),
		UpdatedUserID = @UserID, IsDeleted=@IsDeleted, OnlyOwnerView = @OnlyOwnerView
		WHERE ContentID = @VideoID

	END
END
