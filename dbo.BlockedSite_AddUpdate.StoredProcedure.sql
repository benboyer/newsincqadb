USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[BlockedSite_AddUpdate]
	--	declare	
	@Host varchar(1000), 
	@Active bit, 
	@UID varchar(20),
	@Rules varchar(2000) = null, 
	@Comment varchar(200) = null
AS
BEGIN
	SET FMTONLY OFF
	SET NOCOUNT ON
	exec SprocTrackingUpdate 'BlockedSite_AddUpdate'

	declare @DateUpdatedGMT datetime = GETUTCDATE()
	if @Host = ''
	begin
		return
	end

	select @Host = ltrim(RTRIM(@Host))

	if @Rules is null
	begin
		set @Rules = @Host + '/*'
	end
	
	select @Rules = LTRIM(rtrim(@Rules))
	
	if not exists 
		(select 1
		from BlockedSite 
		where	HOST = @Host)
	begin
		insert into BlockedSite(Host, Rules, Active, DateCreatedGMT, DateUpdatedGMT, CreatedBy, UpdatedBy, Comment)
		select @Host, @Rules, @Active, @DateUpdatedGMT, @DateUpdatedGMT, @UID, @UID, @Comment
	end
	else
	begin
		update	BlockedSite
		set Rules = @Rules, Active = @Active, @DateUpdatedGMT = @DateUpdatedGMT, UpdatedBy = @UID, Comment = @Comment
		where host = @Host
	end
	SET NOCOUNT OFF
END
GO
