TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	DefaultSettingType	DefaultSettingTypeID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	DefaultSettingType	Name	12	varchar	40	40	None	None	1	None	None	12	None	40	2	YES	39
NewsincQA	dbo	DefaultSettingType	Comment	12	varchar	120	120	None	None	1	None	None	12	None	120	3	YES	39
NewsincQA	dbo	DefaultSettingType	Active	-7	bit	1	1	None	None	1	None	None	-7	None	None	4	YES	50

index_name	index_description	index_keys
IX_DefaultSettingType_Nm_id	nonclustered located on PRIMARY	Name, DefaultSettingTypeID, Active
PK_DefaultSettingType	clustered, unique, primary key located on PRIMARY	DefaultSettingTypeID
