CREATE PROCEDURE [dbo].[NDNUsers_ADD_sp]
		   @UserTypeID tinyint = null,
		   @UserName nvarchar(150),
		   @EmailID nvarchar(50),
           @PasswordFormat int,
           @PasswordSalt varbinary(128),
           @Password varbinary(128),
           @ReceiveDailyIQEmail bit,
           @FirstName nvarchar(50)='',
           @MI nvarchar(60),
           @LastName nvarchar(50)='',
           @Gender char(1),
           @DOB datetime,
           @AddressID int,
           @OrganizationID int,
           @ContentItemID int,
           @PartnerId nvarchar(255),
           @FreeWheelType nvarchar(20),
           @Active bit,
           @IsFtpEnabled bit,
           @XSLT text,
           @AccountStatus int,
           @LastModified smalldatetime,
           @IsAdmin bit,
           @PasswordToken nvarchar(255),
           @AllowVideoUpload bit,
           @ProposedOrganization nvarchar(500),
           @EnableRssFeed bit,
           @LandingURL nvarchar(2083),
           @CustomRssFeedXSLT nvarchar(255)
AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'NDNUsers_ADD_sp'

	INSERT INTO [User]
           ([PartnerID],
            [FirstName],
			[LastName],
			[Login],
			[Password],
			[Active],
			[CreatedUserID],
			CreatedDate,
			UpdatedDate,
			[PasswordFormat],
            [PasswordSalt],
            [PasswordToken],
            [StatusID])
       Values(@OrganizationID, (Case When @FirstName IS NULL then '' else @FirstName End), (Case When @LastName IS NULL then '' else @LastName End),
		    @UserName,@Password, @Active, -1, GETUTCDATE(), @LastModified,
           @PasswordFormat, @PasswordSalt, @PasswordToken, 2)

        Declare @NewID int;

        Select @NewID = SCOPE_IDENTITY();

        Insert into NDNUsersLEGACY
        (UserID,
        EmailID,
        ReceiveDailyIQEmail,
        MI,
        Gender,
        DOB,
        AddressID,
        ContentItemID,
        PartnerId,
        FreeWheelType,
        IsAdmin,
        UserTypeID,
        IsFtpEnabled,
        XSLT,
        AccountStatus,
        AllowVideoUpload,
        ProposedOrganization,
        EnableRssFeed,
        LandingURL,
        CustomRssFeedXSLT)
        Values(@NewID,
           @EmailID,
           @ReceiveDailyIQEmail,
           @MI,
           @Gender,
           @DOB,
           @AddressID,
           @ContentItemID,
           @PartnerId,
           @FreeWheelType,
           @IsAdmin,
           @UserTypeID,
           @IsFtpEnabled,
           @XSLT,
           @AccountStatus,
           @AllowVideoUpload,
           @ProposedOrganization,
           @EnableRssFeed,
           @LandingURL,
           @CustomRssFeedXSLT)

	Select @NewID as ID

END
