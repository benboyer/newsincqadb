USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[WidgetPlayer_ADD_sp]
	  @WidgetId int,
      @DisableSocialNetworking bit = null,
      @DisableAds bit = null,
      @DisableVideoPlayOnLoad bit = null,
      @DisableSingleEmbed bit= null,
      @PartnerLogo varbinary(max) = null,
      @StyleSheet varbinary(max) = null,
      @ContinuousPlay int = null,
      @PlayerID int = null,
      @Layout int = null,
      @DisableKeywords bit = null
AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'WidgetPlayer_ADD_sp'

	DECLARE @NewID int;

	BEGIN TRANSACTION

	UPDATE Launcher set PlayerTypeID = @PlayerID
	WHERE LauncherID = @WidgetId

	IF @@ERROR = 0
	BEGIN
		Insert WidgetPlayerLegacy(LauncherID, DisableSocialNetworking, DisableAds, DisableVideoPlayOnLoad, DisableSingleEmbed, PartnerLogo, StyleSheet, DisableKeywords, ContinuousPlay)
		VALUES (@WidgetId, @DisableSocialNetworking, @DisableAds, @DisableVideoPlayOnLoad, @DisableSingleEmbed, @PartnerLogo, @StyleSheet, @DisableKeywords, @ContinuousPlay)
	END
	IF @@ERROR = 0
		COMMIT TRANSACTION
	ELSE
		ROLLBACK TRANSACTION

   Return @@ERROR;

END
GO
