CREATE procedure [dbo].[SectionPartner_Add_UPdate]
--	declare
	@SectionPartnerID int = null,
	@partnerid int,
	@value varchar(26),
	@description varchar(26) = null,
	@isDefault bit = 0
AS
BEGIN
	set fmtonly off
	SET NOCOUNT ON
	exec SprocTrackingUpdate 'SectionPartner_Add_UPdate'

	if @SectionPartnerID is null
	BEGIN
		insert into SectionPartner(PartnerID, Value, Description, isDefault)
		select @partnerid, @value, @description, @isDefault
		where not exists
			(select 1
			from SectionPartner
			where PartnerID = @partnerid
			and		Value = @value)
	END
	ELSE
	BEGIN
		update SectionPartner
		set PartnerID = @partnerid,
			Value = @value,
			Description = @description,
			isDefault = @isDefault
		where	SectionPartnerID = @SectionPartnerID
	END

	if @isDefault = 1
	BEGIN
		update	SectionPartner
		set		isDefault = 0
		where	PartnerID = @partnerid
		and		SectionPartnerID <> @SectionPartnerID
		and		isDefault = 1
	END
	set nocount off
END
