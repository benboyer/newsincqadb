TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	TargetPlatform	TargetPlatformID	5	smallint identity	5	2	0	10	0	None	None	5	None	None	1	NO	52
NewsincQA	dbo	TargetPlatform	Name	12	varchar	20	20	None	None	0	None	None	12	None	20	2	NO	39
NewsincQA	dbo	TargetPlatform	Format	12	varchar	20	20	None	None	1	None	None	12	None	20	3	YES	39
NewsincQA	dbo	TargetPlatform	Comments	-1	text	2147483647	2147483647	None	None	1	None	None	-1	None	2147483647	4	YES	39
NewsincQA	dbo	TargetPlatform	CopyBack	-7	bit	1	1	None	None	0	None	((0))	-7	None	None	5	NO	50

index_name	index_description	index_keys
PK_TargetDeviceClass	clustered, unique, primary key located on PRIMARY	TargetPlatformID
