TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	Address	AddressID	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	Address	AddressTypeID	-6	tinyint	3	1	0	10	0	None	None	-6	None	None	2	NO	48
NewsincQA	dbo	Address	CountryID	4	int	10	4	0	10	0	None	None	4	None	None	3	NO	56
NewsincQA	dbo	Address	Address1	-9	nvarchar	100	200	None	None	1	None	None	-9	None	200	4	YES	39
NewsincQA	dbo	Address	Address2	-9	nvarchar	100	200	None	None	1	None	None	-9	None	200	5	YES	39
NewsincQA	dbo	Address	City	-9	nvarchar	100	200	None	None	1	None	None	-9	None	200	6	YES	39
NewsincQA	dbo	Address	StateID	5	smallint	5	2	0	10	1	None	None	5	None	None	7	YES	38
NewsincQA	dbo	Address	ProvinceCounty	-9	nvarchar	100	200	None	None	1	None	None	-9	None	200	8	YES	39
NewsincQA	dbo	Address	Zip	-9	nvarchar	10	20	None	None	1	None	None	-9	None	20	9	YES	39
NewsincQA	dbo	Address	PlusFour	-9	nvarchar	10	20	None	None	1	None	None	-9	None	20	10	YES	39
NewsincQA	dbo	Address	IsVerified	-7	bit	1	1	None	None	0	None	None	-7	None	None	11	NO	50
NewsincQA	dbo	Address	IsPrimary	-7	bit	1	1	None	None	0	None	None	-7	None	None	12	NO	50
NewsincQA	dbo	Address	Deleted	-7	bit	1	1	None	None	0	None	None	-7	None	None	13	NO	50
NewsincQA	dbo	Address	CreatedDate	11	smalldatetime	16	16	0	None	1	None	(getutcdate())	9	3	None	14	YES	111
NewsincQA	dbo	Address	CreatedUserID	4	int	10	4	0	10	0	None	None	4	None	None	15	NO	56
NewsincQA	dbo	Address	UpdatedDate	11	datetime	23	16	3	None	1	None	None	9	3	None	16	YES	111
NewsincQA	dbo	Address	UpdatedUserID	4	int	10	4	0	10	1	None	None	4	None	None	17	YES	38

index_name	index_description	index_keys
PK_Address	clustered, unique, primary key located on PRIMARY	AddressID
