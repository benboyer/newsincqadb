CREATE VIEW dbo.NDNUserTypes
AS
SELECT     UserTypeID, Name
FROM         dbo.UserTypesLegacy
