TABLE_QUALIFIER	TABLE_OWNER	TABLE_NAME	COLUMN_NAME	DATA_TYPE	TYPE_NAME	PRECISION	LENGTH	SCALE	RADIX	NULLABLE	REMARKS	COLUMN_DEF	SQL_DATA_TYPE	SQL_DATETIME_SUB	CHAR_OCTET_LENGTH	ORDINAL_POSITION	IS_NULLABLE	SS_DATA_TYPE
NewsincQA	dbo	LookupList	Id	4	int identity	10	4	0	10	0	None	None	4	None	None	1	NO	56
NewsincQA	dbo	LookupList	Text	-9	nvarchar	100	200	None	None	0	None	None	-9	None	200	2	NO	39
NewsincQA	dbo	LookupList	Value	4	int	10	4	0	10	0	None	None	4	None	None	3	NO	56
NewsincQA	dbo	LookupList	SortOrder	4	int	10	4	0	10	0	None	None	4	None	None	4	NO	56
NewsincQA	dbo	LookupList	Category	-9	nvarchar	50	100	None	None	0	None	None	-9	None	100	5	NO	39

index_name	index_description	index_keys
PK_Lookup	clustered, unique, primary key located on PRIMARY	Id
