CREATE procedure [dbo].[mon_GetPartnerContentByDay]
	-- declare 
	@StartD date,
	@EndD date = null,
	@partnerid int = null,
	@trackinggroup int = null
as
BEGIN
	--	set @startd = '2012-04-10' set @partnerid = 446 set @endD = '2012-07-13'
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'mon_GetPartnerContentByDay'
	
	declare @then datetime = (select convert(datetime, @StartD))
	if @then < DATEADD(yy, -1, getdate()) 	set @then = DATEADD(yy, -1, getdate())
	declare @now datetime = (select convert(datetime, @EndD))
	if @now is null set @now = (select dateadd(hh, 24, @then))

	if @PartnerID is null
		set @partnerid = (select partnerid from mTrackingGroup (nolock) where TrackingGroup = @trackinggroup)
	
	-- declare @then date = '2013-05-01', @now date = '2013-05-04', @partnerid int = 499 -- select *
	select	r.CountDate, r.Name, isnull(c.NewVideos, 0) NewVideos
	from	(select p.Name, sd.CountDate
			from	(select name from Partner (nolock) where PartnerID = @partnerid) p,	
					(select distinct CountDate from db02.reports2.dbo.summarydates where CountDate between @then and @now) sd ) r
	left join	(select CONVERT(date, createddate) CreatedDate, COUNT(*) NewVideos
				from Content (nolock)
				where PartnerID = @partnerid and CONVERT(date, CreatedDate) between @then and @now
				group by CONVERT(date, createddate))  c
	on		r.countdate = convert(date, c.CreatedDate)
	order by r.countdate
	
	set nocount off
END
