USE [NewsincQA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideosLEGACY](
	[ContentID] [int] NOT NULL,
	[ContentItemID] [int] NULL,
	[NumberOfReviews] [int] NULL,
	[Duration] [float] NULL,
	[ClipPopularity] [float] NULL,
	[IsAdvertisement] [bit] NULL,
	[CandidateOfInterest] [float] NULL,
	[NumberOfPlays] [int] NULL,
	[SentToFreeWheel] [bit] NOT NULL,
	[FileExtension] [nvarchar](5) NULL,
	[IngestionStatusMessage] [nvarchar](2000) NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_VideosLEGACY] PRIMARY KEY CLUSTERED 
(
	[ContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
