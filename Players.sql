CREATE VIEW dbo.Players
AS
SELECT     CAST(PlayerTypeID AS int) AS PlayerId, Name, Description, CAST([Order] AS int) AS Layout, ConfigurationFileName AS Location, Height, Width
FROM         dbo.PlayerType
